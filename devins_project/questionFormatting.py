import random
from pylatex.utils import NoEscape
from pylatex import VerticalSpace, NewLine

def isFloat(s):
	try:
		float(s)
		return True
	except ValueError:
		return False

def isNumber(s):
	try:
		s.isdigit()
		if s.isdigit == True:
			return True
		else:
			return False
	except ValueError:
		return False

def multipleChoice(choices = ['1','2','3','4','5'], fifthVariation = True, doc = None):
	print(choices)
	if len(choices) != len(set(choices)):
		print('DUPLICATES!!!!!!!!!!!!!!')
	if isFloat(choices[0]) == True or isNumber(choices[0]) == True:
		print(choices)
		choices.sort(key=float)
		x = random.randint(1,2)
		if x == 1:
			choices = list(reversed(choices))
	else:		
		random.shuffle(choices)
	
	for item, n in zip(range(len(choices)), [1,2,3,4,5]):
		doc.append(NoEscape('(%s) ' % str(item+1) + choices[item]))
		if n == 1:
			doc.append(VerticalSpace('.05in'))

		if n != 5:
			doc.append(NewLine())
			doc.append(VerticalSpace('.05in'))
	if fifthVariation == True:
		doc.append(NoEscape(r'(%d) I do not know. (Worth $\frac{1}{3}$ points)' % (len(choices)+1)))

	return choices

	