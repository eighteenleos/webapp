#center = ['vertex','origin','inside','outside','on side']
#scaleFactorType = ['integer','fraction']
#integer = 2, 3, 4, 5
#fraction = 1/2, 1/3, 1/4, 1/5, 3/2, 2/3 etc etc
from transformations import translateAnywhere
from pdfFunctions import sizingXY, grid, graphPolygon, annotatePolygon, com, pointsForDiagram, graphPoint
from pylatex.utils import NoEscape
import random
from pylatex import VerticalSpace, NewLine, Center, TikZ
from fractions import Fraction, gcd

class performDilation():
	def __init__(self, center = ['vertex','origin','inside','outside','on side'],
						scaleFactorType = ['integer','fraction'],
						maxx = 10,
						polygonType = ['line','triangle'],
						scaleFactorAmount = ['<1','>1','1']):
		self.center = center
		self.scaleFactorType = scaleFactorType
		self.maxx = maxx
		self.polygonType = polygonType
		self.scaleFactorAmount = scaleFactorAmount
		self.typeOfQuestionChosen = 'SA'

		self.subjects = ['Geometry']
		self.topics = ['Transformations', 'Dilations']
		self.skills = [['Perform a dilation'], ['Perform a dilation']]
		self.standard = "CCSS.MATH.CONTENT.HSG.CO.A.2"
		self.ID = self.subjects[0] + 'performDilation'

	def addQuestion(self, doc = None):
		scaleFactorTypeChosen = random.choice(self.scaleFactorType)
		scaleFactorAmountChosen = random.choice(self.scaleFactorAmount)
		polygonTypeChosen = random.choice(self.polygonType)
		centerChosen = random.choice(self.center)
		
		if scaleFactorTypeChosen == 'integer':
			#starting with bigger triangle and making smaller, so I'll reverse the fraction etc
			numerator = 1
			denominator = random.randint(2, (2*self.maxx)//4)
			if scaleFactorAmountChosen == '1':
				numerator = 1
				denominator = 1
		else:
			if scaleFactorAmountChosen == '<1':
				denominator = random.randint(2, (2*self.maxx)//4)
				numerator = random.choice([x for x in range(1, denominator)])
			elif scaleFactorAmountChosen == '>1':
				#1/2 to probably 1/5
				denominator = random.randint(2, (2*self.maxx)//4)
				#starting with denominator+1/denominator, so 6/5, making it already greater than 1
				#6/5 to denominatorMax*denominator, so 25/5, not including fractions that
				#just do between 1 and 2
				numerator = random.choice([x for x in range(denominator+1, denominator*2+1) if x % denominator != denominator and x % denominator != 0])
				#we should also reduce this fraction if possible

		scaleFactor = Fraction(numerator/denominator).limit_denominator(100)
		numerator = scaleFactor.numerator
		denominator = scaleFactor.denominator
		#When defining points based on center, steps needs to be of a number % denominator   == 0

		#inside triangle: keep 1st point in top left or top right, keep 2nd point bottom left, 3rd point bottom right
		#vertex: center is 1st point, 2nd point somewhere, 3rd point somewhere
		#on side: make 1st point, then make 2ndpoint opposite values of first
		#outside: points are either <0 or >0

		

		centerX = 0
		centerY = 0

		if centerChosen == 'inside': #make x1 same as center, so ensuring inside
			x1 = 0
			print(denominator)
			print(numerator)
			y1 = random.choice([x for x in range(1, self.maxx+1) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])

			#make x2 on the bottom left quadrant
			#anywhere from left to not touching y-axis
			x2 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			#anywhere from bottom to - 1 (one below center point)
			y2 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])

			#make x3 on the bottom right quadrant
			#1 to 10
			x3 = random.choice([x for x in range(1, self.maxx+1) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			#-10 to -1
			y3 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
		elif centerChosen == 'vertex':
			x1 = 0
			y1 = 0

			#since vertex is 0,0 and triangle won't be above this at all, let's stret

			#make x2 on bottom left
			#-10 to -1
			x2 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			#-10 to -1
			y2 = random.choice([x for x in range(-self.maxx*2, 0) if x % denominator == 0 
				and abs(x*numerator/denominator) <= self.maxx])

			#make x3 on the bottom right quadrant
			#1 to 10
			x3 = random.choice([x for x in range(1, self.maxx) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			#-1
			y3 = random.choice([x for x in range(-self.maxx*2, 0) if x % denominator == 0 
				and abs(x*numerator/denominator) <= self.maxx])
		elif centerChosen == 'on side':
			#make 1st point top left
			x1 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			y1 = random.choice([x for x in range(1, self.maxx+1) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])

			#make 2nd point opposite of first point so (0,0) is on sidet
			x2 = -x1
			y2 = -y1

			#make x3 on the bottom left quadrant
			x3 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			y3 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
		elif centerChosen == 'outside':
			print(numerator)
			print(denominator)
			#make similar to vertex but not containing vertex etc
			#make x between half of self.maxx etc
			x1 = random.choice([x for x in range(-self.maxx//2, self.maxx//2) if x % denominator == 0 and abs(x*numerator/denominator) <= self.maxx])
			#make y between -self.maxx and 0 so I can make the other y’s less than that
			y1 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and abs(x*numerator/denominator) <= self.maxx*2])

			#make x2 any range but y2 lower than y1
			x2 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and
			abs(x*numerator/denominator) <= self.maxx])
			y2 = random.choice([x for x in range(-self.maxx*2, -self.maxx+5) if x % denominator == 0 
				and abs(x*numerator/denominator) <= self.maxx*2])

			#make x3 on the bottom right quadrant
			x3 = random.choice([x for x in range(1, self.maxx+1) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			y3 = random.choice([x for x in range(-self.maxx*2, -self.maxx+5) if x % denominator == 0 
				and abs(x*numerator/denominator) <= self.maxx*2])

		#rotate whole thing 90 or 180
		#relfect over y-axis or x-axis
		#these can be added later

		#make coordinates - later might be dependent on scaleFactorType if decimal etc


		#make coordinates
		if polygonTypeChosen == 'triangle':
			xs = [x1, x2, x3]
			ys = [y1, y2, y3]
		elif polygonTypeChosen == 'line':
			xs = [x1, x2]
			ys = [y1, y2]
		#dilate coordinates - center is 0,0 so dilation will work
		dXs = [int(x*numerator/denominator) for x in xs]
		dYs = [int(y*numerator/denominator) for y in ys]
		
		if scaleFactorTypeChosen == 'integer':
			#everything was backwards, so we need to switch coordinates
			oldXs = dXs
			oldYs = dYs

			newXs = xs
			newYs = ys

			xs = oldXs
			ys = oldYs
			dXs = newXs
			dYs = newYs

		#translate back and/or translate anywhere on the graph
		allXs = xs + dXs + [centerX]
		allYs = ys + dYs + [centerY]
		print(centerChosen)
		transX, transY, xChange, yChange = translateAnywhere(xs = allXs, ys = allYs, minn = -self.maxx, maxx = self.maxx)
		centerX = xChange
		centerY = yChange

		del transX[-1]
		del transY[-1]
		
		xs = transX[:len(transX)//2]
		ys = transY[:len(transY)//2]

		dXs = transX[len(transX)//2:]
		dYs = transY[len(transY)//2:]

		if scaleFactorTypeChosen == 'integer':
			scaleFactorString = '%d' % denominator
		else:
			scaleFactorString = r'$\frac{%d}{%d}$' % (numerator, denominator)

		one, two, three = annotatePolygon(vertices = 3, prime = True)

		#now we need the question
		if polygonTypeChosen == 'triangle':
			questionString = r'Given $\bigtriangleup{%s}$ below, graph $\bigtriangleup{%s}$ after a dilation centered at $(%d,%d)$ with a scale factor of %s.' % (one[0]+one[1]+one[2], two[0]+two[1]+two[2], centerX, centerY, scaleFactorString)
		else:
			questionString = r'Given $\overline{%s}$ below, graph $\overline{%s}$ after a dilation centered at $(%d,%d)$ with a scale factor of %s.' % (one[0]+one[1], two[0]+two[1], centerX, centerY, scaleFactorString)
		doc.append(NoEscape(questionString))
		self.answer = r'$%s(%d,%d)$' % (two[1], dXs[1], dYs[1])
		#then we need the graph with the figures on it etc.
		doc.append(NewLine())
		picLength = sizingXY(minn = -self.maxx, maxx = self.maxx, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = -self.maxx, xMax = self.maxx, yMin = -self.maxx, yMax = self.maxx)
				graphPolygon(doc = doc, x = xs, y = ys, annotations = one, color = 'black')
				#graphPolygon(doc = doc, x = dXs, y = dYs, annotations = two, color = 'red')

				#graphPoint(doc = doc, x = centerX, y = centerY, annotations = ['center'], color = 'blue')
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class identifyDilation():
	def __init__(self, center = ['vertex','origin','inside','outside','on side'],
						scaleFactorType = ['integer','fraction'],
						maxx = 10,
						polygonType = ['line','triangle'],
						scaleFactorAmount = ['<1','>1','1'],
						prime = True):
		self.center = center
		self.scaleFactorType = scaleFactorType
		self.maxx = maxx
		self.polygonType = polygonType
		self.scaleFactorAmount = scaleFactorAmount
		self.typeOfQuestionChosen = 'SA'
		self.prime = prime

		self.subjects = ['Geometry']
		self.topics = ['Transformations', 'Dilations']
		self.skills = [['Identify a dilation'], ['Identify a dilation']]
		self.standard = "CCSS.MATH.CONTENT.HSG.CO.A.2"

		self.ID = self.subjects[0] + 'identifyDilation'
	def addQuestion(self, doc = None):
		scaleFactorTypeChosen = random.choice(self.scaleFactorType)
		scaleFactorAmountChosen = random.choice(self.scaleFactorAmount)
		polygonTypeChosen = random.choice(self.polygonType)
		centerChosen = random.choice(self.center)
		
		if scaleFactorTypeChosen == 'integer':
			#starting with bigger triangle and making smaller, so I'll reverse the fraction etc
			numerator = 1
			denominator = random.randint(2, (2*self.maxx)//4)
			if scaleFactorAmountChosen == '1':
				numerator = 1
				denominator = 1
		else:
			if scaleFactorAmountChosen == '<1':
				denominator = random.randint(2, (2*self.maxx)//4)
				numerator = random.choice([x for x in range(1, denominator)])
			elif scaleFactorAmountChosen == '>1':
				#1/2 to probably 1/5
				denominator = random.randint(2, (2*self.maxx)//4)
				#starting with denominator+1/denominator, so 6/5, making it already greater than 1
				#6/5 to denominatorMax*denominator, so 25/5, not including fractions that
				#just do between 1 and 2
				numerator = random.choice([x for x in range(denominator+1, denominator*2+1) if x % denominator != denominator and x % denominator != 0])
				#we should also reduce this fraction if possible

		scaleFactor = Fraction(numerator/denominator).limit_denominator(100)
		numerator = scaleFactor.numerator
		denominator = scaleFactor.denominator
		#When defining points based on center, steps needs to be of a number % denominator   == 0

		#inside triangle: keep 1st point in top left or top right, keep 2nd point bottom left, 3rd point bottom right
		#vertex: center is 1st point, 2nd point somewhere, 3rd point somewhere
		#on side: make 1st point, then make 2ndpoint opposite values of first
		#outside: points are either <0 or >0

		

		centerX = 0
		centerY = 0

		if centerChosen == 'inside': #make x1 same as center, so ensuring inside
			x1 = 0
			print(denominator)
			print(numerator)
			y1 = random.choice([x for x in range(1, self.maxx+1) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])

			#make x2 on the bottom left quadrant
			#anywhere from left to not touching y-axis
			x2 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			#anywhere from bottom to - 1 (one below center point)
			y2 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])

			#make x3 on the bottom right quadrant
			#1 to 10
			x3 = random.choice([x for x in range(1, self.maxx+1) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			#-10 to -1
			y3 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
		elif centerChosen == 'vertex':
			x1 = 0
			y1 = 0

			#since vertex is 0,0 and triangle won't be above this at all, let's stret

			#make x2 on bottom left
			#-10 to -1
			x2 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			#-10 to -1
			y2 = random.choice([x for x in range(-self.maxx*2, 0) if x % denominator == 0 
				and abs(x*numerator/denominator) <= self.maxx])

			#make x3 on the bottom right quadrant
			#1 to 10
			x3 = random.choice([x for x in range(1, self.maxx) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			#-1
			y3 = random.choice([x for x in range(-self.maxx*2, 0) if x % denominator == 0 
				and abs(x*numerator/denominator) <= self.maxx])
		elif centerChosen == 'on side':
			#make 1st point top left
			x1 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			y1 = random.choice([x for x in range(1, self.maxx+1) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])

			#make 2nd point opposite of first point so (0,0) is on sidet
			x2 = -x1
			y2 = -y1

			#make x3 on the bottom left quadrant
			x3 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			y3 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
		elif centerChosen == 'outside':
			print(numerator)
			print(denominator)
			#make similar to vertex but not containing vertex etc
			#make x between half of self.maxx etc
			x1 = random.choice([x for x in range(-self.maxx//2, self.maxx//2) if x % denominator == 0 and abs(x*numerator/denominator) <= self.maxx])
			#make y between -self.maxx and 0 so I can make the other y’s less than that
			y1 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and abs(x*numerator/denominator) <= self.maxx*2])

			#make x2 any range but y2 lower than y1
			x2 = random.choice([x for x in range(-self.maxx, 0) if x % denominator == 0 and
			abs(x*numerator/denominator) <= self.maxx])
			y2 = random.choice([x for x in range(-self.maxx*2, -self.maxx+5) if x % denominator == 0 
				and abs(x*numerator/denominator) <= self.maxx*2])

			#make x3 on the bottom right quadrant
			x3 = random.choice([x for x in range(1, self.maxx+1) if x % denominator == 0 and 
				abs(x*numerator/denominator) <= self.maxx])
			y3 = random.choice([x for x in range(-self.maxx*2, -self.maxx+5) if x % denominator == 0 
				and abs(x*numerator/denominator) <= self.maxx*2])

		#rotate whole thing 90 or 180
		#relfect over y-axis or x-axis
		#these can be added later

		#make coordinates - later might be dependent on scaleFactorType if decimal etc


		#make coordinates
		if polygonTypeChosen == 'triangle':
			xs = [x1, x2, x3]
			ys = [y1, y2, y3]
		elif polygonTypeChosen == 'line':
			xs = [x1, x2]
			ys = [y1, y2]
		#dilate coordinates - center is 0,0 so dilation will work
		dXs = [int(x*numerator/denominator) for x in xs]
		dYs = [int(y*numerator/denominator) for y in ys]
		
		if scaleFactorTypeChosen == 'integer':
			#everything was backwards, so we need to switch coordinates
			oldXs = dXs
			oldYs = dYs

			newXs = xs
			newYs = ys

			xs = oldXs
			ys = oldYs
			dXs = newXs
			dYs = newYs

		#translate back and/or translate anywhere on the graph
		allXs = xs + dXs + [centerX]
		allYs = ys + dYs + [centerY]
		print(centerChosen)
		transX, transY, xChange, yChange = translateAnywhere(xs = allXs, ys = allYs, minn = -self.maxx, maxx = self.maxx)
		centerX = xChange
		centerY = yChange

		del transX[-1]
		del transY[-1]
		
		xs = transX[:len(transX)//2]
		ys = transY[:len(transY)//2]

		dXs = transX[len(transX)//2:]
		dYs = transY[len(transY)//2:]

		if scaleFactorTypeChosen == 'integer':
			scaleFactorString = '%d' % denominator
		else:
			scaleFactorString = r'$\frac{%d}{%d}$' % (numerator, denominator)

		one, two, three = annotatePolygon(vertices = 3, prime = self.prime)

		#now we need the question
		if polygonTypeChosen == 'triangle':
			questionString = r'Given the graph below, describe the transformation that maps $\bigtriangleup{%s}$ onto $\bigtriangleup{%s}$.' % (one[0]+one[1]+one[2], two[0]+two[1]+two[2])
		else:
			questionString = r'Given the graph below, describe the transformation that maps $\overline{%s}$ onto $\overline{%s}$.' % (one[0]+one[1], two[0]+two[1])
		doc.append(NoEscape(questionString))
		self.answer = r'A dilation centered at $(%d,%d)$ with a scale factor of %s.' % (centerX, centerY, scaleFactorString)
		
		#then we need the graph with the figures on it etc.
		doc.append(NewLine())
		picLength = sizingXY(minn = -self.maxx, maxx = self.maxx, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = -self.maxx, xMax = self.maxx, yMin = -self.maxx, yMax = self.maxx)
				graphPolygon(doc = doc, x = xs, y = ys, annotations = one, color = 'black')
				graphPolygon(doc = doc, x = dXs, y = dYs, annotations = two, color = 'black')

				#graphPoint(doc = doc, x = centerX, y = centerY, annotations = ['center'], color = 'blue')
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))