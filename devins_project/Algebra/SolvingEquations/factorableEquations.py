from pdfFunctions import sizingXY, grid, graphPolygon, annotatePolygon, com, pointsForDiagram, graphPoint
from pylatex.utils import NoEscape
import random
from pylatex import VerticalSpace, NewLine, Center, TikZ
from fractions import Fraction, gcd
import math
#see if this works to get shit that I need in here??
def exponentString(variableString, exponentValue):
			if exponentValue == 0:
				return ''
			elif exponentValue == 1:
				return variableString
			else:
				return r'%s^{%d}' % (variableString, exponentValue)


def numberString(value):
	if value == 1:
		return ''
	elif value == -1:
		return r'-'
	else:
		return r'%d' % value

def noSharedFactors(num, items):
	return [item for item in items if math.gcd(item, num) == 1]

def equationFormatting(value, inBetween = True, nextToVariable = False):
	if inBetween == True:
		if nextToVariable == True:
			if value == 1:
				return "+"
			elif value == -1:
				return "-"
		if value > 0:
			return "+%d" % value
		elif value < 0:
			return "-%d" % abs(value)
		elif value == 0:
			return ""
	else:
		if nextToVariable == True:
			if value == 1:
				return ""
			elif value == -1:
				return "-"
			else:
				return "%d" % value
		else:
			return "{%d}" % value
class solveGCF():
	def __init__(self, numberMax = 5, standard = True):
		self.dick = 1
		self.numberMax = numberMax
		self.typeOfQuestionChosen = 'SA'
		self.subjects = ['Algebra']
		self.topics = ['Solving Equations', 'Factoring']
		self.skills = [['Solve a quadratic equation'],['Factor by GCF']]
		self.standards = ['CCSS.MATH.CONTENT.HSA.REI.B.4']
		self.standard = standard
		#let's do 1-3 variables, 1-3 terms
		#so we have 3 numbers, 3 variabes, 3 variables exponents
		#types: no number GCF
		#for numbers, go backwards, pick coprime numbers and random answer in front, then multiply
		#exponents of same variable shoudn't be equal to eliminate having the same terms
	
		#when is GCF useful???
		#taking out a number, taking out like 1 or 2 x's, maybe 4 etc for completely factoring
		#


	def addQuestion(self, doc = None):
		numberOfTermsChosen = 2
		#COEF ( num1 + num2 + num3)

		num1 = random.choice([x for x in range(2, 26) if x not in [0]])
		num2 = random.choice(noSharedFactors(num1, range(-25,25+1)))

		answerNum = random.randint(2,self.numberMax)
		
		answerXE = random.randint(1,10) # 7
	
		temp1 = 1 #so that we can solve it etc
		temp3 = 0
		listOfExponents = [temp1, temp3]
		x1E = random.choice(listOfExponents)
		listOfExponents.remove(x1E)
		x2E = random.choice(listOfExponents)

		gcfPart = r'%s%s' % (numberString(answerNum), exponentString('x', answerXE))
		answerPart1 = r'%s%s' % (numberString(num1), exponentString('x', x1E))
		answerPart2 = r'-%s%s' % (numberString(abs(num2)), exponentString('x', x2E))
		if num2 > 0:
			answerPart2 = r'+%s%s' % (numberString(abs(num2)), exponentString('x', x2E))

		self.answer = r'$' + gcfPart + '(' + answerPart1 + answerPart2 + ")" + r'$'
		#we want x1E to be 1
		#so based on this answer number*x(num1*x + num2)
		#x = 0 and x = -num2/num1
		sol1 = '0'
		if x1E == 1:
			sol2 = -num2/num1	
			if num2 % num1 != 0:
				sol2Frac = Fraction(-num2/num1).limit_denominator(100)
				sol2 = r'$\frac{%d}{%d}$' % (sol2Frac.numerator, sol2Frac.denominator)
			else:
				sol2 = str(int(-num2/num10))
			
		else:
			sol2 = -num1/num2
			if num1 % num2 != 0:
				sol2Frac = Fraction(-num1/num2).limit_denominator(100)
				sol2 = r'$\frac{%d}{%d}$' % (sol2Frac.numerator, sol2Frac.denominator)
			else:
				sol2 = str(int(-num1/num2))
				

		self.answer = r'x = %s and x = %s'  % (sol1, sol2)

		question1 = r'%s%s' % (numberString(answerNum*num1), exponentString('x', answerXE+x1E))
		
		if answerNum * num2 < 0:
			question2 = r'-%s%s' % (numberString(abs(answerNum*num2)), exponentString('x', answerXE+x2E))
		else:
			question2 = r'+%s%s' % (numberString(abs(answerNum*num2)), exponentString('x', answerXE+x2E))
		
		if self.standard == True:
			self.question = r'$' + question1 + question2 + '=0' + r'$'
		else:
			#mix it up, ah i dunno yet
			dick = 3
		doc.append(NoEscape(self.question))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
		docAnswer.append(VerticalSpace('.1in'))

class solveDOTS():
	def __init__(self, difficulty = [1,2], GCFMax = 10, GCFNumber = False, GCFVariable = False, standard = True):
		china = 1
		#types:
		#TYPE1: x^2 - ps
		#TYPE2: psx^2 - ps
		#TYPE3: GCF first
		#TYPE4: trick where two psquares have a common factor
		self.difficulty = difficulty
		self.GCFNumber = GCFNumber
		self.GCFVariable = GCFVariable
		self.typeOfQuestionChosen = 'SA'
		self.GCFMax = GCFMax

		self.subjects = ['Algebra']
		self.topics = ['Solving Equations', 'Factoring']
		self.skills = [['Solve a quadratic equation'],['Factor by DOTS']]
		self.standards = ['CCSS.MATH.CONTENT.HSA.REI.B.4']
		self.standard = standard

	def addQuestion(self, doc = None):
		perfectSquares = [x**2 for x in range(15) if x not in [0]]
		self.p1 = random.choice(perfectSquares)
		factorsNotShared = noSharedFactors(self.p1, perfectSquares)
		self.p2 = random.choice(factorsNotShared)
		self.GCF = random.randint(2,self.GCFMax)
		self.variable = random.choice(['x','y','z'])
		difficultyChosen = random.choice(self.difficulty)


		if difficultyChosen == 1:
			if self.GCFNumber == True:
				if self.GCFVariable == True:
					takeOut = random.randint(1,5)
					if self.standard == True:
						doc.append(NoEscape(r'$%d%s^{%d}-%d%s=0$' % (self.GCF, self.variable, takeOut+2, self.GCF*self.p1, exponentString(self.variable, takeOut))))

					self.answer = r'$%d%s(%s+%d)(%s-%d)$' % (self.GCF, exponentString(self.variable, takeOut), self.variable, self.p1**(1/2), self.variable, self.p1**(1/2))
					#based on this answer
					#we have x = 0
					#x = -self.p1**(1/2)
					#x = self.p1**(1/2)
					self.answer = r'$x = 0, x = %d, and x = %d$' % (self.p1**(1/2), -self.p1**(1/2))
				else:
					doc.append(NoEscape(r'$%d%s^{2}-%d=0$' % (self.GCF, self.variable, self.GCF*self.p1)))
					self.answer = r'$%d(%s+%d)(%s-%d)$' % (self.GCF, self.variable, self.p1**(1/2), self.variable, self.p1**(1/2))
					self.answer = r'$x = %d and x = %d$' % (self.p1**(1/2), -self.p1**(1/2))

			else:
				doc.append(NoEscape(r'$%s^{2}-%d=0$' % (self.variable, self.p1)))
				self.answer = r'$(%s+%d)(%s-%d)$' % (self.variable, self.p1**(1/2), self.variable, self.p1**(1/2))
				self.answer = r'$x = %d and x = %d$' % (self.p1**(1/2), -self.p1**(1/2))


		elif difficultyChosen == 2:
			if self.GCFNumber == True:
				if self.GCFVariable == True:
					takeOut = random.randint(2,5)
					doc.append(NoEscape(r'$%d%s^{%d}-%d%s=0$' % (self.p1*self.GCF, self.variable, 2+takeOut, self.p2*self.GCF, exponentString(self.variable, takeOut))))
					self.answer = r'$%d%s(%d%s+%d)(%d%s-%d)$' % (self.GCF, exponentString(self.variable, takeOut), self.p1**(1/2), self.variable, self.p2**(1/2), self.p1**(1/2), self.variable, self.p2**(1/2))
					#x = 0
					#x = -self.p2**(1/2) / self.p1**(1/2)
					#x = self.p2**(1/2) / self.p1**(1/2)
					sol1 = 0
					sol2 = Fraction(-self.p2**(1/2) / self.p1**(1/2)).limit_denominator(100)
					sol3 = Fraction(self.p2**(1/2) / self.p1**(1/2)).limit_denominator(100)
					self.answer = r'$x = %d, x = \frac{%d}{%d}, and x = \frac{%d}{%d}$' % (sol1, sol2.numerator, sol2.denominator, sol3.numerator, sol3.denominator)

				else:
					doc.append(NoEscape(r'$%d%s^{2}-%d=0$' % (self.p1*self.GCF, self.variable, self.p2*self.GCF)))
					self.answer = r'$%d(%d%s+%d)(%d%s-%d)$' % (self.GCF, self.p1**(1/2), self.variable, self.p2**(1/2), self.p1**(1/2), self.variable, self.p2**(1/2))
					#x = -self.p2**(1/2) / self.p1**(1/2)
					#x = self.p2**(1/2) / self.p1**(1/2)
					sol2 = Fraction(-self.p2**(1/2) / self.p1**(1/2)).limit_denominator(100)
					sol3 = Fraction(self.p2**(1/2) / self.p1**(1/2)).limit_denominator(100)
					self.answer = r'$x = \frac{%d}{%d} and x = \frac{%d}{%d}$' % (sol2.numerator, sol2.denominator, sol3.numerator, sol3.denominator)
			else:
				doc.append(NoEscape(r'$%d%s^{2}-%d=0$' % (self.p1, self.variable, self.p2)))
				self.answer = r'$(%d%s+%d)(%d%s-%d)$' % (self.p1**(1/2), self.variable, self.p2**(1/2), self.p1**(1/2), self.variable, self.p2**(1/2))
				#x = -self.p2**(1/2) / self.p1**(1/2)
				#x = self.p2**(1/2) / self.p1**(1/2)
				sol2 = Fraction(-self.p2**(1/2) / self.p1**(1/2)).limit_denominator(100)
				sol3 = Fraction(self.p2**(1/2) / self.p1**(1/2)).limit_denominator(100)
				self.answer = r'$x = \frac{%d}{%d} and x = \frac{%d}{%d}$' % (sol2.numerator, sol2.denominator, sol3.numerator, sol3.denominator)
		
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
		docAnswer.append(VerticalSpace('.2in'))

class solveTrinomials():
	def __init__(self, bSign = ['positive','negative'], cSign = ['positive','negative'], aEqualsOne = True):
		self.bSign = bSign
		self.cSign = cSign
		self.aEqualsOne = aEqualsOne
		self.typeOfQuestionChosen = 'SA'

		self.subjects = ['Algebra']
		self.topics = ['Solving Equations', 'Factoring']
		self.skills = [['Solve a quadratic equation'],['Factor Trinomials']]
		self.standards = ['CCSS.MATH.CONTENT.HSA.REI.B.4']
		
	def addQuestion(self, doc = None):
		bSignChosen = random.choice(self.bSign)
		cSignChosen = random.choice(self.cSign)
		#(tx+r)(vx+s)
		if self.aEqualsOne == True:
			if cSignChosen == 'positive':
				r = random.randint(1,20)
				s = random.randint(1,20)
				if bSignChosen == 'positive':
					doNothing = 1
				else:
					r = -1*r
					s = -1*s
			else:
				nums = [x for x in range(21) if x not in [0]]
				r = random.choice(nums)
				s = random.choice([x for x in nums if x != r])
				while r == s:
					print('uhh')
					r = random.randint(1,20)
					s = random.randint(1,20)
				if bSignChosen == 'positive':
					#make two random numbers, make smaller number negative
					if r > s:
						s = s*-1
					else:
						r = r*-1
				elif bSignChosen == 'negative':
					if r > s:
						r = r*-1
					else:
						s = s*-1
			
			#(tx+r)(vx+s)
			questionString = r'$x^{2}%sx%s=0$' % (equationFormatting(r+s, nextToVariable = True), equationFormatting(r*s))
			self.answer = r'$x=%d$ and $x=%d$' % (-r, -s)
			doc.append(NoEscape(questionString))
		elif self.aEqualsOne == False:
			if cSignChosen == 'positive':
				tvItems = [x for x in range(1,4)]
				t = random.choice(tvItems)
				if t == 1:
					tvItems.remove(t)
				rItems = [x for x in range(15) if x not in [0]]
				r = random.choice(noSharedFactors(t, rItems))

				v = random.choice(tvItems) #note v and t can't be both 1
				sItems = [x for x in range(15) if x not in [0]]
				s = random.choice(noSharedFactors(v, sItems))

				if bSignChosen == 'positive':
					doNothing = 1
				else:
					r = -1*r
					s = -1*s
			else:
				tvItems = [x for x in range(1,4)]
				t = random.choice(tvItems)
				if t == 1:
					tvItems.remove(t)
				rItems = [x for x in range(15) if x not in [0]]
				r = random.choice(noSharedFactors(t, rItems))

				v = random.choice(tvItems) #note v and t can't be both 1
				sItems = [x for x in range(15) if x not in [0]]
				s = random.choice(noSharedFactors(v, sItems))
				#(tx+r)(vx+s)
				if bSignChosen == 'positive':
					#make two random numbers, make smaller number negative
					if r*v > s*t:
						s = s*-1
					else:
						r = r*-1
					if r*v == s*t:
						print('error')
				elif bSignChosen == 'negative':
					if r*v > s*t:
						r = r*-1
					else:
						s = s*-1
					if r*v == s*t:
						print('error')
			
			questionString = r'$%dx^{2}%sx%s=0$' % (t*v, equationFormatting(r*v+s*t, nextToVariable = True), equationFormatting(r*s))
			self.answer = r'$(%sx%s)(%sx%s)$' % (equationFormatting(t, inBetween = False, nextToVariable = True),equationFormatting(r, inBetween = True, nextToVariable = False), equationFormatting(v, inBetween = False, nextToVariable = True), equationFormatting(s, inBetween = True, nextToVariable = False))
			sol1 = -r/t
			if -r % t == 0:
				sol1String = '%d' % (int(-r/t))
			else:
				sol1Frac = Fraction(-r/t).limit_denominator(100)
				sol1String = r'\frac{%d}{%d}' % (sol1Frac.numerator, sol1Frac.denominator)
			sol2 = -s/v
			if -s % v == 0:
				sol2String = '%d' % (int(-s/v))
			else:
				sol2Frac = Fraction(-s/v).limit_denominator(100)
				sol2String = r'\frac{%d}{%d}' % (sol2Frac.numerator, sol2Frac.denominator)
			self.answer = r'$x=%s$ and $x=%s$' % (sol1String, sol2String)
			doc.append(NoEscape(questionString))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class factorByGrouping():
	def __init__(self, numberMax = 10, groupingGCF = ['random','1','-1','positive','negative','positiveNot1','negativeNot-1']):
		dick = 3
		#if groupingGCF == 1, then for factoring the second pair, GCF is 1, same with if groupbGCF is -1
		#GCFNumber is for factoring completely
		#GCFVariable is for factoring completely
		"""
		type1: (x^2+2)(x-3), so x^3 + 2x - 3x^2 - 6
		-subtle things to notice, if gcf is 1, -1

		type2: (x^2+2x)(y-3), so x^2y + 2xy - 3x^2 - 6x
		-subtle things to notice, if gcf is 1,-1 etc
		type3: (x+2)(x^2-3x+1), so x^3+2x^2 -3x^2-6x +x+2

		"""
		self.numberMax = numberMax
		self.groupingGCF = random.choice(groupingGCF)

	def addQuestion(self, doc = None):
		#(ax^2+b)(cx+d)
		#b can't be a perfect square and also not share a factor with a
		#c can't share a factor with d
		#acx^3+adx^2+bcx+bd -- don't mess with order lol
		#ax^2(cx+d) + b(cx+d)
		#so now I can control what I take out

		a = random.choice([x for x in range(1,self.numberMax+1) if x not in [0]])
		pSquares = [x**2 for x in range(2,self.numberMax+1)]
		if self.groupingGCF == '1':
			b = 1
		elif self.groupingGCF == '-1':
			b = -1
		elif self.groupingGCF == 'positive':
			b = random.choice([x for x in range(1,self.numberMax+1) if gcd(x,a) == 1 and x not in pSquares])
		elif self.groupingGCF == 'negative':
			b = random.choice([x for x in range(-self.numberMax,0) if gcd(x,a) == 1 and x not in pSquares])
		elif self.groupingGCF == 'positiveNot1':
			b = random.choice([x for x in range(2,self.numberMax+1) if gcd(x,a) == 1 and x not in pSquares])
		elif self.groupingGCF == 'negativeNot-1':
			b = random.choice([x for x in range(-self.numberMax,-1) if gcd(x,a) == 1 and x not in pSquares])
		else:
			b = random.choice([x for x in range(-self.numberMax,self.numberMax+1) if gcd(x,a) == 1 and x not in pSquares])

		c = random.choice([x for x in range(1,self.numberMax+1) if x not in [0]])

		if c == 1:
			d = random.choice([x for x in range(-self.numberMax,self.numberMax+1) if x not in [0]])
		else:
			d = random.choice([x for x in range(-self.numberMax,self.numberMax+1) if gcd(x,c) == 1])

		questionString = r'$%sx^3%sx^2%sx%s$' % (equationFormatting(value = a*c, inBetween = False, nextToVariable = True),
		equationFormatting(value = a*d, inBetween = True, nextToVariable = True),
		equationFormatting(value = b*c, inBetween = True, nextToVariable = True),
		equationFormatting(value = b*d, inBetween = True, nextToVariable = False) )

		self.answer = r'$(%sx^2%s)(%sx%s)$' % ( equationFormatting(value = a, inBetween = False, nextToVariable = True),equationFormatting(value = b, nextToVariable = False, inBetween = True), equationFormatting(value = c, nextToVariable = True, inBetween = False), equationFormatting(value = d, nextToVariable = False, inBetween = True))

		doc.append(NoEscape(questionString))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))