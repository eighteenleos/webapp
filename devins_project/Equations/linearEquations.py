#for negative enclsoe is {}
from functools import reduce
from fractions import fractions

def factors(n):    
    return set(reduce(list.__add__, 
                ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))


def oneStepEquation(operation = 'Addition', variable = 'x', integerSolution = True, standardOrder = True):
	valueDict = {}
	minNumberValue = -10
	maxNumberValue = 10
	if operation == 'Addition': #x + a = b
		minNumberValue = -10
		maxNumberValue = 10
		excluding = 0
		aVal = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [excluding])
		bVal = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [excluding])
		answer = r'$%s=%s$' % (variable, '{%d}' % (bVal - aVal))
		if standardOrder == True: #x + a = b
			place1 = variable
			place2 = '{%d}' % aVal
			place3 = '%d' % bVal
			expression = r'$%s%s=%s' % (place1, place2, place3)
		else:  
			version = random.randint(1,3)
			if version == 1: #a + x = b
				place1 = '{%d}' % aVal
				place2 = variable
				place3 = '{%d}' % bVal
				expression = r'$%s%s=%s$' % (place1, place2, place3)
				
			elif version == 2: #b = a + x
				place1 = '{%d}' % bVal
				place2 = '{%d}' % aVal
				place3 = variable
				expression = r'$%s=%s%s$' % (place1, place2, place3)

			elif version == 3: #b = x + a
				place1 = '{%d}' % bVal
				place2 = variable
				place3 = 
				expression = r'$%s=%s%s$' % (place1, place2, place3)
	
	elif operation == 'Multiplication':
		#ax = b OR b = ax

		excluding = 0
		if integerSolution == True:
			answerValue = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [excluding])
			answer = r'$%s={%d}$' % (variable, '{%d}' % (answerValue))
			aVal = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [excluding])
			bVal = answerValue * aVal
		else:
			bVal = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [excluding])		
			bValFactors = factors(bVal)
			aVal = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [bValFactors])
			numerator, denominator = Fraction(bVal, aVal)
			answer = r'$%s={\frac{%d}{%d}}$' % (variable, numerator, denominator)

		if standardOrder == True:
			expression = r'${%d}%s={%d}$' % (aVal, variable, bVal)
		else:
			expression = r'${%d}={%d}%s$' % (aVal, bVal, variable)

	elif operation == 'Division': #x/a = b OR b = x/a ........ b*a = x
		aVal = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [0, -1, 1])
		bVal = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [0, -1, 1])
		answerValue = aVal * bVal
		answer = r'$%s={%d}$' % (variable, answerValue)
		if standardOrder == True:
			expression = r'$\frac{%s}{%d}={%d}$' % (variable, aVal, bVal)
		else:
			expression = r'${%d}=\frac{%s}{%d}$' % (bVal, variable, aVal)

	elif operation == 'Fractional':	
		#ax/b = c .... c = ax/b
		#a, b, c , not 0, -1, 1
		#c * b / a = x
		#for integers, x = int
		#xFactors make b, then random a should work
		#fractional xFactors, b not an xfactor etc
		if integerSolution == True:
			answer = random.choice([i for i in range(minNumberValue, maxNumberValue)] if not in [0, -1, 1])
			xFactors = factors(x)
			bVal = random.choice([i for i in xFactors] if not in [0, -1, 1])
	return expression, answer