#default - if True, will use default options for database
#xMingraph, xMaxGraph, yMinGraph, yMaxGraph, mustHaveNoFlatSides, mustHaveIntPoints - defaults are integers
#polygonSize, gridSize - when only a finite number, include options as strings inside lists
class template():
    def __init__(self, default = False, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, mustHaveNoFlatSides = False, mustHaveIntPoints = False, polygonSize = ["small", "medium", "large"], gridSize = ['small', 'medium', 'large']):
        
        #This is used for the database aspect, what you want to be used for defaults if other than noted above (for example for options that are in a list)
        if default == True:
            self.polygonSize = ['medium']
            self.gridSize = ['medium']
        
        #This is what should be used if default is False - for setting up the options - could this be removed??
        else:
            self.defaultKwargs = {'default': False, 'xMinGraph': -10, 'xMaxGraph': 10, 'yMinGraph': -10, 'yMaxGraph': 10, 'mustHaveNoFlatSides': False, 'mustHaveIntPoints': False, 'polygonSize': ['small', "medium", 'large'], 
            'gridSize':['small', 'medium', 'large']}

        #Have not handled more than 1 topic yet
        self.topics = ['Transformations']
        self.skills = ['Determine minimum degrees of rotation to map onto itself','Identify the number of sides for a regular polygon']
        #Skill unless there is a specific type of option asssociated like subskills etc
        ##################at some point need to rework database?
        self.option = 

        #Non - default KWARGS setting selfs
        self.xMinGraph = xMinGraph
        self.xMaxGraph = xMaxGraph
        self.yMinGraph = yMinGraph
        self.yMaxGraph = yMaxGraph

        self.mustHaveNoFlatSides = mustHaveNoFlatSides
        self.mustHaveIntPoints = mustHaveIntPoints

        self.polygonSize = polygonSize
        self.gridSize = gridSize

        #ID is topic + MAIN SKILL
        self.ID = self.topics[0] + "GraphThisShit"
    
    def addQuestion(self, doc = None):
        #If more than 1 checkbox is checked, it will be random of a list, if specific only 1 item will be in list.
        self.gridSize = random.choice(self.gridSize)
        self.polygonSize = random.choice(self.polygonSize)

        #Text
        doc.append(NoEscape(
            r'Check out this graph below'))

        doc.append(NewLine())

        #If needing to insert diagram or graph
        length = sizingXY( min([self.xMinGraph, self.yMinGraph]), max([self.xMaxGraph, self.yMaxGraph]), self.gridSize)

        with doc.create(Center()):
            with doc.create(TikZ(
                    options='x=%gcm, y=%gcm' % (length, length))): # length, length makes the graph square
                grid(doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)

                rightTriangleObject = MakeRightTriangle(self.xMinGraph, self.xMaxGraph, self.yMinGraph, self.yMaxGraph, mustHaveNoFlatSides = self.mustHaveNoFlatSides, mustHaveIntPoints = self.mustHaveIntPoints, size = self.polygonSize)

                #get coorrdinates of shape
                self.x, self.y = ReturnPointCoordsAsLists(rightTriangleObject)

                #graphs the actual polygon and annotations
                #optional: labelSize, labelDist
                #If you don't want annotations, don't specify anything
                graphPolygon(doc, shape = shape, annotations = ['A', 'B', 'C'], color = 'black')

        doc.append(VerticalSpace(r'1in'))

    def addAnswer(self, doc = None):
        china = 'lol'
        #same as document but whatever the answer is, maybe want to include a graph to show the solution etc.


class testQuestion1():
    def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
        self.typeOfQuestionChosen = random.choice(typeOfQuestion)
        self.rotate = rotate
    def addQuestion(self, doc = None):
    def addAnswer(self, docAnswer = None):