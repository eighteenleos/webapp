import random
from questionFormatting import multipleChoice
from pylatex.utils import NoEscape
from pylatex import VerticalSpace, NewLine, Center, TikZ
from pdfFunctions import sizingXY, grid, graphPolygon, makeShapeObjectFromPoints, annotatePolygon, com, pointsForDiagram, graphPoint
from documentCreation import snippet, createWorksheet
from transformations import translation, rotateCoordinatesAsList, reflection, rotation, sequence, reflectCoordinates, translateCoordinates
import sys
sys.path.insert(0, '/home/devin/webapp/devins_project/Geometry/3D')
from shapes import roundGivenString, cone, cylinder, sphere, rectangularPrism, cube, regularPyramid, rectangularPyramid, triangularPyramid, triangularPrism, hemisphere, halfcylinder, draw3DShape, rectangle, rightTriangle, circle, cylinderHemisphere, parallelogram, triangleCoordinates, rectangleCoordinates

sys.path.insert(0, '/home/devin/webapp/devins_project/Geometry/Dilations')
from _dilations_ import performDilation, identifyDilation

sys.path.insert(0, '/home/devin/webapp/devins_project/Algebra/SolvingEquations')
from factorableEquations import solveGCF, solveDOTS, solveTrinomials
import math
import time
import os
from PyPDF2 import PdfFileWriter, PdfFileReader
from fractions import Fraction, gcd
import string

def mergePDFs(output_path, input_paths):
	pdf_writer = PdfFileWriter()

	print(output_path)
	print(input_paths)
	
	for path in input_paths:
		pdf_reader = PdfFileReader(path)
		for page in range(pdf_reader.getNumPages()):
			pdf_writer.addPage(pdf_reader.getPage(page))

	with open(output_path, 'wb') as fh:
		pdf_writer.write(fh)

	#remove pdfs
	for original in input_paths:
		os.remove(original)
def zipFiles(outputPath, filesWithPath): 
	fantasy_zip = zipfile.ZipFile(outputPath + '.zip', 'w')
	 
	for file in filesWithPath:
		fantasy_zip.write(file, basename(file))
def noSharedFactors(num, items):
	return [item for item in items if math.gcd(item, num) == 1]
def equationFormatting(value, inBetween = True, nextToVariable = False):
	if inBetween == True:
		if nextToVariable == True:
			if value == 1:
				return "+"
			elif value == -1:
				return "-"
		if value > 0:
			return "+%d" % value
		elif value < 0:
			return "-%d" % abs(value)
		elif value == 0:
			return ""
	else:
		if nextToVariable == True:
			if value == 1:
				return ""
			elif value == -1:
				return "-"
			else:
				return "%d" % value
		else:
			return "{%d}" % value

#######Transformations############
#class rotMinGivenPolygonFindMin():
	def __init__(self, default = False, option = ["Given polygon, find minimum", "Higher vocabulary"], polygon = ['triangle', 'quadrilateral', 'pentagon', 'hexagon', 'heptagon', 'septagon', 'octagon', 'nonagon', 'decagon', 'n-sided polygon'], typeOfQuestion = ['Multiple Choice', 'Short Answer']):
	   
		if default == True:
			self.option = ["Given polygon, find minimum"]
			self.polygon = ["triangle"]
			self.type = ['Multiple Choice']
		else:
			self.defaultKwargs = {'default': False, 
			'option': ["Given polygon, find minimum", "Higher vocabulary"], 
			'polygon': ['triangle', 'quadrilateral', 'pentagon', 'hexagon', 'heptagon', 'septagon', 'octagon', 'nonagon', 'decagon', 'n-sided polygon'], 
			'typeOfQuestion': ['Multiple Choice', 'Short Answer']}

			#When populating SQLdb, loop through all possible
			self.option = option
			self.polygon = polygon
			self.type = typeOfQuestion

		#double list, so those two skills go with transformations
		self.topics = ['Transformations']
		self.skills = ['Determine minimum degrees of rotation to map onto itself','Identify the number of sides for a regular polygon']
		
		

		#ID is topic + MAIN SKILL
		self.ID = self.topics[0] + "GivenPolygonFindMinimum"

 


	def addQuestion(self, doc = None):
		#below is for n-sided polygon
		factors360 = []
		numbers = list(range(11,361)) #starting at 11, since 10 downward has a name
		for item in numbers:
			if 360 % item == 0:
				factors360.append(item)
		if 0 in factors360:
			factors360.remove(0)
		if 360 in factors360:
			factors360.remove(360)

		x = random.choice(factors360)

		polygonDict = {'triangle': 3, 'quadrilateral': 4, 'pentagon': 5, 'hexagon': 6, 'heptagon':7, 'septagon': 7, 'octagon': 8, 'nonagon': 9, 'decagon': 10, '%d-sided polygon' % x: x}

		givenPolygon = random.choice(self.polygon)

		#Want to replace n-sided with actual side of polygon    
		if givenPolygon == 'n-sided polygon':
			givenPolygon = '%d-sided polygon' % x
		
		option = random.choice(self.option)
		
		typeOfQuestion = random.choice(self.type)
		
		if option == "Given polygon, find minimum": #minimum number of degrees basic
			#Same question for both kinds of questions
			self.questionString = r'What is the minimum number degrees of rotation to carry a %s onto itself?' % givenPolygon

			if typeOfQuestion == 'Short Answer':
				self.questionString = r'What is the minimum number degrees of rotation to carry a %s onto itself? Explain.' % givenPolygon

			elif typeOfQuestion == 'Multiple Choice':
				wrongChoices = [key for key in polygonDict]
				
				wrongChoices.remove(givenPolygon)
				if givenPolygon == 'heptagon':
					wrongChoices.remove('septagon')
				elif givenPolygon == 'septagon':
					wrongChoices.remove('heptagon')

				c2 = random.choice(wrongChoices)
				self.choice2 = r'$%d^{\circ}$' % (360/polygonDict[c2])
				wrongChoices.remove(c2)

				c3 = random.choice(wrongChoices)
				self.choice3 = r'$%d^{\circ}$' % (360/polygonDict[c3])
				wrongChoices.remove(c3)

				c4 = random.choice(wrongChoices)
				self.choice4 = r'$%d^{\circ}$' % (360/polygonDict[c4])
				wrongChoices.remove(c4)

		elif option == "Higher vocabulary":
			if typeOfQuestion == 'Short Answer':
				self.questionString = r'A regular %s is rotated in a %s direction about its center. Determine and state the minimum number of degrees in the rotation such that the %s will coincide with itself.' % (givenPolygon, random.choice(['clockwise','counterclockwise']), givenPolygon)   
			elif typeOfQuestion == 'Multiple Choice':
				self.questionString = r'A regular %s is rotated in a %s direction about its center. What is the minimum number of degrees in the rotation such that the %s will coincide with itself?' % (givenPolygon, random.choice(['clockwise','counterclockwise']), givenPolygon)   
				wrongChoices = ['triangle','quadrilateral','pentagon','hexagon','heptagon','octagon','nonagon','decagon']
				print(givenPolygon, wrongChoices)
				
				if givenPolygon in wrongChoices:
					wrongChoices.remove(givenPolygon)

				if givenPolygon == 'heptagon':
					wrongChoices.remove('septagon')
				elif givenPolygon == 'septagon':
					wrongChoices.remove('heptagon')

				c2 = random.choice(wrongChoices)
				self.choice2 = r'$%d^{\circ}$' % (360/polygonDict[c2])
				wrongChoices.remove(c2)

				c3 = random.choice(wrongChoices)
				self.choice3 = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
				wrongChoices.remove(c3)
				
				c4 = random.choice(wrongChoices)
				self.choice4 = r'$%d^{\circ}$' % (360/polygonDict[c4])
				wrongChoices.remove(c4) 

		self.answerString = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
		
		doc.append(NoEscape(self.questionString))
		doc.append(NewLine())
		if typeOfQuestion == 'Multiple Choice':
			choices = multipleChoice(doc = doc, choices = [self.answerString, self.choice2, self.choice3, self.choice4])
			answerChoice = choices.index(r'$%d^{\circ}$' % (360/polygonDict[givenPolygon]))
			self.answerString = r'(%d) $%d^{\circ}$' % (answerChoice+1,360/polygonDict[givenPolygon])
			
		doc.append(VerticalSpace(r'1in'))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answerString))
#class performTranslation(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, polygonType = ['right triangle'], polygonSize = ['small', 'medium', 'large'], notation = True, prime = True, xChangeAmount = ['-','+','0'], yChangeAmount = ['-','+','0'], overlap = False, option = ['Perform a translation']):
		
		if default == True:
			self.polygonType = ['right triangle']
			self.xChangeAmount = ['+']
			self.yChangeAmount = ['+']
			self.polygonSize = ['small']
		else:
			self.defaultKwargs = {'default': False, 'xMinGraph': -10, 'xMaxGraph': 10, 'yMinGraph': -10, 'yMaxGraph': 10, 'polygonType': ['right triangle'], 'notation': True, 'prime': True, 'xChangeAmount': ['-','+','0'], 'yChangeAmount': ['-','+','0'], 'overlap': False, 'polygonSize':['small','medium','large'], 'option':['Perform a Translation']}

			
			self.xChangeAmount = xChangeAmount
			self.yChangeAmount = yChangeAmount
			self.polygonType = polygonType
			self.polygonSize = polygonSize
		  

		self.option = ['Perform a Translation']
		self.prime = prime
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
			
		self.overlap = overlap
		self.type = 'SA'
		self.notation = notation


		self.topics = ['Transformations']
		self.skills = ['Perform a Translation', 'Understand algebraic translations notation']
		self.ID = self.topics[0] + 'performTranslation'

	def addQuestion(self, doc = None):
		polygonTypeChosen = random.choice(self.polygonType)
		polygonSizeChosen = random.choice(self.polygonSize)
		xChangeAmountChosen = random.choice(self.xChangeAmount)
		yChangeAmountChosen = random.choice(self.yChangeAmount)

		optionChosen = random.choice(self.option)

		self.one, self.two, self.three = annotatePolygon(self.prime, 3)

		self.shape, self.x, self.y, self.tX, self.tY, self.xVal, self.yVal = translation(xMinGraph = self.xMinGraph, xMaxGraph = self.xMaxGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, xChangeAmount = xChangeAmountChosen, yChangeAmount = yChangeAmountChosen, overlap = self.overlap, polygonType = polygonTypeChosen, polygonSize = polygonSizeChosen)

		if self.notation == False:
			if self.xVal < 0:
				xString = '%d units to the left ' % abs(self.xVal)
			elif xVal > 0:
				xString = '%d units to the right ' % abs(self.xVal)
			else:
				xString = ''

			if self.yVal < 0:
				yString = '%d units down' % abs(self.yVal)
			elif self.yVal > 0:
				yString = '%d units up' % abs(self.yVal)
			else:
				yString = ''

			if len(xString) > 0 and len(yString) > 0:
				doc.append(NoEscape(r'Given $\bigtriangleup %s$ on the set of axes below, graph $\bigtriangleup %s$ after a translation of ' % (self.one[0]+self.one[1]+self.one[2], self.two[0]+self.two[1]+self.two[2]) + xString + 'and '+ yString + '.'))
			elif len(xString) == 0 and len(yString) != 0:
				doc.append(NoEscape(
					r'Given $\bigtriangleup %s$ on the set of axes below, graph $\bigtriangleup %s$ after a translation of ' % (
					self.one[0] + self.one[1] + self.one[2], self.two[0] + self.two[1] + self.two[2]) + xString + yString + '.'))
			elif len(xString) != 0 and len(yString) == 0:
				doc.append(NoEscape(
					r'Given $\bigtriangleup %s$ on the set of axes below, graph $\bigtriangleup %s$ after a translation of ' % (
						self.one[0] + self.one[1] + self.one[2], self.two[0] + self.two[1] + self.two[2]) + xString + yString + '.'))
		else:
			if self.xVal < 0:
				xString = '$(x, y)\Rightarrow(x - %d, ' % abs(self.xVal)
			elif self.xVal > 0:
				xString = '$(x, y)\Rightarrow(x + %d, '  % abs(self.xVal)
			else:
				xString = '$(x, y)\Rightarrow(x, '

			if self.yVal < 0:
				yString = 'y - %d)$' % abs(self.yVal)
			elif self.yVal > 0:
				yString = 'y + %d)$' % abs(self.yVal)
			else:
				yString = 'y)$'

			doc.append(NoEscape(r'Given $\bigtriangleup %s$ on the set of axes below, graph $\bigtriangleup %s$ after a translation of ' % (self.one[0]+self.one[1]+self.one[2], self.two[0]+self.two[1]+self.two[2]) + xString + yString + '.'))

		doc.append(NewLine())
		
		self.length = sizingXY( min([self.xMinGraph, self.yMinGraph]), max([self.xMaxGraph, self.yMaxGraph]), 'medium')
		print('LENGTH OF THE GRAPH',self.length)
		print('about to add graph to question')
		with doc.create(Center()):
			with doc.create(TikZ(
					options='x=%gcm, y=%gcm' % (self.length, self.length))): # length, length makes the graph square
				grid(doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)

				graphPolygon(doc, points = [self.x, self.y], annotations = [self.one[0], self.one[1], self.one[2]], color = 'black')

	def addAnswer(self, docAnswer = None):
		#in the future may want to include all of the question text for the solution more specifically
		with docAnswer.create(Center()):
			with docAnswer.create(TikZ(
					options='x=%gcm, y=%gcm' % (self.length, self.length))): # length, length makes the graph square
				grid(docAnswer, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)

				graphPolygon(doc = docAnswer, points = [self.x, self.y], annotations = [self.one[0], self.one[1], self.one[2]], color = 'black')
				graphPolygon(doc = docAnswer, points = [self.tX, self.tY], annotations = [self.two[0], self.two[1], self.two[2]], color = 'red')


#################3D###############
class findConeVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, radiusDrawn = True, heightDrawn = True, diameterDrawn = False, radiusLabeledOnDiagram = True, heightLabeledOnDiagram = True, diameterLabeledOnDiagram = False, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'], pictureDrawn = True, option = ['Given radius', 'Given diameter'], partsGivenInWords = True, slantHeightLabeledOnDiagram = False, typeOfQuestion = ['SA']):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.radiusDrawn = True
			self.heightDrawn = True
			self.diameterDrawn = False

			self.radiusLabeledOnDiagram = True
			self.heightLabeledOnDiagram = True
			self.diameterLabeledOnDiagram = False

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Given radius']
			self.partsGivenInWords = True
			self.slantHeightLabeledOnDiagram = False
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 
			'radiusDrawn':True,
			'heightDrawn':True,
			'diameterDrawn':False,
			'radiusLabeledOnDiagram':True,
			'heightLabeledOnDiagram':True,
			'diameterLabeledOnDiagram':False, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'],
			'pictureDrawn':True,
			'option':['Given radius', 'Given diameter'],
			'partsGivenInWords':True,
			'slantHeightLabeledOnDiagram':False}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.radiusDrawn = radiusDrawn
			self.heightDrawn = heightDrawn
			self.diameterDrawn = diameterDrawn

			self.radiusLabeledOnDiagram = radiusLabeledOnDiagram
			self.heightLabeledOnDiagram = heightLabeledOnDiagram
			self.diameterLabeledOnDiagram = diameterLabeledOnDiagram
			self.slantHeightLabeledOnDiagram = slantHeightLabeledOnDiagram
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.topics = ['3D']
		self.skills = ['Determine the volume of a cone']
		self.ID = self.topics[0] + 'determineVolumeCone'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		height = random.randint(80, 100)
		diameter = random.randint(height-4, height+2) #keeps cone right size
		radius = diameter/2 #radius shoudl be smaller than height??
		while radius == height + 1 or diameter == height:
			print('uh oh line 343')
			diameter = random.randint(height-4, height+2) #keeps cone right size
			radius = diameter/2 #radius shoudl be smaller than height??
		volume = 1/3 * math.pi * radius ** 2 * height
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the traffic cone modeled as a right circular cone below, '
		else:
			introString = 'Given a traffic cone modeled as a right circular cone, '

		if optionChosen == 'Given radius' and self.partsGivenInWords == True:
			givenString = 'the radius is %g cm and the height is %g cm, ' % (radius, height)
		elif optionChosen == 'Given diameter' and self.partsGivenInWords == True:
			givenString = 'the diameter is %g cm and the height is %g cm, ' % (diameter, height)
		else:
			givenString = ""

		if roundingChosen == 'in terms of pi':
			roundingString = 'find the volume in terms of $\pi$.'
			self.answer = '%g$\pi$' % roundedVolume
		else:
			roundingString = 'find the volume, in cubic centimeters, rounded to the nearest %s.' % (roundingChosen)
			self.answer = str(roundedVolume)

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())
		
		if self.pictureDrawn == True:
			with doc.create(Center()):
				cone(options = 'rotate=%d, x=3cm, y=3cm' % (self.wholeFigureRotation), 
					doc = doc, 
					radiusDrawn = self.radiusDrawn, 
					heightDrawn = self.heightDrawn, 
					diameterDrawn = self.diameterDrawn, 
					radiusLabeledOnDiagram = self.radiusLabeledOnDiagram, 
					heightLabeledOnDiagram = self.heightLabeledOnDiagram, 
					diameterLabeledOnDiagram = self.diameterLabeledOnDiagram, 
					radiusValue = radius, 
					diameterValue = diameter, 
					heightValue = height,
					slantHeightLabeledOnDiagram = self.slantHeightLabeledOnDiagram)


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findCylinderVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, radiusDrawn = True, diameterDrawn = False, radiusLabeledOnDiagram = True, heightLabeledOnDiagram = True, diameterLabeledOnDiagram = False, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'], pictureDrawn = True, option = ['Given radius', 'Given diameter'], partsGivenInWords = True):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.radiusDrawn = True
			self.diameterDrawn = False

			self.radiusLabeledOnDiagram = True
			self.heightLabeledOnDiagram = True
			self.diameterLabeledOnDiagram = False

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Given radius']
			self.partsGivenInWords = True
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 
			'radiusDrawn':True,
			'diameterDrawn':False,
			'radiusLabeledOnDiagram':True,
			'heightLabeledOnDiagram':True,
			'diameterLabeledOnDiagram':False, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'],
			'pictureDrawn':True,
			'option':['Given radius', 'Given diameter'],
			'partsGivenInWords':True}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.radiusDrawn = radiusDrawn
			self.diameterDrawn = diameterDrawn

			self.radiusLabeledOnDiagram = radiusLabeledOnDiagram
			self.heightLabeledOnDiagram = heightLabeledOnDiagram
			self.diameterLabeledOnDiagram = diameterLabeledOnDiagram
		  
		self.topics = ['3D']
		self.skills = ['Determine the volume of a cylinder']
		self.ID = self.topics[0] + 'determineVolumeCylinder'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		print('self.rouding is', roundingChosen)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		height = random.randint(6, 12)
		diameter = random.randint(height-4, height+2) #keeps cone right size
		radius = diameter/2 #radius shoudl be smaller than height??
		volume = math.pi * radius ** 2 * height
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the right circular cylinder below, '
		else:
			introString = 'Given a right circular cylinder, '

		if optionChosen == 'Given radius' and self.partsGivenInWords == True:
			givenString = 'the radius is %g and the height is %g, ' % (radius, height)
		elif optionChosen == 'Given diameter' and self.partsGivenInWords == True:
			givenString = 'the diameter is %g and the height is %g, ' % (diameter, height)
		else:
			givenString = ""

		if roundingChosen == 'in terms of pi':
			roundingString = 'find the volume in terms of $\pi$.'
			self.answer = '%g$\pi$' % roundedVolume
		else:
			roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
			self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())
		
		if self.pictureDrawn == True:
			with doc.create(Center()):
				cylinder(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					radiusDrawn = self.radiusDrawn, 
					diameterDrawn = self.diameterDrawn, 
					radiusLabeledOnDiagram = self.radiusLabeledOnDiagram, 
					heightLabeledOnDiagram = self.heightLabeledOnDiagram, 
					diameterLabeledOnDiagram = self.diameterLabeledOnDiagram, 
					radiusValue = radius, 
					diameterValue = diameter, 
					heightValue = height)
		else:
			doc.append(VerticalSpace('2in'))
	   


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findHalfcylinderVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, radiusDrawn = True, diameterDrawn = False, radiusLabeledOnDiagram = True, heightLabeledOnDiagram = True, diameterLabeledOnDiagram = False, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'], pictureDrawn = True, option = ['Given radius', 'Given diameter'], partsGivenInWords = True):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.radiusDrawn = True
			self.diameterDrawn = False

			self.radiusLabeledOnDiagram = True
			self.heightLabeledOnDiagram = True
			self.diameterLabeledOnDiagram = False

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Given radius']
			self.partsGivenInWords = True
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 
			'radiusDrawn':True,
			'diameterDrawn':False,
			'radiusLabeledOnDiagram':True,
			'heightLabeledOnDiagram':True,
			'diameterLabeledOnDiagram':False, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'],
			'pictureDrawn':True,
			'option':['Given radius', 'Given diameter'],
			'partsGivenInWords':True}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.radiusDrawn = radiusDrawn
			self.diameterDrawn = diameterDrawn

			self.radiusLabeledOnDiagram = radiusLabeledOnDiagram
			self.heightLabeledOnDiagram = heightLabeledOnDiagram
			self.diameterLabeledOnDiagram = diameterLabeledOnDiagram
		  
		self.topics = ['3D']
		self.skills = ['Determine the volume of a halfcylinder']
		self.ID = self.topics[0] + 'determineVolumeHalfcylinder'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		height = random.randint(6, 12)
		diameter = random.randint(height-4, height+2) #keeps cone right size
		radius = diameter/2 #radius shoudl be smaller than height??
		volume = math.pi * radius ** 2 * height * 1/2
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the right circular halfcylinder below, '
		else:
			introString = 'Given a right circular halfcylinder, '

		if optionChosen == 'Given radius' and self.partsGivenInWords == True:
			givenString = 'the radius is %g and the height is %g, ' % (radius, height)
		elif optionChosen == 'Given diameter' and self.partsGivenInWords == True:
			givenString = 'the diameter is %g and the height is %g, ' % (diameter, height)
		else:
			givenString = ""

		if roundingChosen == 'in terms of pi':
			roundingString = 'find the volume in terms of $\pi$.'
			self.answer = '%g$\pi$' % roundedVolume
		else:
			roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
			self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())
		
		if self.pictureDrawn == True:
			with doc.create(Center()):
				halfcylinder(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					radiusDrawn = self.radiusDrawn, 
					diameterDrawn = self.diameterDrawn, 
					radiusLabeledOnDiagram = self.radiusLabeledOnDiagram, 
					heightLabeledOnDiagram = self.heightLabeledOnDiagram, 
					diameterLabeledOnDiagram = self.diameterLabeledOnDiagram, 
					radiusValue = radius, 
					diameterValue = diameter, 
					heightValue = height)
		else:
			doc.append(VerticalSpace('2in'))


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findSphereVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, radiusDrawn = True, diameterDrawn = False, radiusLabeledOnDiagram = True, diameterLabeledOnDiagram = False, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'], pictureDrawn = True, option = ['Given radius', 'Given diameter'], partsGivenInWords = True, realLife = False, typeOfQuestion = ['SA']):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.radiusDrawn = True
			self.diameterDrawn = False

			self.radiusLabeledOnDiagram = True
			self.diameterLabeledOnDiagram = False

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Given radius']
			self.partsGivenInWords = True
			self.realLife = False
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 
			'radiusDrawn':True,
			'diameterDrawn':False,
			'radiusLabeledOnDiagram':True,
			'diameterLabeledOnDiagram':False, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'],
			'pictureDrawn':True,
			'option':['Given radius', 'Given diameter'],
			'partsGivenInWords':True,
			'realLife':False}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.radiusDrawn = radiusDrawn
			self.diameterDrawn = diameterDrawn

			self.radiusLabeledOnDiagram = radiusLabeledOnDiagram
			self.diameterLabeledOnDiagram = diameterLabeledOnDiagram

			self.realLife = realLife
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.topics = ['3D']
		self.skills = ['Determine the volume of a sphere']
		self.ID = self.topics[0] + 'determineVolumeSphere'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?


		if self.realLife == True:
			shape = random.choice(['basketball','bowling ball','tennis ball','golf ball', 'soccer ball', 'volley ball', 'beach ball', 'ping pong ball', 'baseball'])
		else:
			shape = 'sphere'

		if shape == 'basketball':
			volume = random.randint(6900,7200)
		elif shape == 'bowling ball':
			volume = random.randint(5000,5300)
		elif shape == 'soccer ball':
			volume = random.randint(5300,5800)
		elif shape == 'volley ball':
			volume = random.randint(4600,5400)
		elif shape == 'beach ball':
			volume = random.randint(14000,15000)
		elif shape == 'tennis ball':
			volume = random.randint(100,200)
		elif shape == 'golf ball':
			volume = random.randint(30,50)
		elif shape == 'ping pong ball':
			volume = random.randint(50,70)
		elif shape == 'baseball':
			volume = random.randint(175,215)
		else:
			volume = random.randint(100,200)

		radius = ( volume / (4 / 3 * math.pi ) ) ** (1/3) #cm
		radius = round(radius,0)
		diameter = radius * 2 #keeps cone right size
		volume = 4/3 * math.pi * radius ** 3
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)


		if self.pictureDrawn == True:
			introString = 'Given the %s below, ' % shape
		else:
			introString = 'Given a %s, ' % shape

		if optionChosen == 'Given radius' and self.partsGivenInWords == True:
			givenString = 'the radius is %g cm, ' % (radius)
		elif optionChosen == 'Given diameter' and self.partsGivenInWords == True:
			givenString = 'the diameter is %g cm, ' % (diameter)
		else:
			givenString = ""

		if roundingChosen == 'in terms of pi':
			roundingString = 'find the volume in terms of $\pi$.'
			self.answer = '%g$\pi$' % roundedVolume
		else:
			roundingString = 'find the volume, in cubic centimeters, rounded to the nearest %s.' % (roundingChosen)
			self.answer = str(roundedVolume)

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())
		
		if self.pictureDrawn == True:
			with doc.create(Center()):
				sphere(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					radiusDrawn = self.radiusDrawn, 
					diameterDrawn = self.diameterDrawn, 
					radiusLabeledOnDiagram = self.radiusLabeledOnDiagram, 
					diameterLabeledOnDiagram = self.diameterLabeledOnDiagram, 
					radiusValue = radius, 
					diameterValue = diameter)
		else:
			doc.append(VerticalSpace('2in'))


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findHemisphereVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, radiusDrawn = True, diameterDrawn = False, radiusLabeledOnDiagram = True, diameterLabeledOnDiagram = False, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'], pictureDrawn = True, option = ['Given radius', 'Given diameter'], partsGivenInWords = True):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.radiusDrawn = True
			self.diameterDrawn = False

			self.radiusLabeledOnDiagram = True
			self.diameterLabeledOnDiagram = False

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Given radius']
			self.partsGivenInWords = True
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 
			'radiusDrawn':True,
			'diameterDrawn':False,
			'radiusLabeledOnDiagram':True,
			'diameterLabeledOnDiagram':False, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth', 'in terms of pi'],
			'pictureDrawn':True,
			'option':['Given radius', 'Given diameter'],
			'partsGivenInWords':True}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.radiusDrawn = radiusDrawn
			self.diameterDrawn = diameterDrawn

			self.radiusLabeledOnDiagram = radiusLabeledOnDiagram
			self.diameterLabeledOnDiagram = diameterLabeledOnDiagram
		  
		self.topics = ['3D']
		self.skills = ['Determine the volume of a hemisphere']
		self.ID = self.topics[0] + 'determineVolumeHemisphere'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		radius = random.randint(2,10)
		diameter = radius * 2 #keeps cone right size
		volume = 2/3 * math.pi * radius ** 3
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the hemisphere below, '
		else:
			introString = 'Given a hemisphere, '

		if optionChosen == 'Given radius' and self.partsGivenInWords == True:
			givenString = 'the radius is %g, ' % (radius)
		elif optionChosen == 'Given diameter' and self.partsGivenInWords == True:
			givenString = 'the diameter is %g, ' % (diameter)
		else:
			givenString = ""

		if roundingChosen == 'in terms of pi':
			roundingString = 'find the volume in terms of $\pi$.'
			self.answer = '%g$\pi$' % roundedVolume
		else:
			roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
			self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())
		
		if self.pictureDrawn == True:
			with doc.create(Center()):
				hemisphere(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					radiusDrawn = self.radiusDrawn, 
					diameterDrawn = self.diameterDrawn, 
					radiusLabeledOnDiagram = self.radiusLabeledOnDiagram, 
					diameterLabeledOnDiagram = self.diameterLabeledOnDiagram, 
					radiusValue = radius, 
					diameterValue = diameter)
		else:
			doc.append(VerticalSpace('2in'))


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findRectangularPrismVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, lengthLabeledOnDiagram = True, heightLabeledOnDiagram = True, widthLabeledOnDiagram = True, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth'], pictureDrawn = True, option = ['Rectangular Prism'], partsGivenInWords = True, baseRotation = 0):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0

			self.heightLabeledOnDiagram = True
			self.widthLabeledOnDiagram = True
			self.lengthLabeledOnDiagram = True

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Rectangular Prism']
			self.partsGivenInWords = True
			self.baseRotation = 0
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 

			'heightLabeledOnDiagram':True,
			'widthLabeledOnDiagram':True,
			'lengthLabeledOnDiagram':True, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth'],
			'pictureDrawn':True,
			'option':['Rectangular Prism'],
			'partsGivenInWords':True,
			'baseRotation':0}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.widthLabeledOnDiagram = widthLabeledOnDiagram
			self.heightLabeledOnDiagram = heightLabeledOnDiagram
			self.lengthLabeledOnDiagram = lengthLabeledOnDiagram
			self.baseRotation = baseRotation

		self.topics = ['3D']
		self.skills = ['Determine the volume of a rectangular prism']
		self.ID = self.topics[0] + 'determineVolumeRectangularPrism'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		length = random.randint(10,20)
		width = random.randint(5,length-1)
		height = random.randint(5,15)
		while length == width and length == height and width == height:
			length = random.randint(5,20)
		shape = 'rectangular prism'

		volume = length * width * height
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the %s below, ' % shape
		else:
			introString = 'Given a %s, ' % shape

		if optionChosen == 'Cube' and self.partsGivenInWords == True:
			givenString = 'the side length is %g,' % (length)
		elif optionChosen == 'Rectangular Prism' and self.partsGivenInWords == True:
			givenString = 'the length is %g, the width is %g, and the height is %g,' % (length, width, height)
		else:
			givenString = ""

		roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
		self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())

		if self.pictureDrawn == True:
			with doc.create(Center()):
				rectangularPrism(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					heightLabeledOnDiagram = self.heightLabeledOnDiagram, 
					widthLabeledOnDiagram = self.widthLabeledOnDiagram,
					lengthLabeledOnDiagram = self.lengthLabeledOnDiagram, 
					heightValue = height,
					widthValue = width,
					lengthValue = length,
					baseRotation = self.baseRotation)
		else:
			doc.append(VerticalSpace('2in'))


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findCubeVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, sideLabeledOnDiagram = True, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth'], pictureDrawn = True, option = ['Cube'], partsGivenInWords = True):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.sideLabeledOnDiagram = True

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Cube']
			self.partsGivenInWords = True
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 

			'sideLabeledOnDiagram':True, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth'],
			'pictureDrawn':True,
			'option':['Cube'],
			'partsGivenInWords':True}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.sideLabeledOnDiagram = sideLabeledOnDiagram

		self.topics = ['3D']
		self.skills = ['Determine the volume of a cube']
		self.ID = self.topics[0] + 'determineVolumeCube'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		length = random.randint(10,20)
		width = length
		height = length
		shape = 'cube'

		volume = length * width * height
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the %s below, ' % shape
		else:
			introString = 'Given a %s, ' % shape

		if self.partsGivenInWords == True:
			givenString = 'the side length is %g, ' % (length)
		else:
			givenString = ""

		roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
		self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())

		if self.pictureDrawn == True:
			with doc.create(Center()):
				cube(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					sideLabeledOnDiagram = self.sideLabeledOnDiagram, 
					sideValue = length)
		else:
			doc.append(VerticalSpace('2in'))


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findRegularPyramidVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, sideLabeledOnDiagram = True, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth'], pictureDrawn = True, option = ['Regular Pyramid'], partsGivenInWords = True, heightLabeledOnDiagram = True, typeOfQuestion = ['MC','SA']):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.sideLabeledOnDiagram = True

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Regular Pyramid']
			self.partsGivenInWords = True
			self.heightLabeledOnDiagram = True
			self.typeOfQuestion = ['MC']
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 

			'sideLabeledOnDiagram':True, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth'],
			'pictureDrawn':True,
			'option':['Regular Pyramid'],
			'partsGivenInWords':True,
			'heightLabeledOnDiagram':True,
			'typeOfQuestion':['MC','SA']}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.sideLabeledOnDiagram = sideLabeledOnDiagram
			self.heightLabeledOnDiagram = heightLabeledOnDiagram
			self.typeOfQuestion = typeOfQuestion
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.topics = ['3D']
		self.skills = ['Determine the volume of a regular pyramid']
		self.ID = self.topics[0] + 'determineVolumeRegularPyramid'
		self.answer = ""
	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		length = random.randint(4,10)
		width = length
		height = round(random.uniform(length-3,length+3),1)
		while length == height:
			height = random.randint(length-3,length+3)
		shape = 'regular square pyramid'

		volume = length * width * height * 1/3
		print(length, width, height, volume)
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the %s below, ' % shape
		else:
			introString = 'Given a %s, ' % shape

		if self.partsGivenInWords == True:
			givenString = 'if the side length of the square base is %g and the height is %g, ' % (length, height)
		else:
			givenString = ""

		roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
		self.answer = str(roundedVolume)

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())

		if self.pictureDrawn == True:
			with doc.create(Center()):
				regularPyramid(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					sideLabeledOnDiagram = self.sideLabeledOnDiagram, 
					sideValue = length,
					heightLabeledOnDiagram = self.heightLabeledOnDiagram,
					heightValue = height)
		else:
			doc.append(VerticalSpace('2in'))

		if self.typeOfQuestionChosen == 'MC':
			choice1 = length * height * 1 / 3
			choice2 = length * length * height
			choice3 = length * height
			choices = [str(self.answer), str(roundGivenString(value = choice1, string = roundingChosen)), str(roundGivenString(value = choice2, string = roundingChosen)), str(roundGivenString(value = choice3, string = roundingChosen))]
			
			newChoices = multipleChoice(choices = choices, doc = doc)
			self.answer = 'Choice %d' % (newChoices.index(str(self.answer))+1)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'%s' % self.answer))
class findRectangularPyramidVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, widthLabeledOnDiagram = True, lengthLabeledOnDiagram = True, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth'], pictureDrawn = True, option = ['Rectangular Pyramid'], partsGivenInWords = True, heightLabeledOnDiagram = True):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.lengthLabeledOnDiagram = True
			self.widthLabeledOnDiagram = True

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Regular Pyramid']
			self.partsGivenInWords = True
			self.heightLabeledOnDiagram = True
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 

			'lengthLabeledOnDiagram':True,
			'widthLabeledOnDiagram':True, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth'],
			'pictureDrawn':True,
			'option':['Rectangular Pyramid'],
			'partsGivenInWords':True,
			'heightLabeledOnDiagram':True}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.widthLabeledOnDiagram = widthLabeledOnDiagram
			self.lengthLabeledOnDiagram = lengthLabeledOnDiagram
			self.heightLabeledOnDiagram = heightLabeledOnDiagram

		self.topics = ['3D']
		self.skills = ['Determine the volume of a rectangular pyramid']
		self.ID = self.topics[0] + 'determineVolumeRectangularPyramid'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		length = random.randint(10,20)
		width = random.choice([x for x in range(10,length)] + [x for x in range(length+1,20)])#so width and length are not same
		height = random.randint(10,20)
		shape = 'rectangular pyramid'

		volume = length * width * height * 1/3
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the %s below, ' % shape
		else:
			introString = 'Given a %s, ' % shape

		if self.partsGivenInWords == True:
			givenString = 'if the length of rectangular base is %g and the width is %g, and the height is %g, ' % (length, width, height)
		else:
			givenString = ""

		roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
		self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())

		if self.pictureDrawn == True:
			with doc.create(Center()):
				rectangularPyramid(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					widthLabeledOnDiagram = self.widthLabeledOnDiagram,
					lengthLabeledOnDiagram = self.lengthLabeledOnDiagram, 
					lengthValue = length,
					widthValue = width,
					heightValue = height,
					heightLabeledOnDiagram = self.heightLabeledOnDiagram)
		else:
			doc.append(VerticalSpace('2in'))


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findTrianglularPyramidVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, widthLabeledOnDiagram = True, lengthLabeledOnDiagram = True, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth'], pictureDrawn = True, option = ['Triangular Pyramid'], partsGivenInWords = True, heightLabeledOnDiagram = True):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0
			
			self.lengthLabeledOnDiagram = True
			self.widthLabeledOnDiagram = True

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Triangular Pyramid']
			self.partsGivenInWords = True
			self.heightLabeledOnDiagram = True
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 

			'lengthLabeledOnDiagram':True,
			'widthLabeledOnDiagram':True, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth'],
			'pictureDrawn':True,
			'option':['Triangular Pyramid'],
			'partsGivenInWords':True,
			'heightLabeledOnDiagram':True}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.widthLabeledOnDiagram = widthLabeledOnDiagram
			self.lengthLabeledOnDiagram = lengthLabeledOnDiagram
			self.heightLabeledOnDiagram = heightLabeledOnDiagram

		self.topics = ['3D']
		self.skills = ['Determine the volume of a triangular pyramid']
		self.ID = self.topics[0] + 'determineVolumeTriangularPyramid'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		length = random.randint(10,20)
		width = random.choice([x for x in range(10,length)] + [x for x in range(length+1,20)])#so width and length are not same
		height = random.randint(10,20)
		shape = 'triangular pyramid'

		volume = length * width * height * 1/3 * 1/2
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the %s below, ' % shape
		else:
			introString = 'Given a %s, ' % shape

		if self.partsGivenInWords == True:
			givenString = 'if the length of the triangular base is %g and the width is %g, and the height of the pyramid is %g, ' % (length, width, height)
		else:
			givenString = ""

		roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
		self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())

		if self.pictureDrawn == True:
			with doc.create(Center()):
				triangularPyramid(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					widthLabeledOnDiagram = self.widthLabeledOnDiagram,
					lengthLabeledOnDiagram = self.lengthLabeledOnDiagram, 
					lengthValue = length,
					widthValue = width,
					heightValue = height,
					heightLabeledOnDiagram = self.heightLabeledOnDiagram)
		else:
			doc.append(VerticalSpace('2in'))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findTriangularPrismVolume(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, wholeFigureRotation = 0, lengthLabeledOnDiagram = True, heightLabeledOnDiagram = True, widthLabeledOnDiagram = True, rounding = ['whole number', 'tenth', 'hundredth', 'thousandth'], pictureDrawn = True, option = ['Triangular Prism'], partsGivenInWords = True, baseRotation = 0):
		#Oblique

		if default == True:
			self.wholeFigureRotation = 0

			self.heightLabeledOnDiagram = True
			self.widthLabeledOnDiagram = True
			self.lengthLabeledOnDiagram = True

			self.rounding = ['whole number']
			self.pictureDrawn = True
			self.option = ['Triangular Prism']
			self.partsGivenInWords = True
			self.baseRotation = 0
		else:
			self.defaultKwargs = {'default':False, 
			'wholeFigureRotation':0, 

			'heightLabeledOnDiagram':True,
			'widthLabeledOnDiagram':True,
			'lengthLabeledOnDiagram':True, 
			'rounding':['whole number', 'tenth', 'hundredth', 'thousandth'],
			'pictureDrawn':True,
			'option':['Triangular Prism'],
			'partsGivenInWords':True,
			'baseRotation':0}

			self.wholeFigureRotation = wholeFigureRotation
			self.rounding = rounding
			self.pictureDrawn = pictureDrawn
			self.option = option
			self.partsGivenInWords = partsGivenInWords

			self.widthLabeledOnDiagram = widthLabeledOnDiagram
			self.heightLabeledOnDiagram = heightLabeledOnDiagram
			self.lengthLabeledOnDiagram = lengthLabeledOnDiagram
			self.baseRotation = baseRotation

		self.topics = ['3D']
		self.skills = ['Determine the volume of a triangular prism']
		self.ID = self.topics[0] + 'determineVolumeTriangularPrism'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		optionChosen = random.choice(self.option)

		#If picture: Given the right circular cone below, if the radius is 4 and the height is 10, what is the volume rounded to the nearest whole number?
		length = random.randint(10,20)
		width = random.randint(5,length-1)
		height = random.randint(5,15)
		while length == width and length == height and width == height:
			length = random.randint(5,20)
		shape = 'triangular prism'

		volume = length * width * height * 1/2
		roundedVolume = roundGivenString(string = roundingChosen, value = volume)

		if self.pictureDrawn == True:
			introString = 'Given the %s below, ' % shape
		else:
			introString = 'Given a %s, ' % shape


		if optionChosen == 'Triangular Prism' and self.partsGivenInWords == True:
			givenString = 'the length and width of the triangle are %g and %g, and the height of the prism is %g,' % (length, width, height)
		else:
			givenString = ""

		roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
		self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		doc.append(NewLine())

		if self.pictureDrawn == True:
			with doc.create(Center()):
				triangularPrism(options = 'rotate=%d, x=2.5cm, y=2.5cm' % (self.wholeFigureRotation), 
					doc = doc, 
					heightLabeledOnDiagram = self.heightLabeledOnDiagram, 
					widthLabeledOnDiagram = self.widthLabeledOnDiagram,
					lengthLabeledOnDiagram = self.lengthLabeledOnDiagram, 
					heightValue = height,
					widthValue = width,
					lengthValue = length,
					baseRotation = self.baseRotation)
		else:
			doc.append(VerticalSpace('2in'))


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class findCylinderHemisphereVolume():
	def __init__(self, rotate = 0, radiusDrawn = False, diameterDrawn = True, radiusLabeledOnDiagram = False, diameterLabeledOnDiagram = True, heightLabeledOnDiagram = True, rounding = ['tenth', 'whole number', 'hundredth', 'thousandth'], typeOfQuestion = ['SA']):
		self.rotate = rotate
		self.radiusDrawn = radiusDrawn
		self.diameterDrawn = diameterDrawn
		self.radiusLabeledOnDiagram = radiusLabeledOnDiagram
		self.diameterLabeledOnDiagram = diameterLabeledOnDiagram
		self.heightLabeledOnDiagram = heightLabeledOnDiagram
		self.rounding = rounding
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		#Question:
		radius = random.randint(2,20)
		diameter = radius * 2
		height = 3*radius + 1
		volume = math.pi * radius ** 2 * height
		self.answer = str(roundGivenString(value = volume, string = roundingChosen))

		questionString = r'A storage tank is in the shape of a cylinder with a hemisphere on top. The highest point on the inside of the storage tank is %d meters above the floor of the storage tank, and the diameter inside the cylinder is %d meters. Determine and state, to the \textit{nearest cubic meter}, the total volume inside the storage tank.' % (height, diameter)
		
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		with doc.create(Center()):
			cylinderHemisphere(options = 'rotate=%d, scale=4' % self.rotate, doc = doc, radiusDrawn = self.radiusDrawn, diameterDrawn = self.diameterDrawn, radiusLabeledOnDiagram = self.radiusLabeledOnDiagram, heightLabeledOnDiagram = self.heightLabeledOnDiagram, diameterLabeledOnDiagram = self.diameterLabeledOnDiagram, radiusValue = radius, diameterValue = diameter, heightValue = height)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(self.answer)
class findSubtractingVolume():
	def __init__(self, shape = ['cone','cylinder'], typeOfQuestion = ['MC']):
		nothing = 2
		self.shape = shape
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
	def addQuestion(self, doc = None):
		shapeChosen = random.choice(self.shape)
		edgeLength = random.randint(2,20)
		diameter = edgeLength
		altitude = diameter

		questionString = 'A company is creating an object from a wooden cube with an edge length of %d cm. A %s with a diameter of %d cm and an altitude of %d cm will be cut out of the cube. Which expression represents the volume of the remaining wood?' % (edgeLength, shapeChosen, diameter, altitude)
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		if shapeChosen == 'cone':
			self.answer = r'$(%d)^{3}-\frac{1}{3}\pi(%g)^{2}({%d})$' % (edgeLength, diameter/2, altitude)
			choice2 = r'$(%d)^{3}-\pi(%g)^{2}({%d})$' % (edgeLength, diameter/2, altitude)
		elif shapeChosen == 'cylinder':
			self.answer = r'$(%d)^{3}-\pi(%g)^{2}({%d})$' % (edgeLength, diameter/2, altitude)
			choice2 = r'$(%d)^{3}-\frac{1}{3}\pi(%g)^{2}({%d})$' % (edgeLength, diameter/2, altitude)

		choice1 = r'$(%d)^{3}-\frac{1}{3}\pi(%g)^{2}({%d})$' % (edgeLength, diameter, altitude)
		choice3 = r'$(%d)^{3}-\pi(%g)^{2}({%d})$' % (edgeLength, diameter, altitude)

		choices = [self.answer, choice1, choice2, choice3]
		newChoices = multipleChoice(choices = choices, doc = doc)
		self.answer = 'Choice %d' % (newChoices.index(self.answer)+1)
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class rotateAroundVolume():
	def __init__(self, shapeToRotateAround = ['rectangle', 'square', 'circle', 'semicircle', 'quartercircle', 'right triangle'], rounding = ['tenth', 'whole number', 'hundredth', 'in terms of pi', 'thousandth'], pictureDrawn = True, rotate = 0, typeOfQuestion = ['SA']):
		self.shapeToRotateAround = shapeToRotateAround
		self.rounding = rounding
		self.pictureDrawn = pictureDrawn
		self.rotate = rotate
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		#shape to rotate around, then random between side to rotate
		#rounding
		#picture
	def addQuestion(self, doc = None):
		shapeChosen = random.choice(self.shapeToRotateAround)
		roundingChosen = random.choice(self.rounding)
		#words: Given the diagram below, determine the volume to the nearest ROUNDING when SHAPE is rotated continuously around SIDE.
		if self.pictureDrawn == False:
			questionString = 'Given a %s, determine the volume to the nearest %s after being rotated continuously around ' % (shapeChosen, roundingChosen)
		else:
			questionString = 'Given the %s below, determine the volume to the nearest %s after being continuously rotated around ' % (shapeChosen, roundingChosen)

		if shapeChosen == 'rectangle':
			partToRotate = random.choice(['length','width'])
			lengthValue = random.randint(10,20)
			widthValue = random.randint(9,lengthValue-1)
			
			if partToRotate == 'length':
				volume = roundGivenString(string = roundingChosen, value = (math.pi * widthValue**2 * lengthValue))
				if self.pictureDrawn == False:
					questionString += 'its length if the length is %d and the width is %d.' % (lengthValue, widthValue)
					doc.append(NoEscape(questionString))
				else:
					questionString += 'its length.'
					doc.append(NoEscape(questionString))
					doc.append(NewLine())
					with doc.create(Center()):
						rectangle(doc = doc, lengthValue = lengthValue, widthValue = widthValue, rotate = self.rotate)              
			else:
				volume = roundGivenString(string = roundingChosen, value = (math.pi * lengthValue**2 * widthValue))
				if self.pictureDrawn == False:
					questionString += 'its width if the length is %d and the width is %d.' % (lengthValue, widthValue)
					doc.append(NoEscape(questionString))
				else:
					questionString += 'its width.'
					doc.append(NoEscape(questionString))
					doc.append(NewLine())
					with doc.create(Center()):
						rectangle(doc = doc, lengthValue = lengthValue, widthValue = widthValue, rotate = self.rotate)
		elif shapeChosen == 'square':
			lengthValue = random.randint(10,20)
			widthValue = lengthValue
			
			volume = roundGivenString(string = roundingChosen, value = (math.pi * widthValue**2 * lengthValue))
			if self.pictureDrawn == False:
				questionString += 'its side that measures %d.' % (widthValue)
				doc.append(NoEscape(questionString))
			else:
				questionString += 'its side.'
				doc.append(NoEscape(questionString))
				doc.append(NewLine())
				with doc.create(Center()):
					rectangle(doc = doc, lengthValue = lengthValue, widthValue = widthValue, rotate = self.rotate)                              
		elif shapeChosen == 'right triangle':
			partToRotate = random.choice(['length','width'])
			#length is base, width is height
			lengthValue = random.randint(10,20)
			widthValue = random.randint(9,lengthValue)
			
			if partToRotate == 'length':
				volume = roundGivenString(string = roundingChosen, value = (1/3 * math.pi * widthValue**2 * lengthValue))
				if self.pictureDrawn == False:
					questionString += 'its length if the length is %d and the width is %d.' % (lengthValue, widthValue)
					doc.append(NoEscape(questionString))
				else:
					questionString += 'its length if the length is %d and the width is %d.' % (lengthValue, widthValue)
					doc.append(NoEscape(questionString))
					doc.append(NewLine())
					with doc.create(Center()):
						rightTriangle(doc = doc, lengthValue = lengthValue, widthValue = widthValue, rotate = self.rotate)              
			else:
				volume = roundGivenString(string = roundingChosen, value = (1/3 * math.pi * lengthValue**2 * widthValue))
				if self.pictureDrawn == False:
					questionString += 'its width if the length is %d and the width is %d.' % (lengthValue, widthValue)
					doc.append(NoEscape(questionString))
				else:
					questionString += 'its width if the length is %d and the width is %d.' % (lengthValue, widthValue)
					doc.append(NoEscape(questionString))
					doc.append(NewLine())
					with doc.create(Center()):
						rightTriangle(doc = doc, lengthValue = lengthValue, widthValue = widthValue, rotate = self.rotate)
		elif shapeChosen == 'circle' or shapeChosen == 'semicircle' or shapeChosen == 'quartercircle':
			radiusValue = random.randint(2,30)
			diameterValue = 2 * radiusValue

			if shapeChosen == 'circle':
				volume = roundGivenString(string = roundingChosen, value = (4/3 * math.pi * radiusValue ** 3))
				if self.pictureDrawn == False:
					questionString += 'its diameter that measures %d.' % (diameterValue)
					doc.append(NoEscape(questionString))
				else:
					questionString += 'its diameter that measures %d.' % (diameterValue)
					doc.append(NoEscape(questionString))
					doc.append(NewLine())
					with doc.create(Center()):
						with doc.create(TikZ(options='rotate=%d' % self.rotate)):
							diameterScaled = 4
							com('draw (0,0) circle (%dcm)' % (diameterScaled/2), doc = doc)
							#label whole side
							com('draw[|-|] (%g,0) -- (%g,0) node [midway, below] {%s}' % (-diameterScaled/2, diameterScaled/2, diameterValue), doc = doc)
			elif shapeChosen == 'semicircle':
				volume = roundGivenString(string = roundingChosen, value = (4/3 * math.pi * radiusValue ** 3))
				if self.pictureDrawn == False:
					questionString += 'its diameter that measures %d.' % (diameterValue)
					doc.append(NoEscape(questionString))
				else:
					questionString += 'its diameter that measures %d.' % (diameterValue)
					doc.append(NoEscape(questionString))
					doc.append(NewLine())
					with doc.create(Center()):
						with doc.create(TikZ(options='rotate=%d' % self.rotate)):
							diameterScaled = 4
							radiusScaled = 2
							com('draw (%d,0) arc (0:180:%d)' % (radiusScaled, radiusScaled), doc = doc)
							com('draw (%d,0) -- (%d,0)' % (-radiusScaled, radiusScaled), doc = doc)
							com('draw[|-|] (%g,-.2) -- (%g,-.2) node [midway, below] {%s}' % (-radiusScaled, radiusScaled, diameterValue), doc = doc)

			elif shapeChosen == 'quartercircle':
				volume = roundGivenString(string = roundingChosen, value = (1/2 * 4/3 * math.pi * radiusValue ** 3))
				if self.pictureDrawn == False:
					questionString += 'its radius that measures %d.' % (radiusValue)
					doc.append(NoEscape(questionString))
				else:
					questionString += 'its radius that measures %d.' % (radiusValue)
					doc.append(NoEscape(questionString))
					doc.append(NewLine())
					with doc.create(Center()):
						with doc.create(TikZ(options='rotate=%d' % self.rotate)):
							diameterScaled = 4
							radiusScaled = 2
							com('draw (%d,0) arc (0:90:%d)' % (radiusScaled, radiusScaled), doc = doc)
							com('draw (0,%d) -- (0,0) -- (%d,0)' % (radiusScaled, radiusScaled), doc = doc)
							com('draw[|-|] (0,-.2) -- (%g,-.2) node [midway, below] {%s}' % (radiusScaled, radiusValue), doc = doc)
		
		self.answer = str(roundGivenString(string = roundingChosen, value = volume))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class crossSection():
	def __init__(self, shape = ['cylinder', 'sphere', 'rectangular prism', 'cube', 'trianglular prism', 'regular square pyramid', 'triangular pyramid', 'cone'], pictureDrawn = True, crossSection = ['parallel', 'perpendicular'], testVariation = [], typeOfQuestion = ['MC','SA']):
		self.shape = shape
		self.pictureDrawn = pictureDrawn
		self.crossSection = crossSection
		self.testVariation = testVariation
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)

	def addQuestion(self, doc = None):
		shapeChosen = random.choice(self.shape)
		crossSectionChosen = random.choice(self.crossSection)
		if self.testVariation != []:
			testVariationChosen = random.choice(self.testVariation)
		else:
			testVariationChosen = []
		

			if testVariationChosen == '1':
				#Which of the following is italic(not) a cross section of a regular pyramid with a square base?
				if self.typeOfQuestionChosen == 'MC':
					questionString = r'Which of the following is \textit{not} a cross section of a %s?' % shapeChosen
					self.answer = 'circle'
					choices = ['triangle', 'square', 'trapezoid', self.answer]
					doc.append(NoEscape(questionString))
					#doc.append(NewLine())
					newChoices = multipleChoice(choices = choices, doc = doc)
					self.answer = r'Choice %d: circle' % (newChoices.index(self.answer)+1)
				else:
					questionString = 'Name all the cross section of a %s.' % shapeChosen
		#Given a SHAPE, after a slicing parallel to the base, what cross section is formed
		if self.pictureDrawn == False and testVariationChosen == []:
			questionString = 'Given a %s, after slicing %s to the base, what cross section is formed?' % (shapeChosen, crossSectionChosen)
		elif self.pictureDrawn == True and testVariationChosen == []:
			questionString = 'Given the figure below, after slicing %s to the base, what cross section is formed?' % crossSectionChosen
			doc.append(NoEscape(questionString))
			if shapeChosen == 'cylinder':
				if crossSectionChosen == 'parallel':
					self.answer = 'circle'
				elif crossSectionChosen == 'perpendicular':
					self.answer = 'rectangle'
				if self.pictureDrawn == True:
					with doc.create(Center()):
						cylinder(doc = doc, heightLabeledOnDiagram = False, radiusLabeledOnDiagram = False)
			elif shapeChosen == 'sphere':
				self.answer = 'circle'
				if self.pictureDrawn == True:
					with doc.create(Center()):
						sphere(doc = doc, radiusLabeledOnDiagram = False)
			elif shapeChosen == 'rectangular prism':
				self.answer = 'rectangle'
				if self.pictureDrawn == True:
					with doc.create(Center()):
						rectangularPrism(doc = doc, heightLabeledOnDiagram = False, widthLabeledOnDiagram = False, lengthLabeledOnDiagram = False)
			elif shapeChosen == 'cube':
				self.answer = 'square'
				if self.pictureDrawn == True:
					with doc.create(Center()):
						cube(doc = doc, sideLabeledOnDiagram = False)
			elif shapeChosen == 'triangular prism':
				if crossSectionChosen == 'parallel':
					self.answer = 'triangle'
				else:
					self.answer = 'rectangle'
				if self.pictureDrawn == True:
					with doc.create(Center()):
						triangularPrism(doc = doc, lengthLabeledOnDiagram = False, heightLabeledOnDiagram = False, widthLabeledOnDiagram = False)
			elif shapeChosen == 'regular square pyramid':
				if crossSectionChosen == 'parallel':
					self.answer = 'square'
				elif crossSectionChosen == 'perpendicular':
					self.answer = 'triangle'
				if self.pictureDrawn == True:
					with doc.create(Center()):
						regularPyramid(doc = doc, sideLabeledOnDiagram = False, heightLabeledOnDiagram = False)
			elif shapeChosen == 'triangular pyramid':
				self.answer = 'triangle'
				if self.pictureDrawn == True:
					with doc.create(Center()):
						triangularPyramid(doc = doc, lengthLabeledOnDiagram = False, heightLabeledOnDiagram = False, widthLabeledOnDiagram = False)
			elif shapeChosen == 'cone':
				if crossSectionChosen == 'parallel':
					self.answer = 'circle'
				elif crossSectionChosen == 'perpendicular':
					self.answer = 'triangle'
				if self.pictureDrawn == True:
					with doc.create(Center()):
						cone(doc = doc, radiusLabeledOnDiagram = False, heightLabeledOnDiagram = False)
	def addAnswer(self, docAnswer = None):
		docAnswer.append(self.answer)
class rotateAroundFindObject():
	def __init__(self, shape = ['right triangle', 'circle'], typeOfQuestion = ['SA','MC'], rotate = 0):
		self.shape = shape
		self.typeOfQuestion = typeOfQuestion
		self.rotate = rotate
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)

	def addQuestion(self, doc = None):
		triangleName, triangleName1, triangleName2 = annotatePolygon(prime = False, vertices = 3)
		shapeChosen = random.choice(self.shape)

		#Question: Which object is formed when right triangle RST shown below is rotated aroung leg RS?
		if shapeChosen == 'right triangle':
			questionString = r'Which object is formed when %s %s shown below is rotated around leg $\overline{%s}$?' % (shapeChosen, triangleName[0]+triangleName[1]+triangleName[2], triangleName[2]+triangleName[1])
		elif shapeChosen == 'circle':
			questionString = r'Which object is formed when %s %s shown below is rotated around diameter $\overline{%s}$?' % (shapeChosen, triangleName[0], triangleName[1]+triangleName[2])

		doc.append(NoEscape(questionString))
		if shapeChosen == 'right triangle':
			self.answer = 'cone'
			#eventually full answerkey which choice is which etc???
			doc.append(NewLine())
			rightTriangle(doc = doc, verticesLabels = [triangleName[0],triangleName[1],triangleName[2]], rotate = self.rotate)
		elif shapeChosen == 'circle':
			self.answer = 'sphere'
			doc.append(NewLine())
			with doc.create(TikZ(options='')):
				com('draw[black,fill=black] (0,0) circle (.5ex)', doc = doc)
				com('draw (0,0) circle (2)', doc = doc) #circle
				com('draw (-2,0) node [left] {%s} -- (0,0) node [above] {%s} -- (2,0) node [right] {%s}' % (triangleName[1], triangleName[0],triangleName[2]), doc = doc) #diameter
		if shapeChosen == 'right triangle':	
			if self.typeOfQuestionChosen == 'MC':
				choices = ['a pyramid with a square base', 'an isosceles triangle', 'a right triangle', self.answer]
				#doc.append(NewLine())
				newChoices = multipleChoice(choices = choices, doc = doc)
				self.answer = 'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)
		elif shapeChosen == 'circle':
			if self.typeOfQuestionChosen == 'MC':
				choices = ['circle', 'cone', 'cylinder', self.answer]
				#doc.append(NewLine())
				newChoices = multipleChoice(choices = choices, doc = doc)
				self.answer = 'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)


	def addAnswer(self, docAnswer = None):
		docAnswer.append(self.answer)

####Reversing Formulas ---- add in real life scenarios as an option etc
class reverseFormula():
	def __init__(self, option = ['Cylinder', 'Halfcylinder', 'Sphere', 'Hemisphere', 'Rectangular Prism', 'Triangular Prism', 'Cube', 'Regular Square Pyramid', 'Rectangular Pyramid', 'Triangular Pyramid', 'Cone' ], pictureDrawn = False, shapeInWords = True, partToFind = ['easy', 'medium', 'hard'], rounding = ['whole number', 'tenth', 'hundredth', 'thousandth'], typeOfQuestion = ['MC','SA']):
		
		#etc web app stuff
		self.option = option
		self.shapeInWords = shapeInWords
		self.partToFind = partToFind
		self.pictureDrawn = pictureDrawn
		self.rounding = rounding
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)


	def addQuestion(self, doc = None):
		optionChosen = random.choice(self.option)
		partToFindChosen = random.choice(self.partToFind)
		roundingChosen = random.choice(self.rounding)
		
		#Given a SHAPE with a volume of X, if the _____ is _____, determine the size of the ______.
		shape = optionChosen
		volume = random.randint(100,200)

		#Given a SHAPE with a volume of X, if the _____ is _____, determine the size of the ______.
		if self.pictureDrawn == False:
			questionString = r'Given a %s with a volume of %g, ' % (shape.lower(), volume)
		else:
			questionString = r'Given the %s below that has a volume of %d, ' % (shape.lower(), volume)
		
		if optionChosen == 'Cylinder':
			if partToFindChosen == 'easy':
				radius = random.randint(2,10)
				height = volume / (math.pi * radius ** 2)
				print(roundingChosen)
				print(roundGivenString(value = height, string = roundingChosen))
				self.answer = r'%g' % roundGivenString(value=height, string=roundingChosen)
				questionString += r'if the radius is %g, determine the size of the height to the nearest %s.' % (radius, roundingChosen)
			elif partToFindChosen == 'medium':
				height = random.randint(2,10)
				radius = ( volume / (math.pi * height) ) ** (1/2)
				self.answer = r'%g' % roundGivenString(value = radius, string = roundingChosen)
				questionString += r'if the height is %g, determine the size of the radius to the nearest %s.' % (height, roundingChosen)
			elif partToFindChosen == 'hard':
				height = random.randint(2,10)
				radius = ( volume / (math.pi * height) ) ** (1/2)
				diameter = radius * 2
				self.answer = r'%g' % roundGivenString(value = diameter, string = roundingChosen)
				questionString += r'if the height is %g, determine the size of the diameter to the nearest %s.' % (height, roundingChosen)
		elif optionChosen == 'Halfcylinder':
			if partToFindChosen == 'easy':
				radius = random.randint(2,10)
				height = (2*volume) / (math.pi * radius ** 2)
				self.answer = r'%g' % roundGivenString(value = height, string = roundingChosen)
				questionString += r'if the radius is %g, determine the size of the height to the nearest %s.' % (radius, roundingChosen)
			elif partToFindChosen == 'medium':
				height = random.randint(2,10)
				radius = ( (2*volume) / (math.pi * height) ) ** (1/2)
				self.answer = r'%g' % roundGivenString(value = radius, string = roundingChosen)
				questionString += r'if the height is %g, determine the size of the radius to the nearest %s.' % (height, roundingChosen)
			elif partToFindChosen == 'hard':
				height = random.randint(2,10)
				radius = ( (2*volume) / (math.pi * height) ) ** (1/2)
				diameter = radius * 2
				self.answer = r'%g' % roundGivenString(value = diameter, string = roundingChosen)
				questionString += r'if the height is %g, determine the size of the diameter to the nearest %s.' % (height, roundingChosen)
		elif optionChosen == 'Cone':
			if partToFindChosen == 'easy':
				radius = random.randint(2,10)
				height = (3*volume) / (math.pi * radius ** 2)
				self.answer = r'%g' % roundGivenString(value = height, string = roundingChosen)
				questionString += r'if the radius is %g, determine the size of the height to the nearest %s.' % (radius, roundingChosen)
			elif partToFindChosen == 'medium':
				height = random.randint(2,10)
				radius = ( (3*volume) / (math.pi * height) ) ** (1/2)
				self.answer = r'%g' % roundGivenString(value = radius, string = roundingChosen)
				questionString += r'if the height is %g, what is the size of the radius to the nearest %s?' % (height, roundingChosen)
			elif partToFindChosen == 'hard':
				height = random.randint(2,10)
				radius = ( (3*volume) / (math.pi * height) ) ** (1/2)
				diameter = radius * 2
				self.answer = r'%g' % roundGivenString(value = diameter, string = roundingChosen)
				questionString += r'if the height is %g, determine the size of the diameter to the nearest %s.' % (height, roundingChosen)
		elif optionChosen == 'Sphere':
			if partToFindChosen == 'easy':
				radius = ( (3/4 * volume) / (math.pi) ) ** (1/3)
				self.answer = r'%g' % roundGivenString(value = radius, string =roundingChosen)
				questionString += r'determine the size of the radius to the nearest %s.' % (roundingChosen)
			elif partToFindChosen == 'medium':
				radius = ( (3/4 * volume) / (math.pi) ) ** (1/3)
				diameter = 2 * radius
				self.answer = r'%g' % roundGivenString(value = diameter, string = roundingChosen)
				questionString += r'determine the size of the diameter to the nearest %s.' % (roundingChosen)
			elif partToFindChosen == 'hard':
				radius = ( (3/4 * volume) / (math.pi) ) ** (1/3)
				diameter = 2 * radius
				self.answer = r'%g' % roundGivenString(value = diameter, string = roundingChosen)
				questionString += r'determine the size of the diameter to the nearest %s.' % (roundingChosen)
		elif optionChosen == 'Hemisphere':
			if partToFindChosen == 'easy':
				radius = ( (2*3/4 * volume) / (math.pi) ) ** (1/3)
				self.answer = r'%g' % roundGivenString(value = radius, string = roundingChosen)
				questionString += r'determine the size of the radius to the nearest %s.' % (roundingChosen)
			elif partToFindChosen == 'medium':
				radius = ( (2*3/4 * volume) / (math.pi) ) ** (1/3)
				diameter = 2 * radius
				self.answer = r'%g' % roundGivenString(value = diameter, string = roundingChosen)
				questionString += r'determine the size of the diameter to the nearest %s.' % (roundingChosen)
			elif partToFindChosen == 'hard':
				radius = ( (2*3/4 * volume) / (math.pi) ) ** (1/3)
				diameter = 2 * radius
				self.answer = r'%g' % roundGivenString(value = diameter, string = roundingChosen)
				questionString += r'determine the size of the diameter to the nearest %s.' % (roundingChosen)
		elif optionChosen == 'Rectangular Prism':
			if partToFindChosen == 'easy':
				length = random.randint(1,10)
				width = random.randint(1,10)
				height = volume / (length * width)
				self.answer = r'%g' % roundGivenString(value = height, string = roundingChosen)
				questionString += r'if the length is %g and width is %g, determine the size of the height to the nearest %s.' % (length, width, roundingChosen)
			elif partToFindChosen == 'medium':
				height = random.randint(1,10)
				width = random.randint(1,10)
				length = volume / (height * width)
				self.answer = r'%g' % roundGivenString(value = length, string = roundingChosen)
				questionString += r'if the height is %g and width is %g, determine the size of the length to the nearest %s.' % (height, width, roundingChosen)
			elif partToFindChosen == 'hard':
				height = random.randint(1,10)
				width = random.randint(1,10)
				length = volume / (height * width)
				self.answer = r'%g' % roundGivenString(value = length, string = roundingChosen)
				questionString += r'if the height is %g and width is %g, determine the size of the length to the nearest %s.' % (height, width, roundingChosen)
		elif optionChosen == 'Cube':
			if partToFindChosen == 'easy':
				sideLength = (volume) ** (1/3)
				self.answer = r'%g' % roundGivenString(value = sideLength, string = roundingChosen)
				questionString += r'determine the size of the side length of the cube to the nearest %s.' % (roundingChosen)
			elif partToFindChosen == 'medium':
				sideLength = (volume) ** (1/3)
				self.answer = r'%g' % roundGivenString(value = sideLength, string = roundingChosen)
				questionString += r'determine the size of the side length of the cube to the nearest %s.' % (roundingChosen)
			elif partToFindChosen == 'hard':
				sideLength = (volume) ** (1/3)
				self.answer = r'%g' % roundGivenString(value = sideLength, string = roundingChosen)
				questionString += r'determine the size of the side length of the cube to the nearest %s.' % (roundingChosen)
		elif optionChosen == 'Regular Square Pyramid':
			if partToFindChosen == 'easy': #find height
				sideLength = random.randint(1,8)
				height = (3*volume / (sideLength * sideLength))
				self.answer = r'%g' % roundGivenString(value = height, string = roundingChosen)
				questionString += r'determine the size of the height of the regular square pyramid to the nearest %s.' % (roundingChosen)
			elif partToFindChosen == 'medium' or partToFindChosen == 'hard': #find side length
				height = random.randint(4,15)
				sideLength = (3*volume / height) ** (1/2)
				self.answer = r'%g' % roundGivenString(value = sideLength, string = roundingChosen)
				questionString += r'determine the size of the side length of the regular square pyramid to the nearest %s.' % (roundingChosen)
				choice1 = str(roundGivenString(value = (3*volume / height), string = roundingChosen))
				choice2 = str(roundGivenString(value = (volume / height) ** (1/2) , string = roundingChosen))
				choice3 = str(roundGivenString(value = volume * 3, string = roundingChosen))#not square rooting
				choices = [self.answer, choice1, choice2, choice3]
		elif optionChosen == 'Rectangular Pyramid':
			if partToFindChosen == 'easy':
				length = random.randint(1,10)
				width = random.randint(1,10)
				height = (3*volume) / (length * width)
				self.answer = r'%g' % roundGivenString(value = height, string = roundingChosen)
				questionString += r'if the length is %g and width is %g, determine the size of the height to the nearest %s.' % (length, width, roundingChosen)
			elif partToFindChosen == 'medium':
				height = random.randint(1,10)
				width = random.randint(1,10)
				length = (3*volume) / (height * width)
				self.answer = r'%g' % roundGivenString(value=length, string = roundingChosen)
				questionString += r'if the height is %g and width is %g, determine the size of the length to the nearest %s.' % (height, width, roundingChosen)
			elif partToFindChosen == 'hard':
				height = random.randint(1,10)
				width = random.randint(1,10)
				length = (3*volume) / (height * width)
				self.answer = r'%g' % roundGivenString(value=length, string = roundingChosen)
				questionString += r'if the height is %g and width is %g, determine the size of the length to the nearest %s.' % (height, width, roundingChosen)
		elif optionChosen == 'Triangular Prism':
			if partToFindChosen == 'easy':
				length = random.randint(1,10)
				width = random.randint(1,10)
				height = (2*volume) / (length * width)
				self.answer = r'%g' % roundGivenString(value=height, string=roundingChosen)
				questionString += r'if the base of the prism has a height of %g and a length of %g, determine the height of the prism rounded to the nearest %s.' % (length, width, roundingChosen)
			elif partToFindChosen == 'medium':
				height = random.randint(1,10)
				width = random.randint(1,10)
				length = (2*volume) / (height * width)
				self.answer = r'%g' % roundGivenString(value=length, string=roundingChosen)
				questionString += r'if the base of the prism has a height of %g and prism has a height of %g, determine the length of the base of the prism rounded to the nearest %s.' % (width, height, roundingChosen)
			elif partToFindChosen == 'hard':
				height = random.randint(1,10)
				width = random.randint(1,10)
				length = (2*volume) / (height * width)
				self.answer = r'%g' % roundGivenString(value=length, string=roundingChosen)
				questionString += r'if the base of the prism has a height of %g and prism has a height of %g, determine the length of the base of the prism rounded to the nearest %s.' % (width, height, roundingChosen)
		elif optionChosen == 'Triangular Pyramid':
			if partToFindChosen == 'easy':
				length = random.randint(1,10)
				width = random.randint(1,10)
				height = (3*2*volume) / (length * width)
				self.answer = r'%g' % roundGivenString(value=height, string=roundingChosen)
				questionString += r'if the base of the prism has a height of %g and a length of %g, determine the height of the prism rounded to the nearest %s.' % (length, width, roundingChosen)
			elif partToFindChosen == 'medium':
				height = random.randint(1,10)
				width = random.randint(1,10)
				length = (3*2*volume) / (height * width)
				self.answer = r'%g' % roundGivenString(value=length, string=roundingChosen)
				questionString += r'if the base of the prism has a height of %g and prism has a height of %g, determine the length of the base of the prism rounded to the nearest %s.' % (width, height, roundingChosen)
			elif partToFindChosen == 'hard':
				height = random.randint(1,10)
				width = random.randint(1,10)
				length = (3*2*volume) / (height * width)
				self.answer = r'%g' % roundGivenString(value=length, string=roundingChosen)
				questionString += r'if the base of the prism has a height of %g and prism has a height of %g, determine the length of the base of the prism rounded to the nearest %s.' % (width, height, roundingChosen)
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		print(self.answer)

		if self.pictureDrawn == True:
			if optionChosen == 'Cylinder' or optionChosen == 'Halfcylinder' or optionChosen == 'Cone':
				draw3DShape(optionChosen, doc = doc, radiusDrawn = False, radiusLabeledOnDiagram = False, heightLabeledOnDiagram = False)
			elif optionChosen == 'Sphere' or optionChosen == 'Hemisphere':
				draw3DShape(optionChosen, doc = doc, radiusDrawn = False, radiusLabeledOnDiagram = False)
			elif optionChosen == 'Rectangular Prism' or optionChosen == 'Triangular Prism':
				draw3DShape(optionChosen, doc = doc, lengthLabeledOnDiagram = False, widthLabeledOnDiagram = False, heightLabeledOnDiagram = False)
			elif optionChosen == 'Cube':
				draw3DShape(optionChosen, doc = doc, sideLabeledOnDiagram = False)
			elif optionChosen == 'Regular Square Pyramid':
				draw3DShape(optionChosen, doc = doc, sideLabeledOnDiagram = False, heightLabeledOnDiagram = True, sideValue = sideLength, heightValue = height)
				if self.typeOfQuestionChosen == 'MC':
					print('dick')
					doc.append(NewLine())
					newChoices = multipleChoice(choices = choices, doc = doc)
					self.answer = 'Choice %d' % (newChoices.index(self.answer)+1)
			elif optionChosen == 'Rectangular Pyramid':
				draw3DShape(optionChosen, doc = doc, lengthLabeledOnDiagram = False, widthLabeledOnDiagram = False, heightLabeledOnDiagram = False)
			elif optionChosen == 'Triangular Pyramid':
				draw3DShape(optionChosen, doc = doc, lengthLabeledOnDiagram = False, widthLabeledOnDiagram = False, heightLabeledOnDiagram = False)
		else:
			if optionChosen == 'Regular Square Pyramid':
				if self.typeOfQuestionChosen == 'MC':
					print('dick')
					doc.append(NewLine())
					newChoices = multipleChoice(choices = choices, doc = doc)
					self.answer = 'Choice %d' % (newChoices.index(self.answer)+1)
			if optionChosen == 'Cone':
				if self.typeOfQuestionChosen == 'MC':
					choice1 = ( (volume) / (math.pi * height) ) ** (1/2)
					choice2 = ( (3*volume) / (height) ) ** (1/2)
					choice3 = ( (volume) / (math.pi * height) )
					choices = [self.answer, str(roundGivenString(value=choice1,string=roundingChosen)), str(roundGivenString(value=choice2, string = roundingChosen)), str(roundGivenString(value=choice3,string=roundingChosen))]
					newChoices = multipleChoice(choices = choices, doc = doc)
					self.answer = 'Choice %d' % (newChoices.index(self.answer)+1)
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class reverseFormulaCompare():
	def __init__(self, shape1 = ['sphere'], shape2 = ['sphere'], partToCompare = ['diameter'], pictureDrawn = False, realLife = True, typeOfQuestion = ['MC']):
		self.shape1 = shape1
		self.shape2 = shape2
		self.partToCompare = partToCompare
		self.pictureDrawn = pictureDrawn
		self.realLife = realLife
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
	def addQuestion(self, doc = None):
		shape1Chosen = random.choice(self.shape1)
		shape2Chosen = random.choice(self.shape2)
		partToCompareChosen = random.choice(self.partToCompare)
		
		#First going to compare diameter of spheres
		#Question: Given a basketball with a volume of X and a tennis ball of volume X, how many times bigger is the diameter of the basketball?
		largeSpheres = ['basketball', 'bowling ball', 'soccer ball','volleyball','beach ball']
		smallSpheres = ['tennis ball','golfball','ping pong ball','baseball']
		s1 = random.choice(largeSpheres)
		s2 = random.choice(smallSpheres)
		print(s1,s2)
		volumeUnits = random.choice(['cubic centimeters'])
		if s1 == 'basketball':
			volume1 = random.randint(6900,7200)
		elif s1 == 'bowling ball':
			volume1 = random.randint(5000,5300)
		elif s1 == 'soccer ball':
			volume1 = random.randint(5300,5800)
		elif s1 == 'volleyball':
			volume1 = random.randint(4600,5400)
		elif s1 == 'beach ball':
			volume1 = random.randint(14000,15000)

		if s2 == 'tennis ball':
			volume2 = random.randint(100,200)
		elif s2 == 'golfball':
			volume2 = random.randint(30,50)
		elif s2 == 'ping pong ball':
			volume2 = random.randint(50,70)
		elif s2 == 'baseball':
			volume2 = random.randint(175,215)

		radius1 = ( volume1 / (4 / 3 * math.pi) ) ** (1/3)
		radius2 = ( volume2 / (4 / 3 * math.pi) ) ** (1/3)

		diameter1 = radius1 * 2
		diameter2 = radius2 * 2

		comparedValue = roundGivenString(value = diameter1/diameter2, string = 'whole number')
		questionString = 'Given a %s with a volume of %d %s and a %s with a volume of %d %s, about how many times bigger is the diameter of the %s compared to that of the %s?' % (s1, volume1, volumeUnits, s2, volume2, volumeUnits, s1, s2)
		self.answer = str(comparedValue)
		choice1 = str(roundGivenString(string='whole number', value = volume1/volume2)) #Divide the volumes
		choice2 = str(roundGivenString(string='whole number', value = volume1-volume2)) #multiply the volumes
		choice3 = str(roundGivenString(string='whole number', value = diameter1 - diameter2))#subtract answers
		choices = [self.answer, choice1, choice2, choice3]
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		newChoices = multipleChoice(choices = choices, doc = doc)
		self.answer = 'Choice %d' % (newChoices.index(self.answer)+1)
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
######################ANGLES#############################
class linearPair():
	def __init__(self, rotate = 0, algebraic = False):
		child = 3
		self.rotate = 0
		self.algebraic = algebraic
		#angles given on picture???
		#angles written in words
	def addQuestion(self, doc = None):
		gel = 3
		#Given the diagram below, if $m\angle{ABC}$ is X degrees, find the $m\angle{WEFF}$.

		#for diagram, make a line. halfway on the line make a point and then have the other point of the second line be somehwere above
		ABC, DEF, GHI = annotatePolygon(vertices = 3, prime = False)
		#line left to right is ABD, with line cutting into it as C whether it be up or down
		angleGiven = ABC[0] + ABC[1] + ABC[2]
		angleFind = ABC[2] + ABC[1] + DEF[0]

		angleGivenValue = random.randint(20,160)
		angleFindValue = 180 - angleGivenValue
		
		if self.algebraic == False:
			questionString = r'Given the diagram below, if $m\angle{%s}=%d^{\circ}$, what is the measure of $\angle{%s}$?' % (angleGiven, angleGivenValue, angleFind)
			self.answer = r'$m\angle{%s}=%d^{\circ}$' % (angleFind, angleFindValue)
		else:
			#if i did random numbers for a, b, c, d. then they have to add up to 180, x would just be weird, right?? simplest fractional form??
			#lets go this from a binomial = 180
			x = random.randint(1,20)

			#define a and c, then see what we need bd's sum to be
			coA = random.choice([x for x in range(1,9) if x not in [0]])
			coC = random.choice([x for x in range(1,9) if x not in [0]])
			
			acSum = coA + coC
			bdSum = 180 - (acSum * x)

			coB = random.choice([x for x in range(1,9)])
			coD = bdSum - coB

			angleGivenValue = coA * x + coB
			angleFindValue = coC * x + coD
			if angleGivenValue < 0 or angleFindValue < 0 or (angleGivenValue + angleFindValue != 180):
				print(angleGiven, angleFind)
				print('DICK~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
			
			questionString = r'Given the diagram below, if $m\angle{%s}=%sx%s$ and $m\angle{%s}=%sx%s$, what is the measure of $\angle{%s}$?' % (angleGiven, equationFormatting(value = coA, inBetween = False, nextToVariable = True), equationFormatting(value = coB, inBetween = True, nextToVariable = False), angleFind, equationFormatting(value = coC, nextToVariable = True, inBetween = False), equationFormatting(value = coD, nextToVariable = False, inBetween = True), angleFind)
			self.answer = r'$m\angle{%s}=%d^{\circ}$' % (angleFind, angleFindValue)
		doc.append(NoEscape(questionString))
		
		doc.append(NewLine())
		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			if angleGivenValue <= 90:
				nodeDirection = 'left'
			else:
				nodeDirection = 'right'

			com('draw (0,0) -- (4,0)', doc = doc)
			com('node at (%g,%g) {%s}' % (-.2,0, ABC[0]), doc = doc)
			com('node at (%g,%g) {%s}' % (4.2,0, DEF[0]), doc = doc)
			com('draw (2,0) --++(%d:3)' % (180-angleGivenValue), doc = doc)
			com('node at (2,-.2) {%s}' % (ABC[1]), doc = doc)
			if nodeDirection == 'left':
				com('node at (%g,%g) {%s}' % (-.2+(2-math.cos(math.radians(angleGivenValue))*3), math.sin(math.radians(angleGivenValue))*3, ABC[2]), doc = doc)
			else:
				com('node at (%g,%g) {%s}' % (.2+(2-math.cos(math.radians(angleGivenValue))*3), math.sin(math.radians(angleGivenValue))*3, ABC[2]), doc = doc)


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class verticalAngles():
	def __init__(self, rotate = 0, algebraic = False):
		child = 3
		self.rotate = 0
		self.algebraic = algebraic
		#angles given on picture???
		#angles written in words
	def addQuestion(self, doc = None):
		gel = 3
		#Given the diagram below, if $m\angle{ABC}$ is X degrees, find the $m\angle{WEFF}$.

		#for diagram, make a line. halfway on the line make a point and then have the other point of the second line be somehwere above
		ABC, DEF, GHI = annotatePolygon(vertices = 3, prime = False)
		#line left to right is ABD, with line cutting into it as CE is cutting left to right
		angleGiven = (ABC[0] + ABC[1] + ABC[2])[::random.choice([1,-1])]
		angleFind = (DEF[2] + ABC[1] + DEF[0])[::random.choice([1,-1])]

		angleGivenValue = random.randint(20,160)
		angleFindValue = angleGivenValue
		
		if self.algebraic == False:
			questionString = r'Given the diagram below, if $m\angle{%s}=%d^{\circ}$, what is the measure of $\angle{%s}$?' % (angleGiven, angleGivenValue, angleFind)
			self.answer = r'$m\angle{%s}=%d^{\circ}$' % (angleFind, angleFindValue)
		else:
			#ax + b = cx + d
			#make it so that ax + b is bewteen 50 and 150 or something
			x = random.randint(1,10)
			a = random.choice([x for x in range(4,11) if x not in [0]])
			b = random.choice([x for x in range(4,11) if x not in [0]])
			#max and min are good
			angleFindValue = a*x + b


			angleFindValue = angleGivenValue
			
			questionString = r'Given the diagram below, if $m\angle{%s}=%sx%s$ and $m\angle{%s}=%sx%s$, what is the measure of $\angle{%s}$?' % (angleGiven, equationFormatting(value = coA, inBetween = False, nextToVariable = True), equationFormatting(value = coB, inBetween = True, nextToVariable = False), angleFind, equationFormatting(value = coC, nextToVariable = True, inBetween = False), equationFormatting(value = coD, nextToVariable = False, inBetween = True), angleFind)
			self.answer = r'$m\angle{%s}=%d^{\circ}$' % (angleFind, angleFindValue)
		doc.append(NoEscape(questionString))
		
		doc.append(NewLine())
		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			if angleGivenValue <= 90:
				nodeDirection = 'left'
			else:
				nodeDirection = 'right'

			com('draw (0,0) -- (4,0)', doc = doc)
			com('node at (%g,%g) {%s}' % (-.2,0, ABC[0]), doc = doc)
			com('node at (%g,%g) {%s}' % (4.2,0, DEF[0]), doc = doc)
			com('draw (%g,%g) -- (%g,%g)' % (2-math.cos(math.radians(angleGivenValue))*2, math.sin(math.radians(angleGivenValue))*2, 2+math.cos(math.radians(angleGivenValue))*2, -math.sin(math.radians(angleGivenValue))*2)
				, doc = doc)
			
			if nodeDirection == 'left':
				com('node at (1.8,-.2) {%s}' % ABC[1], doc = doc) #middle angle
				com('node at (%g,%g) {%s}' % (-.2+(2-math.cos(math.radians(angleGivenValue))*2), math.sin(math.radians(angleGivenValue))*2, ABC[2]), doc = doc) #top left angle
				com('node at (%g,%g) {%s}' % (.2+(2+math.cos(math.radians(angleGivenValue))*2), -math.sin(math.radians(angleGivenValue))*2, DEF[1]), doc = doc) #bottom right angle
			else:
				com('node at (2.2,-.2 {%s}' % ABC[1], doc = doc) #middle angle
				com('node at (%g,%g) {%s}' % (.2+(2+math.cos(math.radians(180-angleGivenValue))*2), math.sin(math.radians(180-angleGivenValue))*2, ABC[2]), doc = doc) #top right angle
				com('node at (%g,%g) {%s}' % (-.2+(2-math.cos(math.radians(180-angleGivenValue))*2), -math.sin(math.radians(180-angleGivenValue))*2, DEF[1]), doc = doc) #bottom left angle

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class triangleSum():
	def __init__(self, isosceles = False, difficulty = ['easy','medium','hard'], linearPair = False, rotate = 0):
		dick = 2
		self.rotate = rotate
		self.linearPair = linearPair
		self.isosceles = isosceles
		self.difficulty = random.choice(difficulty)
	def addQuestion(self, doc = None):
		ABC, DEF, GHI = annotatePolygon(vertices = 3, prime = False)
		#draw a line, 

		#know both base angles
		triangleName = ABC[0] + ABC[1] + ABC[2]
		if self.isosceles == True:
			leftBaseAngle = random.choice([x for x in range(20,55)] + [x for x in range(65,80)])
			rightBaseAngle = leftBaseAngle
		else:
			leftBaseAngle = random.randint(20,80)
			rightBaseAngle = random.randint(20,80)
		vertexAngle = 180 - leftBaseAngle - rightBaseAngle

		#thing on the end, makes it random between forward and backwards letter names
		leftBaseAngleName = (ABC[2]+ABC[0]+ABC[1])[::random.choice([1,-1])]
		rightBaseAngleName = (ABC[0]+ABC[1]+ABC[2])[::random.choice([1,-1])]
		vertexAngleName = (ABC[1]+ABC[2]+ABC[0])[::random.choice([1,-1])]

		#for isosceles
		leftBaseName = (ABC[2]+ABC[1])[::random.choice([1,-1])]
		rightBaseName = (ABC[0]+ABC[2])[::random.choice([1,-1])]

		#for linear pair
		if self.linearPair == True:
			if self.difficulty == 'easy': #exterior angle is that of the vertex angle
				exteriorAngleName = (DEF[0]+ABC[2]+ABC[1])[::random.choice([1,-1])]#DEF[0] + half of vertex angle
				exteriorAngle = 180 - vertexAngle #180-vertex
			elif self.difficulty == 'medium': #exterior angle of base angle
				exteriorAngleName = (DEF[0]+ABC[0]+ABC[2])[::random.choice([1,-1])] #bunch of randomness between sides of the triangle etc etc
				exteriorAngle = 180 - leftBaseAngle

		#law of sines to find left side distance
		#5/sin(vertexAngle) = x/sin(oppangle)

		#dist between base vertices = 5
		#find leftDistance

		#sometimes when base angles are really small or large, triangle gets crazy, so
		if leftBaseAngle < 31 or rightBaseAngle < 31 or leftBaseAngle > 70 or rightBaseAngle > 70:
			baseDistance = 3
		else:
			baseDistance = 4

		leftDistance = (baseDistance / math.sin(math.radians(vertexAngle))) * math.sin(math.radians(rightBaseAngle))
		rightDistance = (baseDistance / math.sin(math.radians(vertexAngle))) * math.sin(math.radians(leftBaseAngle))

		if self.isosceles == True:
			if self.difficulty == 'easy': #find vertex angle
				questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, leftBaseName, rightBaseName, vertexAngleName)	
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (vertexAngleName, vertexAngle)
				
				if self.linearPair == True: #find vertex angle, then find the linear pair aka exterior angle
					questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, leftBaseName, rightBaseName, exteriorAngleName)	
					self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (exteriorAngleName, exteriorAngle)	
			
			elif self.difficulty == 'medium': #find base angle
				questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$, what is the measure of ${\angle{%s}}$?' % (triangleName, vertexAngleName, vertexAngle, leftBaseName, rightBaseName, leftBaseAngleName)	
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (leftBaseAngleName, leftBaseAngle)
				
				if self.linearPair == True: #find base angle, then find the linear pair aka exterior angle
					questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$, what is the measure of ${\angle{%s}}$?' % (triangleName, vertexAngleName, vertexAngle, leftBaseName, rightBaseName, exteriorAngleName)	
					self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (exteriorAngleName, exteriorAngle)	
		else:
			questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, rightBaseAngleName, rightBaseAngle, vertexAngleName)	
			self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (vertexAngleName, vertexAngle)

			if self.linearPair == True: #find vertex angle, then find the linear pair aka exterior angle
				questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, rightBaseName, rightBaseAngle, exteriorAngleName)	
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (exteriorAngleName, exteriorAngle)	
		
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			com('draw (0,0) -- (%g,0)' % (baseDistance), doc = doc)
			com('node at (-.2,-.2) {%s}' % (ABC[0]), doc = doc)
			com('node at (%g,%g) {%s}' % (baseDistance, -.2, ABC[1]), doc = doc)
			if self.linearPair == True:
				if self.difficulty == 'easy': #for vertex angle
					com('draw (0,0) --++(%g:%g) --++(%g:%g)' % (leftBaseAngle, leftDistance, -rightBaseAngle, rightDistance), doc = doc)
					com('draw (0,0) --++(%g:%g)' % (leftBaseAngle, 1.3*leftDistance), doc = doc) #same line but extend it farther	
					com('node at (%g,%g) {%s}' % (math.cos(math.radians(leftBaseAngle))*1.3*leftDistance, .2+math.sin(math.radians(leftBaseAngle))*1.3*leftDistance, DEF[0]), doc = doc)
				elif self.difficulty == 'medium': #for left base angle
					com('draw (0,0) --++(%g:%g) --++(%g:%g)' % (leftBaseAngle, leftDistance, -rightBaseAngle, rightDistance), doc = doc)
					com('draw (0,0) -- (%g,0)' % (-.25*baseDistance), doc = doc) #same line but extend it farther	
					com('node at (%g,%g) {%s}' % (-.2+-.25*baseDistance, 0, DEF[0]), doc = doc)
			else:
				com('draw (0,0) --++(%g:%g) --++(%g:%g)' % (leftBaseAngle, leftDistance, -rightBaseAngle, rightDistance), doc = doc)
				
			com('node at (%g,%g) {%s}' % (-.2+math.cos(math.radians(leftBaseAngle))*leftDistance, .2+math.sin(math.radians(leftBaseAngle))*leftDistance, ABC[2]), doc = doc)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class triangleSumLinearPair():
	def __init__(self, findExteriorAngle = True, rotate = 0):
		dick = 2
		self.rotate = rotate
		self.findExteriorAngle = findExteriorAngle

	def addQuestion(self, doc = None):
		ABC, DEF, GHI = annotatePolygon(vertices = 3, prime = False)
		#draw a line, 

		#know both base angles
		triangleName = ABC[0] + ABC[1] + ABC[2]
		leftBaseAngle = random.randint(20,80)
		rightBaseAngle = random.randint(20,80)
		vertexAngle = 180 - leftBaseAngle - rightBaseAngle

		#thing on the end, makes it random between forward and backwards letter names
		leftBaseAngleName = (ABC[2]+ABC[0]+ABC[1])[::random.choice([1,-1])]
		rightBaseAngleName = (ABC[0]+ABC[1]+ABC[2])[::random.choice([1,-1])]
		vertexAngleName = (ABC[1]+ABC[2]+ABC[0])[::random.choice([1,-1])]

		exteriorAngleName = (ABC[2]+ABC[1]+DEF[0])[::random.choice([1,-1])]
		exteriorAngle = 180 - rightBaseAngle
		
		#law of sines to find left side distance
		#5/sin(vertexAngle) = x/sin(oppangle)

		#dist between base vertices = 5
		#find leftDistance
		baseDistance = 5
		leftDistance = (5 / math.sin(math.radians(vertexAngle))) * math.sin(math.radians(rightBaseAngle))
		rightDistance = (5 / math.sin(math.radians(vertexAngle))) * math.sin(math.radians(leftBaseAngle))

		if self.findExteriorAngle == False: #find vertex angle
			questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, exteriorAngleName, exteriorAngle, vertexAngleName)
			self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (vertexAngleName, vertexAngle)
		elif self.findExteriorAngle == True: #find exterior angle
			questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, vertexAngleName, vertexAngle, exteriorAngleName)
			self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (exteriorAngleName, exteriorAngle)
		
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			com('draw (0,0) -- (%g,0)' % (baseDistance), doc = doc)
			com('node at (-.2,0) {%s}' % (ABC[0]), doc = doc)
			com('node at (%g,%g) {%s}' % (baseDistance, -.2, ABC[1]), doc = doc)
			com('draw (0,0) --++(%g:%g) --++(%g:%g)' % (leftBaseAngle, leftDistance, -rightBaseAngle, rightDistance), doc = doc)
			com('node at (%g,%g) {%s}' % (math.cos(math.radians(leftBaseAngle))*leftDistance, .2+math.sin(math.radians(leftBaseAngle))*leftDistance, ABC[2]), doc = doc)

			com('draw (0,0) -- (%g,0)' % (baseDistance + 2), doc = doc)
			com('node at (%g,0) {%s}' % (baseDistance + 2 + .2, DEF[0]), doc = doc)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class alternateInteriorAngles():
	def __init__(self, rotate = 0, triangleSum = False, verticalAngles = False, linearPair = False, isosceles = False):
		self.rotate = rotate
		self.triangleSum = triangleSum
		self.verticalAngles = verticalAngles
		self.linearPair = linearPair
		self.isosceles = isosceles
		self.typeOfQuestionChosen = 'SA'
	def addQuestion(self, doc = None):
		#Define some letters
		ABCD, EFGH, IJKL = annotatePolygon(vertices = 4, prime = False)

		#hourglass figure - top line ABCD, F in middle, bottom line GHIJ

		#create bottom triangle with line, then rotate all the points around the top (vertex)
		if self.isosceles == True:
			print('ISOSCELES')
			leftBaseAngle = random.choice([x for x in range(30,55)] + [x for x in range(65,70)])
			rightBaseAngle = leftBaseAngle
		else:
			leftBaseAngle = random.randint(30,70)
			rightBaseAngle = random.choice([x for x in range(30,70) if x not in [leftBaseAngle]]) #ensuring not isosceles or equilateral

		vertexAngle = 180 - leftBaseAngle - rightBaseAngle

		#define angle names when I need them
		
		#for isosceles
		#[::random.choice([1,-1])] for randomized etc

		#law of sines to find left side distance
		#5/sin(vertexAngle) = x/sin(oppangle)

		#dist between base vertices = 5
		#find leftDistance

		#sometimes when base angles are really small or large, triangle gets crazy, so
		if leftBaseAngle < 31 or rightBaseAngle < 31 or leftBaseAngle > 70 or rightBaseAngle > 70:
			baseDistance = 2
		else:
			baseDistance = 3

		leftDistance = (baseDistance / math.sin(math.radians(vertexAngle))) * math.sin(math.radians(rightBaseAngle))
		rightDistance = (baseDistance / math.sin(math.radians(vertexAngle))) * math.sin(math.radians(leftBaseAngle))

		if self.isosceles == False and self.linearPair == False and self.triangleSum == False and self.verticalAngles == False: #solely alternate interior
			#givenInfo = top left angle, which is rightBaseAngle CBE
			#find Info = bottom right angle, which is rightBaseAngle EHF
			givenInfo = r'${m\angle{%s}=%d^{\circ}}$' % ((ABCD[2]+ABCD[1]+EFGH[0])[::random.choice([1,-1])], rightBaseAngle) 
			findInfo = r'${\angle{%s}}$' % (EFGH[0]+EFGH[3]+EFGH[1])[::random.choice([1,-1])]
			self.answer = r'${\angle{%s}=%d^{\circ}}$' % ((EFGH[0]+EFGH[3]+EFGH[1])[::random.choice([1,-1])], rightBaseAngle)

		elif self.isosceles == False and self.linearPair == False and self.triangleSum == True and self.verticalAngles == False:
			#givenInfo = top left angle, which is rightBaseAngle CBE
			#givenInfo2 = bottom left angle, which is leftBaseAngle EGH
			#find Info = bottom triangle top angle, which is vertexAngle GEH
			givenInfo = r'${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$' % ((ABCD[2]+ABCD[1]+EFGH[0])[::random.choice([1,-1])], rightBaseAngle, (EFGH[0]+EFGH[2]+EFGH[3])[::random.choice([1,-1])], leftBaseAngle) 
			findInfo = r'${\angle{%s}}$' % (EFGH[2]+EFGH[0]+EFGH[3])[::random.choice([1,-1])]
			self.answer = r'${\angle{%s}=%d^{\circ}}$' % ((EFGH[2]+EFGH[0]+EFGH[3])[::random.choice([1,-1])], vertexAngle)

		elif self.isosceles == False and self.linearPair == True and self.triangleSum == True and self.verticalAngles == False:
			#givenInfo = top left angle, which is rightBaseAngle CBE
			#givenInfo2 = bottom left angle, which is leftBaseAngle EGH
			#find Info = exterior middle angle, which is 180-vertexAngle BEG
			givenInfo = r'${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$' % ((ABCD[2]+ABCD[1]+EFGH[0])[::random.choice([1,-1])], rightBaseAngle, (EFGH[0]+EFGH[2]+EFGH[3])[::random.choice([1,-1])], leftBaseAngle) 
			findInfo = r'${\angle{%s}}$' % (ABCD[1]+EFGH[0]+EFGH[2])[::random.choice([1,-1])]
			self.answer = r'${\angle{%s}=%d^{\circ}}$' % ((ABCD[1]+EFGH[0]+EFGH[2])[::random.choice([1,-1])], 180-vertexAngle)

		elif self.isosceles == True and self.linearPair == False and self.triangleSum == True and self.verticalAngles == False:
			#givenInfo = top left angle, which is rightBaseAngle CBE
			#givenInfo2 = bottom left angle, which is leftBaseAngle EGH
			#find Info = bottom triangle top angle, which is vertexAngle GEH
			givenInfo = r'${m\angle{%s}=%d^{\circ}}$, and $\overline{%s}\cong\overline{%s}$' % ((ABCD[2]+ABCD[1]+EFGH[0])[::random.choice([1,-1])], rightBaseAngle, ABCD[1]+EFGH[0], ABCD[2]+EFGH[0]) 
			findInfo = r'${\angle{%s}}$' % (EFGH[2]+EFGH[0]+EFGH[3])[::random.choice([1,-1])]
			self.answer = r'${\angle{%s}=%d^{\circ}}$' % ((EFGH[2]+EFGH[0]+EFGH[3])[::random.choice([1,-1])], vertexAngle)

		elif self.isosceles == True and self.linearPair == True and self.triangleSum == True and self.verticalAngles == False:
			#givenInfo = top left angle, which is rightBaseAngle CBE
			#givenInfo2 = bottom left angle, which is leftBaseAngle EGH
			#find Info = exterior middle angle, which is 180-vertexAngle BEG
			givenInfo = r'${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$' % ((ABCD[2]+ABCD[1]+EFGH[0])[::random.choice([1,-1])], rightBaseAngle, ABCD[1]+EFGH[0], ABCD[2]+EFGH[0]) 
			findInfo = r'${\angle{%s}}$' % (ABCD[1]+EFGH[0]+EFGH[2])[::random.choice([1,-1])]
			self.answer = r'${\angle{%s}=%d^{\circ}}$' % ((ABCD[1]+EFGH[0]+EFGH[2])[::random.choice([1,-1])], 180-vertexAngle)

		'''
		if self.isosceles == True:
			if self.difficulty == 'easy': #find vertex angle
				questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, leftBaseName, rightBaseName, vertexAngleName)	
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (vertexAngleName, vertexAngle)
				
				if self.linearPair == True: #find vertex angle, then find the linear pair aka exterior angle
					questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, leftBaseName, rightBaseName, exteriorAngleName)	
					self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (exteriorAngleName, exteriorAngle)	
			
			elif self.difficulty == 'medium': #find base angle
				questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$, what is the measure of ${\angle{%s}}$?' % (triangleName, vertexAngleName, vertexAngle, leftBaseName, rightBaseName, leftBaseAngleName)	
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (leftBaseAngleName, leftBaseAngle)
				
				if self.linearPair == True: #find base angle, then find the linear pair aka exterior angle
					questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and $\overline{%s}\cong\overline{%s}$, what is the measure of ${\angle{%s}}$?' % (triangleName, vertexAngleName, vertexAngle, leftBaseName, rightBaseName, exteriorAngleName)	
					self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (exteriorAngleName, exteriorAngle)	
		else:
			questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, rightBaseAngleName, rightBaseAngle, vertexAngleName)	
			self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (vertexAngleName, vertexAngle)

			if self.linearPair == True: #find vertex angle, then find the linear pair aka exterior angle
				questionString = r'Given ${\bigtriangleup{%s}}$ below, ${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$, what is the measure of ${\angle{%s}}$?' % (triangleName, leftBaseAngleName, leftBaseAngle, rightBaseName, rightBaseAngle, exteriorAngleName)	
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % (exteriorAngleName, exteriorAngle)	
		'''
		questionString = r'In the diagram below $\overline{%s}||\overline{%s}$, if ' % (ABCD[0]+ABCD[3], EFGH[1]+IJKL[0]) 
		questionString += givenInfo + ', what is the measure of ' + findInfo + '?'

		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			#draw bottom line, with 4 nodes ABCD, EFGH, IJKL
			#so bottom line has a distance of 1/2 base distance in either direction etc
			com('draw (0,0) -- (%g,0)' % (.5*baseDistance+baseDistance+.5*baseDistance), doc = doc)
			#F bottom left
			com('node at (%g,0) {%s}' % (-.2, EFGH[1]), doc = doc)
			xs = []
			xs.append(0)
			ys = []
			ys.append(0)
			#G bottom left of triangle
			com('node at (%g,%g) {%s}' % (.5*baseDistance, -.2, EFGH[2]), doc = doc)
			xs.append(.5*baseDistance)
			ys.append(0)
			#H bottom right of triangle
			com('node at (%g,%g) {%s}' % (1.5*baseDistance, -.2, EFGH[3]), doc = doc)
			xs.append(1.5*baseDistance)
			ys.append(0)
			#I bottom right
			com('node at (%g,0) {%s}' % (2*baseDistance+.2, IJKL[0]), doc = doc)
			xs.append(2*baseDistance)
			ys.append(0)

			#middle E
			com('node at (%g,%g) {%s}' % (.5*baseDistance+math.cos(math.radians(leftBaseAngle))*leftDistance-.2, math.sin(math.radians(leftBaseAngle))*leftDistance, EFGH[0]), doc = doc)

			#now i need to rotate all this shit
			rotatedX, rotatedY = rotateCoordinatesAsList(xs, ys, 'clockwise', 180, centerX = .5*baseDistance+math.cos(math.radians(leftBaseAngle))*leftDistance, centerY = math.sin(math.radians(leftBaseAngle))*leftDistance)

			#so now rotatedX is right to left 

			#top line
			com('draw (%g,%g) -- (%g,%g)' % (rotatedX[3], rotatedY[3], rotatedX[0], rotatedY[0]), doc = doc)
			#draw diagonals which make 2 triangles
			com('draw (%g,%g) -- (%g,%g)' % (rotatedX[1], rotatedY[1], xs[1], ys[1]), doc = doc)
			com('draw (%g,%g) -- (%g,%g)' % (rotatedX[2], rotatedY[2], xs[2], ys[2]), doc = doc)

			#A
			com('node at (%g,%g) {%s}' % (rotatedX[3]-.2, rotatedY[3], ABCD[0]), doc = doc)
			#B
			com('node at (%g,%g) {%s}' % (rotatedX[2], rotatedY[3]+.2, ABCD[1]), doc = doc)
			#C
			com('node at (%g,%g) {%s}' % (rotatedX[1], rotatedY[1]+.2, ABCD[2]), doc = doc)
			#D
			com('node at (%g,%g) {%s}' % (rotatedX[0]+.2, rotatedY[0], ABCD[3]), doc = doc)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class parallelogramAngles():
	#version is not that much different etc
	def __init__(self, rotate = 0, standard = True, difficulty = ['easy','medium','hard']):
		self.standard = standard
		self.difficulty = random.choice(difficulty)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		#Easy first, draw parallelogram
		baseAngle = random.randint(60, 120) #for sizing etc
		baseDistance = 3
		topLeftAngle = random.randint(baseAngle//4, baseAngle//2) #really topleftright etc
		ABCD, EFGH, IJKL = annotatePolygon(vertices = 4, prime = False)
		A = ABCD[0]
		B = ABCD[1]
		C = ABCD[2]
		D = ABCD[3]

		if self.difficulty == 'medium' or self.difficulty == 'hard':
			#for now we're just finding the bottom left angle of triangle added on, could switch it up

			#need to add on a triangle on the end of the parallelogram, 
			E = EFGH[0]
			#make triangle vertex part of the leftover (180-baseAngle), make other part the remaining angle in the triangle
			if self.difficulty == 'medium':
				ah = 180 - baseAngle
				rightTriangleAngle = 180 - baseAngle
				vertexAngle = random.randint(rightTriangleAngle//3, rightTriangleAngle//2) # so the other angle can be something
				leftTriangleAngle = 180 - rightTriangleAngle - vertexAngle
			else:
				isoscelesVersion = random.randint(1,3)
				if isoscelesVersion == 1: #vertex and left are the same
					rightTriangleAngle = 180 - baseAngle
					vertexAngle = rightTriangleAngle/2 # float!
					leftTriangleAngle = rightTriangleAngle/2
					congruencyStatement = r'$\overline{%s}\cong\overline{%s}$' % (A+D, D+E)
				elif isoscelesVersion == 2: #vertex and right are the same
					baseAngle = random.randint(100, 120) #for sizing etc
					baseDistance = 3
					topLeftAngle = random.randint(baseAngle//4, baseAngle//2) #really topleftright etc
					rightTriangleAngle = 180 - baseAngle
					vertexAngle = rightTriangleAngle
					leftTriangleAngle = 180 - 2*vertexAngle
					congruencyStatement = r'$\overline{%s}\cong\overline{%s}$' % (A+E, D+E)
				elif isoscelesVersion == 3: #left and right are the same
					baseAngle = random.randint(100, 120) #for sizing etc
					baseDistance = 3
					topLeftAngle = random.randint(baseAngle//4, baseAngle//2) #really topleftright etc
					rightTriangleAngle = 180 - baseAngle
					leftTriangleAngle = rightTriangleAngle
					vertexAngle = 180 - 2*leftTriangleAngle
					congruencyStatement = r'$\overline{%s}\cong\overline{%s}$' % (A+E, A+D)

		if self.standard == True and self.difficulty == 'easy':
			givenString = r'${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$' % ( (B+A+C)[::random.choice([1,-1])], topLeftAngle, (A+B+C)[::random.choice([1,-1])], baseAngle)
			findString = r'$\angle{%s}$' % ( (C+A+D)[::random.choice([1,-1])] )

			questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
			self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (C+A+D)[::random.choice([-1,1])], 180 - baseAngle - topLeftAngle)
		elif self.standard == False and self.difficulty == 'easy':
			#given topleft angle and other topleft angle
			#find topright angle which is base angle essentially
			givenString = r'${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$' % ( (B+A+C)[::random.choice([1,-1])], topLeftAngle, (D+A+C)[::random.choice([1,-1])], 180-baseAngle-topLeftAngle)
			findString = r'${\angle{%s}}$' % ( (A+B+C)[::random.choice([1,-1])] )

			questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
			self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (A+B+C)[::random.choice([-1,1])], baseAngle)

		if self.difficulty == 'medium' and self.standard == True:
			
			version = random.randint(1,2)

			if version == 1: #find leftTriangleAngle, given vertex and top right parallelogram angle (base)
				givenString = r'${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$' % ( (E+A+D)[::random.choice([1,-1])], vertexAngle, (A+B+C)[::random.choice([1,-1])], baseAngle)
				findString = r'${\angle{%s}}$' % ( (A+E+D)[::random.choice([1,-1])] )

				questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (A+E+D)[::random.choice([-1,1])], leftTriangleAngle)

			else: #find vertexAngle, given top right parallelogram angle (base) and leftTriangleAngle
				givenString = r'${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$' % ( (A+B+C)[::random.choice([1,-1])], baseAngle, (A+E+D)[::random.choice([1,-1])], leftTriangleAngle)
				findString = r'${\angle{%s}}$' % ( (E+A+D)[::random.choice([1,-1])] )

				questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (E+A+D)[::random.choice([-1,1])], vertexAngle)

		elif self.difficulty == 'medium' and self.standard == False:
			#given vertex and leftTriangleAngle, find top right parallelogram angle
			givenString = r'${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$' % ( (E+A+D)[::random.choice([1,-1])], vertexAngle, (A+E+D)[::random.choice([1,-1])], leftTriangleAngle)
			findString = r'${\angle{%s}}$' % ( (A+B+C)[::random.choice([1,-1])] )

			questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
			self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (A+B+C)[::random.choice([-1,1])], baseAngle)

		if self.difficulty == 'hard' and self.standard == True:
			
			version = random.randint(1,2)

			if version == 1: #find leftTriangleAngle, given vertex and top right parallelogram angle (base)
				givenString = r'${m\angle{%s}=%d^{\circ}}$ and ' % ( (A+B+C)[::random.choice([1,-1])], baseAngle) + congruencyStatement
				findString = r'${\angle{%s}}$' % ( (A+E+D)[::random.choice([1,-1])] )

				questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (A+E+D)[::random.choice([-1,1])], leftTriangleAngle)

			else: #find vertexAngle, given top right parallelogram angle (base) and leftTriangleAngle
				givenString = r'${m\angle{%s}=%d^{\circ}}$ and ' % ( (A+B+C)[::random.choice([1,-1])], baseAngle) + congruencyStatement
				findString = r'${\angle{%s}}$' % ( (E+A+D)[::random.choice([1,-1])] )

				questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (E+A+D)[::random.choice([-1,1])], vertexAngle)

		elif self.difficulty == 'hard' and self.standard == False:
			#given vertex and leftTriangleAngle, find top right parallelogram angle
			#only need one or othe other i believe
			whichOne = random.randint(1,2)
			if whichOne == 1:
				givenString = r'${m\angle{%s}=%d^{\circ}}$ and ' % ( (E+A+D)[::random.choice([1,-1])], vertexAngle) + congruencyStatement
				findString = r'${\angle{%s}}$' % ( (A+B+C)[::random.choice([1,-1])] )

				questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (A+B+C)[::random.choice([-1,1])], baseAngle)
			else:
				givenString = r'${m\angle{%s}=%d^{\circ}}$ and ' % ( (A+E+D)[::random.choice([1,-1])], leftTriangleAngle) + congruencyStatement
				findString = r'${\angle{%s}}$' % ( (A+B+C)[::random.choice([1,-1])] )

				questionString = 'Parallelogram %s is drawn below, if ' % (A+B+C+D) + givenString + ', what is the measure of ' + findString + '?'
				self.answer = r'${m\angle{%s}=%d^{\circ}}$' % ( (A+B+C)[::random.choice([-1,1])], baseAngle)

		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			sideLength = baseDistance*2//3
			xs, ys = parallelogram(baseAngle = baseAngle, doc = doc, baseDistance = baseDistance, sideLength = sideLength)
			#sideLength for rougly a good looking parallelogram

			if self.difficulty == 'easy':
				#draw diagonal
				com('draw (%g,%g) -- (%g,%g)' % (xs[0], ys[0], xs[2], ys[2]), doc = doc)

			#label vertices
			com('node at (%g,%g) {%s}' % (xs[0]-.2, ys[0]+.2, A), doc = doc)
			com('node at (%g,%g) {%s}' % (xs[1]+.2, ys[1]+.2, B), doc = doc)
			com('node at (%g,%g) {%s}' % (xs[2]+.2, ys[2]-.2, C), doc = doc)
			com('node at (%g,%g) {%s}' % (xs[3]-.2, ys[3]-.2, D), doc = doc)

			if self.difficulty == 'medium' or self.difficulty == 'hard':
				#draw triangle on the side
				#law of sines to find how far out the triangle goes
				#baseChange/sin(vertex) = sideLength/sin(leftAngle)
				#sideLength/sin(leftAngle)*sin(vertex)
				com('draw (%g,%g) -- (%g,%g) -- (%g,%g)' % (xs[0], ys[0], xs[3] - sideLength/math.sin(math.radians(leftTriangleAngle))*math.sin(math.radians(vertexAngle)), 0, 0, 0), doc = doc)

				#label E
				com('node at (%g,%g) {%s}' % (-.2 + xs[3] - sideLength/math.sin(math.radians(leftTriangleAngle))*math.sin(math.radians(vertexAngle)), 0, E), doc = doc)


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
#basic: given whole angle, find part
#basic: given part, find whole angle
'''
bisecting angles (given whole or given part etc) (easy)

bisecting angles
-linear pair = True, two angles, one is bisected, given part of angle find find other linear pair
-linear pair = True, two angles, one is bisected, given whole angle not bisected, find part of other angle


bisect angles - one where isosceles and one where not isosceles
(three angle bisectors in a triangle)
GIVEN: part of bisected angle
if isosceles == False:
GIVEN: otherBigAngle
'''


#medium: basically pick an angle to be bisected and find that angle

#hard: big triangle, triple bisect the angles

def labelAngle(annotation, coordinates = [1,2], position = 'bottom left', doc = None):
	if 'left' in position:
		xChange = -.2
	elif 'right' in position:
		xChange = .2
	else:
		xChange = 0

	if 'top' in position:
		yChange = .2
	elif 'bottom' in position:
		yChange = -.2
	else:
		yChange = 0

	com('node at (%g,%g) {%s}' % (coordinates[0]+xChange, coordinates[1]+yChange, annotation), doc = doc)
def angleName(listOfAnnotations = [], randomLets = True):
	stringName = ''
	for x in listOfAnnotations:
		stringName  += x
	if randomLets == True:
		return stringName[::random.choice([-1,1])]
	else:
		return stringName

class bisectAngles():
	def __init__(self, difficulty = ['easy','medium','hard'], given = ['easy','medium'], rotate = 0):
		self.difficulty = random.choice(difficulty)
		self.rotate = rotate
		self.given = random.choice(given)
	def addQuestion(self, doc = None):
		#given whole angle, find part
		ABCD, EFGH, IJKL = annotatePolygon(vertices = 4, prime = False)
		A = ABCD[0]
		B = ABCD[1]
		C = ABCD[2]
		D = ABCD[3]

		E = EFGH[0]
		F = EFGH[1]
		G = EFGH[2]
		H = EFGH[3]

		#angle is ABC, line that bisects is DB

		#draw an angle between (45, 135)
		angle = random.randint(45, 135)

		rayLength = 3

		anglePart = angle/2

		if self.difficulty == 'easy':
			ABC = (A+B+C)[::random.choice([-1,1])]
			DB = (D+B)[::random.choice([-1,1])]
			#either given the big angle or given part of the angle, etc find each other etc
			if self.given == 'easy': #give the big angle, find the part of the angle
				givenString = r'$m\angle{%s}=%g^{\circ}$ and $\overline{%s}$ bisects $\angle{%s}$' % ( ABC, angle, DB, ABC )
				whichAngle = random.choice([0,2])
				smallAngle = (ABCD[whichAngle] + B + D)[::random.choice([-1,1])]
				findString = r'$\angle{%s}$' % ( smallAngle )
				self.answer =  r'$\angle{%s}=%g^{\circ}$' % ( smallAngle, angle/2 )
			elif self.given == 'medium': #give the part of the angle, find the big angle
				whichAngle = random.choice([0,2])
				smallAngle = (ABCD[whichAngle] + B + D)[::random.choice([-1,1])]
				givenString = r'$m\angle{%s}=%g^{\circ}$ and $\overline{%s}$ bisects $\angle{%s}$' % ( smallAngle, angle/2, DB, ABC )
				findString = r'$\angle{%s}$' % ( ABC )
				self.answer =  r'$\angle{%s}=%g^{\circ}$' % ( ABC, angle )
			

		elif self.difficulty == 'medium':
			#going to do linear pair etc
			#line ABC, with angle making linear pair, DB, and angle bisector to DBC BE
			angleBisected = (D+B+C)[::random.choice([-1,1])]
			angleBisectedValue = random.randint(60,120)
			
			angleBisector = (E+B)[::random.choice([-1,1])]
			whichAngle = random.choice([D,C])
			
			smallBisectedAngle = (whichAngle+B+E)[::random.choice([-1,1])]
			smallBisectedAngleValue = angleBisectedValue/2
			
			otherAngle = (A+B+D)[::random.choice([-1,1])]
			otherAngleValue = 180 - angleBisectedValue
			
			if self.given == 'easy': #given smallAngle of bisected, find the other linear pair
				givenString = r'$m\angle{%s}=%g^{\circ}$ and $\overline{%s}$ bisects $\angle{%s}$' % ( smallBisectedAngle, smallBisectedAngleValue, angleBisector, angleBisected )
				findString = r'$\angle{%s}$' % ( otherAngle )
				self.answer =  r'$\angle{%s}=%g^{\circ}$' % ( otherAngle, otherAngleValue )

			elif self.given == 'medium': #given other angle, find small bisected 
				givenString = r'$m\angle{%s}=%g^{\circ}$ and $\overline{%s}$ bisects $\angle{%s}$' % ( otherAngle, otherAngleValue, angleBisector, angleBisected )
				findString = r'$\angle{%s}$' % ( smallBisectedAngle )
				self.answer =  r'$\angle{%s}=%g^{\circ}$' % ( smallBisectedAngle, smallBisectedAngleValue )

		elif self.difficulty == 'hard':
			rayLength = 5
			#needs to be bigger
			#3 angle bisectors and triangle may be isosceles
			#triangle leftA, rightB, topC make it not equilateral and not isosceles
			angleA = random.choice([x for x in range(30,80,2)])
			if self.given == 'medium':
				angleB = angleA
				angleC = 180 - angleA - angleB
				while angleC == angleB or angleC == angleA:
					print('yeahea')
					angleA = random.choice([x for x in range(30,80,2)])
					angleB = random.choice([x for x in range(30,80,2) if x not in [angleA]])
					angleC = 180 - angleA - angleB
			else:
				angleB = random.choice([x for x in range(30,80,2) if x not in [angleA]])
				angleC = 180 - angleA - angleB
				while angleC == angleB or angleC == angleA:
					print('yeahea')
					angleA = random.choice([x for x in range(30,80,2)])
					angleB = random.choice([x for x in range(30,80,2) if x not in [angleA]])
					angleC = 180 - angleA - angleB

			#AD bisector
			AD = (A+D)[::random.choice([-1,1])]
			#BE biesctor
			BE = (B+E)[::random.choice([-1,1])]
			#CF bisector
			CF = (C+F)[::random.choice([-1,1])]

			#AB side
			AB = (A+B)[::random.choice([-1,1])]

			#AC side
			AC = (A+C)[::random.choice([-1,1])]

			#BC side
			BC = (B+C)[::random.choice([-1,1])]

			#intersetction G
			ABG = (A+B+G)[::random.choice([-1,1])]
			CGB = (C+G+B)[::random.choice([-1,1])]

			if self.given == 'easy':
				#given one whole angle A, one part of one B, find central angle CGB
				givenString = r'$m\angle{%s}=%g^{\circ}$, $m\angle{%s}=%g^{\circ}$, $\overline{%s}$ bisects $\angle{%s}$, $\overline{%s}$ bisects $\angle{%s}$, and $\overline{%s}$ bisects $\angle{%s}$' % ( A, angleA, ABG, angleB/2, AD, A, BE, B, CF, C )
			else:
				givenString = r'$m\angle{%s}=%g^{\circ}$, $\overline{%s}$ bisects $\angle{%s}$, $\overline{%s}$ bisects $\angle{%s}$, $\overline{%s}$ bisects $\angle{%s}$, and $\overline{%s}\cong\overline{%s}$' % ( A, angleA, AD, A, BE, B, CF, C , BC, AC)
			findString = r'$\angle{%s}$' % ( CGB )
			self.answer =  r'$\angle{%s}=%g^{\circ}$' % ( CGB, (180-angleB/2-angleC/2))

		questionString = 'In the diagram below, ' + givenString + ', what is the measure of' + findString + '?'
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		
		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			if self.difficulty == 'easy':
				#draw angle ABC with DB as bisector
				AX = math.cos(math.radians(angle))*rayLength
				AY = math.sin(math.radians(angle))*rayLength
				com('draw (%g,%g) -- (%g,%g) -- (%g,%g)' % (AX
				, AY, 0, 0, rayLength, 0), doc = doc)

				DX = math.cos(math.radians(angle/2))*rayLength
				DY = math.sin(math.radians(angle/2))*rayLength

				#draw bisector
				com('draw (%g,%g) -- (%g,%g)' % (0, 0, DX,
				DY), doc = doc)

				#label angles
				labelAngle(doc = doc, annotation = A, coordinates = [AX, AY], position = 'top')
				labelAngle(doc = doc, annotation = B, coordinates = [0,0], position = 'bottom')
				labelAngle(doc = doc, annotation = C, coordinates = [rayLength, 0], position = 'right')

				labelAngle(doc = doc, annotation = D, coordinates = [DX, DY], position  = '{place}'.format(place = 'right' if angle/2<=45 else 'top'))
			
			elif self.difficulty == 'medium':
				#draw straight line ABC
				com('draw (0,0) -- (%g,0)' % (rayLength), doc = doc)
				BX = rayLength/2
				BY = 0

				#label A, B, C
				labelAngle(doc = doc, annotation = A, coordinates = [0,0], position = 'left')
				labelAngle(doc = doc, annotation = B, coordinates = [BX,0], position = 'bottom')
				labelAngle(doc = doc, annotation = C, coordinates = [rayLength,0], position = 'right')

				#draw angle to create bisectedAngle and other angle
				DX = BX - math.cos(math.radians(otherAngleValue))*rayLength
				DY = BY + math.sin(math.radians(otherAngleValue))*rayLength
				com('draw (%g,0) -- (%g,%g)' % (rayLength/2, DX, DY), doc = doc)

				#label D
				labelAngle(doc = doc, annotation = D, coordinates = [DX, DY], position = '{place}'.format(place = 'left' if otherAngleValue<=45 else 'top' if otherAngleValue <= 135 else 'right'))

				#draw angle bisector BE
				EX = BX + math.cos(math.radians(smallBisectedAngleValue))*rayLength
				EY = BY + math.sin(math.radians(smallBisectedAngleValue))*rayLength
				com('draw (%g,%g) -- (%g,%g)' % (BX, BY, EX, EY), doc = doc)

				#label E
				labelAngle(doc = doc, annotation = E, coordinates = [EX, EY], position = '{place}'.format(place = 'right' if smallBisectedAngleValue<=45 else 'top' if smallBisectedAngleValue <= 135 else 'left'))
			
			elif self.difficulty == 'hard':
				sidec = rayLength
				#law of sines for sidea
				#sidea/sin(A) = sideb/sin(B), so sidea/sin(A)*sin(B)
				sidea = sidec/math.sin(math.radians(angleC))*math.sin(math.radians(angleA))
				sideb = sidec/math.sin(math.radians(angleC))*math.sin(math.radians(angleB))

				#draw triangle ABC
				AX = 0
				AY = 0
				BX = sidec
				BY = 0
				CX = AX + math.cos(math.radians(angleA))*sideb
				CY = AY + math.sin(math.radians(angleA))*sideb
				
				com('draw (%g,%g) -- (%g,%g) -- (%g,%g) -- cycle' % (AX, AY, BX, BY, CX, CY), doc = doc)
				#label ABC
				labelAngle(doc = doc, annotation = A, coordinates = [AX, AY], position = 'left')
				labelAngle(doc = doc, annotation = B, coordinates = [BX, BY], position = 'right')
				labelAngle(doc = doc, annotation = C, coordinates = [CX, CY], position = 'top')
				
				#draw bisector AD with D using A/2
				ADValue = ( ( sideb*sidec / (sideb + sidec)**2 ) * ( (sideb + sidec)**2 - sidea**2 ) ) ** (1/2)
				DX = math.cos(math.radians(angleA/2))*ADValue
				DY = math.sin(math.radians(angleA/2))*ADValue
				com('draw (%g,%g) -- (%g,%g)' % (AX, AY, DX, DY), doc = doc)
				labelAngle(doc = doc, annotation = D, coordinates = [DX, DY], position = 'top right')

				#draw bisector BE with E using B/2
				BEValue = ( ( sidea*sidec / (sidea + sidec)**2 ) * ( (sidea + sidec)**2 - sideb**2 ) ) ** (1/2)
				EX = BX - math.cos(math.radians(angleB/2))*BEValue
				EY = BY + math.sin(math.radians(angleB/2))*BEValue
				com('draw (%g,%g) -- (%g,%g)' % (BX, BY, EX, EY), doc = doc)
				labelAngle(doc = doc, annotation = E, coordinates = [EX, EY], position = 'top left')
				
				#draw bisector CF with F using C/2
				CFValue = ( ( sidea*sideb / (sidea + sideb)**2 ) * ( (sidea + sideb)**2 - sidec**2 ) ) ** (1/2)
				#we can use law of sines to find AG, then law of sines to find AF
				AG = sidec/math.sin(math.radians(180-angleA/2-angleB/2))*math.sin(math.radians(angleB/2))
				#AFG = (180 - angleC/2 - angleA)
				#AGF = 180 - angleA/2 - AFG
				AF = AG/math.sin(math.radians(180-angleA-angleC/2))*math.sin(math.radians(180-angleA/2-(180-angleC/2-angleA)))
				FX = AX + AF
				FY = AY

				com('draw (%g,%g) -- (%g,%g)' % (CX, CY, FX, FY), doc = doc)
				labelAngle(doc = doc, annotation = F, coordinates = [FX, FY], position = 'bottom')

				#draw incenter using
				GX = ( sidea*AX + sideb*BX + sidec*CX ) / ( sidea + sideb + sidec ) 
				GY = ( sidea*AY + sideb*BY + sidec*CY ) / ( sidea + sideb + sidec ) 
				labelAngle(doc = doc, annotation = G, coordinates = [GX, GY], position = 'top right')
				


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion1():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
		self.topic = [['Angles']]
		self.skills = [['Understand that parallel lines create alternate interior angles', 'Understand that angles on a line add up to 180', 'Understand that angles in a triangle add up to 180']]
	def addQuestion(self, doc = None):
		points = pointsForDiagram()			
		#see testQuestion1.png for shits
		AGHValue = random.randint(45,135)
		BGHValue = 180 - AGHValue

		#divide GHC into 2 substantial parts
		GHCValue = BGHValue
		GHIValue = random.randint(GHCValue//3,GHCValue//2)
		CHIValue = GHCValue - GHIValue

		GIHValue = CHIValue

		#now all the values are set up, make sure strings are good
		AGHString = angleName([points['A'], points['G'], points['H']])
		BGHString = angleName([points['B'], points['G'], points['H']])		#draw line

		GHCString = angleName([points['G'], points['H'], points['C']])
		GHIString = angleName([points['G'], points['H'], points['I']])
		CHIString = angleName([points['C'], points['H'], points['I']])
		
		GIHString = angleName([points['G'], points['I'], points['H']])
		
		givenString = r'$m\angle{%s}=%d^{\circ}$ and $m\angle{%s}=%d^{\circ}$' % (BGHString, BGHValue, CHIString, CHIValue)
		findString = r'$m\angle{%s}$' % (GHIString)
		self.answer = r'$m\angle{%s}=%d^{\circ}$' % (GHIString, GHIValue)

		questionString = r'In the diagram below $\overline{%s}$ || $\overline{%s}$, ' % (angleName([points['A'],points['B']]), angleName([points['C'], points['D']])) + givenString + ', what is the measure of ' + findString + '?' + ' Explain your reasoning.'

		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		line1Length = 10
		#picLength = sizingXY(minn = -line1Length, maxx = line1Length, size = 'medium')
		with doc.create(TikZ(options='rotate=%d' % (self.rotate))):
			
			#Draw bottom line CD
			HX = 0
			HY = 0
			CX = HX-line1Length/2
			CY = HY
			DX = HX+line1Length/2
			DY = HY
			com('draw (%g,%g) -- (%g,%g)' % (CX, CY, DX, DY), doc = doc)
			labelAngle(doc = doc, annotation = points['C'], coordinates = [CX, CY], position = 'left')
			labelAngle(doc = doc, annotation = points['D'], coordinates = [DX, DY], position = 'right')

			#draw second line above AB
			GHLength = line1Length/4 # try this out
			GX = HX - math.cos(math.radians(GHCValue))*GHLength
			GY = HY + math.sin(math.radians(GHCValue))*GHLength
			AX = GX - line1Length/2
			AY = GY
			BX = GX + line1Length/2
			BY = GY
			com('draw (%g,%g) -- (%g,%g)' % (AX, AY, BX, BY), doc = doc)
			labelAngle(doc = doc, annotation = points['A'], coordinates = [AX, AY], position = 'left')
			labelAngle(doc = doc, annotation = points['B'], coordinates = [BX, BY], position = 'right')

			#draw transversal EF
			EGLength = GHLength/3
			EX = GX - math.cos(math.radians(GHCValue))*EGLength
			EY = GY + math.sin(math.radians(GHCValue))*EGLength

			HFLength = EGLength
			FX = HX + math.cos(math.radians(GHCValue))*HFLength
			FY = HY - math.sin(math.radians(GHCValue))*HFLength
			com('draw (%g,%g) -- (%g,%g)' % (EX, EY, FX, FY), doc = doc) #E TO F

			labelAngle(doc = doc, annotation = points['E'], coordinates = [EX, EY], position = 'top')
			labelAngle(doc = doc, annotation = points['F'], coordinates = [FX, FY], position = 'bottom')

			#label G and H dependent on angle values???
			labelAngle(doc = doc, annotation = points['G'], coordinates = [GX, GY], position = 'top right')
			labelAngle(doc = doc, annotation = points['H'], coordinates = [HX, HY], position = 'top right')
			
			#draw GHLength bottom is (0,0), top is (cos(GHC)*transversal length)

			#draw IH, label I
			IGHValue = 180-GHCValue
			IHLength = GHLength/math.sin(math.radians(GIHValue))*math.sin(math.radians(IGHValue))
			IX = GX - math.cos(math.radians(CHIValue))*IHLength
			IY = GY
			com('draw (%g,%g) -- (%g,%g)' % (IX, IY, HX, HY), doc = doc) #IH
			labelAngle(doc = doc, annotation = points['I'], coordinates = [IX, IY], position = 'top')

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion2():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		#triangle A(left), B(right), C(top)
		#exteriorAngle outside is D
		points = pointsForDiagram()
		DBCValue = random.choice([x for x in range (122,150,2)])
		DBCString = angleName([points['D'], points['B'], points['C']])

		ABCValue = 180 - DBCValue
		
		BCAValue = 180 - 2*ABCValue
		BCAString = angleName([points['B'], points['C'], points['A']])
		ACString = angleName([points['A'], points['C']])
		BCString = angleName([points['B'], points['C']])

		givenString = r'$m\angle{%s}=%d^{\circ}$ and $\overline{%s}\cong\overline{%s}$' % (DBCString, DBCValue, ACString, BCString)
		findString = r'$\angle{%s}$' % (BCAString)
		self.answer = r'$\angle{%s}=%d^{\circ}$' % (BCAString, BCAValue)

		questionString = 'In the diagram below, ' + givenString + '.'
		doc.append(NoEscape(questionString))
		doc.append(NewLine())

		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			#draw line (0,0) is A
			AX = 0
			AY = 0

			ACValue = 3
			CX = AX + math.cos(math.radians(ABCValue))*ACValue
			CY = AY + math.sin(math.radians(ABCValue))*ACValue

			BX = AX + 2*math.cos(math.radians(ABCValue))*ACValue
			BY = AY

			DX = BX + ACValue
			DY = AY

			#draw triangle, labe,
			com('draw (%g,%g) -- (%g,%g) -- (%g,%g) -- cycle' % (AX, AY, BX, BY, CX, CY), doc = doc)
			labelAngle(doc = doc, annotation = points['A'], coordinates = [AX, AY], position = 'left')
			labelAngle(doc = doc, annotation = points['B'], coordinates = [BX, BY], position = 'bottom')
			labelAngle(doc = doc, annotation = points['C'], coordinates = [CX, CY], position = 'top')

			#draw extended line, and label D
			com('draw (%g,%g) -- (%g,%g)' % (BX, BY, DX, DY), doc = doc)
			labelAngle(doc = doc, annotation = points['D'], coordinates = [DX, DY], position = 'right')

		
		questionString2 = 'What is the measure of ' + findString + '?'
		doc.append(NewLine())
		doc.append(NoEscape(questionString2))
		doc.append(NewLine())
		if self.typeOfQuestionChosen == 'MC':
			correct = r'$%d^{\circ}$' % (BCAValue)
			baseAngle = r'$%d^{\circ}$' % (180 - DBCValue)
			givenAngle = r'$%d^{\circ}$' % DBCValue
			wrongAngle = r'$%g^{\circ}$' % abs((BCAValue/2))
			newChoices = multipleChoice(doc = doc, choices = [correct, baseAngle, givenAngle, wrongAngle])
			self.answer = r'Choice %d: $\angle{%s}=%d^{\circ}$' % (newChoices.index(correct)+1, BCAString, BCAValue) 
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion3():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		points = pointsForDiagram()			
		#see testQuestion1.png for shits
		AGHValue = random.randint(45,135)
		GHCValue = AGHValue
		EGBValue = AGHValue

		#divide GHC into 2 substantial parts
		CHFValue = EGBValue

		#now all the values are set up, make sure strings are good
		AGHString = angleName([points['A'], points['G'], points['H']])
		EGBString = angleName([points['E'], points['G'], points['B']])
		CHFString = angleName([points['C'], points['H'], points['F']])

		givenString = r'$m\angle{%s}=%d^{\circ}$' % (EGBString, EGBValue)
		findString = r'$m\angle{%s}$' % (CHFString)
		self.answer = r'$m\angle{%s}=%d^{\circ}$' % (CHFString, CHFValue)

		questionString = r'In the diagram below $\overline{%s}$ || $\overline{%s}$, ' % (angleName([points['A'],points['B']]), angleName([points['C'], points['D']])) + givenString + ', what is the measure of ' + findString + '?' + ' Explain your reasoning.'

		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		line1Length = 3
		picLength = sizingXY(minn = -line1Length, maxx = line1Length, size = 'large')
		with doc.create(TikZ(options='rotate=%d, x=%dcm, y=%dcm, scale=3' % (self.rotate, picLength, picLength))):
			
			#Draw bottom line CD
			HX = 0
			HY = 0
			CX = HX-line1Length/2
			CY = HY
			DX = HX+line1Length/2
			DY = HY
			com('draw (%g,%g) -- (%g,%g)' % (CX, CY, DX, DY), doc = doc)
			labelAngle(doc = doc, annotation = points['C'], coordinates = [CX, CY], position = 'left')
			labelAngle(doc = doc, annotation = points['D'], coordinates = [DX, DY], position = 'right')

			#draw second line above AB
			GHLength = line1Length # try this out
			GX = HX - math.cos(math.radians(GHCValue))*GHLength
			GY = HY + math.sin(math.radians(GHCValue))*GHLength
			AX = GX - line1Length/2
			AY = GY
			BX = GX + line1Length/2
			BY = GY
			com('draw (%g,%g) -- (%g,%g)' % (AX, AY, BX, BY), doc = doc)
			labelAngle(doc = doc, annotation = points['A'], coordinates = [AX, AY], position = 'left')
			labelAngle(doc = doc, annotation = points['B'], coordinates = [BX, BY], position = 'right')

			#draw transversal EF
			EGLength = GHLength/3
			EX = GX - math.cos(math.radians(GHCValue))*EGLength
			EY = GY + math.sin(math.radians(GHCValue))*EGLength

			HFLength = EGLength
			FX = HX + math.cos(math.radians(GHCValue))*HFLength
			FY = HY - math.sin(math.radians(GHCValue))*HFLength
			com('draw (%g,%g) -- (%g,%g)' % (EX, EY, FX, FY), doc = doc) #E TO F

			labelAngle(doc = doc, annotation = points['E'], coordinates = [EX, EY], position = 'top')
			labelAngle(doc = doc, annotation = points['F'], coordinates = [FX, FY], position = 'bottom')

			#label G and H dependent on angle values???
			labelAngle(doc = doc, annotation = points['G'], coordinates = [GX, GY], position = 'top right')
			labelAngle(doc = doc, annotation = points['H'], coordinates = [HX, HY], position = 'top right')
			
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion4():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		#given whole angle, find part
		ABCD, EFGH, IJKL = annotatePolygon(vertices = 4, prime = False)
		A = ABCD[0]
		B = ABCD[1]
		C = ABCD[2]
		D = ABCD[3]

		E = EFGH[0]
		F = EFGH[1]
		G = EFGH[2]
		H = EFGH[3]

		#angle is ABC, line that bisects is DB

		#draw an angle between (45, 135)
		angle = random.randint(45, 135)

		rayLength = 3

		anglePart = angle/2

		rayLength = 5
		#needs to be bigger
		#3 angle bisectors and triangle may be isosceles
		#triangle leftA, rightB, topC make it not equilateral and not isosceles
		angleA = random.choice([x for x in range(30,80,2)])
		angleB = random.choice([x for x in range(30,80,2) if x not in [angleA]])
		angleC = 180 - angleA - angleB
		while angleC == angleB or angleC == angleA or angleB == angleA:
			print('yeahea')
			angleA = random.choice([x for x in range(30,80,2)])
			angleB = random.choice([x for x in range(30,80,2) if x not in [angleA]])
			angleC = 180 - angleA - angleB

		#AD bisector
		AD = (A+D)[::random.choice([-1,1])]
		#BE biesctor
		BE = (B+E)[::random.choice([-1,1])]
		#CF bisector
		CF = (C+F)[::random.choice([-1,1])]

		#AB side
		AB = (A+B)[::random.choice([-1,1])]

		#AC side
		AC = (A+C)[::random.choice([-1,1])]

		#BC side
		BC = (B+C)[::random.choice([-1,1])]

		#intersetction G
		ABG = (A+B+G)[::random.choice([-1,1])]
		CGB = (C+G+B)[::random.choice([-1,1])]

		#given one whole angle A, one part of one B, find central angle CGB
		givenString = r'$m\angle{%s}=%g^{\circ}$, $m\angle{%s}=%g^{\circ}$, $\overline{%s}$ bisects $\angle{%s}$, $\overline{%s}$ bisects $\angle{%s}$, and $\overline{%s}$ bisects $\angle{%s}$' % ( C+A+B, angleA, ABG, angleB/2, AD, C+A+B, BE, C+B+A, CF, A+C+B )
		
		findString = r'$\angle{%s}$' % ( CGB )
		self.answer =  r'$\angle{%s}=%g^{\circ}$' % ( CGB, (180-angleB/2-angleC/2))

		questionString = 'In the diagram below, ' + givenString + '.'
		questionString2 =  'What is the measure of ' + findString + '?'
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		
		with doc.create(TikZ(options='rotate=%d' % self.rotate)):
			sidec = rayLength
			#law of sines for sidea
			#sidea/sin(A) = sideb/sin(B), so sidea/sin(A)*sin(B)
			sidea = sidec/math.sin(math.radians(angleC))*math.sin(math.radians(angleA))
			sideb = sidec/math.sin(math.radians(angleC))*math.sin(math.radians(angleB))

			#draw triangle ABC
			AX = 0
			AY = 0
			BX = sidec
			BY = 0
			CX = AX + math.cos(math.radians(angleA))*sideb
			CY = AY + math.sin(math.radians(angleA))*sideb
			
			com('draw (%g,%g) -- (%g,%g) -- (%g,%g) -- cycle' % (AX, AY, BX, BY, CX, CY), doc = doc)
			#label ABC
			labelAngle(doc = doc, annotation = A, coordinates = [AX, AY], position = 'left')
			labelAngle(doc = doc, annotation = B, coordinates = [BX, BY], position = 'right')
			labelAngle(doc = doc, annotation = C, coordinates = [CX, CY], position = 'top')
			
			#draw bisector AD with D using A/2
			ADValue = ( ( sideb*sidec / (sideb + sidec)**2 ) * ( (sideb + sidec)**2 - sidea**2 ) ) ** (1/2)
			DX = math.cos(math.radians(angleA/2))*ADValue
			DY = math.sin(math.radians(angleA/2))*ADValue
			com('draw (%g,%g) -- (%g,%g)' % (AX, AY, DX, DY), doc = doc)
			labelAngle(doc = doc, annotation = D, coordinates = [DX, DY], position = 'top right')

			#draw bisector BE with E using B/2
			BEValue = ( ( sidea*sidec / (sidea + sidec)**2 ) * ( (sidea + sidec)**2 - sideb**2 ) ) ** (1/2)
			EX = BX - math.cos(math.radians(angleB/2))*BEValue
			EY = BY + math.sin(math.radians(angleB/2))*BEValue
			com('draw (%g,%g) -- (%g,%g)' % (BX, BY, EX, EY), doc = doc)
			labelAngle(doc = doc, annotation = E, coordinates = [EX, EY], position = 'top left')
			
			#draw bisector CF with F using C/2
			CFValue = ( ( sidea*sideb / (sidea + sideb)**2 ) * ( (sidea + sideb)**2 - sidec**2 ) ) ** (1/2)
			#we can use law of sines to find AG, then law of sines to find AF
			AG = sidec/math.sin(math.radians(180-angleA/2-angleB/2))*math.sin(math.radians(angleB/2))
			#AFG = (180 - angleC/2 - angleA)
			#AGF = 180 - angleA/2 - AFG
			AF = AG/math.sin(math.radians(180-angleA-angleC/2))*math.sin(math.radians(180-angleA/2-(180-angleC/2-angleA)))
			FX = AX + AF
			FY = AY

			com('draw (%g,%g) -- (%g,%g)' % (CX, CY, FX, FY), doc = doc)
			labelAngle(doc = doc, annotation = F, coordinates = [FX, FY], position = 'bottom')

			#draw incenter using
			GX = ( sidea*AX + sideb*BX + sidec*CX ) / ( sidea + sideb + sidec ) 
			GY = ( sidea*AY + sideb*BY + sidec*CY ) / ( sidea + sideb + sidec ) 
			labelAngle(doc = doc, annotation = G, coordinates = [GX, GY], position = 'top right')
	
		if self.typeOfQuestionChosen == 'MC':
			doc.append(NewLine())
			doc.append(NoEscape(questionString2))
			doc.append(NewLine())

			correct = r'$%g^{\circ}$' % (180 - angleB/2 - angleC/2)
			choice1 = r'$%g^{\circ}$' % (180 - angleA - angleB/2)
			choice2 = r'$%g^{\circ}$' % (180 - angleA - angleB)
			choice3 = r'$%g^{\circ}$' % (angleA/2+angleB/2)
			newChoices = multipleChoice(doc = doc, choices = [correct, choice1, choice2, choice3])
			self.answer = r'$Choice %d: \angle{%s}=%g^{\circ}$' % (newChoices.index(correct)+1, CGB, (180-angleB/2-angleC/2))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion5():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		#right triangle but not on axes, but vertical and horizontal
		points = pointsForDiagram()
		#ABC where B = 90
		xChange = random.randint(1,5)
		yChange = random.choice([x for x in range(1,6) if x not in [xChange]])
		AX = random.randint(-5,5)
		AY = random.randint(-5,5)

		BX = AX + xChange
		BY = AY

		CX = AX
		CY = AY + yChange

		questionString = r'$\bigtriangleup{%s}$ has the following coordinates: $%s(%d,%d)$, $%s(%d,%d)$, and $%s(%d,%d)$.' % (points['A']+points['B']+points['C'], points['A'], AX, AY, points['B'], BX, BY, points['C'], CX, CY)
		questionString2 = r'What type of triangle is $\bigtriangleup{%s}$?' % (points['A']+points['B']+points['C'])

		doc.append(NoEscape(questionString))
		doc.append(NewLine())

		lengthForPic = sizingXY(minn = -10, maxx = 10, size = 'medium')
		with doc.create(TikZ(options='x=%gcm, y=%gcm' % (lengthForPic, lengthForPic))):
			grid(doc = doc)
		doc.append(NewLine())
		doc.append(NoEscape(questionString2))
		doc.append(NewLine())

		newChoices = multipleChoice(doc = doc, choices = ['Right', 'Isosceles', 'Obtuse','Acute'])
		self.answer = r'Choice %d: %s' % (newChoices.index('Right')+1, 'Right')

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion6():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		points = pointsForDiagram()
		CDValue = random.randint(2,10)
		ABValue = CDValue+1

		ABString = angleName([points['A'], points['B']])
		CDString = angleName([points['C'], points['D']])

		questionString = r'In the diagram below, $\overline{%s}$ bisects $\overline{%s}$ at %s.' % (ABString, CDString, points['E'])
		questionString2 = 'Which of the following statements is always true?'

		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		lengthForPic = sizingXY(minn = -ABValue, maxx = ABValue, size = 'medium')
		with doc.create(TikZ(options='x=%gcm, y=%gcm' % (lengthForPic, lengthForPic))):
			#draw line CD
			com('draw (%g,%g) -- (%g,%g)' % (-CDValue/2, 0, CDValue/2, 0), doc = doc)
			#draw line AB
			com('draw (%g,%g) -- (%g,%g)' % (0, ABValue/2, 0, -ABValue/2), doc = doc)

			#label ABCDE
			labelAngle(doc = doc, annotation = points['A'], coordinates = [0, ABValue/2], position = 'top')
			labelAngle(doc = doc, annotation = points['B'], coordinates = [0, -ABValue/2], position = 'bottom')
			labelAngle(doc = doc, annotation = points['C'], coordinates = [-CDValue/2, 0], position = 'left')
			labelAngle(doc = doc, annotation = points['D'], coordinates = [CDValue/2, 0], position = 'right')
			labelAngle(doc = doc, annotation = points['E'], coordinates = [0, 0], position = 'top right')
		if self.typeOfQuestionChosen == 'MC':
			doc.append(NewLine())
			doc.append(NoEscape(questionString2))
			doc.append(NewLine())
			correct = r'$\overline{%s}\cong\overline{%s}$' % (points['C']+points['E'], points['E']+points['D'])
			choice1 = r'$\overline{%s}\cong\overline{%s}$' % (points['A']+points['B'], points['C']+points['D'])
			choice2 = r'$\overline{%s}\cong\overline{%s}$' % (points['A']+points['E'], points['E']+points['B'])
			choice3 = r'$\overline{%s}\cong\overline{%s}$' % (points['C']+points['E'], points['E']+points['B'])
			newChoices = multipleChoice(doc = doc, choices = [correct, choice1, choice2, choice3])
			self.answer = r'Choice %d: %s' % (newChoices.index(correct)+1, correct)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion7():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		#Define some letters
		ABCD, EFGH, IJKL = annotatePolygon(vertices = 4, prime = False)

		#hourglass figure - top line ABCD, F in middle, bottom line GHIJ

		#create bottom triangle with line, then rotate all the points around the top (vertex)
		
		leftBaseAngle = random.randint(30,70)
		rightBaseAngle = random.choice([x for x in range(30,70) if x not in [leftBaseAngle]]) #ensuring not isosceles or equilateral

		vertexAngle = 180 - leftBaseAngle - rightBaseAngle

		#define angle names when I need them
		
		#for isosceles
		#[::random.choice([1,-1])] for randomized etc

		#law of sines to find left side distance
		#5/sin(vertexAngle) = x/sin(oppangle)

		#dist between base vertices = 5
		#find leftDistance

		#sometimes when base angles are really small or large, triangle gets crazy, so
		if leftBaseAngle < 31 or rightBaseAngle < 31 or leftBaseAngle > 70 or rightBaseAngle > 70:
			baseDistance = 2
		else:
			baseDistance = 3

		leftDistance = (baseDistance / math.sin(math.radians(vertexAngle))) * math.sin(math.radians(rightBaseAngle))
		rightDistance = (baseDistance / math.sin(math.radians(vertexAngle))) * math.sin(math.radians(leftBaseAngle))
	
		#givenInfo = top left angle, which is rightBaseAngle CBE
		#givenInfo2 = bottom left angle, which is leftBaseAngle EGH
		#find Info = exterior middle angle, which is 180-vertexAngle BEG
		givenInfo = r'${m\angle{%s}=%d^{\circ}}$ and ${m\angle{%s}=%d^{\circ}}$' % ((ABCD[2]+ABCD[1]+EFGH[0])[::random.choice([1,-1])], rightBaseAngle, (EFGH[0]+EFGH[2]+EFGH[3])[::random.choice([1,-1])], leftBaseAngle) 
		findInfo = r'${\angle{%s}}$' % (ABCD[1]+EFGH[0]+EFGH[2])[::random.choice([1,-1])]
		self.answer = r'${\angle{%s}=%d^{\circ}}$' % ((ABCD[1]+EFGH[0]+EFGH[2])[::random.choice([1,-1])], 180-vertexAngle)

		questionString = r'In the diagram below $\overline{%s}||\overline{%s}$, ' % (ABCD[0]+ABCD[3], EFGH[1]+IJKL[0]) 
		questionString += givenInfo + '.'
		questionString2 = 'What is the measure of ' + findInfo + '?'

		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		picLength = sizingXY(minn = -baseDistance, maxx = baseDistance, size = 'medium')
		with doc.create(TikZ(options='rotate=%d, x=%gcm, y=%gcm' % (self.rotate, picLength, picLength))):
			#draw bottom line, with 4 nodes ABCD, EFGH, IJKL
			#so bottom line has a distance of 1/2 base distance in either direction etc
			com('draw (0,0) -- (%g,0)' % (.5*baseDistance+baseDistance+.5*baseDistance), doc = doc)
			#F bottom left
			com('node at (%g,0) {%s}' % (-.2, EFGH[1]), doc = doc)
			xs = []
			xs.append(0)
			ys = []
			ys.append(0)
			#G bottom left of triangle
			com('node at (%g,%g) {%s}' % (.5*baseDistance, -.2, EFGH[2]), doc = doc)
			xs.append(.5*baseDistance)
			ys.append(0)
			#H bottom right of triangle
			com('node at (%g,%g) {%s}' % (1.5*baseDistance, -.2, EFGH[3]), doc = doc)
			xs.append(1.5*baseDistance)
			ys.append(0)
			#I bottom right
			com('node at (%g,0) {%s}' % (2*baseDistance+.2, IJKL[0]), doc = doc)
			xs.append(2*baseDistance)
			ys.append(0)

			#middle E
			com('node at (%g,%g) {%s}' % (.5*baseDistance+math.cos(math.radians(leftBaseAngle))*leftDistance-.2, math.sin(math.radians(leftBaseAngle))*leftDistance, EFGH[0]), doc = doc)

			#now i need to rotate all this shit
			rotatedX, rotatedY = rotateCoordinatesAsList(xs, ys, 'clockwise', 180, centerX = .5*baseDistance+math.cos(math.radians(leftBaseAngle))*leftDistance, centerY = math.sin(math.radians(leftBaseAngle))*leftDistance)

			#so now rotatedX is right to left 

			#top line
			com('draw (%g,%g) -- (%g,%g)' % (rotatedX[3], rotatedY[3], rotatedX[0], rotatedY[0]), doc = doc)
			#draw diagonals which make 2 triangles
			com('draw (%g,%g) -- (%g,%g)' % (rotatedX[1], rotatedY[1], xs[1], ys[1]), doc = doc)
			com('draw (%g,%g) -- (%g,%g)' % (rotatedX[2], rotatedY[2], xs[2], ys[2]), doc = doc)

			#A
			com('node at (%g,%g) {%s}' % (rotatedX[3]-.2, rotatedY[3], ABCD[0]), doc = doc)
			#B
			com('node at (%g,%g) {%s}' % (rotatedX[2], rotatedY[3]+.2, ABCD[1]), doc = doc)
			#C
			com('node at (%g,%g) {%s}' % (rotatedX[1], rotatedY[1]+.2, ABCD[2]), doc = doc)
			#D
			com('node at (%g,%g) {%s}' % (rotatedX[0]+.2, rotatedY[0], ABCD[3]), doc = doc)
		if self.typeOfQuestionChosen == 'MC':
			doc.append(NewLine())
			correct = r'$%d^{\circ}$' % (180-vertexAngle)
			choice1 = r'$%d^{\circ}$' % (vertexAngle)
			choice2 = r'$%d^{\circ}$' % (rightBaseAngle)
			choice3 = r'$%d^{\circ}$' % (180-leftBaseAngle)
			doc.append(NoEscape(questionString2))
			doc.append(NewLine())
			newChoices = multipleChoice(doc = doc, choices = [correct, choice1, choice2, choice3])
			self.answer = r'Choice %d: %s' % (newChoices.index(correct)+1, self.answer)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion8():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		equilateralDistance = 3
		points = pointsForDiagram()

		#triangle A left, B, right, D top
		#then triangle B, C, D
		BDCString = angleName([points['B'], points['D'], points['C']])

		self.answer = r'$m\angle{%s}=%d^{\circ}$' % (BDCString, (30))

		questionString = r'In the diagram below of $\bigtriangleup{%s}$, $\overline{%s}$ intersects $\overline{%s}$, such that $\bigtriangleup{%s}$ is an equilateral triangle and $\overline{%s}\cong\overline{%s}$. What is the measure of $\angle{%s}$?' % (points['A']+points['D']+points['C'], points['D']+points['B'], points['A']+points['C'], points['A']+points['B']+points['D'], points['D']+points['B'], points['B']+points['C'], BDCString)
		
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		picLength = sizingXY(minn = -equilateralDistance, maxx = equilateralDistance, size = 'large')
		with doc.create(TikZ(options='rotate=%d, x=%gcm, y=%gcm' % (self.rotate, picLength, picLength))):
			#draw line on bottom
			DX = math.cos(math.radians(60))*equilateralDistance
			DY = math.sin(math.radians(60))*equilateralDistance
			BX = equilateralDistance
			BY = 0
			CX = equilateralDistance*2
			CY = 0
			com('draw (%g,%g) -- (%g,%g)' % (0, 0, CX, 0), doc = doc)
			#draw triangle
			com('draw (%g,%g) -- (%g, %g) -- (%g, %g) -- cycle'  % (0, 0, DX, DY, BX, BY), doc = doc)

			#draw 2nd triangle (connect D to C)
			com('draw (%g, %g) -- (%g, %g)' % ( DX, DY, CX, CY ), doc = doc)

			#label ABCD
			labelAngle(doc = doc, annotation = points['A'], coordinates = [0,0], position = 'left')
			labelAngle(doc = doc, annotation = points['B'], coordinates = [BX,BY], position = 'bottom')
			labelAngle(doc = doc, annotation = points['C'], coordinates = [CX,CY], position = 'right')
			labelAngle(doc = doc, annotation = points['D'], coordinates = [DX,DY], position = 'top')

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion9():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		smallBisectedAngleValue = random.choice([x for x in range(30,60,2)])
		#ax + b = smallBisected Angle
		#cx + d = smallBisected Angle
		#ax + b = cx + d
		#x(a-c) = d-b
		b = random.choice([x for x in range(-10,10) if x not in [0]])
		#define a based on smallBisectedAngle-b, factors etc
		factors = []
		for item in range(1, smallBisectedAngleValue-b+1):
			if (smallBisectedAngleValue-b) % item == 0:
				factors.append(item)
		a = random.choice(factors)
		x = int((smallBisectedAngleValue-b)/a)

		#x(a-c?) = d?-b
		c = random.choice([x for x in range(-10, 10) if x not in [0, a]])
		d = x*(a-c) + b
		print(a,b,c,d,x)
		#angle ABC with BE bisector
		points = pointsForDiagram()
		ABCString = angleName([points['A'], points['B'], points['C']])
		DBString = angleName([points['D'], points['B']])
		which = random.choice(['A','C'])
		findString = angleName([points['D'], points['B'], points[which]])

		questionString = r'In the diagram below, $\overline{%s}$ bisects $\angle{%s}$. The $m\angle{%s} = %sx%s$ and $m\angle{%s} = %sx%s$. ' % (DBString, ABCString, points['D']+points['B']+points['C'], equationFormatting(value = a, nextToVariable = True, inBetween = False), equationFormatting(value = b, nextToVariable = False, inBetween = True), points['A']+points['B']+points['D'], equationFormatting(value = c, nextToVariable = True, inBetween = False), equationFormatting(value = d, nextToVariable = False, inBetween = True))
		questionString2 = r'What is the measure of $\angle{%s}$?' % findString
		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		baseDistance = 3
		picLength = sizingXY(minn = -baseDistance, maxx = baseDistance, size = 'medium')
		with doc.create(TikZ(options='rotate=%d, x=%gcm, y=%gcm' % (self.rotate, picLength, picLength))):
			#draw a line 
			rayLength = baseDistance
			#draw angle ABC with DB as bisector
			AX = math.cos(math.radians(smallBisectedAngleValue*2))*rayLength
			AY = math.sin(math.radians(smallBisectedAngleValue*2))*rayLength
			com('draw (%g,%g) -- (%g,%g) -- (%g,%g)' % (AX
			, AY, 0, 0, rayLength, 0), doc = doc)

			DX = math.cos(math.radians(smallBisectedAngleValue))*rayLength
			DY = math.sin(math.radians(smallBisectedAngleValue))*rayLength

			#draw bisector
			com('draw (%g,%g) -- (%g,%g)' % (0, 0, DX,
			DY), doc = doc)

			#label angles
			labelAngle(doc = doc, annotation = points['A'], coordinates = [AX, AY], position = 'top')
			labelAngle(doc = doc, annotation = points['B'], coordinates = [0,0], position = 'bottom')
			labelAngle(doc = doc, annotation = points['C'], coordinates = [rayLength, 0], position = 'right')

			labelAngle(doc = doc, annotation = points['D'], coordinates = [DX, DY], position  = '{place}'.format(place = 'right' if smallBisectedAngleValue<=45 else 'top'))
		doc.append(NewLine())
		if self.typeOfQuestionChosen == 'MC':
			correct = r'$%d^{\circ}$' % (smallBisectedAngleValue)
			choice1 = r'$%d^{\circ}$' % (x)
			choice2 = r'$%d^{\circ}$' % (smallBisectedAngleValue*2)
			choice3 = r'$%d^{\circ}$' % (180-(a+c)*x - b - d)
			doc.append(NewLine())
			doc.append(NoEscape(questionString2))
			doc.append(NewLine())
			newChoices = multipleChoice(doc = doc, choices = [correct, choice1, choice2, choice3])
			self.answer = r'Choice %d: %s' % (newChoices.index(correct)+1, correct)
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class testQuestion10():
	def __init__(self, typeOfQuestion = ['MC','SA'], rotate = 0):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.rotate = rotate
	def addQuestion(self, doc = None):
		points = pointsForDiagram()
		questionString = r'Given $\overline{%s}$ below, construct a perpendicular bisector to $\overline{%s}$ using your compass and straightedge.' % (points['A']+points['B'], points['A']+points['B'])
		doc.append(NoEscape(questionString))
		doc.append(VerticalSpace('3in'))
		doc.append(NewLine())
		
		baseDistance = 8
		picLength = sizingXY(minn = -baseDistance, maxx = baseDistance, size = 'medium')
		with doc.create(TikZ(options='rotate=%d, x=%gcm, y=%gcm, scale = 3' % (self.rotate, picLength, picLength))):
			#draw a line lol
			com('draw (%g,%g) -- (%g,%g)' % (0, 0, baseDistance, 0), doc = doc)
			com('draw[black,fill=black] (%g,%g) circle (.05)' % (0, 0), doc = doc)
			com('draw[black,fill=black] (%g,%g) circle (.05)' % (baseDistance, 0), doc = doc)
			labelAngle(doc = doc, annotation = points['A'], coordinates = [0,0], position = 'left')
			labelAngle(doc = doc, annotation = points['B'], coordinates = [baseDistance, 0], position = 'right')
		self.answer = 'WOW that construction!'	
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))


##############Transformations#################
class performTranslation():
	def __init__(self, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, xChangeAmount = ['+','-','0'], yChangeAmount = ['+','-','0'], overlap = False, polygonType = ['right triangle'], polygonSize = ['small','medium', 'large'], prime = True, algebraic = False):
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.xChangeAmount = xChangeAmount
		self.yChangeAmount = yChangeAmount
		self.overlap = overlap
		self.polygonType = polygonType
		self.polygonSize = polygonSize
		self.prime = prime
		self.algebraic = algebraic
	def addQuestion(self, doc = None):
		polygonTypeChosen = random.choice(self.polygonType)
		polygonSizeChosen = random.choice(self.polygonSize)
		
		x, y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = [polygonTypeChosen], polygonSize = [polygonSizeChosen])

		x, y, newX, newY, xTransVal, yTransVal = translation(xMinGraph = self.xMinGraph, xMaxGraph = self.xMaxGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, xChangeAmount = self.xChangeAmount, yChangeAmount = self.yChangeAmount, overlap = self.overlap, x = x, y = y)

		ABC, DEF, blah = annotatePolygon(vertices = len(x), prime = self.prime)
		shapeName = ''
		for letter in [ABC[x] for x in range(0, len(x))]:
			shapeName += letter
		shapeName2 = ''
		for letter in [DEF[x] for x in range(0, len(x))]:
			shapeName2 += letter
		#graph polygon
		if self.algebraic == False:
			if xTransVal < 0:
				transXString = '%d left' % abs(xTransVal)
			elif xTransVal > 0:
				transXString = '%d right' % abs(xTransVal)
			else:
				transXString = ''
			if yTransVal < 0:
				transYString = '%d down' % abs(yTransVal)
			elif yTransVal > 0:
				transYString = '%d up' % abs(yTransVal)
			else:
				transYString = ''

			if xTransVal == 0:
				transString = transYString
			elif yTransVal == 0:
				transString = transXString
			else:
				transString = transXString + ' and ' + transYString
			questionString = r'Given the graph below of %s $%s$, graph %s $%s$ after a translation {%s}.' % ( polygonTypeChosen, shapeName, polygonTypeChosen, shapeName2,  transString)
		else:
			if xTransVal < 0:
				transXString = '(x-%d,' % abs(xTransVal)
			elif xTransVal > 0:
				transXString = '(x+%d,' % abs(xTransVal)
			else:
				transXString = '(x,'
			if yTransVal < 0:
				transYString = 'y-%d)' % abs(yTransVal)
			elif yTransVal > 0:
				transYString = 'y+%d)' % abs(yTransVal)
			else:
				transYString = 'y)'

			transString = r'${(x,y)\rightarrow%s}$' % (transXString+transYString)
			questionString = r'Given the graph below of %s $%s$, graph %s $%s$ after a translation %s.' % ( polygonTypeChosen, shapeName, polygonTypeChosen, shapeName2,  transString)
		doc.append(NoEscape(questionString))
		self.answer = '...'
		doc.append(NewLine())
		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):
			grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
			graphPolygon(doc = doc, x = x, y = y, annotations = [ABC[0],ABC[1],ABC[2]], color = 'black')

		#quesiton string

		#answer

		#annotations etc	
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class performReflection():
	def __init__(self, prime = True, xMinGraph = -10, yMinGraph = -10, xMaxGraph = 10, yMaxGraph = 10, nameOfReflectionList = ['x=not0','y=not0','yaxis','xaxis','x=','y=','x=0','y=0'], overlap = False, polygonType = ['right triangle'], polygonSize = ['small','medium', 'large']):
		self.prime = prime
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.nameOfReflectionList = nameOfReflectionList
		self.overlap = overlap
		self.typeOfQuestionChosen = 'SA'
		self.polygonType = polygonType
		self.polygonSize = polygonSize
	def addQuestion(self, doc = None):
		polygonTypeChosen = random.choice(self.polygonType)
		polygonSizeChosen = random.choice(self.polygonSize)
		
		self.x, self.y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = [polygonTypeChosen], polygonSize = [polygonSizeChosen])
		
		self.one, self.two, self.three = annotatePolygon(prime = self.prime, vertices = 3)

		self.x, self.y, self.rX, self.rY, self.refVal, self.refValString = reflection(xMinGraph = self.xMinGraph, xMaxGraph = self.xMaxGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, nameOfReflectionList  = self.nameOfReflectionList, overlap = self.overlap, x = self.x, y = self.y)

		if len(self.x) == 3:
			doc.append(NoEscape(r'Given $\bigtriangleup %s$ on the set of axes below, graph $\bigtriangleup %s$ after a reflection over the ' % (self.one[0]+self.one[1]+self.one[2], self.two[0]+self.two[1]+self.two[2]) + self.refValString + '.'))

		doc.append(NewLine())

		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
				graphPolygon(doc = doc, x = self.x, y = self.y, annotations = [self.one[0],self.one[1],self.one[2]], color = 'black')

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'$%s(%d,%d)$' % (self.two[0], self.rX[0], self.rY[0])))
class performRotation():#answerkey!!!
	def __init__(self, prime = True, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, center = ['origin','other'], degrees = [90,180,270], direction = ['clockwise','counterclockwise'], overlap = False, polygonType = ['right triangle'], polygonSize = ['small','medium', 'large'], typeOfQuestion = ['SA']):
		self.prime = prime
		self.overlap = overlap
		self.center = center
		self.degrees = degrees
		self.direction = direction
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.polygonType = polygonType
		self.polygonSize = polygonSize
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)

	def addQuestion(self, doc = None):
		polygonTypeChosen = random.choice(self.polygonType)
		polygonSizeChosen = random.choice(self.polygonSize)
		
		self.x, self.y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = [polygonTypeChosen], polygonSize = [polygonSizeChosen])
		
		xChange = random.choice([x for x in range(self.xMinGraph-min(self.x), self.xMaxGraph-max(self.x)) if x not in [0]])
		yChange = random.choice([x for x in range(self.yMinGraph-min(self.y), self.yMaxGraph-max(self.y)) if x not in [0]])

		#moving shit to a quadrant
		self.x, self.y = translateCoordinates(self.x, self.y, xChange, yChange)

		self.one, self.two, self.three = annotatePolygon(prime = self.prime, vertices = 3)

		self.xChosen, self.yChosen, self.rotXChosen, self.rotYChosen, self.centerChosen, self.degreesChosen, self.directionChosen = rotation(xMinGraph = self.xMinGraph, xMaxGraph = self.xMaxGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, center = self.center, overlap = self.overlap, degrees = self.degrees, direction = self.direction, x = self.x, y = self.y)
		
		if self.centerChosen == [0,0]:
			centerString = 'the origin.'
		else:
			centerString = r'the point $(%d,%d)$.' % (self.centerChosen[0], self.centerChosen[1])

		if len(self.xChosen) == 3:
			doc.append(NoEscape(r'Given $\bigtriangleup %s$ on the set of axes below, graph $\bigtriangleup %s$ after a rotation of ' % (self.one[0]+self.one[1]+self.one[2], self.two[0]+self.two[1]+self.two[2]) + '$%d^\circ$' % self.degreesChosen + ' ' + self.directionChosen + ' around ' + centerString))

		doc.append(NewLine())

		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
				graphPolygon(doc = doc, x = self.xChosen, y = self.yChosen, annotations = [self.one[0],self.one[1],self.one[2]], color = 'black')

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'$%s(%d,%d)$' % (self.two[0], self.rotXChosen[0], self.rotYChosen[0])))
class rigidMotionStatement():
	def __init__(self, transformation = ['translation','reflection','rotation','dilation'], symbol = False, typeOfQuestion = ['MC','SA']):
		self.transformation = random.choice(transformation)
		self.symbol = symbol
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
	def addQuestion(self, doc = None):
		ABC, DEF, GHI = annotatePolygon(prime = True, vertices = 3)
		if self.typeOfQuestionChosen == 'MC':
			#choice 1
			xTransVal = random.randint(0,10)
			yTransVal = random.choice([x for x in range(0,10) if x not in [xTransVal]])
			if xTransVal < 0:
				transXString = '%d left' % abs(xTransVal)
			elif xTransVal > 0:
				transXString = '%d right' % abs(xTransVal)
			else:
				transXString = ''
			if yTransVal < 0:
				transYString = '%d down' % abs(yTransVal)
			elif yTransVal > 0:
				transYString = '%d up' % abs(yTransVal)
			else:
				transYString = ''

			if xTransVal == 0:
				transString = transYString
			elif yTransVal == 0:
				transString = transXString
			else:
				transString = transXString + ' and ' + transYString
			transformation1Details = 'A translation ' + transString

			#choice2
			nameOfReflectionList = ['x=not0','y=not0','yaxis','xaxis','x=','y=','x=0','y=0']
			nameOfReflection = random.choice(nameOfReflectionList)
			if nameOfReflection == 'xaxis' or nameOfReflection == 'yaxis':
				refValString = '$' + nameOfReflection[0] + '-' + nameOfReflection[1:] + '$'
			elif nameOfReflection == 'y=' or nameOfReflection == 'x=':
				refValString = 'line $' + nameOfReflection[0] + ' ={%d}' % random.randint(-10,10) + '$'
			elif nameOfReflection == 'y=0' or nameOfReflection == 'x=0':
				refValString = 'line $' + nameOfReflection[0] + ' = 0 $'
			elif nameOfReflection == 'x=not0' or nameOfReflection == 'y=not0':
				refValString = 'line $' + nameOfReflection[0] + ' ={%d}' % random.randint(-10,10) + '$'
			transformation2Details = 'A reflection over the ' + refValString

			#choice3
			deg = random.choice([90,180])
			direction = random.choice(['counterclockwise','clockwise'])
			centerX = random.randint(-10,10)
			centerY = random.randint(-10,10)
			centerString = r'the point $(%d,%d)$' % (centerX, centerY)
			if centerX == 0 and centerY == 0:
				centerString = 'the origin'
			transformation3Details = 'A rotation ' + r'$%d^{\circ}$' % deg + direction + ' around ' + centerString

			#choice4
			scaleFactor = random.randint(2,5)
			transformation4Details = 'A dilation with a scale factor of %d' % (scaleFactor)

			doc.append(NoEscape(r'Given $\bigtriangleup{%s}$, after which of the following transformations, will $\bigtriangleup{%s}$ \textit{not} be congruent to $\bigtriangleup{%s}$?' % (ABC[0]+ABC[1]+ABC[2], ABC[0]+ABC[1]+ABC[2], DEF[0]+DEF[1]+DEF[2])))

			doc.append(NewLine())
			self.answer = transformation4Details
			newChoices = multipleChoice(choices = [transformation1Details, transformation2Details, transformation3Details, transformation4Details], doc = doc)
			self.answer = 'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)
		else:
			if self.transformation == 'translation':
				xTransVal = random.randint(0,10)
				yTransVal = random.choice([x for x in range(0,10) if x not in [xTransVal]])
				if xTransVal < 0:
					transXString = '%d left' % abs(xTransVal)
				elif xTransVal > 0:
					transXString = '%d right' % abs(xTransVal)
				else:
					transXString = ''
				if yTransVal < 0:
					transYString = '%d down' % abs(yTransVal)
				elif yTransVal > 0:
					transYString = '%d up' % abs(yTransVal)
				else:
					transYString = ''

				if xTransVal == 0:
					transString = transYString
				elif yTransVal == 0:
					transString = transXString
				else:
					transString = transXString + ' and ' + transYString
				transformationDetails = transString
			elif self.transformation == 'reflection':
				nameOfReflectionList = ['x=not0','y=not0','yaxis','xaxis','x=','y=','x=0','y=0']
				nameOfReflection = random.choice(nameOfReflectionList)
				if nameOfReflection == 'xaxis' or nameOfReflection == 'yaxis':
					refValString = '$' + nameOfReflection[0] + '-' + nameOfReflection[1:] + '$'
				elif nameOfReflection == 'y=' or nameOfReflection == 'x=':
					refValString = 'line $' + nameOfReflection[0] + ' ={%d}' % random.randint(-10,10) + '$'
				elif nameOfReflection == 'y=0' or nameOfReflection == 'x=0':
					refValString = 'line $' + nameOfReflection[0] + ' = 0 $'
				elif nameOfReflection == 'x=not0' or nameOfReflection == 'y=not0':
					refValString = 'line $' + nameOfReflection[0] + ' ={%d}' % random.randint(-10,10) + '$'
				transformationDetails = refValString

			elif self.transformation == 'rotation':
				deg = random.choice([90,180])
				direction = random.choice(['counterclockwise','clockwise'])
				centerX = random.randint(-10,10)
				centerY = random.randint(-10,10)
				centerString = r'the point $(%d,%d)$' % (centerX, centerY)
				if centerX == 0 and centerY == 0:
					centerString = 'the origin'
				transformationDetails = r'$%d^{\circ}$' % deg + direction + ' around ' + centerString

			if self.symbol == True:
				symbolString = r'$\cong$'
			else:
				symbolString = 'congruent'

			questionString = r'Given $\bigtriangleup{%s}$, after a %s %s, is $\bigtriangleup{%s}$ %s to $\bigtriangleup{%s}$?' % (ABC[0]+ABC[1]+ABC[2], self.transformation, transformationDetails, ABC[0]+ABC[1]+ABC[2], symbolString, DEF[0]+DEF[1]+DEF[2])
			doc.append(NoEscape(questionString))
		
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape('yahoo'))
class identifyTranslation():
	def __init__(self, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, xChangeAmount = ['+','-','0'], yChangeAmount = ['+','-','0'], overlap = False, polygonType = ['right triangle'], polygonSize = ['medium'], prime = True):
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.xChangeAmount = xChangeAmount
		self.yChangeAmount = yChangeAmount
		self.overlap = overlap
		self.polygonType = polygonType
		self.polygonSize = polygonSize
		self.prime = prime

	def addQuestion(self, doc = None):
		polygonTypeChosen = random.choice(self.polygonType)
		polygonSizeChosen = random.choice(self.polygonSize)
		
		x, y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = [polygonTypeChosen], polygonSize = [polygonSizeChosen])

		x, y, newX, newY, xTransVal, yTransVal = translation(xMinGraph = self.xMinGraph, xMaxGraph = self.xMaxGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, xChangeAmount = self.xChangeAmount, yChangeAmount = self.yChangeAmount, overlap = self.overlap, x = x, y = y)

		ABC, DEF, blah = annotatePolygon(vertices = len(x), prime = self.prime)
		shapeName = ''
		for letter in [ABC[x] for x in range(0, len(x))]:
			shapeName += letter
		shapeName2 = ''
		for letter in [DEF[x] for x in range(0, len(x))]:
			shapeName2 += letter
		#graph polygon
		if xTransVal < 0:
			transXString = '%d left' % abs(xTransVal)
		elif xTransVal > 0:
			transXString = '%d right' % abs(xTransVal)
		else:
			transXString = ''
		if yTransVal < 0:
			transYString = '%d down' % abs(yTransVal)
		elif yTransVal > 0:
			transYString = '%d up' % abs(yTransVal)
		else:
			transYString = ''

		if xTransVal == 0:
			transString = transYString
		elif yTransVal == 0:
			transString = transXString
		else:
			transString = transXString + ' and ' + transYString

		questionString = r'Given the graph below, identify the transformation used to map %s $%s$ onto %s $%s$.' % ( polygonTypeChosen, shapeName, polygonTypeChosen, shapeName2)
		doc.append(NoEscape(questionString))
		self.answer = 'Translation %s' % transString
		doc.append(NewLine())
		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):
			grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
			graphPolygon(doc = doc, x = x, y = y, annotations = [ABC[0],ABC[1],ABC[2]], color = 'black')
			graphPolygon(doc = doc, x = newX, y = newY, annotations = [DEF[0],DEF[1],DEF[2]], color = 'black')
		#quesiton string

		#answer

		#annotations etc	
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class identifyReflection():
	def __init__(self, prime = True, xMinGraph = -10, yMinGraph = -10, xMaxGraph = 10, yMaxGraph = 10, nameOfReflectionList = ['x=not0','y=not0','yaxis','xaxis','x=','y=','x=0','y=0'], overlap = False, polygonType = ['right triangle'], polygonSize = ['medium']):
		self.prime = prime
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.nameOfReflectionList = nameOfReflectionList
		self.overlap = overlap
		self.typeOfQuestionChosen = 'SA'
		self.polygonType = polygonType
		self.polygonSize = polygonSize
	def addQuestion(self, doc = None):
		polygonTypeChosen = random.choice(self.polygonType)
		polygonSizeChosen = random.choice(self.polygonSize)
		
		self.x, self.y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = [polygonTypeChosen], polygonSize = [polygonSizeChosen])
		
		self.one, self.two, self.three = annotatePolygon(prime = self.prime, vertices = 3)

		self.x, self.y, self.rX, self.rY, self.refVal, self.refValString = reflection(xMinGraph = self.xMinGraph, xMaxGraph = self.xMaxGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, nameOfReflectionList  = self.nameOfReflectionList, overlap = self.overlap, x = self.x, y = self.y)

		if len(self.x) == 3:
			doc.append(NoEscape(r'Given the graph below, identify the transformation used to map $\bigtriangleup %s$ onto $\bigtriangleup %s$.' % (self.one[0]+self.one[1]+self.one[2], self.two[0]+self.two[1]+self.two[2])))
			self.answer = self.refValString
		doc.append(NewLine())

		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
				graphPolygon(doc = doc, x = self.x, y = self.y, annotations = [self.one[0],self.one[1],self.one[2]], color = 'black')
				graphPolygon(doc = doc, x = self.rX, y = self.rY, annotations = [self.two[0],self.two[1],self.two[2]], color = 'black')
	
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class identifyRotation():#answerkey!!!
	def __init__(self, prime = True, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, center = ['origin','other'], degrees = [90,180,270], direction = ['clockwise','counterclockwise'], overlap = False, polygonType = ['right triangle'], polygonSize = ['medium']):
		self.prime = prime
		self.overlap = overlap
		self.center = center
		self.degrees = degrees
		self.direction = direction
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.polygonType = polygonType
		self.polygonSize = polygonSize

	def addQuestion(self, doc = None):
		polygonTypeChosen = random.choice(self.polygonType)
		polygonSizeChosen = random.choice(self.polygonSize)
		
		self.x, self.y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = [polygonTypeChosen], polygonSize = [polygonSizeChosen])
		
		self.one, self.two, self.three = annotatePolygon(prime = self.prime, vertices = 3)

		self.xChosen, self.yChosen, self.rotXChosen, self.rotYChosen, self.centerChosen, self.degreesChosen, self.directionChosen = rotation(xMinGraph = self.xMinGraph, xMaxGraph = self.xMaxGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, center = self.center, overlap = self.overlap, degrees = self.degrees, direction = self.direction, x = self.x, y = self.y)
		
		if self.centerChosen == [0,0]:
			centerString = 'the origin.'
		else:
			centerString = r'the point $(%d,%d)$.' % (self.centerChosen[0], self.centerChosen[1])

		if len(self.xChosen) == 3:
			doc.append(NoEscape(r'Given the graph below, identify the transformation used to map $\bigtriangleup %s$ onto $\bigtriangleup %s$.' % (self.one[0]+self.one[1]+self.one[2], self.two[0]+self.two[1]+self.two[2])))
			self.answer = r'Rotation $%d^{\circ}$ %s around %s.' % (self.degreesChosen, self.directionChosen, self.centerChosen)

		doc.append(NewLine())

		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
				graphPolygon(doc = doc, x = self.xChosen, y = self.yChosen, annotations = [self.one[0],self.one[1],self.one[2]], color = 'black')
				graphPolygon(doc = doc, x = self.rotXChosen, y = self.rotYChosen, annotations = [self.two[0],self.two[1],self.two[2]], color = 'black')
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class identifySequence():
	def __init__(self, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, transformation1 = ['translation','reflection','rotation','dilation'], transformation2 = ['translation','reflection','rotation','dilation'], secondShape = False, prime = True, rigidMotions = False, typeOfQuestion = ['SA']):
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.transformation1 = transformation1
		self.transformation2 = transformation2
		self.secondShape = secondShape
		self.prime = prime
		self.rigidMotions = rigidMotions
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
	def addQuestion(self, doc = None):

		x, y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = ['right triangle'], polygonSize = ['medium'])
		self.one, self.two, self.three = annotatePolygon(prime = self.prime, vertices = 3)

		trans1, trans2, x1, y1, x2, y2, x3, y3 = sequence(xMinGraph = self.xMinGraph, xMaxGraph = self.xMaxGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, transformation1 = self.transformation1, transformation2 = self.transformation2, x = x, y = y)
		print(max(x1)-min(x1))
		print(max(x2)-min(x2))
		print(max(x3)-min(x3))
		
		print(max(y1)-min(y1))
		print(max(y2)-min(y2))
		print(max(y3)-min(y3))
		
		doc.append(NoEscape(r'Given the graph below, identify the sequence of transformations used to map $\bigtriangleup %s$ onto $\bigtriangleup %s$.' % (self.one[0]+self.one[1]+self.one[2], self.three[0]+self.three[1]+self.three[2])))
		self.answer = r'There are lots of different ways, but two transformations that you might use are %s and %s. Also YOU NEED DETAILS. Since there are so many different ways, I will not write them all here.' % (trans1, trans2)

		doc.append(NewLine())

		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
				graphPolygon(doc = doc, x = x1, y = y1, annotations = [self.one[0],self.one[1],self.one[2]], color = 'black')
				graphPolygon(doc = doc, x = x3, y = y3, annotations = [self.three[0],self.three[1],self.three[2]], color = 'black')
				if self.secondShape == True:
					graphPolygon(doc = doc, x = x2, y = y2, annotations = [self.two[0],self.two[1],self.two[2]], color = 'black')

		if self.rigidMotions == True:
			doc.append(NoEscape(r'Explain why $\bigtriangleup %s$ is congruent to $\bigtriangleup %s$.' % (self.one[0]+self.one[1]+self.one[2], self.three[0]+self.three[1]+self.three[2])))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class rotationMinimumNumberOfDegrees():
	def __init__(self, variation = ["Given a polygon, find the mininum number of degrees", "Given a minimum number of degrees, find the polygon", "Given a polygon, which number of degrees (multiples)"], polygonType = ['triangle', 'quadrilateral', 'pentagon', 'hexagon', 'octagon', 'nonagon', 'decagon', 'n-sided polygon'], typeOfQuestion = ['MC','SA']):
		
		self.variation1 = variation
		self.polygonType1 = polygonType
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)



	def addQuestion(self, doc = None):
		self.variation = random.choice(self.variation1)
		self.polygonType = random.choice(self.polygonType1)
		print(self.typeOfQuestionChosen)
		x = random.choice([x for x in range(11,30) if 360 % x == 0])
		
		polygonDict = {'triangle': 3, 'quadrilateral': 4, 'pentagon': 5, 'hexagon': 6, 'octagon': 8, 'nonagon': 9, 'decagon': 10,
					   '%d-sided polygon' % x: x}
		
		if self.polygonType == 'n-sided polygon':
			givenPolygon = '%d-sided polygon' % x
		else:
			givenPolygon = self.polygonType

		if self.variation == "Given a polygon, find the mininum number of degrees": #minimum number of degrees basic
			self.questionString = r'What is the minimum number of degrees for a regular %s to rotate onto itself?' % givenPolygon
			self.answer = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
			if self.typeOfQuestionChosen == 'MC':
				options = list(polygonDict)
				options.remove(givenPolygon)
				choice1 = random.choice(options)
				options.remove(choice1)
				
				choice2 = random.choice(options)
				options.remove(choice2)
				
				choice3 = random.choice(options)
				options.remove(choice3)
				choices = [r'$%d^{\circ}$' % (360/polygonDict[choice1]), r'$%d^{\circ}$' % (360/polygonDict[choice2]), r'$%d^{\circ}$' % (360/polygonDict[choice3]), self.answer]
				
		elif self.variation == "Given a minimum number of degrees, find the polygon":
			self.questionString = r'What is the name of regular polygon whose minimum number of degrees to rotate onto itself is $%d^{\circ}$?' % (360/polygonDict[givenPolygon])
			self.answer = r'%s' % givenPolygon
			if self.typeOfQuestionChosen == 'MC':
				options = list(polygonDict)
				options.remove(givenPolygon)
				choice1 = random.choice(options)
				options.remove(choice1)
				
				choice2 = random.choice(options)
				options.remove(choice2)
				
				choice3 = random.choice(options)
				options.remove(choice3)
				choices = [choice1, choice2, choice3, givenPolygon]

		elif self.variation == "Given a polygon, which number of degrees (multiples)": #multiples            
			minDegrees = 360//polygonDict[givenPolygon]
			print(minDegrees)
			multiples = [x for x in range(minDegrees*2,361-minDegrees, minDegrees)] # so it won't be the min etc or 360   
			multiplesGiven = random.choice(multiples)
			if self.typeOfQuestionChosen == 'SA':
				self.questionString = r'If a regular %s is rotated $%d^{\circ}$, why will it rotate onto itself? Explain' % (givenPolygon, multiplesGiven)
				self.answer = r'Yes. $%d^{\circ}$ is %d rotations. $%d^{\circ}$ is the minimum number of degrees. Every $%d^{\circ}$ rotates the polygon onto itself.' % (multiplesGiven, multiplesGiven/minDegrees, minDegrees, minDegrees)
			else:
				self.questionString = r'Given a regular %s, which of the following number of degrees will rotate the regular %s onto itself?' % (givenPolygon, givenPolygon)

				self.answer = r'$%d^{\circ}$' % (multiplesGiven)
				minsNotInMultiples = [x for x in [360//3, 360//4, 360//5, 360//6, 360//8, 360//9, 360//10] if x not in multiples]
				print(minsNotInMultiples)
				choice1 = random.choice(minsNotInMultiples)
				minsNotInMultiples.remove(choice1)
				choice2 = random.choice(minsNotInMultiples)
				minsNotInMultiples.remove(choice2)
				choice3 = random.choice(minsNotInMultiples)
				minsNotInMultiples.remove(choice3)
				choices = [r'$%d^{\circ}$' % (choice1), r'$%d^{\circ}$' % (choice2), r'$%d^{\circ}$' % (choice3), self.answer]

		doc.append(NoEscape(self.questionString))
		doc.append(NewLine())
		if self.typeOfQuestionChosen == 'MC':
			newChoices = multipleChoice(doc = doc, choices = choices)
			self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
		docAnswer.append(NewLine())
class linesOfReflection():
	def __init__(self, typeOfQuestion = ['MC','SA'], typeOfPolygon = ['square'], xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10):
		self.typeOfPolygon = typeOfPolygon
		self.typeOfQuestion = typeOfQuestion
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph

	def addQuestion(self, doc = None):
		self.typeOfQuestionChosen = random.choice(self.typeOfQuestion)
		self.typeOfPolygon = random.choice(self.typeOfPolygon)
		#Draw a square on the coordinate plane somewhere
		squareX, squareY = rectangleCoordinates(typeOfRectangle = ['square'], polygonSize = ['medium','large'], xMaxGraph = self.xMaxGraph, yMaxGraph = self.yMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, evenSided = True, unevenCenterOption = True)
		centerX = (max(squareX)+min(squareX))//2
		centerY = (max(squareY)+min(squareY))//2
		labels = pointsForDiagram()
		
		if self.typeOfQuestionChosen == 'SA':
			self.questionString = r'Given the diagram below of square $%s$, name all the lines of reflection that would map the square onto itself.' % (labels['A']+labels['B']+labels['C']+labels['D'])
			line1 = r'$x=%d$' % centerX
			line2 = r'$y=%d$' % centerY
			#(y-centerY)=1(x-centerX), so b = -centerX+centerY
			line3 = r'$y=x%s$' % (equationFormatting(value = -1*centerX+centerY, nextToVariable = False, inBetween = True))
			line4 = r'$y=-x%s$' % (equationFormatting(value = centerX+centerY))
			self.answer = r'%s, %s, %s, and %s' % (line1, line2, line3, line4)
		elif self.typeOfQuestionChosen == 'MC':
			self.questionString = r'Given the diagram below of square $%s$, which line would \textit{not} map the square onto itself?.' % (labels['A']+labels['B']+labels['C']+labels['D'])
			answer = r'$x=%d$' % centerY
			line2 = r'$y=%d$' % centerY
			#(y-centerY)=1(x-centerX), so b = -centerX+centerY
			line3 = r'$y=x%s$' % (equationFormatting(value = -1*centerX+centerY, nextToVariable = False, inBetween = True))
			line4 = r'$y=-x%s$' % (equationFormatting(value = centerX+centerY))
			choices = [answer, line2, line3, line4]
			self.answer = answer

		doc.append(NoEscape(self.questionString))
		doc.append(NewLine())
		if self.typeOfQuestionChosen == 'MC':
			newChoices = multipleChoice(choices = choices, doc = doc)
			self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)



		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
				graphPolygon(doc = doc, x = squareX, y = squareY, annotations = [labels['A'], labels['B'], labels['C'], labels['D']], color = 'black')
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))				

class transformationsTest1():
	def __init__(self, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, transformation1 = ['translation'], transformation2 = ['rotation'], secondShape = True, prime = True, typeOfQuestion = ['MC','SA'], axes = True, gridLines = False):
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.transformation1 = transformation1
		self.transformation2 = transformation2
		self.secondShape = secondShape
		self.prime = prime
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.axes = axes
		self.gridLines = gridLines
	
	def addQuestion(self, doc = None):

		x, y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = ['right triangle'], polygonSize = ['medium'])
		self.one, self.two, self.three = annotatePolygon(prime = self.prime, vertices = 3)

		#put triangle in one of four quandrants, move it clockwise, rotate 90 clockwise etc
		quadrant = random.randint(1,4)
		if quadrant == 1:
			lowX = self.xMinGraph
			highX = -1
			lowY = self.yMinGraph
			highY = -1
			xTransVal = 0
			yTransVal = 10
		elif quadrant == 2:
			lowX = self.xMinGraph
			highX = -1
			lowY = 1
			highY = self.yMaxGraph
			xTransVal = 10
			yTransVal = 0
		elif quadrant == 3:
			lowX = 1
			highX = self.xMaxGraph
			lowY = 1
			highY = self.yMaxGraph
			xTransVal = 0
			yTransVal = -10
		elif quadrant == 4:
			lowX = 1
			highX = self.xMaxGraph
			lowY = self.yMinGraph
			highY = -1
			xTransVal = -10
			yTransVal = 0

		xChange = random.randint(lowX-min(x), highX-max(x))
		yChange = random.randint(lowY-min(y), highY-max(y))

		#moving shit to a quadrant
		x1, y1 = translateCoordinates(x, y, xChange, yChange)

		#now translate over clockwise
		x2, y2 = translateCoordinates(x1, y1, xTransVal, yTransVal)

		#now rotate 90 degrees clockwise
		x3, y3 = rotateCoordinatesAsList(x2, y2, 'clockwise', 90, centerX = 0, centerY = 0)

		if self.typeOfQuestionChosen == 'MC':
			doc.append(NoEscape(r'In the graph below, $\bigtriangleup %s$ follows a sequence of transformations to make $\bigtriangleup %s$.' % (self.one[0]+self.one[1]+self.one[2], self.three[0]+self.three[1]+self.three[2])))
		else:
			doc.append(NoEscape(r'Below, $\bigtriangleup %s$ follows a sequence of transformations to make $\bigtriangleup %s$.' % (self.one[0]+self.one[1]+self.one[2], self.three[0]+self.three[1]+self.three[2])))
		doc.append(NewLine())

		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph, axes = self.axes, gridLines = self.gridLines)
				graphPolygon(doc = doc, x = x1, y = y1, annotations = [self.one[0],self.one[1],self.one[2]], color = 'black')
				graphPolygon(doc = doc, x = x3, y = y3, annotations = [self.three[0],self.three[1],self.three[2]], color = 'black')
				if self.secondShape == True:
					graphPolygon(doc = doc, x = x2, y = y2, annotations = [self.two[0],self.two[1],self.two[2]], color = 'black')

		if self.typeOfQuestionChosen == 'MC':
			doc.append(NoEscape(r'What is the sequence of tranformations?'))
			doc.append(NewLine())
			self.answer = r'Translation then rotation'
			choice1 = r'Translation then reflection'
			choice2 = r'Reflection then rotation'
			choice3 = r'Reflection then reflection'
			newChoices = multipleChoice(choices = [self.answer, choice1, choice2, choice3], doc = doc)
			self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)
		elif self.typeOfQuestionChosen == 'SA':
			doc.append(NoEscape(r'Describe a sequence of transformations that will map $\bigtriangleup %s$ onto $\bigtriangleup %s$.' % (self.one[0]+self.one[1]+self.one[2], self.three[0]+self.three[1]+self.three[2])))
			self.answer = r'Uh there are a lot'
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class transformationsTest2():
	def __init__(self, prime = False, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, center = ['origin'], degrees = [90], direction = ['clockwise','counterclockwise'], overlap = False, polygonType = ['right triangle'], polygonSize = ['medium'], axes = True, gridLines = False, typeOfQuestion = ['MC']):
		self.prime = prime
		self.overlap = overlap
		self.center = center
		self.degrees = degrees
		self.direction = direction
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.polygonType = polygonType
		self.polygonSize = polygonSize
		self.axes = axes
		self.gridLines = gridLines
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
	def addQuestion(self, doc = None):
		polygonTypeChosen = random.choice(self.polygonType)
		polygonSizeChosen = random.choice(self.polygonSize)
		
		self.x = random.choice([x for x in range(self.xMinGraph, self.xMaxGraph+1) if x not in range(self.xMinGraph+5, self.xMaxGraph-5+1)])
		self.y = random.choice([-2,-1,1,2])
		self.x = [self.x]
		self.y = [self.y]
		
		self.one, self.two, self.three = annotatePolygon(prime = self.prime, vertices = 3)

		#for correct answer!
		#rotate coordinates now, still we're only using the first one
		random.shuffle(self.direction)
		self.rotX, self.rotY = rotateCoordinatesAsList(self.x, self.y, self.direction[0], 90, centerX = 0, centerY = 0)

		#rotate other way 90 degrees
		self.rot1X, self.rot1Y = rotateCoordinatesAsList(self.x, self.y, self.direction[1], 90, centerX = 0, centerY = 0)

		#reflect over y-axis
		self.refX, self.refY = reflectCoordinates(self.x, self.y, 'x', 0)

		#reflect over x-axis
		self.ref1X, self.ref1Y = reflectCoordinates(self.x, self.y, 'y', 0)

		doc.append(NoEscape(r'In the graph below, there are several points plotted.'))

		doc.append(NewLine())

		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph, axes = self.axes, gridLines = self.gridLines)
				graphPoint(doc = doc, x = self.x[0], y = self.y[0], annotations = [self.one[0]], color = 'black') #original
				graphPoint(doc = doc, x = self.rotX[0], y = self.rotY[0], annotations = [self.one[1]], color = 'black')#correct
				graphPoint(doc = doc, x = self.rot1X[0], y = self.rot1Y[0], annotations = [self.one[2]], color = 'black')#90 other
				graphPoint(doc = doc, x = self.refX[0], y = self.refY[0], annotations = [self.two[0]], color = 'black')#y-axis
				graphPoint(doc = doc, x = self.ref1X[0], y = self.ref1Y[0], annotations = [self.two[1]], color = 'black')#x-axis

		doc.append(NoEscape(r'After point %s is rotated $90^{\circ}$ %s around the origin, which point is its image?' % (self.one[0], self.direction[0])))
		doc.append(NewLine())
		self.answer = r'%s' % (self.one[1])
		choice1 = r'%s' % self.one[2]
		choice2 = r'%s' % self.two[0]
		choice3 = r'%s' % self.two[1]
		newChoices = multipleChoice(choices = [self.answer, choice1, choice2, choice3], doc = doc)
		self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class transformationsTest4():
	def __init__(self, typeOfQuestion = ['MC'], typeOfPolygon = ['square'], xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10):
		self.typeOfPolygon = typeOfPolygon
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph

	def addQuestion(self, doc = None):
		self.typeOfPolygon = random.choice(self.typeOfPolygon)
		#Draw a square on the coordinate plane somewhere
		squareX, squareY = rectangleCoordinates(typeOfRectangle = ['square'], polygonSize = ['medium','large'], xMaxGraph = self.xMaxGraph, yMaxGraph = self.yMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, evenSided = True, unevenCenterOption = True)
		centerX = (max(squareX)+min(squareX))//2
		centerY = (max(squareY)+min(squareY))//2
		labels = pointsForDiagram()
		
		if self.typeOfQuestionChosen == 'MC':
			self.questionString = r'In the diagram below square $%s$ is drawn.' % (labels['A']+labels['B']+labels['C']+labels['D'])

			answer = r'$x=%d$' % centerY
			line2 = r'$y=%d$' % centerY
			#(y-centerY)=1(x-centerX), so b = -centerX+centerY
			line3 = r'$y=x%s$' % (equationFormatting(value = -1*centerX+centerY, nextToVariable = False, inBetween = True))
			line4 = r'Rotation $90^{\circ}$ around (%d,%d)' % (centerX, centerY)
			choices = [answer, line2, line3, line4]
			self.answer = answer
			doc.append(NoEscape(self.questionString))
			doc.append(NewLine())
			picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
			with doc.create(Center()):
				with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
					grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
					graphPolygon(doc = doc, x = squareX, y = squareY, annotations = [labels['A'], labels['B'], labels['C'], labels['D']], color = 'black')
			doc.append(NoEscape(r'Which of the following will \textit{not} map the square onto itself?'))
			doc.append(NewLine())
			newChoices = multipleChoice(choices = choices, doc = doc)
			self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))	
class transformationsTest5():
	def __init__(self, typeOfQuestion = ['MC']):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
	def addQuestion(self, doc = None):
		sides = random.choice([3,4,5,6,8,9,10])
		sidesDict = {3: random.choice(['equilateral triangle','regular triangle']), 4: random.choice(['square','regular quadrilateral']), 5: 'regular pentagon', 6: 'regular hexagon', 8: 'regular octagon', 9: 'regular nonagon', 10: 'regular decagon'}

		questionString = r'Which shape always has exactly %d lines of reflection that will map it onto itself?' % sides
		self.answer = sidesDict[sides]
		listo = list(sidesDict)
		print(self.answer, listo)
		listo.remove(sides)
		choice1 = random.choice(listo)
		listo.remove(choice1)
		choice2 = random.choice(listo)
		listo.remove(choice2)
		choice3 = random.choice(listo)
		listo.remove(choice3)

		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		newChoices = multipleChoice(choices = [self.answer, sidesDict[choice1], sidesDict[choice2], sidesDict[choice3]], doc = doc)
		self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class transformationsTest10():
	def __init__(self, xMinGraph = -10, xMaxGraph = 10, yMinGraph = -10, yMaxGraph = 10, transformation1 = ['translation'], transformation2 = ['reflection'], prime = True, typeOfQuestion = ['SA'], axes = True, gridLines = True):
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph
		self.transformation1 = transformation1
		self.transformation2 = transformation2
		self.prime = prime
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.axes = axes
		self.gridLines = gridLines
	
	def addQuestion(self, doc = None):

		x, y = triangleCoordinates(xMaxGraph = self.xMaxGraph, xMinGraph = self.xMinGraph, yMinGraph = self.yMinGraph, yMaxGraph = self.yMaxGraph, typeOfTriangle = ['right triangle'], polygonSize = ['medium'])
		self.one, self.two, self.three = annotatePolygon(prime = self.prime, vertices = 3)

		#put triangle in one of four quandrants, move it clockwise, rotate 90 clockwise etc
		quadrant = random.randint(1,4)
		if quadrant == 1:
			lowX = self.xMinGraph
			highX = -1
			lowY = self.yMinGraph
			highY = -1
			xTransVal = 0
			yTransVal = 10
			transDetails = 'up 10'
		elif quadrant == 2:
			lowX = self.xMinGraph
			highX = -1
			lowY = 1
			highY = self.yMaxGraph
			xTransVal = 10
			yTransVal = 0
			transDetails = 'right 10'
		elif quadrant == 3:
			lowX = 1
			highX = self.xMaxGraph
			lowY = 1
			highY = self.yMaxGraph
			xTransVal = 0
			yTransVal = -10
			transDetails = 'down 10'
		elif quadrant == 4:
			lowX = 1
			highX = self.xMaxGraph
			lowY = self.yMinGraph
			highY = -1
			xTransVal = -10
			yTransVal = 0
			transDetails = 'left 10'

		xChange = random.randint(lowX-min(x), highX-max(x))
		yChange = random.randint(lowY-min(y), highY-max(y))

		#moving shit to a quadrant
		x1, y1 = translateCoordinates(x, y, xChange, yChange)

		#now translate over clockwise
		x2, y2 = translateCoordinates(x1, y1, xTransVal, yTransVal)

		#now reflect over y=0 or x=0
		#y = 0 if quadrant == 1 or quadrant == 3
		if quadrant == 2 or quadrant == 4:
			x3, y3 = reflectCoordinates(x2, y2, 'y', 0)
			refString = r'over the line $y=0$'
		else:
			x3, y3 = reflectCoordinates(x2, y2, 'x', 0)
			refString = r'over the line $x=0$'

		doc.append(NoEscape(r'In the graph below of $\bigtriangleup %s$, peform a translation %s followed by a reflection %s to make $\bigtriangleup %s$.' % (self.one[0]+self.one[1]+self.one[2], transDetails, refString, self.three[0]+self.three[1]+self.three[2])))
		doc.append(NewLine())

		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph, axes = self.axes, gridLines = self.gridLines)
				graphPolygon(doc = doc, x = x1, y = y1, annotations = [self.one[0],self.one[1],self.one[2]], color = 'black')

		self.answer = r'Uh there are a lot'
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
###############ALGEBRA#################
class oneStepEquation(): #need to fix answer key!!!!!!!!! self.l... and docs etc
	def __init__(self, default = False, option = ['Addition', 'Multiplication', 'Division','Fractional'], standardOrder = True, integerSolution = True, variable = ['x', 'w', 'y', 'p']):
		if default == True:
			self.option = ['Addition']
			self.integerSolution = True
			self.variable = ['x']
			self.standardOrder = True
		else:
			self.defaultKwargs = {'default':False, 
			'option':['Addition', 'Multiplication', 'Division','Fractional'],
			'integerSolution':True,
			'variable':['x', 'w', 'y', 'p'],
			'standardOrder':True}

			self.option = option
			self.integerSolution = integerSolution
			self.variabe = variable
			self.standardOrder = standardOrder

		self.subject = ['Algebra']
		self.topics = ['Solving Equations']
		self.skills = ['Solve a one step equation']
		self.ID = self.topics[0] + 'determineVolumeCone'

	def addQuestion(self, doc = None):
		#ax = b or b = ax
		#ax +- b = c
		#x/a = b
		#a/bx = c
		optionChosen = random.choice(self.option)

		#if optionChosen == 



		if self.pictureDrawn == True:
			introString = 'Given the right circular cone below, '
		else:
			introString = 'Given a right circular cone, '

		if optionChosen == 'Given radius' and self.partsGivenInWords == True:
			givenString = 'the radius is %g and the height is %g, ' % (radius, height)
		elif optionChosen == 'Given diameter' and self.partsGivenInWords == True:
			givenString = 'the diameter is %g and the height is %g, ' % (diameter, height)
		else:
			givenString = ""

		if roundingChosen == 'in terms of pi':
			roundingString = 'find the volume in terms of $\pi$.'
			self.answer = '%g$\pi$' % roundedVolume
		else:
			roundingString = 'find the volume rounded to the nearest %s.' % (roundingChosen)
			self.answer = roundedVolume

		doc.append(NoEscape(r'' + introString + givenString + roundingString))

		#doc.append(NewLine())
		
		if self.pictureDrawn == True:
			with doc.create(Center()):
				cone(options = 'rotate=%d, x=3cm, y=3cm' % (self.wholeFigureRotation), 
					doc = doc, 
					radiusDrawn = self.radiusDrawn, 
					heightDrawn = self.heightDrawn, 
					diameterDrawn = self.diameterDrawn, 
					radiusLabeledOnDiagram = self.radiusLabeledOnDiagram, 
					heightLabeledOnDiagram = self.heightLabeledOnDiagram, 
					diameterLabeledOnDiagram = self.diameterLabeledOnDiagram, 
					radiusValue = radius, 
					diameterValue = diameter, 
					heightValue = height)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(r'' + str(self.answer)))
class testingEquation():
	def __init__(self, typeOfQuestion = ['x both sides', 'x both sides distributive', 'common denominator']):
		self.typeOfQuestion = typeOfQuestion
	def addQuestion(self, doc = None):
		typeChosen = random.choice(self.typeOfQuestion)
		#ax + b = cx + d
		#ax - cx = d - b
		#x(a-c) = d-b
		#x = (d-b)/(a-c)
		if typeChosen == 'x both sides':
			a = random.choice([i for i in range(-10,10) if i not in [0]])
			b = random.choice([i for i in range(-10,10) if i not in [0]])
			c = random.choice([i for i in range(-10,10) if i not in [0]])
			d = random.choice([i for i in range(-10,10) if i not in [0]])
			if (a-c) != 0:
				self.answer = r'%g' % (d-b)/(a-c)
			#if 0 on bottom, infinite
			if (a-c) == 0:
				self.answerExpression = r'Infinite Solutions'
			elif (d-b) % (a-c) == 0: #integer solution
				self.answerExpression = r'$x={%d}$' % answer
			else: #fractional
				answerFrac = Fraction(d-b, a-c)
				self.answerExpression = r'x=$\frac{%d}{%d}$' % (answerFrac.numerator, answerFrac.denominator)
			def signForNumber(number):
				if number > 0:
					return '+%d' % (abs(number))
				elif number < 0:
					return '-%d' % (abs(number))
			def one(number):
				if number == 1:
					return ""
				elif number == -1:
					return '-'
				else:
					return str(number)
			print(a, signForNumber(b), c, signForNumber(d))
			expression = r'${%s}x%s={%s}x%s$' % (one(a), signForNumber(b), one(c), signForNumber(d))
		elif typeChosen == 'x both sides distributive':
			#a(bx+c) = d(ex+f)
			a = random.choice([i for i in range(-10,10) if i not in [0]])
			b = random.choice([i for i in range(-10,10) if i not in [0]])
			c = random.choice([i for i in range(-10,10) if i not in [0]])
			d = random.choice([i for i in range(-10,10) if i not in [0]])
			e = random.choice([i for i in range(-10,10) if i not in [0]])
			f = random.choice([i for i in range(-10,10) if i not in [0]])
			#abx + ac = dex + df
			#abx - dex = df - ac
			#x(ab - de) = df - ac
			if (a*b - d*e) != 0:
				self.answer = r'%g' % (d*f - a*c)/(a*b - d*e)
			#if 0 on bottom, infinite
			if (a*b - d*e) == 0:
				self.answerExpression = r'Infinite Solutions'
			elif (d*f - a*c) % (a*b - d*e) == 0: #integer solution
				self.answerExpression = r'$x={%d}$' % answer
			else: #fractional
				answerFrac = Fraction((d*f - a*c), (a*b - d*e))
				self.answerExpression = r'$x=\frac{%d}{%d}$' % (answerFrac.numerator, answerFrac.denominator)
			def signForNumber(number):
				if number > 0:
					return '+%d' % (abs(number))
				elif number < 0:
					return '-%d' % (abs(number))
			def one(number):
				if number == 1:
					return ""
				elif number == -1:
					return '-'
				else:
					return str(number)
			expression = r'${%d}({%s}x%s)={%d}({%s}x%s)$' % (a, one(b), signForNumber(c), d, one(e), signForNumber(f))
		elif typeChosen == 'common denominator':
			#ax/b + c/d = e/f
			#bdf not -1, 0, 1, not the same\
			#ace not 0
			#bottom #'s can be bigger range (2,25), top # (1,10)
			#adfx + cbf = edb
			#adfx = edb - cbf
			#x = (edb-cbf)/adf
			a = random.choice([i for i in range(-10,10) if i not in [0]])
			c = random.choice([i for i in range(-10,10) if i not in [0]])
			e = random.choice([i for i in range(-10,10) if i not in [0]])
			
			b = random.choice([i for i in range(2,10) if i not in [1,0]])
			d = random.choice([i for i in range(2,10) if i not in [1,0]])
			f = random.choice([i for i in range(2,10) if i not in [1,0]])
			whoGetsX = random.randint(1,3)
			if whoGetsX == 1:
				part1 = r'$\frac{%d}{%d}x$' % (a, b)
				self.answer = r'%g' % (e*d*b - c*b*f)/(a*d*f)
				if (e*d*b - c * b * f) % (a*d*f) == 0:
					self.answerExpression = r'$x={%d}$' % answer
				else:
					answerFrac = Fraction((e*d*b - c*b*f), (a*d*f))
					self.answerExpression = r'$x=\frac{%d}{%d}$' % (answerFrac.numerator, answerFrac.denominator)
			else:
				part1 = r'$\frac{%d}{%d}$' % (a, b)


			if whoGetsX == 2:
				part2 = r'$\frac{%d}{%d}x$' % (c, d)
				#a/b + c/dx = e/f -- (edb - adf)/(cb) 
				self.answer = r'%g' % (e*d*b-a*d*f)/(c*b)
				if (e*d*b-a*d*f) % (c*b*f) == 0:
					self.answerExpression = r'$x={%d}$' % answer
				else:
					answerFrac = Fraction((e*d*b - c*b*f), (a*d*f))
					self.answerExpression = r'$x=\frac{%d}{%d}$' % (answerFrac.numerator, answerFrac.denominator)
			else:
				part2 = r'$\frac{%d}{%d}$' % (c, d)
				
				
			if whoGetsX == 3:
				part3 = r'$\frac{%d}{%d}x$' % (e, f)
				self.answer = r'%g' % (a*d*f+c*b*f)/(e*d*b)
				if (a*d*f+c*b*f) % (e*d*b) == 0:
					self.answerExpression = r'$x={%d}$' % answer
				else:
					answerFrac = Fraction((e*d*b - c*b*f), (a*d*f))
					self.answerExpression = r'$x=\frac{%d}{%d}$' % (answerFrac.numerator, answerFrac.denominator)
			else:
				part3 = r'$\frac{%d}{%d}$' % (e, f)
						
 
			
			expression = part1 + "+" + part2 + "=" + part3
			


		#doc.append(Command('large'))
		doc.append(NoEscape(expression))
	
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answerExpression))

class absoluteValueEquation():
	def __init__(self, typeOfEquation = ['linear'], standardOrder = True, combineLikeTerms = False):
		self.typeOfEquation = random.choice(typeOfEquation)
		self.standardOrder = standardOrder
		self.combineLikeTerms = combineLikeTerms
	def addQuestion(self, doc = None):
		#abs(ax+b) = c

		#pick any a,x,b that determines c etc
		if self.typeOfEquation == 'linear':
			coA = random.choice([x for x in range(-10,10) if x not in [0]])
			x = random.choice([x for x in range(-10,10) if x not in [0]])
			coB = random.choice([x for x in range(-10,10) if x not in [0]])

			c1 = coA * x + coB
			c2 = -(coA * x + coB)

			if (c1 - coB) % coA != 0:
				part1 = Fraction(c1-coB, coA)
				part1String = r'$x=\frac{%d}{%d}$' % (part1.numerator, part1.denominator)
			else:
				part1String = r'$x=%d$' % ( (c1-coB)/coA )
			if (c2 - coB) % coA != 0:
				part2 = Fraction(c2-coB, coA)
				part2String = r'$x=\frac{%d}{%d}$' % (part2.numerator, part2.denominator)
			else:
				part2String = r'$x=%d$' % ( (c2-coB)/coA )

			answerString = part1String + ' and ' + part2String

			if self.standardOrder == True and self.combineLikeTerms == False:
				questionString = r'$|%sx%s|=%s$' % (equationFormatting(value = coA, nextToVariable = True, inBetween = False), equationFormatting(value = coB, inBetween = True, nextToVariable = False), equationFormatting(value = c1, nextToVariable = False, inBetween = False))
			elif self.standardOrder == False and self.combineLikeTerms == False:
				questionString = r'$|%s%sx|=%s$' % (equationFormatting(value = coB, nextToVariable = False, inBetween = False), equationFormatting(value = coA, inBetween = True, nextToVariable = True), equationFormatting(value = c1, nextToVariable = False, inBetween = False))
			elif self.combineLikeTerms == True:
				#take coA, split into 2, take coB split into 2
				coA1 = random.choice([x for x in range(-10,10) if x not in [0,coA]])
				coA2 = coA - coA1
				coB1 = random.choice([x for x in range(-10,10) if x not in [0,coB]])
				coB2 = coB - coB1

				questionString = r'$|%sx%s%sx%s|=%s$' % (equationFormatting(value = coA1, nextToVariable = True, inBetween = False), equationFormatting(value = coB1, inBetween = True, nextToVariable = False), equationFormatting(value = coA2, nextToVariable = True, inBetween = True), equationFormatting(value = coB2, inBetween = True, nextToVariable = True), equationFormatting(value = c1, nextToVariable = False, inBetween = False))


		self.answer = answerString
		doc.append(NoEscape(questionString))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class squareRootEquation():
	#squareRootLinear = number?
	def __init__(self, typeOfEquation = ['linear'], standardOrder = True, combineLikeTerms = False):
		self.typeOfEquation = random.choice(typeOfEquation)
		self.standardOrder = standardOrder
		self.combineLikeTerms = combineLikeTerms
	def addQuestion(self, doc = None):
		#abs(ax+b) = c

		#pick any a,x,b that determines c etc
		if self.typeOfEquation == 'linear':
			coA = random.choice([x for x in range(-10,10) if x not in [0]])
			x = random.choice([x for x in range(-10,10) if x not in [0]])
			pSquare = random.choice([x**2 for x in range(1,12)])

			coB = pSquare - (coA * x)

			#ax + b needs to be a perfect square
			c1 = (coA * x + coB)**(1/2)

			answerString = r'$x=%d$' % ( (c1**2-coB)/coA )
			if self.standardOrder == True and self.combineLikeTerms == False:
				questionString = r'$\sqrt{%sx%s}=%s$' % (equationFormatting(value = coA, nextToVariable = True, inBetween = False), equationFormatting(value = coB, inBetween = True, nextToVariable = False), equationFormatting(value = c1, nextToVariable = False, inBetween = False))
			elif self.standardOrder == False and self.combineLikeTerms == False:
				questionString = r'$\sqrt{%s%sx}=%s$' % (equationFormatting(value = coB, nextToVariable = False, inBetween = False), equationFormatting(value = coA, inBetween = True, nextToVariable = True), equationFormatting(value = c1, nextToVariable = False, inBetween = False))
			elif self.combineLikeTerms == True:
				#take coA, split into 2, take coB split into 2
				coA1 = random.choice([x for x in range(-10,10) if x not in [0,coA]])
				coA2 = coA - coA1
				coB1 = random.choice([x for x in range(-10,10) if x not in [0,coB]])
				coB2 = coB - coB1

				questionString = r'$\sqrt{%sx%s%sx%s}=%s$' % (equationFormatting(value = coA1, nextToVariable = True, inBetween = False), equationFormatting(value = coB1, inBetween = True, nextToVariable = False), equationFormatting(value = coA2, nextToVariable = True, inBetween = True), equationFormatting(value = coB2, inBetween = True, nextToVariable = True), equationFormatting(value = c1, nextToVariable = False, inBetween = False))

		self.answer = answerString
		doc.append(NoEscape(questionString))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class squaredEquation():
	def __init__(self, radicandMax = 100, cPerfectSquare = False, cBreakDown = True):
		self.radicandMax = radicandMax
		self.cPerfectSquare = cPerfectSquare
		self.cBreakDown = cBreakDown
	def addQuestion(self, doc = None):
		# (ax+b)^2 = c, where c may or may not break down more
		a = random.choice([x for x in range(1,9) if x not in [0]]) #making it only positive cause it's like quad formula
		b = random.choice([x for x in range(-9,9) if x not in [0]])
		if self.cPerfectSquare == True:
			c = random.choice([x**2 for x in range(0, int(self.radicandMax**(1/2)+1))])
		else:
			pSquares = [x**2 for x in range(2, int(self.radicandMax**(1/2)+1))]
			if self.cBreakDown == True:
				#need only count numbers that are divisible by psquares
				options = []
				for x in range(2, self.radicandMax+1):
					for ps in pSquares:
						if x % ps == 0 and x not in pSquares:
							options.append(x)
				c = random.choice(options)
			else:
				#need only count numbers that are divisible by psquares
				options = []
				for x in range(1, self.radicandMax+1):
					psCount = 0
					for ps in pSquares:
						if x % ps == 0:
							psCount += 1
					if psCount == 0:
						options.append(x)
				c = random.choice(options)

			#find radicand and simplify
			
			for ps in [x**2 for x in range(1,int(c**(1/2)+1))]:
				if c % ps == 0:
					psChosen = ps

			radicand = c/psChosen
			numInFront = psChosen**(1/2)
			# (shit - b)/a need to see if gcd(numInFront, b) == 1 or not
			# -b + shit
			gcdStuff = gcd(a, gcd(numInFront, -b))
			gcdStuff = abs(gcdStuff)
			print(gcdStuff)
		if self.cPerfectSquare == True:
			#check if fraction or not
			answerFraction = Fraction(int(c**(1/2)-b),a)
			if answerFraction.denominator == 1:
				self.answer = r'$x=%d$' % ( answerFraction.numerator )
			else: #fraction
				self.answer = r'$x=\frac{%d}{%d}$' % ( answerFraction.numerator, answerFraction.denominator )
		else:
			if gcdStuff == 1: #can't divide at all, so keep same
				if numInFront == 1 or numInFront == -1:
					numInFrontString = ''
				else:
					numInFrontString = '%d' % abs(numInFront)
				if a == 1 or a == -1:
					self.answer = r'$%s\pm%s\sqrt{%d}$' % (equationFormatting(value = -b/a, inBetween = False, nextToVariable = False), numInFrontString, radicand)
				else:
					self.answer = r'$x=\frac{%s\pm%s\sqrt{%d}}{%d}$' % (equationFormatting(value = -b, inBetween = False, nextToVariable = False), numInFrontString, radicand, a)
			else: #meaning you can divide each number by something, ie simplifying it
				#if a/gcd == 1-1 or not
				if numInFront/(gcdStuff*a) == 1 or numInFront/(gcdStuff*a) == -1:
					numInFrontString = ''
				else:
					numInFrontString = '%d' % int(abs(numInFront/(gcdStuff)))
				if a/gcdStuff == 1 or a/gcdStuff == -1:
					self.answer = r'$%s\pm%s\sqrt{%d}$' % (equationFormatting(value = -b/(gcdStuff*a), inBetween = False, nextToVariable = False), numInFrontString, radicand)
				else: #still a fraction
					self.answer = r'$x=\frac{%s\pm%s\sqrt{%d}}{%d}$' % (equationFormatting(value = -b/gcdStuff, inBetween = False, nextToVariable = False), numInFrontString, radicand, a/gcdStuff)

		questionString = r'$(%sx%s)^2=%s$' % (equationFormatting(value = a, nextToVariable = True, inBetween = False), equationFormatting(value = b, nextToVariable = False, inBetween = True), equationFormatting(value = c, inBetween = False, nextToVariable = False))

		doc.append(NoEscape(questionString))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
		docAnswer.append(VerticalSpace('.1in'))
		docAnswer.append(NewLine())

def exponentString(variableString, exponentValue):
			if exponentValue == 0:
				return ''
			elif exponentValue == 1:
				return variableString
			else:
				return r'%s^{%d}' % (variableString, exponentValue)


def numberString(value):
	if value == 1:
		return ''
	elif value == -1:
		return r'-'
	else:
		return r'%d' % value
class GCF():
	def __init__(self, numberOfTerms = [2], numberMax = 5):
		self.dick = 1
		self.numberOfTerms = numberOfTerms
		self.numberMax = numberMax
		self.typeOfQuestionChosen = 'SA'
		#let's do 1-3 variables, 1-3 terms
		#so we have 3 numbers, 3 variabes, 3 variables exponents
		#types: no number GCF
		#for numbers, go backwards, pick coprime numbers and random answer in front, then multiply
		#exponents of same variable shoudn't be equal to eliminate having the same terms
	
		#when is GCF useful???
		#taking out a number, taking out like 1 or 2 x's, maybe 4 etc for completely factoring
		#


	def addQuestion(self, doc = None):
		numberOfTermsChosen = random.choice(self.numberOfTerms)
		#COEF ( num1 + num2 + num3)

		num1 = random.choice([x for x in range(2, 26) if x not in [0]])
		num2 = random.choice(noSharedFactors(num1, range(-25,25+1)))
		if numberOfTermsChosen == 3:
			num3 = random.choice([x for x in range(-25, 26) if x not in [0]])

		answerNum = random.randint(2,self.numberMax)
		

		answerXE = random.randint(1,10) # 7
		if numberOfTermsChosen == 2:
			temp1 = random.randint(1,answerXE)  #5
			temp3 = 0
			listOfExponents = [temp1, temp3]
			x1E = random.choice(listOfExponents)
			listOfExponents.remove(x1E)
			x2E = random.choice(listOfExponents)

		else:
			temp1 = random.randint(1,answerXE)  #5
			temp2 = random.randint(1,temp1) # 3
			temp3 = 0
			listOfExponents = [temp1, temp2, temp3]
			x1E = random.choice(listOfExponents)
			listOfExponents.remove(x1E)
			x2E = random.choice(listOfExponents)
			listOfExponents.remove(x2E)
			x3E = random.choice(listOfExponents)

		gcfPart = r'%s%s' % (numberString(answerNum), exponentString('x', answerXE))
		answerPart1 = r'%s%s' % (numberString(num1), exponentString('x', x1E))
		answerPart2 = r'-%s%s' % (numberString(abs(num2)), exponentString('x', x2E))
		if num2 > 0:
			answerPart2 = r'+%s%s' % (numberString(abs(num2)), exponentString('x', x2E))

		if numberOfTermsChosen == 3:
			if num3 < 0:
				answerPart3 = r'-%s%s' % (numberString(abs(num3)), exponentString('x', x3E))
			elif num3 > 0:
				answerPart3 = r'+%s%s' % (numberString(num3), exponentString('x', x3E))

		if numberOfTermsChosen == 2:
			self.answer = r'$' + gcfPart + '(' + answerPart1 + answerPart2 + ")" + r'$'
		else:
			self.answer = r'$' + gcfPart + "(" + answerPart1 + answerPart2 + answerPart3 + ")" + r'$'
		

		question1 = r'%s%s' % (numberString(answerNum*num1), exponentString('x', answerXE+x1E))

		if answerNum * num2 < 0:
			question2 = r'-%s%s' % (numberString(abs(answerNum*num2)), exponentString('x', answerXE+x2E))
		else:
			question2 = r'+%s%s' % (numberString(abs(answerNum*num2)), exponentString('x', answerXE+x2E))
		
		if numberOfTermsChosen == 3:
			if answerNum * num3 < 0:
				question3 = r'-%s%s' % (numberString(abs(answerNum*num3)), exponentString('x', answerXE+x3E))
			elif answerNum * num3 > 0:
				question3 = r'+%s%s' % (numberString(answerNum*num3), exponentString('x', answerXE+x3E))

		if numberOfTermsChosen == 2:
			self.question = r'$' + question1 + question2 + r'$'
		else:
			self.question = r'$' + question1 + question2 + question3 + r'$'
		doc.append(NoEscape(self.question))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class DOTS():
	def __init__(self, difficulty = [1,2], GCFMax = 10, GCFNumber = False, GCFVariable = False):
		china = 1
		#types:
		#TYPE1: x^2 - ps
		#TYPE2: psx^2 - ps
		#TYPE3: GCF first
		#TYPE4: trick where two psquares have a common factor
		self.difficulty = difficulty
		self.GCFNumber = GCFNumber
		self.GCFVariable = GCFVariable
		self.typeOfQuestionChosen = 'SA'
		self.GCFMax = GCFMax
	def addQuestion(self, doc = None):
		perfectSquares = [x**2 for x in range(15) if x not in [0]]
		self.p1 = random.choice(perfectSquares)
		factorsNotShared = noSharedFactors(self.p1, perfectSquares)
		self.p2 = random.choice(factorsNotShared)
		self.GCF = random.randint(2,self.GCFMax)
		self.variable = random.choice(['x','y','z'])
		difficultyChosen = random.choice(self.difficulty)


		if difficultyChosen == 1:
			if self.GCFNumber == True:
				if self.GCFVariable == True:
					takeOut = random.randint(1,5)
					doc.append(NoEscape(r'$%d%s^{%d}-%d%s$' % (self.GCF, self.variable, takeOut+2, self.GCF*self.p1, exponentString(self.variable, takeOut))))
					self.answer = r'$%d%s(%s+%d)(%s-%d)$' % (self.GCF, exponentString(self.variable, takeOut), self.variable, self.p1**(1/2), self.variable, self.p1**(1/2))
				else:
					doc.append(NoEscape(r'$%d%s^{2}-%d$' % (self.GCF, self.variable, self.GCF*self.p1)))
					self.answer = r'$%d(%s+%d)(%s-%d)$' % (self.GCF, self.variable, self.p1**(1/2), self.variable, self.p1**(1/2))
			else:
				doc.append(NoEscape(r'$%s^{2}-%d$' % (self.variable, self.p1)))
				self.answer = r'$(%s+%d)(%s-%d)$' % (self.variable, self.p1**(1/2), self.variable, self.p1**(1/2))


		elif difficultyChosen == 2:
			if self.GCFNumber == True:
				if self.GCFVariable == True:
					takeOut = random.randint(2,5)
					doc.append(NoEscape(r'$%d%s^{%d}-%d%s$' % (self.p1*self.GCF, self.variable, 2+takeOut, self.p2*self.GCF, exponentString(self.variable, takeOut))))
					self.answer = r'$%d%s(%d%s+%d)(%d%s-%d)$' % (self.GCF, exponentString(self.variable, takeOut), self.p1**(1/2), self.variable, self.p2**(1/2), self.p1**(1/2), self.variable, self.p2**(1/2))
				else:
					doc.append(NoEscape(r'$%d%s^{2}-%d$' % (self.p1*self.GCF, self.variable, self.p2*self.GCF)))
					self.answer = r'$%d(%d%s+%d)(%d%s-%d)$' % (self.GCF, self.p1**(1/2), self.variable, self.p2**(1/2), self.p1**(1/2), self.variable, self.p2**(1/2))
			else:
				doc.append(NoEscape(r'$%d%s^{2}-%d$' % (self.p1, self.variable, self.p2)))
				self.answer = r'$(%d%s+%d)(%d%s-%d)$' % (self.p1**(1/2), self.variable, self.p2**(1/2), self.p1**(1/2), self.variable, self.p2**(1/2))
		print(self.answer)
		#elif self.difficulty == 3:
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class factorTrinomial():
	def __init__(self, bSign = ['positive','negative'], cSign = ['positive','negative'], aEqualsOne = True):
		self.bSign = bSign
		self.cSign = cSign
		self.aEqualsOne = aEqualsOne
		self.typeOfQuestionChosen = 'SA'
		
	def addQuestion(self, doc = None):
		bSignChosen = random.choice(self.bSign)
		cSignChosen = random.choice(self.cSign)
		#(tx+r)(vx+s)
		if self.aEqualsOne == True:
			if cSignChosen == 'positive':
				r = random.randint(1,20)
				s = random.randint(1,20)
				if bSignChosen == 'positive':
					doNothing = 1
				else:
					r = -1*r
					s = -1*s
			else:
				nums = [x for x in range(21) if x not in [0]]
				r = random.choice(nums)
				s = random.choice([x for x in nums if x != r])
				while r == s:
					print('uhh')
					r = random.randint(1,20)
					s = random.randint(1,20)
				if bSignChosen == 'positive':
					#make two random numbers, make smaller number negative
					if r > s:
						s = s*-1
					else:
						r = r*-1
				elif bSignChosen == 'negative':
					if r > s:
						r = r*-1
					else:
						s = s*-1
			
			#(tx+r)(vx+s)
			questionString = r'$x^{2}%sx%s$' % (equationFormatting(r+s, nextToVariable = True), equationFormatting(r*s))
			self.answer = r'$(x%s)(x%s)$' % (equationFormatting(r), equationFormatting(s))
			doc.append(NoEscape(questionString))
		elif self.aEqualsOne == False:
			if cSignChosen == 'positive':
				tvItems = [x for x in range(1,4)]
				t = random.choice(tvItems)
				if t == 1:
					tvItems.remove(t)
				rItems = [x for x in range(15) if x not in [0]]
				r = random.choice(noSharedFactors(t, rItems))

				v = random.choice(tvItems) #note v and t can't be both 1
				sItems = [x for x in range(15) if x not in [0]]
				s = random.choice(noSharedFactors(v, sItems))

				if bSignChosen == 'positive':
					doNothing = 1
				else:
					r = -1*r
					s = -1*s
			else:
				tvItems = [x for x in range(1,4)]
				t = random.choice(tvItems)
				if t == 1:
					tvItems.remove(t)
				rItems = [x for x in range(15) if x not in [0]]
				r = random.choice(noSharedFactors(t, rItems))

				v = random.choice(tvItems) #note v and t can't be both 1
				sItems = [x for x in range(15) if x not in [0]]
				s = random.choice(noSharedFactors(v, sItems))
				#(tx+r)(vx+s)
				if bSignChosen == 'positive':
					#make two random numbers, make smaller number negative
					if r*v > s*t:
						s = s*-1
					else:
						r = r*-1
					if r*v == s*t:
						print('error')
				elif bSignChosen == 'negative':
					if r*v > s*t:
						r = r*-1
					else:
						s = s*-1
					if r*v == s*t:
						print('error')
			
			questionString = r'$%dx^{2}%sx%s$' % (t*v, equationFormatting(r*v+s*t, nextToVariable = True), equationFormatting(r*s))
			self.answer = r'$(%sx%s)(%sx%s)$' % (equationFormatting(t, inBetween = False, nextToVariable = True),equationFormatting(r, inBetween = True, nextToVariable = False), equationFormatting(v, inBetween = False, nextToVariable = True), equationFormatting(s, inBetween = True, nextToVariable = False))
			doc.append(NoEscape(questionString))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class factorByGrouping():
	def __init__(self, numberMax = 10, groupingGCF = ['random','1','-1','positive','negative','positiveNot1','negativeNot-1']):
		dick = 3
		#if groupingGCF == 1, then for factoring the second pair, GCF is 1, same with if groupbGCF is -1
		#GCFNumber is for factoring completely
		#GCFVariable is for factoring completely
		"""
		type1: (x^2+2)(x-3), so x^3 + 2x - 3x^2 - 6
		-subtle things to notice, if gcf is 1, -1

		type2: (x^2+2x)(y-3), so x^2y + 2xy - 3x^2 - 6x
		-subtle things to notice, if gcf is 1,-1 etc
		type3: (x+2)(x^2-3x+1), so x^3+2x^2 -3x^2-6x +x+2

		"""
		self.numberMax = numberMax
		self.groupingGCF = random.choice(groupingGCF)

	def addQuestion(self, doc = None):
		#(ax^2+b)(cx+d)
		#b can't be a perfect square and also not share a factor with a
		#c can't share a factor with d
		#acx^3+adx^2+bcx+bd -- don't mess with order lol
		#ax^2(cx+d) + b(cx+d)
		#so now I can control what I take out

		a = random.choice([x for x in range(1,self.numberMax+1) if x not in [0]])
		pSquares = [x**2 for x in range(2,self.numberMax+1)]
		if self.groupingGCF == '1':
			b = 1
		elif self.groupingGCF == '-1':
			b = -1
		elif self.groupingGCF == 'positive':
			b = random.choice([x for x in range(1,self.numberMax+1) if gcd(x,a) == 1 and x not in pSquares])
		elif self.groupingGCF == 'negative':
			b = random.choice([x for x in range(-self.numberMax,0) if gcd(x,a) == 1 and x not in pSquares])
		elif self.groupingGCF == 'positiveNot1':
			b = random.choice([x for x in range(2,self.numberMax+1) if gcd(x,a) == 1 and x not in pSquares])
		elif self.groupingGCF == 'negativeNot-1':
			b = random.choice([x for x in range(-self.numberMax,-1) if gcd(x,a) == 1 and x not in pSquares])
		else:
			b = random.choice([x for x in range(-self.numberMax,self.numberMax+1) if gcd(x,a) == 1 and x not in pSquares])

		c = random.choice([x for x in range(1,self.numberMax+1) if x not in [0]])

		if c == 1:
			d = random.choice([x for x in range(-self.numberMax,self.numberMax+1) if x not in [0]])
		else:
			d = random.choice([x for x in range(-self.numberMax,self.numberMax+1) if gcd(x,c) == 1])

		questionString = r'$%sx^3%sx^2%sx%s$' % (equationFormatting(value = a*c, inBetween = False, nextToVariable = True),
		equationFormatting(value = a*d, inBetween = True, nextToVariable = True),
		equationFormatting(value = b*c, inBetween = True, nextToVariable = True),
		equationFormatting(value = b*d, inBetween = True, nextToVariable = False) )

		self.answer = r'$(%sx^2%s)(%sx%s)$' % ( equationFormatting(value = a, inBetween = False, nextToVariable = True),equationFormatting(value = b, nextToVariable = False, inBetween = True), equationFormatting(value = c, nextToVariable = True, inBetween = False), equationFormatting(value = d, nextToVariable = False, inBetween = True))

		doc.append(NoEscape(questionString))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class factorPerfectCubes():
	def __init__(self, cubeMax = 5, difficulty = [1,2,3], version = ['sum','difference'], GCFNumber = False, GCFNumberMax = 10):
		self.cubeMax = 5
		self.version = random.choice(version)
		self.difficulty = random.choice(difficulty)
		self.GCFNumber = GCFNumber
		self.GCFNumberMax = GCFNumberMax
	def addQuestion(self, doc = None):
		#ax^3 + b^3
		#ax^3 - b^3
		#x^3 - 2^3
		#diff == 1 --> x^3 - b^3
		#diff == 2 --> ax^3 - b^3
		#diff == 3 --> ax^3 - b^3

		#basic x^3 - ax^3: sum or difference

		#only gonna use abs(a) and abs(b) and I'll put the correct signs in there myself
		#a^3 + b^3 = (a+b)(a^2 - ab + b^2)
		#a^3 - b^3 = (a-b)(a^2 + ab + b^2)

		aVariable = random.choice(['x','a','b','c'])
		bVariable = random.choice(['y','z','k','m','n'])
		exponentDegree = 3
		#basic type where a = just x^3 and b = random.randint(1,5)
		if self.difficulty == 1:
			b = random.randint(1,self.cubeMax)
			a = 1
			aVariableString = r'%s^%d' % (aVariable, exponentDegree)
			bVariableString = ''

		elif self.difficulty == 2 or self.difficulty == 3: #a^3x^3 +- b^3 OR a^3x^3 +- b^3y^3
			perfectCubes = [x**3 for x in range(2,self.cubeMax+1)]
			a = random.choice([x for x in perfectCubes])
			b = random.choice([x for x in perfectCubes if gcd(a,x) == 1])
			print(a,b)
			if gcd(a,b) != 1:
				print('awoeifjaowiefjaowiefjaowiefj',a,b)
			a = int(round(a**(1/3),0))
			b = int(round(b**(1/3),0))
			if a == b:
				print('dick')
				print(a,b)
			#a and b are both perfect cubes but they can't have a common factor between them
			aVariableString = r'%s^%d' % (aVariable, exponentDegree)
			if self.difficulty == 3:
				bVariableString = r'%s^%d' % (bVariable, exponentDegree)
			else:
				bVariableString = ''
		print(a,b)
		if self.GCFNumber == True:
			GCFNumber = random.randint(2,self.GCFNumberMax)
			#random any number greater than 2 to take out of the equation etc
		else:
			GCFNumber = 1

		if self.version == 'sum':
			questionString = r'$%s%s+%s%s$' % (equationFormatting(value = GCFNumber*a**3, inBetween = False, nextToVariable = True), aVariableString, equationFormatting(value = GCFNumber*b**3, inBetween = False, nextToVariable = False), bVariableString )
			self.answer = r'$%s(%d%s+%d)(%d%s^2-%d%s+%d)$' % (equationFormatting(value = GCFNumber, inBetween = False, nextToVariable = True), a, aVariable, b, a**2, aVariable, a*b, aVariable,b**2)
		else: #version == difference
			questionString = r'$%s%s-%s%s$' % (equationFormatting(value = GCFNumber*a**3, inBetween = False, nextToVariable = True), aVariableString,
			equationFormatting(value = GCFNumber*b**3, inBetween = False, nextToVariable = False), bVariableString )
			self.answer = r'$%s(%d%s-%d)(%d%s^2+%d%s+%d)$' % (equationFormatting(value = GCFNumber, inBetween = False, nextToVariable = True),
			a, aVariable, b, a**2, aVariable, a*b, aVariable, b**2)

		doc.append(NoEscape(questionString))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class simplifyRadicals():
	def __init__(self, coefficient = False, squareMax = 12):
		self.coefficient = coefficient
		self.squareMax = squareMax
	def addQuestion(self, doc = None):
		#sqrt(psquare * (number that is not divisble by any psquares) )
		pSquares = [x**2 for x in range(2,self.squareMax)]
		underRootNumbers = []
		pSquareChosen = random.choice(pSquares)
		for x in range(1, self.squareMax//2):
			if x * pSquareChosen not in [x**2 for x in range(2,self.squareMax**2)]:
				underRootNumbers.append(x)
		print(underRootNumbers)
		
		underRootNumberChosen = random.choice(underRootNumbers)
		radicand = pSquareChosen * underRootNumberChosen
		
		if self.coefficient == False:
			questionString = r'$\sqrt{%d}$' % radicand
			self.answer = r'$%d\sqrt{%d}$' % ( pSquareChosen**(1/2), underRootNumberChosen)
		else:
			coefficient = random.randint(1, self.squareMax)
			questionString = r'$%d\sqrt{%d}$' % (coefficient, radicand)
			self.answer = r'$%d\sqrt{%d}$' % ( coefficient*pSquareChosen**(1/2), underRootNumberChosen)

		doc.append(NoEscape(questionString))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

import random
import string
import math
def systemOfEquations(numberOfVariables = 2, difficulty = [1,2,3], standardOrder = True, typeOfSolution = ['integer','infinite','none'], constantMax = 10, solutionMax = 10):
	#3x3 doesn't have infinite or none

	"""
	difficulty1 = add equations
	difficulty2 = multiply one equation by a number
	difficulty3 = multiply both equations by a number

	standard order means ax + by = c instead of ax = c + by

	typeOfSoution: integer, start with integers
	fraction, start with fractions
	infinite, two equations that are multiples of each other
	none, two equations that are multiples of each other but one of the constants is not a multiple
	"""

	# a1*v1 + b1*v2 = c1
	# a2*v1 + b2*v2 = c2

	#determine types etc
	difficulty = random.choice(difficulty)
	typeOfSolution = random.choice(typeOfSolution)

	#Determine solutions first
	v1Value = random.randint(-1*solutionMax, solutionMax)
	if v1Value == 0: #Eliminates double 0 action, which would make system boring as hell
		v2Value = random.choice([x for x in range(-1*solutionMax, solutionMax+1) if x not in [0]])
	else:
		v2Value = random.randint(-1*solutionMax, solutionMax)
	if v1Value == 0 and v2Value == 0:
		v3Value = random.choice([x for x in range(-1*solutionMax, solutionMax+1) if x not in [0]])
	else:
		v3Value = random.randint(-1*solutionMax, solutionMax)

	if numberOfVariables == 2:
		if typeOfSolution == 'integer':
			rangeExluding0 = [x for x in range(-1*constantMax,constantMax+1) if x not in [0]]
			whichVariable = random.randint(1,2)
			if difficulty == 1: #meaning either first variable gets eliminated or second
				if whichVariable == 1: #make a1 and a2 opposite, then not opposites
					a1 = random.choice([x for x in rangeExluding0])
					a2 = a1 * -1
					b1 = random.choice([x for x in rangeExluding0])
					b2 = random.choice([x for x in rangeExluding0 if x not in [-1*b1]])
				else:
					b1 = random.choice([x for x in rangeExluding0])
					b2 = b1 * -1
					a1 = random.choice([x for x in rangeExluding0])
					a2 = random.choice([x for x in rangeExluding0 if x not in [-1*a1]])
			elif difficulty == 2: #meaning none of the coeffecients are opposite and gcd != 1
				print('difficulty here')
				if whichVariable == 1: #make a1 and a2 easier to eliminate, so b's are coprime etc
					#a's are neither 0, and one is a multiple of the other or the same so multiply by -1
					a1 = random.choice([x for x in rangeExluding0 if x in [1,-1] or x % 2 == 0])
					print(a1)
					if a1 == 1 or a1 == -1:
						a2 = random.choice([x for x in rangeExluding0 if x not in [-a1,a1] ])
					else: #% == 0 so it will be a multiple
						a2 = random.choice([x for x in rangeExluding0 if (x % a1 == 0 or a1 % x == 0) and x not in [-a1,a1] ])

					#b's are not 0, not a multiple of each other ie one % the other doesn't == 0
					b1 = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
					b2 = random.choice([x for x in rangeExluding0 if (x % b1 != 0 and b1 % x != 0)])
				else:
					b1 = random.choice([x for x in rangeExluding0 if x in [1,-1] or x % 2 == 0])
					print(b1)
					if b1 == 1 or b1 == -1:
						b2 = random.choice([x for x in rangeExluding0 if x not in [-b1,b1] ])
					else:
						b2 = random.choice([x for x in rangeExluding0 if (x % b1 == 0 or b1 % x == 0) and x not in [-b1,b1] ])

					#b's are not 0, not a multiple of each other ie gcd == 1
					a1 = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
					a2 = random.choice([x for x in rangeExluding0 if (x % a1 != 0 and a1 % x != 0) ])
			elif difficulty == 3: #all a's and b's to be coprime with each other
				#which variable doesn't really matter that much here
					a1 = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
					print('a1 is', a1)
					a2 = random.choice([x for x in rangeExluding0 if (x % a1 != 0 and a1 % x != 0) and (x not in [-1,1,-a1,a1])])
					if a1 >= a2: # meaning a1 * constant = a2
						constantA = a2/a1
						b1 = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
						print('b1 is',b1)
						b2 = random.choice([x for x in rangeExluding0 if (x % b1 != 0 and b1 % x != 0) and (x not in [b1,-b1,int(round(b1*constantA)),-1,1])])
					elif a2 >= a1: #a2 is bigger
						constantA = a1/a2
						b2 = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
						b1 = random.choice([x for x in rangeExluding0 if (x % b2 != 0 and b2 % x != 0) and (x not in [b2,-b2,int(round(b2*constantA)),-1,1])])

			c1 = a1*v1Value + b1*v2Value
			c2 = a2*v1Value + b2*v2Value
		elif typeOfSolution == 'infinite' or typeOfSolution == 'none':
			#one equation is the exact same but a multiple of the other, just do interegers lawl			
			#defining none will be as simple as making the c's not multiples
			rangeExluding0 = [x for x in range(-1*constantMax,constantMax+1) if x not in [0]]
			a1 = random.choice(rangeExluding0)
			b1 = random.choice(rangeExluding0)
			c1 = a1*v1Value + b1*v2Value
			constantMultiplier = random.randint(2,5)
			a2 = a1 * constantMultiplier
			b2 = b1 * constantMultiplier
			if typeOfSolution == 'infinite':
				c2 = c1 * constantMultiplier
			elif typeOfSolution == 'none':
				c2 = random.choice([x for x in range(-3*constantMax, constantMax+1) if x not in [0,c1*constantMultiplier]])

		return a1, a2, b1, b2, c1, c2, v1Value, v2Value
	elif numberOfVariables == 3:
		#a1, b1, c1 = d1
		#a2, b2, c2 = d2
		#a3, b3, c3 = d3
		rangeExluding0 = [x for x in range(-1*constantMax,constantMax+1) if x not in [0]]

		#pick 1's
		a1 = random.choice(rangeExluding0)
		b1 = random.choice(rangeExluding0)
		c1 = random.choice(rangeExluding0)

		#pick 2's pick a2, then b2 can't be b1 * (a2/a1)
		a2 = random.choice(rangeExluding0)
		b2 = random.choice([x for x in rangeExluding0 if x not in [b1*(a2/a1)]])
		c2 = random.choice(rangeExluding0)

		#pick 3's pick a3, b3, c3 can't be c2 * (a3/a2), c1 * (a3/a1)
		a3 = random.choice(rangeExluding0)
		b3 = random.choice(rangeExluding0)
		c3 = random.choice([x for x in rangeExluding0 if x not in [c2*(a3/a2), c1*(a3/a1)]])

		#this should make it so all 3 equations are not mutliples of each other
		d1 = a1*v1Value + b1*v2Value + c1*v3Value
		d2 = a2*v1Value + b2*v2Value + c2*v3Value
		d3 = a3*v1Value + b3*v2Value + c3*v3Value

		print(a1, b1, c1, d1)
		print(a2, b2, c2, d2)
		print(a3, b3, c3, d3)
		return a1, a2, a3, b1, b2, b3, c1, c2, c3, d1, d2, d3, v1Value, v2Value, v3Value
		
	print( a1, a2, b1, b2, c1, c2)



class solveSystemOfEquations():
	def __init__(self, numberOfVariables = 3, difficulty = [1,2,3], standardOrder = True, typeOfSolution = ['integer','infinit','none'], constantMax = 10, solutionMax = 10):
		self.numberOfVariables = numberOfVariables
		self.difficulty = difficulty
		self.standardOrder = standardOrder
		self.typeOfSolution = typeOfSolution
		self.constantMax = constantMax
		self.solutionMax = solutionMax
		self.typeOfQuestionChosen = 'SA'
	def addQuestion(self, doc = None):
		#random variables
		lowercaseLetters = [x for x in string.ascii_lowercase]
		random.shuffle(lowercaseLetters)
		v1String = lowercaseLetters[0]
		v2String = lowercaseLetters[1]
		v3String = lowercaseLetters[2]

		#2
		if self.numberOfVariables == 2:
			a1, a2, b1, b2, c1, c2, v1Value, v2Value = systemOfEquations(numberOfVariables = self.numberOfVariables, difficulty = self.difficulty, standardOrder = self.standardOrder, typeOfSolution = self.typeOfSolution, constantMax = self.constantMax, solutionMax = self.solutionMax)
			#first equation string
			equationString1 = r'$%s%s%s%s=%s$' % (equationFormatting(value = a1, nextToVariable = True, inBetween = False),
				v1String, 
				equationFormatting(value = b1, nextToVariable = True, inBetween = True), 
				v2String,
				equationFormatting(value = c1, nextToVariable = False, inBetween = False))
			#second equation string
			equationString2 = r'$%s%s%s%s=%s$' % (equationFormatting(value = a2, nextToVariable = True, inBetween = False),
				v1String, 
				equationFormatting(value = b2, nextToVariable = True, inBetween = True), 
				v2String,
				equationFormatting(value = c2, nextToVariable = False, inBetween = False))
			#answer
			self.answer = r'$%s=%s$ and $%s=%s$' % (v1String, equationFormatting(value = v1Value, nextToVariable = False, inBetween = False), v2String, equationFormatting(value = v2Value, nextToVariable = False, inBetween = False))
		#3
		if self.numberOfVariables == 3:
			a1, a2, a3, b1, b2, b3, c1, c2, c3, d1, d2, d3, v1Value, v2Value, v3Value = systemOfEquations(numberOfVariables = self.numberOfVariables, difficulty = self.difficulty, standardOrder = self.standardOrder, typeOfSolution = self.typeOfSolution, constantMax = self.constantMax, solutionMax = self.solutionMax)
			#first equation string
			equationString1 = r'$%s%s%s%s%s%s=%s$' % (equationFormatting(value = a1, nextToVariable = True, inBetween = False),
				v1String, 
				equationFormatting(value = b1, nextToVariable = True, inBetween = True), 
				v2String,
				equationFormatting(value = c1, nextToVariable = True, inBetween = True),
				v3String,
				equationFormatting(value = d1, nextToVariable = False, inBetween = False))
			
			#second equation string
			equationString2 = r'$%s%s%s%s%s%s=%s$' % (equationFormatting(value = a2, nextToVariable = True, inBetween = False),
				v1String, 
				equationFormatting(value = b2, nextToVariable = True, inBetween = True), 
				v2String,
				equationFormatting(value = c2, nextToVariable = True, inBetween = True),
				v3String,
				equationFormatting(value = d2, nextToVariable = False, inBetween = False))
			
			#third equation string
			equationString3 = r'$%s%s%s%s%s%s=%s$' % (equationFormatting(value = a3, nextToVariable = True, inBetween = False),
			v1String, 
			equationFormatting(value = b3, nextToVariable = True, inBetween = True), 
			v2String,
			equationFormatting(value = c3, nextToVariable = True, inBetween = True),
			v3String,
			equationFormatting(value = d3, nextToVariable = False, inBetween = False))
			#answer
			self.answer = r'$%s=%s$, $%s=%s$, and $%s=%s$' % (v1String, equationFormatting(value = v1Value, nextToVariable = False, inBetween = False), v2String, equationFormatting(value = v2Value, nextToVariable = False, inBetween = False), v3String, equationFormatting(value = v3Value, nextToVariable = False, inBetween = False))
		doc.append(NoEscape('Solve the following system of equations for all the variables:'))
		doc.append(NewLine())
		with doc.create(Center()):
			doc.append(NoEscape(equationString1))
		#doc.append(NewLine())
		with doc.create(Center()):
			doc.append(NoEscape(equationString2))
		#doc.append(NewLine())
		if self.numberOfVariables == 3:
			with doc.create(Center()):
				doc.append(NoEscape(equationString3))
			#doc.append(NewLine())
			

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))


		#centered, space between each equation all math mode as well
		#Solve the following system of equations for all the variables:
############FUNCTIONS################
#typeOfFunctions refers to how many functions etc
class evaluateFunction():
	def __init__(self, typeOfFunctions = ['linear','cubic','quadratic'], inputType = ['integer','monomial','binomial','trinomial'], composition = False, numberOfFunctionsComposition = 2, operationBetweenFunctions = ['multiplying','dividing','subtracting','adding'], typeOfQuestion = ['SA']):

		self.typeOfFunctions = typeOfFunctions
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.inputType = inputType
		self.composition = composition
		self.operationBetweenFunctions = operationBetweenFunctions
		print(self.typeOfFunctions, self.typeOfQuestionChosen, self.inputType, self.composition, self.operationBetweenFunctions)
	def addQuestion(self, doc = None):
		inputTypeChosen = random.choice(self.inputType)
		print(inputTypeChosen)
		functions = {}
		functionNames = []
		letters = string.ascii_lowercase
		#define functions given by typeOfFunctions...need letter...need type
		lettersList = [x for x in letters]
		lettersList.remove('x')
		for item in self.typeOfFunctions:
			letterChosen = random.choice(lettersList)
			functionNames.append(letterChosen)
			functions[letterChosen] = item
			lettersList.remove(letterChosen)

		#random letter
		#linear, cubic, quadratic

		#number of functions to evaluate is equal to len of type of functions...if more than 1 then it's implied to use an operationbetween the functions...if 3 same thing etc etc

		def functionByType(typeOfFunction = 'linear', inputValue = 0, minCo = -10, maxCo = 10, functionLetter = 'a'):
			a = random.choice([x for x in range(minCo,maxCo) if x not in [0]])
			b = random.choice([x for x in range(minCo,maxCo) if x not in [0]])
			c = random.choice([x for x in range(minCo,maxCo) if x not in [0]])	
			d = random.choice([x for x in range(minCo,maxCo) if x not in [0]])	
			
			if typeOfFunction == 'linear':
				try:
					functionString = r'${%s(x)=%sx%s}$' % (functionLetter, equationFormatting(value = a, inBetween = False, nextToVariable = True), equationFormatting(value = b, inBetween = True, nextToVariable = False))
					if len(inputValue) == 1: #monomial
						#ax+b --> a(Ax) + b
						answerString = r'$%sx%s$' % (equationFormatting(value = a*inputValue[0], inBetween = False, nextToVariable = True), equationFormatting(value = b, nextToVariable = False, inBetween = True))
					elif len(inputValue) == 2: #binomial
						#ax+b --> a(Ax+B)+b --> aAx + aB + b
						answerString = r'$%sx%s$' % (equationFormatting(value = a*inputValue[0], inBetween = False, nextToVariable = True), equationFormatting(value = a*inputValue[1]+b, inBetween = True, nextToVariable = False))
					elif len(inputValue) == 3: #trinomial
						#ax + b --> a(Ax^2 + Bx + C) + b
						#aAx^2 + aBx + aC + b
						#aAx^2 + aBx + (aC + b)
						answerString =  r'$%sx^{2}%sx%s$' % (equationFormatting(value = a*inputValue[0], inBetween = False, nextToVariable = True), equationFormatting(value = a*inputValue[1], inBetween = True, nextToVariable = True), equationFormatting(value = a*inputValue[2]+b, inBetween = True, nextToVariable = False))
				except:
					blah = 3	
					#ax + b OR b + ax
					typeChosen = random.randint(1,2)
					if typeChosen == 1: #ax + b
						functionString = r'${%s(x)=%sx%s}$' % (functionLetter, equationFormatting(value = a, inBetween = False, nextToVariable = True), equationFormatting(value = b, inBetween = True, nextToVariable = False))
						answer = a*inputValue + b
					elif typeChosen == 2: #b + ax
						functionString = r'${%s(x)=%s%sx}$' % (functionLetter, equationFormatting(value = b, inBetween = False, nextToVariable = False), equationFormatting(value = a, inBetween = True, nextToVariable = True))
						answer = b + a*inputValue
			
			elif typeOfFunction == 'quadratic':
				try:
					functionString = r'${%s(x)=%sx^{2}%sx%s}$' % (functionLetter, equationFormatting(value = a, inBetween = False, nextToVariable = True), equationFormatting(value = b, inBetween = True, nextToVariable = True), equationFormatting(value = c, inBetween = True, nextToVariable = False))
					if len(inputValue) == 1: #monomial
						#ax^2+bx + c --> a(Ax)^2 + b(Ax) + c
						answerString = r'$%sx^{2}%sx%s$' % (equationFormatting(value = a*inputValue[0]**2, inBetween = False, nextToVariable = True), equationFormatting(value = b*inputValue[0], nextToVariable = True, inBetween = True), equationFormatting(value = c, nextToVariable = False, inBetween = True))
					elif len(inputValue) == 2: #binomial
						#ax^2+bx+c --> a(Ax+B)^2+b(Ax+b) + c 
						#a( (Ax)^2 + 2ABx + B^2 ) + Abx + b^2 + c
						#aA^2x^2 + 2ABax + aB^2 + Abx + b^2 + c
						#aA^2x^2 + (2ABa + Ab)x + (aB^2 + b^2 + c)
						answerString = r'$%sx^{2}%sx%s$' % (equationFormatting(value = a*inputValue[0]**2, inBetween = False, nextToVariable = True), equationFormatting(value = 2*a*inputValue[1]*inputValue[0]+inputValue[0]*b, inBetween = True, nextToVariable = True), equationFormatting(value = a*inputValue[1]**2 + b**2 + c, nextToVariable = False, inBetween = True))
					elif len(inputValue) == 3: #trinomial
						#ax^2 + bx + c --> a(Ax^2 + Bx + c)^2 + b(Ax^2 + Bx + c) + c
						#a(Ax^2 + Bx + c)(Ax^2 + Bx + c) + b(Ax^2 + Bx + c) + c
						#a(A^2x^4 + ABx^3 + Acx^2 + ABx^3 + B^2x^2 + Bcx + Acx^2 + Bcx + c^2) + Abx^2 + bBx + bc + c
						#aA^2x^4 + 2aABx^3 + (2Aca + AB + aB^2)x^2 + (2aBc + bB)x + (ac^2 + bc + c)
						answerString =  r'$%sx^{4}%sx^{3}%sx^{2}%sx%s$' % (equationFormatting(value = a*inputValue[0]**2, inBetween = False, nextToVariable = True), equationFormatting(value = 2*a*inputValue[1]*inputValue[0], inBetween = True, nextToVariable = True), equationFormatting(value = (2*inputValue[0]*c*a + inputValue[0]*inputValue[1] + a*inputValue[1]**2), inBetween = True, nextToVariable = True), equationFormatting(value = (2*a*inputValue[1]*c + b*inputValue[1]), inBetween = True, nextToVariable = True), equationFormatting(value = (a*c**2 + b*c + c), inBetween = True, nextToVariable = False))
				except:
					blah = 2
					#ax^2 + bx + c
					functionString = r'${%s(x)=%sx^{2}%sx%s}$' % (functionLetter, equationFormatting(value = a, inBetween = False, nextToVariable = True), equationFormatting(value = b, inBetween = True, nextToVariable = True), equationFormatting(value = c, inBetween = True, nextToVariable = False))

					answer = a * (inputValue) ** 2 + b * inputValue + c

			elif typeOfFunction == 'cubic':
				#ax^3 + bx^2 + cx + d
				functionString = r'${%s(x)=%sx^{3}%sx^{2}%sx%s}$' % (functionLetter, equationFormatting(value = a, inBetween = False, nextToVariable = True), equationFormatting(value = b, inBetween = True, nextToVariable = True), equationFormatting(value = c, inBetween = True, nextToVariable = True), equationFormatting(value = d, inBetween = True, nextToVariable = False))
				answer = a * (inputValue) ** 2 + b * (inputValue) ** 2 + c * (inputValue) + d
			
			try:
				if len(inputValue) >= 1:
					return functionString, answerString
			except:
				return functionString, answer



		if len(self.typeOfFunctions) == 1:
			if inputTypeChosen == 'integer':
				inputValue = random.choice([x for x in range(-10, 10)])
				functionString, answer = functionByType(typeOfFunction = functions[functionNames[0]], inputValue = inputValue, functionLetter = functionNames[0])
				questionString = 'Given function ' + functionString + r' what is the value of ${%s(%d)}$?' % (functionNames[0],inputValue)
				self.answer = r'$%s(%d)=%d$' % (functionNames[0], inputValue, answer)
				self.questionString = questionString
			elif inputTypeChosen == 'monomial':
				coA = random.randint(1,5)
				inputValue = []
				inputValue.append(coA)
				functionString, answerString = functionByType(typeOfFunction = functions[functionNames[0]], inputValue = inputValue, functionLetter = functionNames[0])
				self.questionString = 'Given function ' + functionString + r' what is the value of ${%s(%sx)}$?' % (functionNames[0], equationFormatting(value = inputValue[0], inBetween = False, nextToVariable = True))
				self.answer = answerString
			elif inputTypeChosen == 'binomial':
				coA = random.randint(1,5)
				coB = random.choice([x for x in range(-5,5) if x not in [0]])
				inputValue = []
				inputValue.append(coA)
				inputValue.append(coB)
				functionString, answerString = functionByType(typeOfFunction = functions[functionNames[0]], inputValue = inputValue, functionLetter = functionNames[0])
				self.questionString = 'Given function ' + functionString + r' what is the value of ${%s(%sx%s)}$?' % (functionNames[0], equationFormatting(value = inputValue[0], nextToVariable = True, inBetween = False), equationFormatting(value = inputValue[1], inBetween = True, nextToVariable = False))
				self.answer = answerString
			elif inputTypeChosen == 'trinomial':
				coA = random.randint(1,9)
				coB = random.choice([x for x in range(-9,9) if x not in [0]])
				coC = random.choice([x for x in range(-9,9) if x not in [0]])
				inputValue = []
				inputValue.append(coA)
				inputValue.append(coB)
				inputValue.append(coC)
				functionString, answerString = functionByType(typeOfFunction = functions[functionNames[0]], inputValue = inputValue, functionLetter = functionNames[0])
				self.questionString = 'Given function ' + functionString + r' what is the value of ${%s(%sx^{2}%sx%s)}$?' % (functionNames[0], equationFormatting(value = inputValue[0], nextToVariable = True, inBetween = False), equationFormatting(value = inputValue[1], nextToVariable = True, inBetween = True), equationFormatting(value = inputValue[2], inBetween = True, nextToVariable = False))
				self.answer = answerString
		elif len(self.typeOfFunctions) == 2:
			if inputTypeChosen == 'integer':
				inputValue1 = random.choice([x for x in range(-10, 10)])
				inputValue2 = random.choice([x for x in range(-10, 10)])
				if self.composition == True:
					functionString1, answer1 = functionByType(typeOfFunction = functions[functionNames[0]], inputValue = inputValue1, functionLetter = functionNames[0])
					functionString2, answer2 = functionByType(typeOfFunction = functions[functionNames[1]], inputValue = answer1, functionLetter = functionNames[1])
					self.questionString = 'Given function ' + functionString1 + ' and function ' + functionString2 + r', what is the value of ${%s(%s(%d))}$?' % (functionNames[1], functionNames[0], inputValue1)
					self.answer = r'$x=%d$' % (answer2)
				else:
					functionString1, answer1 = functionByType(typeOfFunction = functions[functionNames[0]], inputValue = inputValue1, functionLetter = functionNames[0])
					functionString2, answer2 = functionByType(typeOfFunction = functions[functionNames[1]], inputValue = inputValue2, functionLetter = functionNames[1])
					if self.operationBetweenFunctions == ['adding']:
						operationString = '+'
						answer = answer1 + answer2
					elif self.operationBetweenFunctions == ['subtracting']:
						operationString = '-'
						answer = answer1 - answer2
					elif self.operationBetweenFunctions == ['multiplying']:
						operationString = r'{\cdot}'
						answer = answer1 * answer2
					elif self.operationBetweenFunctions == ['dividing']:
						if answer2 == 0:
							answer = 'undefined'
						else:
							answer = Fraction(answer1, answer2)
							if answer.numerator % answer.denominator != 0:
								print(answer.numerator, answer.denominator)
								answer = r'$\frac{%d}{%d}$' % (answer.numerator, answer.denominator)
							else:
								answer = '%d' % (answer.numerator)
					if self.operationBetweenFunctions != ['dividing']:
						self.questionString = 'Given function ' + functionString1 + ' and function ' + functionString2 + r', what is the value of ${%s(%d)%s%s(%d)}$?' % (functionNames[0],inputValue1, operationString, functionNames[1],inputValue2)

						self.answer = r'$%s(%d)%s%s(%d)=%d$' % (functionNames[0], inputValue1, operationString, functionNames[1], inputValue2, answer)
					else:
						self.questionString = 'Given function ' + functionString1 + ' and function ' + functionString2 + r', what is the value of ${\frac{%s(%d)}{%s(%d)}}$?' % (functionNames[0],inputValue1, functionNames[1],inputValue2)
						self.answer = r'$\frac{%s(%d)}{%s(%d)}$=%s' % (functionNames[0],inputValue1, functionNames[1],inputValue2, answer)

				
			


		doc.append(NoEscape(self.questionString))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
		#pick a random number for x
		#random coeffecients
		#equate function value
		#express stringQuestion and stringAnswer


#realistically not going to do a rounding din again anyways, but something is weird with the thousandths place or something
class rounding():
	def __init__(self, typeOfQuestion = ['normal', 'special'], place = ['whole number', 'tenth', 'hundredth', 'thousandth']):
		self.typeOfQuestion = typeOfQuestion
		self.place = place
	def addQuestion(self, doc = None):
		placeChosen = random.choice(self.place)
		# #.####
		# a.bcde
		placeList = ['whole number', 'tenth', 'hundredth', 'thousandth']
		if self.typeOfQuestion == 'normal':
			a = random.choice([i for i in range(1,9)])
			b = random.choice([i for i in range(0,9)])#
			c = random.choice([i for i in range(0,9)])
			d = random.choice([i for i in range(0,9)])
			e = random.choice([i for i in range(0,9)])
			numberDict = {0:a, 1:b, 2:c, 3:d, 4:e}
			#I want # right of place I'm rounding to be bigger than 5
			numberDict[placeList.index(placeChosen)] = random.choice([0,1,2,3,4,5,6,7,8])

			number = a + .1*b + .01*c + .001*d + .0001*e
			numberRounded = round(number, placeList.index(placeChosen))
		else:
			a = random.choice([i for i in range(1,8)])
			b = random.choice([i for i in range(0,8)])
			c = random.choice([i for i in range(0,8)])
			d = random.choice([i for i in range(0,8)])
			e = random.choice([i for i in range(0,8)])
			numberDict = {0:a, 1:b, 2:c, 3:d, 4:e}
			#I want # right of place I'm rounding to be bigger than 5
			numberDict[placeList.index(placeChosen)] = 9
			numberDict[placeList.index(placeChosen)+1] = random.choice([5,6,7,8,9])
			number = a + .1*b + .01*c + .001*d + .0001*e
			numberRounded = round(number, placeList.index(placeChosen))
		self.answerExpression = r'$%g$' % numberRounded
		self.expression = r'Round %g to the nearest %s.' % (number, placeChosen)
		doc.append(NoEscape(self.expression))
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answerExpression))


################################GEO - COORDINATES #########################
class partition():
	def __init__(self, typeOfQuestion = ['SA'], xMinGraph = -10, xMaxGraph = 10, yMaxGraph = 10, yMinGraph = -10, typeOfSolution = ['integer'], typeOfLine = ['horizontal','vertical','diagonal'], includeGraph = False, wording = [1,2]):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph

		self.typeOfSolution = typeOfSolution
		self.typeOfLine = typeOfLine
		self.includeGraph = includeGraph
		self.wording = wording

	def addQuestion(self, doc = None):
		typeOfSolution = random.choice(self.typeOfSolution)
		typeOfLine = random.choice(self.typeOfLine)
		wording = random.choice(self.wording)
		#Horizontal and vertical mean x's an y's will be the same

		#if wanting 1:1 fuck it make a new one

		#a and b max is (xMaxGraph-xMinGraph)//4

		#if graph isn't square, it is now
		maxx = min([self.xMaxGraph, self.yMaxGraph])
		minn = max([self.yMinGraph, self.xMinGraph])

		
		a = random.choice([x for x in range(1,(maxx-minn)//3+1)])
		b = random.choice([x for x in range(1, (maxx-minn)//3+1) if x not in [a] and gcd(x,a) == 1])

		#pick x1, y1 as mins of graph, then translate afterwards, this willa llow large a's and b's to work
		x1 = self.xMinGraph
		y1 = self.yMinGraph

		if typeOfSolution == 'integer':
			x2 = random.choice([x for x in range(self.xMinGraph,self.xMaxGraph+1) if (x-x1) % (a+b) == 0 and (x-x1) != 0] )
			y2 = random.choice([y for y in range(self.yMinGraph,self.yMaxGraph+1) if (y-y1) % (a+b) == 0 and (y-y1) != 0] )
			if typeOfLine == 'vertical':
				x2 = x1
		elif typeOfSolution == 'fraction':
			#only do 1/2 for now
			x2 = random.choice([x for x in range(self.xMinGraph,self.xMaxGraph+1) if (x-x1) % 1/2*(a+b) == 0 and (x-x1) != 0] )
			y2 = random.choice([y for y in range(self.yMinGraph,self.yMaxGraph+1) if (y-y1) % 1/2*(a+b) == 0 and (y-y1) != 0] )
			if typeOfLine == 'horizontal':
				y2 = y1
		#translate anywhere on grid
		xChange = random.randint(self.xMinGraph-min([x1,x2]), self.xMaxGraph-max([x1,x2]))
		yChange = random.randint(self.yMinGraph-min([y1,y2]), self.yMaxGraph-max([y1,y2]))

		#translate coordinates
		xs, ys = translateCoordinates(x = [x1, x2], y = [y1, y2], changeX = xChange, changeY = yChange )

		x1 = xs[0]
		x2 = xs[1]
		y1 = ys[0]
		y2 = ys[1]

		#labels
		points = pointsForDiagram()

		A = points['A']
		B = points['B']
		C = points['C']

		#answers
		if typeOfSolution == 'integer':
			xAnswer = int(round( (x1*b+x2*a) / (b + a) ) )
			yAnswer = int(round( (y1*b+y2*a) / (b + a) ) )
			self.answer = r'$%s = (%d,%d)$' % (B, xAnswer, yAnswer)
		elif typeOfSolution == 'fraction':
			#mixed fraction
			numeratorX = (x1*b+x2*a) % (a+b)
			denominatorX = 2
			wholeX = (x1*b+x2*a)/(a+b) - numeratorX/denominatorX
			if numeratorX == 0:
				xString = str(int(round( (x1*b+x2*a) / (b + a) ) ))
			else:
				if wholeX != 0:
					xString = r'%d\frac{%s}{%s}' % (wholeX, numeratorX, denominatorX)
				else:
					xString = r'\frac{%s}{%s}' % (numeratorX, denominatorX)

			numeratorY = (y1*b+y2*a) % (a+b)
			denominatorY = 2
			wholeY = (y1*b+y2*a)/(a+b) - numeratorY/denominatorY

			if numeratorY == 0:
				yString = str(int(round( (y1*b+y2*a) / (b + a) ) ))
			else:
				if wholeY != 0:
					yString = r'%d\frac{%s}{%s}' % (wholeY, numeratorY, denominatorY)
				else:
					yString = r'\frac{%s}{%s}' % (numeratorY, denominatorY)

			self.answer = r'$%s = (%s,%s)$' % (B, xString, yString)

		if wording == 1:
			doc.append(NoEscape(r'Given the directed line segment %s, %s(%d,%d), and %s(%d,%d), determine the coordinates of the point %s that %s $\overline{%s}$ into ratio of %s.' % (A+C, A, x1, y1, C, x2, y2, B, random.choice(['partitions','divides']), A+C, random.choice(['%d:%d' % (a,b), '%d to %d' % (a,b)]))))	
		elif wording == 2:
			doc.append(NoEscape(r'Given the line segment %s, %s(%d,%d), and %s(%d,%d), determine the coordinates of the point %s that %s $\overline{%s}$, such that %s is in a ratio of %s.' % (A+C, A, x1, y1, C, x2, y2, B, random.choice(['partitions','divides']), A+C, random.choice(['%s:%s' % (A+B, B+C), '%s to %s' % (A+B, B+C)]), random.choice(['%d:%d' % (a,b), '%d to %d' % (a,b)]))))	
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class pythagoreanTheorem():
	def __init__(self, rotate = 0, solveForC = True, rounding = ['whole number','tenth','hundredth','thousandth']):
		self.rotate = rotate
		self.solveForC = solveForC
		self.rounding = rounding
		self.typeOfQuestionChosen = 'SA'
	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)

		#pick a,b,c of a triangle
		if self.solveForC == False:
			a = random.randint(10,20)
			b = random.randint(a-5,a+5)
			c = roundGivenString(string = roundingChosen, value = ( a**2 + b**2 ) * (1/2))

		else: #finding a, but when we write question, no one will know if it's a or b, so it doesn't matter
			c = random.randint(10,20)
			b = random.randint(c-5,c-1)
			a = roundGivenString(string = roundingChosen, value = ( c**2 - b**2 ) ** (1/2))

		labels = pointsForDiagram()

		#random between vert side and ho side for both finding and given
		if self.solveForC == True:
			doc.append(NoEscape(r'In $\bigtriangleup{%s}$ below, %s = %d and %s = %d, what is the measure of %s to the nearest %s.' % (labels['A']+labels['B']+labels['C'], labels['A']+labels['B'], a, labels['B']+labels['C'], b, labels['A']+labels['C'], roundingChosen)))
			self.answer = r'%s = %d' % (labels['A']+labels['C'], c)
		else:
			doc.append(NoEscape(r'In $\bigtriangleup{%s}$ below, %s = %d and %s = %d, what is the measure of %s to the nearest %s.' % (labels['A']+labels['B']+labels['C'], labels['A']+labels['B'], a, labels['A']+labels['C'], c, labels['B']+labels['C'], roundingChosen)))
			self.answer = r'%s = %d' % (labels['B']+labels['C'], b)
		
		doc.append(NewLine())
		#draw right triangle
		rightTriangle(doc = doc, lengthValue = a, widthValue = b, labeled = False, rotate = self.rotate, verticesLabels = [labels['A'], labels['B'], labels['C']])

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class distanceBetweenPoints():
	def __init__(self, includeGraph = True, rounding = ['whole number','tenth','hundredth','thousandth'], difficulty = [1, 2, 3], xMaxGraph = 10, xMinGraph = -10, yMaxGraph = 10, yMinGraph = -10):
		#difficulty 1: find dist between two points
		#difficulty 2: find length of radius given center and point on circle
		#difficulty 3: find the perimeter of a regular polygon given two endpoints
		self.includeGraph = includeGraph
		self.rounding = rounding
		self.difficulty = difficulty

		self.xMaxGraph = xMaxGraph
		self.xMinGraph = xMinGraph
		self.yMaxGraph = yMaxGraph
		self.yMinGraph = yMinGraph

		self.typeOfQuestionChosen = 'SA'

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		difficultyChosen = random.choice(self.difficulty)

		points = pointsForDiagram()
		x1 = random.randint(self.xMinGraph, self.xMaxGraph)
		y1 = random.randint(self.yMinGraph, self.yMaxGraph)

		#so x2 can't be x1, -1-2 etc is for making a triangle not uber small
		x2 = random.choice([x for x in range(self.xMinGraph, self.xMaxGraph+1) if x not in [x1, x1-1, x1-2, x1+1, x1+2]])
		y2 = random.choice([y for y in range(self.yMinGraph, self.yMaxGraph+1) if y not in [y1, y1-1, y1-2, y1+1, y1+2]])
		A = points['A']
		B = points['B']

		distance = ( (x1-x2)**2 + (y1-y2)**2 ) ** (1/2)

		if difficultyChosen == 1:
			#pick two lettered points and two points that are diagonal from one another
			questionString = r'Given %s(%d,%d) and %s(%d,%d), how long is $\overline{%s}$ to the nearest %s?' % (A, x1, y1,
				B, x2, y2,
				A+B, roundingChosen)
			self.answer = r'%s = %g' % (A+B, roundGivenString(value = distance, string = roundingChosen))
		elif difficultyChosen == 2:
			questionString = r'Given Circle %s with center (%d,%d), if a point on the circle is %s(%d,%d), how long is the radius of the circle to the nearest %s?' % (A, x1, y1,
				B, x2, y2,
				roundingChosen)
			self.answer = r'The radius is %g.' % (roundGivenString(value = distance, string = roundingChosen))
		elif difficultyChosen == 3:
			polygons = ['equilateral triangle','square','regular pentagon','regular hexagon','','regular octagon','regular nonagon','regular decagon']
			polygonChosen = random.choice([x for x in ['equilateral triangle','square','regular pentagon','regular hexagon','regular octagon','regular nonagon','regular decagon'] if x not in ['']])
			questionString = r'Given that the endpoints of a side of a %s are %s(%g,%g) and %s(%g,%g), what is the perimeter of the %s to the nearest %s?' % (polygonChosen,
				A, x1, y1,
				B, x2, y2, 
				polygonChosen, roundingChosen)
			self.answer = r'%g' % (roundGivenString(string = roundingChosen, value = (polygons.index(polygonChosen)+3)*distance))

		doc.append(NoEscape(questionString))

		if self.includeGraph == True:
			doc.append(NewLine())
			lengthForPic = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (lengthForPic, lengthForPic))):
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class coordinateAreaOfTriangle():
	def __init__(self, rightTriangle = False, xMaxGraph = 10, xMinGraph = -10, yMaxGraph = 10, yMinGraph = -10):
		self.typeOfQuestionChosen = 'SA'
		self.rightTriangle = rightTriangle
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.xMinGraph = xMinGraph
		self.yMaxGraph = yMaxGraph

	def addQuestion(self, doc = None):
		#make a box(rectangle) on the grid with bottom left ax min and min
		x1 = self.xMinGraph
		y1 = self.yMinGraph

		#rectangle going, counterclockwise
		x2 = self.xMinGraph
		y2 = random.randint(self.xMinGraph+5, self.yMaxGraph) #+5 so at least it's somewhere

		x3 = random.randint(self.xMinGraph+5, self.xMaxGraph)
		y3 = y2

		x4 = x3
		y4 = y1

		#Determine poitns for the triangle
		if self.rightTriangle == False:
			#starting on left side of this box, pick any point but not corners
			triangleX1 = x1
			triangleY1 = random.randint(y1 + 1, y2 - 1)
			#then pick any point on top
			triangleX2 = random.randint(x2 + 1, x3 - 1)
			triangleY2 = y2
			#then pick any point on the bottom right corner
			triangleX3 = x4
			triangleY3 = y4
		elif self.rightTriangle == True:
			#make a right triangle inside the constraints of the graph

			#start at minn, minn
			triangleX1 = self.xMinGraph
			triangleY1 = self.yMinGraph

			#assuming graph is going to be square
			xLength = self.xMaxGraph - self.xMinGraph
			yLength = self.yMaxGraph - self.yMinGraph
			xSlopeChange = random.randint(xLength//4, xLength//2-1)
			#we don't want an isoceles triangle
			ySlopeChange = random.choice([x for x in range(yLength//4, yLength//2-1+1) if x not in [xSlopeChange]])

			#second points
			triangleX2 = triangleX1 + xSlopeChange
			triangleY2 = triangleY1 + ySlopeChange

			#y/x --> -x/y
			#third points, after opposite reciprocal of change added to second points, making 2nd coordinate 90
			triangleX3 = triangleX2 + ySlopeChange
			triangleY3 = triangleY2 + -xSlopeChange

		#translate x's and y's anywhere that will fit on the graph
		xs = [triangleX1, triangleX2, triangleX3]
		ys = [triangleY1, triangleY2, triangleY3]
		xChange = random.randint(self.xMinGraph - min(xs), self.xMaxGraph - max(xs))
		yChange = random.randint(self.yMinGraph - min(ys), self.yMaxGraph - max(ys))

		newXs, newYs = translateCoordinates(x = xs, y = ys, changeX = xChange, changeY = yChange)
		x1 = newXs[0]
		x2 = newXs[1]
		x3 = newXs[2]
		y1 = newYs[0]
		y2 = newYs[1]
		y3 = newYs[2]

		points = pointsForDiagram()
		A = points['A']
		B = points['B']
		C = points['C']
		#now we need to have the question
		doc.append(NoEscape(r'Given $\bigtriangleup{%s}$ below, what is the area of the triangle?' % (A+B+C)))
		doc.append(NewLine())
		#followed by a grid with the shape drawn
		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
				graphPolygon(doc = doc, x = [x1,x2,x3], y = [y1,y2,y3], annotations = [A,B,C], color = 'black')

		areaOfTriangle = abs( (x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2 )
		self.answer = r'The area is %g.' % (areaOfTriangle)
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))

class solveForY():
	def __init__(self, numberOfSteps = ['1','2'], difficulty = ['1','2','3','4'], yCoefficient = ['random','1','-1','no1'], xCoefficient = ['random', '1','-1','no1'], coefficientMax = 10, parallel = False, perpendicular = False):
		self.numberOfSteps = numberOfSteps
		self.difficulty = difficulty
		self.yCoefficient = yCoefficient
		self.xCoefficient = xCoefficient
		self.coefficientMax = coefficientMax
		self.typeOfQuestionChosen = 'SA'
		"""
		1. ay = bx
		2. ay = bx + c
		2. y+c=bx
		3. Y+bx = c

		5. c + ay = bx
		6. ay + bx = c
		7. bx + ay = c
		8. ay + c = bx
		"""
	def addQuestion(self, doc = None):
		difficultyChosen = random.choice(self.difficulty)
		yCoefficientChosen = random.choice(self.yCoefficient)
		xCoefficientChosen = random.choice(self.xCoefficient)
		numberOfStepsChosen = random.choice(self.numberOfSteps)

		rangeExluding0 = [x for x in range(-self.coefficientMax,self.coefficientMax+1) if x not in [0]]

		if yCoefficientChosen == '1':
			yCo = 1
		elif yCoefficientChosen == '-1':
			yCo = -1
		elif yCoefficientChosen == 'no1':
			yCo = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
		else:
			yCo = random.choice(rangeExluding0)

		if xCoefficientChosen == '1':
			xCo = 1
		elif xCoefficientChosen == '-1':
			xCo = -1
		elif xCoefficientChosen == 'no1':
			xCo = random.choice([x for x in rangeExluding0 if x not in [1,-1]])
		else:
			xCo = random.choice(rangeExluding0)

		constant = random.choice(rangeExluding0)

		if numberOfStepsChosen == '1':
			if difficultyChosen == '1': #ay = bx + 0
				questionString = r'$%sy=%sx$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = 0
			
			elif difficultyChosen == '2': #ay = bx + c
				questionString = r'$%sy=%sx%s$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True), 
					equationFormatting(value = constant, inBetween = True, nextToVariable = False))
				xCoAnswer = xCo/yCo
				constantAnswer = constant/yCo

			elif difficultyChosen == '3': #y + c = bx
				questionString = r'$y%s=%sx$' % (equationFormatting(value = constant, inBetween = True, nextToVariable = False), 
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo
				constantAnswer = -constant

			elif difficultyChosen == '4': #y + bx = c
				questionString = r'$y%sx=%s$' % (equationFormatting(value = xCo, inBetween = True, nextToVariable = True), 
					equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo
				constantAnswer = constant

		elif numberOfStepsChosen == '2':
			if difficultyChosen == '1': #5. c+ay=bx
				questionString = r'$%s%sy=%sx$' % (equationFormatting(value = constant, inBetween = False, nextToVariable = False),
				equationFormatting(value = yCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = -constant/yCo
			elif difficultyChosen == '2':#6. ay+bx=c
				questionString = r'$%sy%sx=%s$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = xCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo/yCo
				constantAnswer = constant/yCo
			elif difficultyChosen == '3':#7. bx+ay=c
				questionString = r'$%sx%sy=%s$' % (equationFormatting(value = xCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = yCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo/yCo
				constantAnswer = constant/yCo
			elif difficultyChosen == '4':#8. 8. ay + c = bx
				questionString = r'$%sy%s=%sx$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = constant, inBetween = True, nextToVariable = False), 
				equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = -constant/yCo

		doc.append(NoEscape(r'Solve for y:'))
		doc.append(NewLine())
		doc.append(NoEscape(questionString))

		#y = ax + b
		#for answer need to take care of fraction
		aFrac = Fraction(xCoAnswer).limit_denominator(self.coefficientMax)
		if aFrac.denominator == 1: #whole number
			aAnswer = r'%sx' % equationFormatting(value = aFrac.numerator, inBetween = False, nextToVariable = True)
		else: #fraction
			aAnswer = r'\frac{%d}{%d}x' % (aFrac.numerator, aFrac.denominator)

		bFrac = Fraction(constantAnswer).limit_denominator(self.coefficientMax)
		if bFrac.denominator == 1: #whole number
			bAnswer = r'%s' % equationFormatting(value = bFrac.numerator, inBetween = True, nextToVariable = False)
		else: #fraction
			bAnswer = r'%s\frac{%d}{%d}' % (equationFormatting(value = bFrac.numerator//abs(bFrac.numerator), inBetween = True, nextToVariable = True), abs(bFrac.numerator), bFrac.denominator)

		self.answer = r'$y=%s%s$' % (aAnswer, bAnswer)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
		

class findParallelPerpendicularLine():
	def __init__(self, numberOfSteps = ['1','2'], difficulty = ['1','2','3','4'], yCoefficient = ['random','1','-1','no1'], xCoefficient = ['random', '1','-1','no1'], includeGraph = False, graphMax = 10, slope = ['parallel','perpendicular'], yForm = ['slope intercept','point slope'], typeOfQuestion = ['MC','SA']):
		self.numberOfSteps = numberOfSteps
		self.difficulty = difficulty
		self.yCoefficient = yCoefficient
		self.xCoefficient = xCoefficient
		self.coefficientMax = graphMax//2
		self.typeOfQuestionChosen = typeOfQuestion
		self.includeGraph = includeGraph
		self.graphMax = graphMax
		self.slope = slope
		self.yForm = yForm


	def addQuestion(self, doc = None):
		self.typeOfQuestionChosen = random.choice(self.typeOfQuestion)
		numberOfStepsChosen = random.choice(self.numberOfSteps)
		difficultyChosen = random.choice(self.difficulty)
		#using //3 so that they will be small enough that xP can't be divided by both for parallel and perpendicular slopes starting at the same point making integer y-intercepts
		rangeExluding0 = [x for x in range(-3,3) if x not in [0]] #trying this
		slopeChosen = random.choice(self.slope)
		yFormChosen = random.choice(self.yForm)


		yCoefficientChosen = random.choice(self.yCoefficient)
		if yCoefficientChosen == '1':
			yCo = 1
		elif yCoefficientChosen == '-1':
			yCo = -1
		elif yCoefficientChosen == 'no1':
			yCo = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
		else:
			yCo = random.choice(rangeExluding0)

		xCoefficientChosen = random.choice(self.xCoefficient)			
		if xCoefficientChosen == '1':
			xCo = 1
		elif xCoefficientChosen == '-1':
			xCo = -1
		elif xCoefficientChosen == 'no1':
			xCo = random.choice([x for x in rangeExluding0 if x not in [1,-1]])
		else:
			xCo = random.choice(rangeExluding0)

		#for most regular constants unless otherwise specified.
		constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1)])

		if numberOfStepsChosen == '1':
			if difficultyChosen == '1': #ay = bx + 0
				constant = 0
				questionString = r'$%sy=%sx$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = 0
			
			elif difficultyChosen == '2': #ay = bx + c
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x % yCo == 0])
				#forcing y-intercept to be integer
				questionString = r'$%sy=%sx%s$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True), 
					equationFormatting(value = constant, inBetween = True, nextToVariable = False))
				xCoAnswer = xCo/yCo
				constantAnswer = constant/yCo

			elif difficultyChosen == '3': #y + c = bx
				questionString = r'$y%s=%sx$' % (equationFormatting(value = constant, inBetween = True, nextToVariable = False), 
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo
				constantAnswer = -constant

			elif difficultyChosen == '4': #y + bx = c
				questionString = r'$y%sx=%s$' % (equationFormatting(value = xCo, inBetween = True, nextToVariable = True), 
					equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo
				constantAnswer = constant

		elif numberOfStepsChosen == '2':
			if difficultyChosen == '1': #5. c+ay=bx
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if -x % yCo == 0])
				questionString = r'$%s%sy=%sx$' % (equationFormatting(value = constant, inBetween = False, nextToVariable = False),
				equationFormatting(value = yCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = -constant/yCo
			elif difficultyChosen == '2':#6. ay+bx=c
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x % yCo == 0])
				questionString = r'$%sy%sx=%s$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = xCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo/yCo
				constantAnswer = constant/yCo
			elif difficultyChosen == '3':#7. bx+ay=c
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x % yCo == 0])
				questionString = r'$%sx%sy=%s$' % (equationFormatting(value = xCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = yCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo/yCo
				constantAnswer = constant/yCo
			elif difficultyChosen == '4':#8. 8. ay + c = bx
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if -x % yCo == 0])
				questionString = r'$%sy%s=%sx$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = constant, inBetween = True, nextToVariable = False), 
				equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = -constant/yCo

		#y = ax + b
		#for answer need to take care of fraction
		aFrac = Fraction(xCoAnswer).limit_denominator(self.coefficientMax)
		print('slope numerator and denominator are:',aFrac.numerator, aFrac.denominator)
		
		parallelSlope = Fraction(aFrac.numerator/aFrac.denominator).limit_denominator(self.coefficientMax)
		perpendicularSlope = Fraction(-aFrac.denominator/aFrac.numerator).limit_denominator(self.coefficientMax)
		
		print(parallelSlope.numerator, parallelSlope.denominator)
		print(perpendicularSlope.numerator, perpendicularSlope.denominator)
		#we want to make sure that this point will produce an integer y-intercept for both the parallel and perpendicular lines.
		# yP = (n/d)*xP + b1
		# yP = -(d/n)*xP + b2

		#define x that is divisible by both denominators
		xP = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x % parallelSlope.denominator == 0 and 
			x % perpendicularSlope.denominator == 0 and 
			x*parallelSlope.numerator//parallelSlope.denominator <= self.graphMax*2 and
			x*parallelSlope.numerator//parallelSlope.denominator >= -self.graphMax*2 and 
			x*perpendicularSlope.numerator//perpendicularSlope.denominator <= self.graphMax*2 and
			x*perpendicularSlope.numerator//perpendicularSlope.denominator >= -self.graphMax*2]
			 )
		xP = parallelSlope.numerator*parallelSlope.denominator
		#range that I want the y-intercept to be
		rangeWanted = [x for x in range(-self.graphMax, self.graphMax+1)]

		#we don't want either y-intercept to be equal to original
		rangeWanted.remove(constantAnswer)
		print(rangeWanted)
		#lets do a y that picks its b's
		#needs to make a b in range for both equations
		#b can't be equal for both
		print('x is', xP)
		print('parallel slope is', parallelSlope)
		print('constantAnswer is', constantAnswer)

		#yP - slope*xP = b1
		yP = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x - xP*parallelSlope.numerator//parallelSlope.denominator in rangeWanted and x - xP*perpendicularSlope.numerator//perpendicularSlope.denominator in rangeWanted])
		
		b1 = yP - int(round(parallelSlope*xP))
		b2 = yP - int(round(perpendicularSlope*xP))
		
		if self.includeGraph == False:
			questionStart = r'What is the equation of a line that is %s and goes through (%d,%d) to the line whose equation is {%s}?' % (slopeChosen, xP, yP, questionString)
		else:
			points = pointsForDiagram()
			questionStart = r'Given the graph below of graph below of line %s, what is the equation of a line that is %s to $\overline{%s}$ and goes through %s(%d,%d)?' % (points['A']+points['B'], slopeChosen, points['A']+points['B'], points['C'], xP, yP)

		doc.append(NoEscape(questionStart))
		if self.includeGraph == True:
			#need to get original line and two points on the line
			#so xCoAnswer, constantAnswer
			#y = xCoAnswer * x + constantAnswer
			xs = []
			ys = []
			print(xCoAnswer, constantAnswer)
			graphRange = [x for x in range(-self.graphMax, self.graphMax+1)]
			for itemX in graphRange:
				for itemY in graphRange:
					if itemY == (itemX * xCoAnswer + constantAnswer):
						xs.append(itemX)
						ys.append(itemY)
			#now pick two random point on the line, but that always cross the y-axis
			firstX = xs[0]
			firstY = ys[0]

			secondX = xs[-1]
			secondY = ys[-1]

			doc.append(NewLine())
			picLength = sizingXY(minn = -self.graphMax, maxx = self.graphMax, size = 'medium')
			with doc.create(Center()):
				with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
					grid(doc = doc, xMin = -self.graphMax, xMax = self.graphMax, yMin = -self.graphMax, yMax = self.graphMax)
					graphPolygon(doc = doc, x = [firstX, secondX], y = [firstY, secondY], annotations = [points['A'],points['B']], color = 'black')
					graphPoint(doc = doc, x = xP, y = yP, annotations = [points['C']], color = 'black')

		if slopeChosen == 'parallel':
			slopeToUse = parallelSlope
			slope = Fraction(parallelSlope).limit_denominator(self.coefficientMax)
			yIntercept = b1
			if slope.numerator % slope.denominator != 0:
				slopeString = r'%s\frac{%d}{%d}' % (equationFormatting(value = slope.numerator/(abs(slope.numerator)), inBetween = False, nextToVariable = True),
					abs(slope.numerator), abs(slope.denominator))
			else: #whole number
				slopeString = r'%s' % (equationFormatting(value = slope.numerator, inBetween = False, nextToVariable = True))
		else:
			slopeToUse = perpendicularSlope
			slope = Fraction(perpendicularSlope).limit_denominator(self.coefficientMax)
			yIntercept = b2
			if slope.numerator % slope.denominator != 0:
				slopeString = r'%s\frac{%d}{%d}' % (equationFormatting(value = slope.numerator/(abs(slope.numerator)), inBetween = False, nextToVariable = True), abs(slope.numerator), abs(slope.denominator))
			else: #whole number
				slopeString = r'%s' % (equationFormatting(value = slope.numerator, inBetween = False, nextToVariable = True))

		#all of this is assuming parallel
		if yFormChosen == 'point slope':
			self.answer = r'$y%s= %s(x%s)$' % (equationFormatting(value = -yP, nextToVariable = False, inBetween = True),
				slopeString, equationFormatting(value = -xP, nextToVariable = False, inBetween = True))
					
		elif yFormChosen == 'slope intercept':
			self.answer = r'$y= %sx%s$' % (slopeString, equationFormatting(value = yIntercept, nextToVariable = False, inBetween = True))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))



class coordinateProof():
	def __init__(self, typeOfPolygon = ['right triangle','isosceles triangle','isosceles right triangle','parallelogram','rhombus','square','rectangle'], sizeOfGraph = 10):
		self.typeOfPolygon = typeOfPolygon
		self.sizeOfGraph = sizeOfGraph
		self.typeOfQuestionChosen = 'SA'

	def addQuestion(self, doc = None):
		#for quadrilaterals, center is 0,0 use properties then translate the shape around the graph to add variety etc

		#isosceles triangle is 3 points of rhombus

		#isosceles right triangle is 3 points of square

		#right triangle is 3 points of rhombus including center

		centerX = 0
		centerY = 0

		#let's do a parallelogram first then mesh together after

		typeOfPolygonChosen = random.choice(self.typeOfPolygon)

		if typeOfPolygonChosen == 'parallelogram':
			#left then clockwise, 3rd quadrant
			x1 = random.randint(-self.sizeOfGraph, -3)
			y1 = random.choice([x for x in range(-self.sizeOfGraph, -2) if x not in [x1]])

			x3 = -x1
			y3 = -y1

			#stopping x2, y2 from being a rhombus and i think rectangle as well since x1's go with 3's etc
			x2 = random.choice([x for x in range(-self.sizeOfGraph, -2) if x not in [y1, -y1, x1]])
			y2 = random.choice([x for x in range(2, self.sizeOfGraph+1) if x not in [x1, -x1, y1]])

			x4 = -x2
			y4 = -y2
			xs = [x1, x2, x3, x4]
			ys = [y1, y2, y3, y4]
		elif typeOfPolygonChosen == 'rhombus' or typeOfPolygonChosen == 'isosceles triangle':
			#make a right triangle
			#start with x2, y2 as 0,0
			x2 = 0
			y2 = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			x3 = x2 + xChange
			y3 = y2 + yChange

			#now x1 is opposite reciprocal but dilated
			xChangeDilated = xChange*2
			#figure out scale factor
			scaleFactor = xChangeDilated/xChange
			yChangeDilated = yChange * scaleFactor

			#-xChange/yChange
			x1 = x2 + yChangeDilated
			y1 = y2 - xChangeDilated

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]

			#now we have a right triangle, flip it over the longer side to make isosceles
			x2 = -x3
			y2 = -y3

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]
			if typeOfPolygonChosen == 'rhombus':
				x4 = -x1
				y4 = -y1
				xs = [x1, x2, x4, x3]
				ys = [y1, y2, y4, y3]
		elif typeOfPolygonChosen == 'isosceles right triangle' or typeOfPolygonChosen == 'square':
			#make a right triangle
			#start with x2, y2 as 0,0
			x2 = 0
			y2 = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			x3 = x2 + xChange
			y3 = y2 + yChange

			#now x1 is opposite reciprocal but dilated
			xChangeDilated = xChange
			#figure out scale factor
			scaleFactor = xChangeDilated/xChange
			yChangeDilated = yChange * scaleFactor

			#-xChange/yChange
			x1 = x2 + yChangeDilated
			y1 = y2 - xChangeDilated

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]
			if typeOfPolygonChosen == 'square':
				x4 = x1 + xChange
				y4 = y1 + yChange
				xs = [x1, x2, x3, x4]
				ys = [y1, y2, y3, y4]
				print(xs,ys)
		elif typeOfPolygonChosen == 'rectangle' or typeOfPolygonChosen == 'right triangle':
			#make a right triangle
			#start with x2, y2 as 0,0
			x2 = 0
			y2 = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			x3 = x2 + xChange
			y3 = y2 + yChange

			#now x1 is opposite reciprocal but dilated
			xChangeDilated = xChange*2
			#figure out scale factor
			scaleFactor = xChangeDilated/xChange
			yChangeDilated = yChange * scaleFactor

			#-xChange/yChange
			x1 = x2 + yChangeDilated
			y1 = y2 - xChangeDilated

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]

			if typeOfPolygonChosen == 'rectangle':
				x4 = x1 + xChange
				y4 = y1 + yChange
				xs.append(x4)
				ys.append(y4)

		#translate around graph
		xs = [int(round(x)) for x in xs]
		ys = [int(round(x)) for x in ys]
		translateXChange = random.randint(-self.sizeOfGraph - min(xs), self.sizeOfGraph - max(xs))
		translateYChange = random.randint(-self.sizeOfGraph - min(ys), self.sizeOfGraph - max(ys))

		newXs, newYs = translateCoordinates(xs, ys, translateXChange, translateYChange)

		self.xs = newXs
		self.ys = newYs
		print(xs, ys)
		print(self.xs, self.ys)


		points = pointsForDiagram()
		if len(xs) == 4:
			doc.append(NoEscape(r'Given the graph below, if the coordinates of a quadrilateral are $%s(%d,%d)$, $%s(%d,%d)$, $%s(%d,%d)$, and $%s(%d,%d)$, prove $%s%s%s%s$ is a %s.' % (points['A'], self.xs[0], self.ys[0], 
				points['B'], self.xs[1], self.ys[1], 
				points['C'], self.xs[2], self.ys[2], 
				points['D'], self.ys[3], self.ys[3],
				points['A'], points['B'], points['C'], points['D'],
				typeOfPolygonChosen)))
			self.annos = [points['A'], points['B'], points['C'], points['D']]
		else:
			doc.append(NoEscape(r'Given the graph below, if the coordinates of a triangle are $%s(%d,%d)$, $%s(%d,%d)$, and $%s(%d,%d)$, prove $%s%s%s$ is a %s.' % (points['A'], self.xs[0], self.ys[0], 
				points['B'], self.xs[1], self.xs[1], 
				points['C'], self.xs[2], self.ys[2], 
				points['A'], points['B'], points['C'],
				typeOfPolygonChosen)))
			self.annos = [points['A'], points['B'], points['C']]

		doc.append(NewLine())
		picLength = sizingXY(minn = -self.sizeOfGraph, maxx = self.sizeOfGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = -self.sizeOfGraph, xMax = self.sizeOfGraph, yMin = -self.sizeOfGraph, yMax = self.sizeOfGraph)
				#graphPolygon(doc = doc, x = self.xs, y = self.ys, annotations = self.annos, color = 'red')


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape('tototo'))

class coordinateTestQuestion1():
	def __init__(self, numberOfSteps = ['1','2'], difficulty = ['1','2','3','4'], yCoefficient = ['random','1','-1','no1'], xCoefficient = ['random', '1','-1','no1'], includeGraph = True, graphMax = 10, slope = ['parallel','perpendicular'], yForm = ['slope intercept','point slope'], typeOfQuestion = ['MC','SA']):
		self.numberOfSteps = numberOfSteps
		self.difficulty = difficulty
		self.yCoefficient = yCoefficient
		self.xCoefficient = xCoefficient
		self.coefficientMax = graphMax//2
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.includeGraph = includeGraph
		self.graphMax = graphMax
		self.slope = slope
		self.yForm = yForm


	def addQuestion(self, doc = None):
		numberOfStepsChosen = random.choice(self.numberOfSteps)
		difficultyChosen = random.choice(self.difficulty)
		#using //3 so that they will be small enough that xP can't be divided by both for parallel and perpendicular slopes starting at the same point making integer y-intercepts
		rangeExluding0 = [x for x in range(-3,3) if x not in [0]] #trying this
		slopeChosen = random.choice(self.slope)
		yFormChosen = random.choice(self.yForm)


		yCoefficientChosen = random.choice(self.yCoefficient)
		if yCoefficientChosen == '1':
			yCo = 1
		elif yCoefficientChosen == '-1':
			yCo = -1
		elif yCoefficientChosen == 'no1':
			yCo = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
		else:
			yCo = random.choice(rangeExluding0)

		xCoefficientChosen = random.choice(self.xCoefficient)			
		if xCoefficientChosen == '1':
			xCo = 1
		elif xCoefficientChosen == '-1':
			xCo = -1
		elif xCoefficientChosen == 'no1':
			xCo = random.choice([x for x in rangeExluding0 if x not in [1,-1]])
		else:
			xCo = random.choice(rangeExluding0)

		#for most regular constants unless otherwise specified.
		constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1)])

		if numberOfStepsChosen == '1':
			if difficultyChosen == '1': #ay = bx + 0
				constant = 0
				questionString = r'$%sy=%sx$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = 0
			
			elif difficultyChosen == '2': #ay = bx + c
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x % yCo == 0])
				#forcing y-intercept to be integer
				questionString = r'$%sy=%sx%s$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True), 
					equationFormatting(value = constant, inBetween = True, nextToVariable = False))
				xCoAnswer = xCo/yCo
				constantAnswer = constant/yCo

			elif difficultyChosen == '3': #y + c = bx
				questionString = r'$y%s=%sx$' % (equationFormatting(value = constant, inBetween = True, nextToVariable = False), 
					equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo
				constantAnswer = -constant

			elif difficultyChosen == '4': #y + bx = c
				questionString = r'$y%sx=%s$' % (equationFormatting(value = xCo, inBetween = True, nextToVariable = True), 
					equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo
				constantAnswer = constant

		elif numberOfStepsChosen == '2':
			if difficultyChosen == '1': #5. c+ay=bx
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if -x % yCo == 0])
				questionString = r'$%s%sy=%sx$' % (equationFormatting(value = constant, inBetween = False, nextToVariable = False),
				equationFormatting(value = yCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = -constant/yCo
			elif difficultyChosen == '2':#6. ay+bx=c
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x % yCo == 0])
				questionString = r'$%sy%sx=%s$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = xCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo/yCo
				constantAnswer = constant/yCo
			elif difficultyChosen == '3':#7. bx+ay=c
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x % yCo == 0])
				questionString = r'$%sx%sy=%s$' % (equationFormatting(value = xCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = yCo, inBetween = True, nextToVariable = True), 
				equationFormatting(value = constant, inBetween = False, nextToVariable = False))
				xCoAnswer = -xCo/yCo
				constantAnswer = constant/yCo
			elif difficultyChosen == '4':#8. 8. ay + c = bx
				constant = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if -x % yCo == 0])
				questionString = r'$%sy%s=%sx$' % (equationFormatting(value = yCo, inBetween = False, nextToVariable = True),
				equationFormatting(value = constant, inBetween = True, nextToVariable = False), 
				equationFormatting(value = xCo, inBetween = False, nextToVariable = True))
				xCoAnswer = xCo/yCo
				constantAnswer = -constant/yCo

		#y = ax + b
		#for answer need to take care of fraction
		aFrac = Fraction(xCoAnswer).limit_denominator(self.coefficientMax)
		print('slope numerator and denominator are:',aFrac.numerator, aFrac.denominator)
		
		parallelSlope = Fraction(aFrac.numerator/aFrac.denominator).limit_denominator(self.coefficientMax)
		perpendicularSlope = Fraction(-aFrac.denominator/aFrac.numerator).limit_denominator(self.coefficientMax)
		
		print(parallelSlope.numerator, parallelSlope.denominator)
		print(perpendicularSlope.numerator, perpendicularSlope.denominator)
		#we want to make sure that this point will produce an integer y-intercept for both the parallel and perpendicular lines.
		# yP = (n/d)*xP + b1
		# yP = -(d/n)*xP + b2

		#define x that is divisible by both denominators
		xP = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x % parallelSlope.denominator == 0 and 
			x % perpendicularSlope.denominator == 0 and 
			x*parallelSlope.numerator//parallelSlope.denominator <= self.graphMax*2 and
			x*parallelSlope.numerator//parallelSlope.denominator >= -self.graphMax*2 and 
			x*perpendicularSlope.numerator//perpendicularSlope.denominator <= self.graphMax*2 and
			x*perpendicularSlope.numerator//perpendicularSlope.denominator >= -self.graphMax*2]
			 )

		#workaround?????????????
		xP = parallelSlope.numerator*parallelSlope.denominator
		#range that I want the y-intercept to be
		rangeWanted = [x for x in range(-self.graphMax, self.graphMax+1)]

		#we don't want either y-intercept to be equal to original
		rangeWanted.remove(constantAnswer)
		print(rangeWanted)
		#lets do a y that picks its b's
		#needs to make a b in range for both equations
		#b can't be equal for both
		print('x is', xP)
		print('parallel slope is', parallelSlope)
		print('constantAnswer is', constantAnswer)

		#yP - slope*xP = b1
		yP = random.choice([x for x in range(-self.graphMax, self.graphMax+1) if x - xP*parallelSlope.numerator//parallelSlope.denominator in rangeWanted and x - xP*perpendicularSlope.numerator//perpendicularSlope.denominator in rangeWanted])
		
		b1 = yP - int(round(parallelSlope*xP))
		b2 = yP - int(round(perpendicularSlope*xP))
		
		if self.includeGraph == False:
			questionStart = r'What is the equation of a line that is %s and goes through (%d,%d) to the line whose equation is {%s}?' % (slopeChosen, xP, yP, questionString)
		else:
			points = pointsForDiagram()
			questionStart = r'Given the graph below of graph below of line %s, what is the equation of a line that is %s to $\overline{%s}$ and goes through %s(%d,%d)?' % (points['A']+points['B'], slopeChosen, points['A']+points['B'], points['C'], xP, yP)

		doc.append(NoEscape(questionStart))
		if self.includeGraph == True:
			#need to get original line and two points on the line
			#so xCoAnswer, constantAnswer
			#y = xCoAnswer * x + constantAnswer
			xs = []
			ys = []
			print(xCoAnswer, constantAnswer)
			graphRange = [x for x in range(-self.graphMax, self.graphMax+1)]
			for itemX in graphRange:
				for itemY in graphRange:
					if itemY == (itemX * xCoAnswer + constantAnswer):
						xs.append(itemX)
						ys.append(itemY)
			#now pick two random point on the line, but that always cross the y-axis
			firstX = xs[0]
			firstY = ys[0]

			secondX = xs[-1]
			secondY = ys[-1]

			doc.append(NewLine())
			picLength = sizingXY(minn = -self.graphMax, maxx = self.graphMax, size = 'medium')
			with doc.create(Center()):
				with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
					grid(doc = doc, xMin = -self.graphMax, xMax = self.graphMax, yMin = -self.graphMax, yMax = self.graphMax)
					graphPolygon(doc = doc, x = [firstX, secondX], y = [firstY, secondY], annotations = [points['A'],points['B']], color = 'black')
					graphPoint(doc = doc, x = xP, y = yP, annotations = [points['C']], color = 'black')

		slopeValues = {'parallel': parallelSlope, 'perpendicular': perpendicularSlope}
		slopeStrings = {'parallel':'', 'perpendicular':''}
		slope = Fraction(parallelSlope).limit_denominator(self.coefficientMax)
		yIntercept = b1
		if slope.numerator % slope.denominator != 0:
			slopeString = r'%s\frac{%d}{%d}' % (equationFormatting(value = slope.numerator/(abs(slope.numerator)), inBetween = False, nextToVariable = True),
				abs(slope.numerator), abs(slope.denominator))
		else: #whole number
			slopeString = r'%s' % (equationFormatting(value = slope.numerator, inBetween = False, nextToVariable = True))

		slopeStrings['parallel'] = slopeString

		slopeToUse = perpendicularSlope
		slope = Fraction(perpendicularSlope).limit_denominator(self.coefficientMax)
		yIntercept = b2
		if slope.numerator % slope.denominator != 0:
			slopeString = r'%s\frac{%d}{%d}' % (equationFormatting(value = slope.numerator/(abs(slope.numerator)), inBetween = False, nextToVariable = True), abs(slope.numerator), abs(slope.denominator))
		else: #whole number
			slopeString = r'%s' % (equationFormatting(value = slope.numerator, inBetween = False, nextToVariable = True))	

		slopeStrings['perpendicular'] = slopeString

		choices = []
		twoSlopes = ['parallel','perpendicular']
		twoSlopes.remove(slopeChosen)
		slopeNotChosen = random.choice(twoSlopes)

		if yFormChosen == 'point slope':
			self.answer = r'$y%s= %s(x%s)$' % (equationFormatting(value = -yP, nextToVariable = False, inBetween = True),
			slopeStrings[slopeChosen], equationFormatting(value = -xP, nextToVariable = False, inBetween = True))
			choices.append(self.answer)
			#choice1: same slope but reverse points
			choice1 = r'$y%s= %s(x%s)$' % (equationFormatting(value = yP, nextToVariable = False, inBetween = True),
			slopeStrings[slopeChosen], equationFormatting(value = xP, nextToVariable = False, inBetween = True))
			choices.append(choice1)

			#other slope but with same point
			choice2 = r'$y%s= %s(x%s)$' % (equationFormatting(value = -yP, nextToVariable = False, inBetween = True),
			slopeStrings[slopeNotChosen], equationFormatting(value = -xP, nextToVariable = False, inBetween = True))
			choices.append(choice2)

			#other slope but with reverse points
			choice3 = r'$y%s= %s(x%s)$' % (equationFormatting(value = yP, nextToVariable = False, inBetween = True),
			slopeStrings[slopeNotChosen], equationFormatting(value = xP, nextToVariable = False, inBetween = True))
			choices.append(choice3)

		elif yFormChosen == 'slope intercept':
			slopeYIntercept = {'parallel':b1, 'perpendicular':b2}
			self.answer = r'$y= %sx%s$' % (slopeStrings[slopeChosen], equationFormatting(value = slopeYIntercept[slopeChosen], nextToVariable = False, inBetween = True))
			choices.append(self.answer)

			#choice1: same slope but y-interecept of line drawn
			print('constantAnswer is', constantAnswer) #check if fraccty
			choice1 = r'$y= %sx%s$' % (slopeStrings[slopeChosen], equationFormatting(value = constantAnswer, nextToVariable = False, inBetween = True))
			choices.append(choice1)

			#other slope but with same point
			choice2 = r'$y= %sx%s$' % (slopeStrings[slopeNotChosen], equationFormatting(value = slopeYIntercept[slopeNotChosen], nextToVariable = False, inBetween = True))
			choices.append(choice2)

			#other slope but with original y-intercept
			choice3 = r'$y= %sx%s$' % (slopeStrings[slopeNotChosen], equationFormatting(value = constantAnswer, nextToVariable = False, inBetween = True))
			choices.append(choice3)

		if self.typeOfQuestionChosen == 'MC':
			if self.includeGraph == False:
				doc.append(NewLine())
			newChoices = multipleChoice(choices = choices, doc = doc)
			self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class coordinateTestQuestion3():
	def __init__(self, typeOfQuestion = ['MC'], answerType = ['neither']):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.answerType = answerType
		"""
		1. ay = bx
		2. ay = bx + c
		3. y+c=bx
		4. Y+bx = c

		5. c + ay = bx
		6. ay + bx = c
		7. bx + ay = c
		8. ay + c = bx
		"""
	def addQuestion(self, doc = None):
		answerTypeChosen = random.choice(self.answerType)

		#define 1st line
		#use slope (opposite reciprocal to get xCo and yCo values) as well as constant etc
		#the second line might not be able to be as specific, but we'll see how it goes

		#for the first line
		rangeExluding0 = [x for x in range(-10,10+1) if x not in [0]]

		yCo1 = random.choice([x for x in rangeExluding0 if x not in [-1,1]])
		xCo1 = random.choice([x for x in rangeExluding0 if x not in [-1,1,yCo1, -yCo1]])
		
		#the only times we will experience problems....when yCo for both equations is 1 or -1
		constant1 = random.choice(rangeExluding0)
		constant2 = random.choice(rangeExluding0)
		#assume neither time issue!!!!

		#ay = bx + c
		questionString1 = r'$%sy=%sx%s$' % (equationFormatting(value = yCo1, inBetween = False, nextToVariable = True),
			equationFormatting(value = xCo1, inBetween = False, nextToVariable = True), 
			equationFormatting(value = constant1, inBetween = True, nextToVariable = False))

		#ay - bx = c, so for neither we need reciprocals
		#when solved ay = bx + c, so b/a we need to be slope
		#so reciprocal of first is yCo1/xCo1
		questionString2 = r'$%sy%sx=%s$' % (equationFormatting(value = xCo1, inBetween = False, nextToVariable = True),
		equationFormatting(value = -yCo1, inBetween = True, nextToVariable = True), 
		equationFormatting(value = constant2, inBetween = False, nextToVariable = False))

			

		doc.append(NoEscape(r'There are two equations of lines below:'))
		doc.append(NewLine())
		with doc.create(Center()):
			doc.append(NoEscape(questionString1))
		with doc.create(Center()):
			doc.append(NoEscape(questionString2))
		
		doc.append(NoEscape(r'Which of the following best describes the two lines?'))
		doc.append(NewLine())
		self.answer = 'Neither parallel nor perpendicular'
		choice1 = 'Perpendicular'
		choice2 = 'Parallel'
		choice3 = 'They are the same line'
		choices = [self.answer, choice1, choice2, choice3]
		newChoices = multipleChoice(choices = choices, doc = doc)
		self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class coordinateTestQuestion7():
	def __init__(self, typeOfQuestion = ['SA'], xMinGraph = -10, xMaxGraph = 10, yMaxGraph = 10, yMinGraph = -10, typeOfSolution = ['integer'], typeOfLine = ['horizontal','vertical','diagonal'], includeGraph = False, wording = [1,2]):
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)
		self.xMinGraph = xMinGraph
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.yMaxGraph = yMaxGraph

		self.typeOfSolution = typeOfSolution
		self.typeOfLine = typeOfLine
		self.includeGraph = includeGraph
		self.wording = wording

	def addQuestion(self, doc = None):
		typeOfSolution = random.choice(self.typeOfSolution)
		typeOfLine = random.choice(self.typeOfLine)
		wording = random.choice(self.wording)
		#Horizontal and vertical mean x's an y's will be the same

		#if wanting 1:1 fuck it make a new one

		#a and b max is (xMaxGraph-xMinGraph)//4

		#if graph isn't square, it is now
		maxx = min([self.xMaxGraph, self.yMaxGraph])
		minn = max([self.yMinGraph, self.xMinGraph])

		
		a = random.choice([x for x in range(1,(maxx-minn)//3+1)])
		b = random.choice([x for x in range(1, (maxx-minn)//3+1) if x not in [a] and gcd(x,a) == 1])

		#pick x1, y1 as mins of graph, then translate afterwards, this willa llow large a's and b's to work
		x1 = self.xMinGraph
		y1 = self.yMinGraph

		if typeOfSolution == 'integer':
			x2 = random.choice([x for x in range(self.xMinGraph,self.xMaxGraph+1) if (x-x1) % (a+b) == 0 and (x-x1) != 0] )
			y2 = random.choice([y for y in range(self.yMinGraph,self.yMaxGraph+1) if (y-y1) % (a+b) == 0 and (y-y1) != 0] )
			if typeOfLine == 'vertical':
				x2 = x1
		elif typeOfSolution == 'fraction':
			#only do 1/2 for now
			x2 = random.choice([x for x in range(self.xMinGraph,self.xMaxGraph+1) if (x-x1) % 1/2*(a+b) == 0 and (x-x1) != 0] )
			y2 = random.choice([y for y in range(self.yMinGraph,self.yMaxGraph+1) if (y-y1) % 1/2*(a+b) == 0 and (y-y1) != 0] )
			if typeOfLine == 'horizontal':
				y2 = y1
		#translate anywhere on grid
		xChange = random.randint(self.xMinGraph-min([x1,x2]), self.xMaxGraph-max([x1,x2]))
		yChange = random.randint(self.yMinGraph-min([y1,y2]), self.yMaxGraph-max([y1,y2]))

		#translate coordinates
		xs, ys = translateCoordinates(x = [x1, x2], y = [y1, y2], changeX = xChange, changeY = yChange )

		x1 = xs[0]
		x2 = xs[1]
		y1 = ys[0]
		y2 = ys[1]

		#labels
		points = pointsForDiagram()

		A = points['A']
		B = points['B']
		C = points['C']

		#answers
		if typeOfSolution == 'integer':
			xAnswer = int(round( (x1*b+x2*a) / (b + a) ) )
			yAnswer = int(round( (y1*b+y2*a) / (b + a) ) )
			self.answer = r'$%s = (%d,%d)$' % (B, xAnswer, yAnswer)
		elif typeOfSolution == 'fraction':
			#mixed fraction
			numeratorX = (x1*b+x2*a) % (a+b)
			denominatorX = 2
			wholeX = (x1*b+x2*a)/(a+b) - numeratorX/denominatorX
			if numeratorX == 0:
				xString = str(int(round( (x1*b+x2*a) / (b + a) ) ))
			else:
				if wholeX != 0:
					xString = r'%d\frac{%s}{%s}' % (wholeX, numeratorX, denominatorX)
				else:
					xString = r'\frac{%s}{%s}' % (numeratorX, denominatorX)

			numeratorY = (y1*b+y2*a) % (a+b)
			denominatorY = 2
			wholeY = (y1*b+y2*a)/(a+b) - numeratorY/denominatorY

			if numeratorY == 0:
				yString = str(int(round( (y1*b+y2*a) / (b + a) ) ))
			else:
				if wholeY != 0:
					yString = r'%d\frac{%s}{%s}' % (wholeY, numeratorY, denominatorY)
				else:
					yString = r'\frac{%s}{%s}' % (numeratorY, denominatorY)

			self.answer = r'$%s = (%s,%s)$' % (B, xString, yString)

		if wording == 1:
			doc.append(NoEscape(r'Given the directed line segment %s, %s(%d,%d), and %s(%d,%d), determine the coordinates of the point %s that %s $\overline{%s}$ into ratio of %s.' % (A+C, A, x1, y1, C, x2, y2, B, random.choice(['partitions','divides']), A+C, random.choice(['%d:%d' % (a,b), '%d to %d' % (a,b)]))))	
		elif wording == 2:
			doc.append(NoEscape(r'Given the line segment %s, %s(%d,%d), and %s(%d,%d), determine the coordinates of the point %s that %s $\overline{%s}$, such that %s is in a ratio of %s.' % (A+C, A, x1, y1, C, x2, y2, B, random.choice(['partitions','divides']), A+C, random.choice(['%s:%s' % (A+B, B+C), '%s to %s' % (A+B, B+C)]), random.choice(['%d:%d' % (a,b), '%d to %d' % (a,b)]))))

		if self.includeGraph == True:
			doc.append(NewLine())
			picLength = sizingXY(minn = -self.xMaxGraph, maxx = self.xMaxGraph, size = 'medium')
			with doc.create(Center()):
				with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
					grid(doc = doc, xMin = -self.xMaxGraph, xMax = self.xMaxGraph, yMin = -self.xMaxGraph, yMax = self.xMaxGraph)
					#graphPolygon(doc = doc, x = self.xs, y = self.ys, annotations = self.annos, color = 'red')

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class coordinateTestQuestion4():
	def __init__(self, rightTriangle = False, xMaxGraph = 10, xMinGraph = -10, yMaxGraph = 10, yMinGraph = -10, typeOfQuestion = ['SA','MC']):
		self.typeOfQuestionChosen = 'SA'
		self.rightTriangle = rightTriangle
		self.xMaxGraph = xMaxGraph
		self.yMinGraph = yMinGraph
		self.xMinGraph = xMinGraph
		self.yMaxGraph = yMaxGraph
		self.typeOfQuestionChosen = random.choice(typeOfQuestion)

	def addQuestion(self, doc = None):
		#make a box(rectangle) on the grid with bottom left ax min and min
		x1 = self.xMinGraph
		y1 = self.yMinGraph

		#rectangle going, counterclockwise
		x2 = self.xMinGraph
		y2 = random.randint(self.xMinGraph+5, self.yMaxGraph) #+5 so at least it's somewhere

		x3 = random.randint(self.xMinGraph+5, self.xMaxGraph)
		y3 = y2

		x4 = x3
		y4 = y1

		#Determine poitns for the triangle
		if self.rightTriangle == False:
			#starting on left side of this box, pick any point but not corners
			triangleX1 = x1
			triangleY1 = random.randint(y1 + 1, y2 - 1)
			#then pick any point on top
			triangleX2 = random.randint(x2 + 1, x3 - 1)
			triangleY2 = y2
			#then pick any point on the bottom right corner
			triangleX3 = x4
			triangleY3 = y4
		elif self.rightTriangle == True:
			#make a right triangle inside the constraints of the graph

			#start at minn, minn
			triangleX1 = self.xMinGraph
			triangleY1 = self.yMinGraph

			#assuming graph is going to be square
			xLength = self.xMaxGraph - self.xMinGraph
			yLength = self.yMaxGraph - self.yMinGraph
			xSlopeChange = random.randint(xLength//4, xLength//2-1)
			#we don't want an isoceles triangle
			ySlopeChange = random.choice([x for x in range(yLength//4, yLength//2-1+1) if x not in [xSlopeChange]])

			#second points
			triangleX2 = triangleX1 + xSlopeChange
			triangleY2 = triangleY1 + ySlopeChange

			#y/x --> -x/y
			#third points, after opposite reciprocal of change added to second points, making 2nd coordinate 90
			triangleX3 = triangleX2 + ySlopeChange
			triangleY3 = triangleY2 + -xSlopeChange

		#translate x's and y's anywhere that will fit on the graph
		xs = [triangleX1, triangleX2, triangleX3]
		ys = [triangleY1, triangleY2, triangleY3]
		xChange = random.randint(self.xMinGraph - min(xs), self.xMaxGraph - max(xs))
		yChange = random.randint(self.yMinGraph - min(ys), self.yMaxGraph - max(ys))

		newXs, newYs = translateCoordinates(x = xs, y = ys, changeX = xChange, changeY = yChange)
		x1 = newXs[0]
		x2 = newXs[1]
		x3 = newXs[2]
		y1 = newYs[0]
		y2 = newYs[1]
		y3 = newYs[2]

		points = pointsForDiagram()
		A = points['A']
		B = points['B']
		C = points['C']
		#now we need to have the question
		doc.append(NoEscape(r'Given $\bigtriangleup{%s}$ below, what is the area of the triangle?' % (A+B+C)))
		doc.append(NewLine())
		#followed by a grid with the shape drawn
		picLength = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)
				graphPolygon(doc = doc, x = [x1,x2,x3], y = [y1,y2,y3], annotations = [A,B,C], color = 'black')

		areaOfTriangle = abs( (x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2 )
		self.answer = r'%g' % (areaOfTriangle)

		if self.typeOfQuestionChosen == 'MC':
			xLength = max([x1,x2,x3]) - min([x1, x2, x3])
			yLength = max([y1,y2,y3]) - min([y1,y2,y3])
			#box area
			boxArea = xLength*yLength
			choice1 = r'%g' % (boxArea)
			choices = []
			choices.append(choice1)
			choices.append(self.answer)
			#box area -actual area
			choice2 = r'%g' % (boxArea - areaOfTriangle)
			choices.append(choice2)
			#length of side under a square root
			lengthOfSideRadicand = ((x1 - x2)**2 + (y1 - y2)**2)
			choice3 = r'$\sqrt{%d}$' % (lengthOfSideRadicand)
			choices = [choice3] + choices
			newChoices = multipleChoice(choices = choices, doc = doc)
			self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class coordinateTestQuestion5():
	def __init__(self, includeGraph = True, rounding = ['whole number','tenth','hundredth','thousandth'], difficulty = ['distance', 'radius', 'diameter','perimeter'], xMaxGraph = 10, xMinGraph = -10, yMaxGraph = 10, yMinGraph = -10, typeOfQuestion = ['SA','MC']):
		#difficulty 1: find dist between two points
		#difficulty 2: find length of radius given center and point on circle
		#difficulty 3: find the perimeter of a regular polygon given two endpoints
		self.includeGraph = includeGraph
		self.rounding = rounding
		self.difficulty = difficulty

		self.xMaxGraph = xMaxGraph
		self.xMinGraph = xMinGraph
		self.yMaxGraph = yMaxGraph
		self.yMinGraph = yMinGraph

		self.typeOfQuestionChosen = random.choice(typeOfQuestion)

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		difficultyChosen = random.choice(self.difficulty)

		points = pointsForDiagram()
		x1 = random.randint(self.xMinGraph, self.xMaxGraph)
		y1 = random.randint(self.yMinGraph, self.yMaxGraph)

		#so x2 can't be x1, -1-2 etc is for making a triangle not uber small
		x2 = random.choice([x for x in range(self.xMinGraph, self.xMaxGraph+1) if x not in [x1, x1-1, x1-2, x1+1, x1+2]])
		y2 = random.choice([y for y in range(self.yMinGraph, self.yMaxGraph+1) if y not in [y1, y1-1, y1-2, y1+1, y1+2]])
		A = points['A']
		B = points['B']

		distance = ( (x1-x2)**2 + (y1-y2)**2 ) ** (1/2)

		if difficultyChosen == 'distance':
			#pick two lettered points and two points that are diagonal from one another
			questionString = r'Given %s(%d,%d) and %s(%d,%d), how long is $\overline{%s}$ to the nearest %s?' % (A, x1, y1,
				B, x2, y2,
				A+B, roundingChosen)
			self.answer = r'%g' % (roundGivenString(value = distance, string = roundingChosen))
			if self.typeOfQuestionChosen == 'MC':
				choices = []
				#diameter
				choice1 = r'%g' % (roundGivenString(value = distance*2, string = roundingChosen))
				choices.append(choice1)
				choices.append(self.answer)
				#don't square root radius
				choice2 = r'%g' % (roundGivenString(value = distance**2, string = roundingChosen))
				choices.append(choice2)
				#don't square root * 2
				choice3 = r'%g' % (roundGivenString(value = (distance**2)*2, string = roundingChosen))
				choices.append(choice3)
		elif difficultyChosen == 'radius':
			questionString = r'Given Circle %s with center (%d,%d), if a point on the circle is %s(%d,%d), how long is the radius of the circle to the nearest %s?' % (A, x1, y1,
				B, x2, y2,
				roundingChosen)
			self.answer = r'%g' % (roundGivenString(value = distance, string = roundingChosen))
			if self.typeOfQuestionChosen == 'MC':
				choices = []
				#diameter
				choice1 = r'%g' % (roundGivenString(value = distance*2, string = roundingChosen))
				choices.append(choice1)
				choices.append(self.answer)
				#don't square root radius
				choice2 = r'%g' % (roundGivenString(value = distance**2, string = roundingChosen))
				choices.append(choice2)
				#don't square root * 2
				choice3 = r'%g' % (roundGivenString(value = (distance**2)*2, string = roundingChosen))
				choices.append(choice3)
		elif difficultyChosen == 'diameter':
			questionString = r'Given Circle %s with center (%d,%d), if a point on the circle is %s(%d,%d), how long is the diameter of the circle to the nearest %s?' % (A, x1, y1,
				B, x2, y2,
				roundingChosen)
			#diameter
			self.answer = r'%g' % (roundGivenString(value = distance*2, string = roundingChosen))
			if self.typeOfQuestionChosen == 'MC':
				choices = []
				#radius
				choice1 = r'%g' % (roundGivenString(value = distance, string = roundingChosen))
				choices.append(choice1)
				choices.append(self.answer)
				#don't square root radius
				choice2 = r'%g' % (roundGivenString(value = distance**2, string = roundingChosen))
				choices.append(choice2)
				#don't square root * 2
				choice3 = r'%g' % (roundGivenString(value = (distance**2)*2, string = roundingChosen))
				choices.append(choice3)
		elif difficultyChosen == 'perimeter':
			polygons = ['equilateral triangle','square','regular pentagon','regular hexagon','','regular octagon','regular nonagon','regular decagon']
			polygonChosen = random.choice([x for x in ['equilateral triangle','square','regular pentagon','regular hexagon','regular octagon','regular nonagon','regular decagon'] if x not in ['']])
			questionString = r'Given that the endpoints of a side of a %s are %s(%g,%g) and %s(%g,%g), what is the perimeter of the %s to the nearest %s?' % (polygonChosen,
				A, x1, y1,
				B, x2, y2, 
				polygonChosen, roundingChosen)
			self.answer = r'%g' % (roundGivenString(string = roundingChosen, value = (polygons.index(polygonChosen)+3)*distance))
			#if putting this for multiple choice, please do square roots and all that jazz!
			if self.typeOfQuestionChosen == 'MC':
				dick = 3

		doc.append(NoEscape(questionString))

		if self.includeGraph == True:
			doc.append(NewLine())
			lengthForPic = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
			with doc.create(Center()):
				with doc.create(TikZ(options='x=%gcm, y=%gcm' % (lengthForPic, lengthForPic))):
					grid(doc = doc, xMin = self.xMinGraph, xMax = self.xMaxGraph, yMin = self.yMinGraph, yMax = self.yMaxGraph)

		if self.typeOfQuestionChosen == 'MC':
			newChoices = multipleChoice(choices = choices, doc = doc)
			self.answer = r'$Choice %d: %s$' % (newChoices.index(self.answer)+1, self.answer)



	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class coordinateTestQuestion6():
	def __init__(self, maxx = 10):
		self.maxx = maxx
		self.typeOfQuestionChosen = 'MC'

	def addQuestion(self, doc = None):
		x1 = 0
		y1 = 0

		#even slope numerator
		slopeNumerator = random.choice([x for x in range(-self.maxx//2, self.maxx//2+1) if x % 2 == 0 and x != 0])

		#odd
		slopeDenominator = random.choice([x for x in range(1, self.maxx//2+1) if x % 2 == 1 and x != 0 and slopeNumerator % x != 0])

		x2 = slopeNumerator*3
		y2 = slopeDenominator*3

		#equation of perpendicular bisector (y-slopeN)=-D/N(x-slopeD)
		#y = -D/n x + slopeD + slopeN
		#find points on line, then make pick a point and add up and subtract one etc

		perpBisectorX = [x for x in range(-self.maxx//2-1, self.maxx//2+2)]
		perpBisectorY = [-slopeDenominator/slopeNumerator*x + slopeDenominator + slopeNumerator for x in perpBisectorX]

		x3 = x1
		y3 = y2

		xs = [x1, x2, x3]
		ys = [y1, y2, y3]

		xChange = random.randint(-self.maxx-min(xs), self.maxx-max(xs))
		yChange = random.randint(-self.maxx-min(ys), self.maxx-max(ys))

		newXs, newYs = translateCoordinates(xs, ys, xChange, yChange)

		points = pointsForDiagram()
		questionString = r'In the diagram below $\bigtriangleup{%s}$ is drawn.' % (points['A']+points['B']+points['C'])

		doc.append(NoEscape(questionString))
		doc.append(NewLine())
		picLength = sizingXY(minn = -self.maxx, maxx = self.maxx, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = -self.maxx, xMax = self.maxx, yMin = -self.maxx, yMax = self.maxx)
				graphPolygon(doc = doc, x = newXs, y = newYs, annotations = [points['A'], points['B'], points['C']], color = 'black')

		doc.append(NoEscape(r'What is the slope of the altitude drawn from %s to $\overline{%s}$?' % (points['C'], points['A']+points['B'])))

		slope = Fraction(slopeNumerator/slopeDenominator).limit_denominator(100)
		self.answer = r'$%s\frac{%d}{%d}$' % (equationFormatting(value = -slope.denominator/abs(slope.denominator), nextToVariable = True, inBetween = False), abs(slope.denominator), abs(slope.numerator))

		choices = []
		choices.append(self.answer)
		#slope of opposite side
		choice1 = r'$%s\frac{%d}{%d}$' % (equationFormatting(value = slope.numerator/abs(slope.numerator), nextToVariable = True, inBetween = False), abs(slope.numerator), abs(slope.denominator))
		choices.append(choice1)

		#ahg
		choice2 = r'$%s\frac{%d}{%d}$' % (equationFormatting(value = -slope.numerator/abs(slope.numerator), nextToVariable = True, inBetween = False), abs(slope.numerator), abs(slope.denominator))
		choices.append(choice2)

		#ahg
		choice3 = r'$%s\frac{%d}{%d}$' % (equationFormatting(value = slope.denominator/abs(slope.denominator), nextToVariable = True, inBetween = False), abs(slope.denominator), abs(slope.numerator))
		choices.append(choice3)
		doc.append(NewLine())
		newChoices = multipleChoice(choices = choices, doc = doc)
		self.answer = r'Choice %d: %s' % (newChoices.index(self.answer)+1, self.answer)
	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class coordinateTestQuestion8():
	def __init__(self, rounding = ['whole number','tenth','hundredth','thousandth'], xMaxGraph = 10, xMinGraph = -10, yMaxGraph = 10, yMinGraph = -10, typeOfQuestion = ['SA'], polygonType = ['equilateral triangle', 'square', 'regular pentagon', 'regular hexagon', 'regular octagon','regular nonagon','regular decagon']):
		#difficulty 1: find dist between two points
		#difficulty 2: find length of radius given center and point on circle
		#difficulty 3: find the perimeter of a regular polygon given two endpoints
		self.rounding = rounding

		self.xMaxGraph = xMaxGraph
		self.xMinGraph = xMinGraph
		self.yMaxGraph = yMaxGraph
		self.yMinGraph = yMinGraph

		self.polygonType = polygonType

		self.typeOfQuestionChosen = random.choice(typeOfQuestion)

	def addQuestion(self, doc = None):
		roundingChosen = random.choice(self.rounding)
		polygonChosen = random.choice(self.polygonType)
		print(polygonChosen)

		polygonDict = {'equilateral triangle':3, 'square':4, 'regular pentagon':5, 'regular hexagon':6, 'regular octagon':8, 'regular nonagon':9, 'regular decagon':10}
		print(polygonDict[polygonChosen])
		xs = []
		ys = []
		radius = 5
		for item in range(0,polygonDict[polygonChosen]):
			xs.append(radius * math.cos(2*math.pi*item/polygonDict[polygonChosen]))
			ys.append(radius * math.sin(2*math.pi*item/polygonDict[polygonChosen]))



		points = pointsForDiagram()
		x1 = xs[0]
		y1 = ys[0]

		x2 = xs[1]
		y2 = ys[1]

		#fuck to scale LAWL, actually just do ints of decimals
		xLength = int(abs(x2 - x1))
		yLength = int(abs(y2 - y1))

		if xLength == 0:
			xLength = 1
		if yLength == 0:
			yLength = 1

		rightTriangleCornerX = x2
		rightTriangleCornerY = y1

		distance = ( (xLength)**2 + (yLength)**2 ) ** (1/2)

		questionString = r'Given the drawing of the %s below, what is the perimeter of the %s to the nearest %s?' % (polygonChosen, 
			polygonChosen, roundingChosen)
		self.answer = r'%g' % (roundGivenString(string = roundingChosen, value = (polygonDict[polygonChosen])*distance))

		doc.append(NoEscape(questionString))

		doc.append(NewLine())
		lengthForPic = sizingXY(minn = self.xMinGraph, maxx = self.xMaxGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (lengthForPic, lengthForPic))):
				graphPolygon(doc = doc, x = xs, y = ys, annotations = [], color = 'black')
				graphPolygon(doc = doc, x = [x1, rightTriangleCornerX, x2], y = [y1, rightTriangleCornerY, y2], annotations = [], color = 'black')
				#for box on right triangle etc
				com('node at (%g,%g) [%s] {\small $%s$}' % (rightTriangleCornerX + xLength/2, rightTriangleCornerY, 'below', xLength), doc = doc)
				com('node at (%g,%g) [%s] {\small $%s$}' % (rightTriangleCornerX, rightTriangleCornerY+yLength/2, 'left', yLength), doc = doc)
				com('coordinate (A) at (%g,%g) {}' % (x1,y1), doc = doc)
				com('coordinate (B) at (%g,%g) {}' % (rightTriangleCornerX,rightTriangleCornerY), doc = doc)
				com('coordinate (C) at (%g,%g) {}' % (x2,y2), doc = doc)
				com('tkzMarkRightAngle[draw=black,size=.2](A,B,C)', doc = doc)

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape(self.answer))
class coordinateTestQuestion9():
	def __init__(self, typeOfPolygon = ['right triangle','isosceles triangle','isosceles right triangle','parallelogram','rhombus','square','rectangle'], sizeOfGraph = 10):
		self.typeOfPolygon = typeOfPolygon
		self.sizeOfGraph = sizeOfGraph
		self.typeOfQuestionChosen = 'SA'

	def addQuestion(self, doc = None):
		#for quadrilaterals, center is 0,0 use properties then translate the shape around the graph to add variety etc

		#isosceles triangle is 3 points of rhombus

		#isosceles right triangle is 3 points of square

		#right triangle is 3 points of rhombus including center

		centerX = 0
		centerY = 0

		#let's do a parallelogram first then mesh together after

		typeOfPolygonChosen = random.choice(self.typeOfPolygon)

		if typeOfPolygonChosen == 'parallelogram':
			#left then clockwise, 3rd quadrant
			x1 = random.randint(-self.sizeOfGraph, -3)
			y1 = random.choice([x for x in range(-self.sizeOfGraph, -2) if x not in [x1]])

			x3 = -x1
			y3 = -y1

			#stopping x2, y2 from being a rhombus and i think rectangle as well since x1's go with 3's etc
			x2 = random.choice([x for x in range(-self.sizeOfGraph, -2) if x not in [y1, -y1, x1]])
			y2 = random.choice([x for x in range(2, self.sizeOfGraph+1) if x not in [x1, -x1, y1]])

			x4 = -x2
			y4 = -y2
			xs = [x1, x2, x3, x4]
			ys = [y1, y2, y3, y4]
		elif typeOfPolygonChosen == 'rhombus' or typeOfPolygonChosen == 'isosceles triangle':
			#make a right triangle
			#start with x2, y2 as 0,0
			x2 = 0
			y2 = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			x3 = x2 + xChange
			y3 = y2 + yChange

			#now x1 is opposite reciprocal but dilated
			xChangeDilated = xChange*2
			#figure out scale factor
			scaleFactor = xChangeDilated/xChange
			yChangeDilated = yChange * scaleFactor

			#-xChange/yChange
			x1 = x2 + yChangeDilated
			y1 = y2 - xChangeDilated

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]

			#now we have a right triangle, flip it over the longer side to make isosceles
			x2 = -x3
			y2 = -y3

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]
			if typeOfPolygonChosen == 'rhombus':
				x4 = -x1
				y4 = -y1
				xs = [x1, x2, x4, x3]
				ys = [y1, y2, y4, y3]
		elif typeOfPolygonChosen == 'isosceles right triangle' or typeOfPolygonChosen == 'square':
			#make a right triangle
			#start with x2, y2 as 0,0
			x2 = 0
			y2 = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			x3 = x2 + xChange
			y3 = y2 + yChange

			#now x1 is opposite reciprocal but dilated
			xChangeDilated = xChange
			#figure out scale factor
			scaleFactor = xChangeDilated/xChange
			yChangeDilated = yChange * scaleFactor

			#-xChange/yChange
			x1 = x2 + yChangeDilated
			y1 = y2 - xChangeDilated

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]
			if typeOfPolygonChosen == 'square':
				x4 = x1 + xChange
				y4 = y1 + yChange
				xs = [x1, x2, x3, x4]
				ys = [y1, y2, y3, y4]
				print(xs,ys)
		elif typeOfPolygonChosen == 'rectangle' or typeOfPolygonChosen == 'right triangle':
			#make a right triangle
			#start with x2, y2 as 0,0
			x2 = 0
			y2 = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			x3 = xChange
			y3 = yChange
			
			x1 = -yChange*2
			y1 = xChange*2

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]

			if typeOfPolygonChosen == 'rectangle':
				x4 = x1 + xChange
				y4 = y1 + yChange
				xs.append(x4)
				ys.append(y4)

		#translate around graph
		xs = [int(round(x)) for x in xs]
		ys = [int(round(x)) for x in ys]
		translateXChange = random.randint(-self.sizeOfGraph - min(xs), self.sizeOfGraph - max(xs))
		translateYChange = random.randint(-self.sizeOfGraph - min(ys), self.sizeOfGraph - max(ys))

		newXs, newYs = translateCoordinates(xs, ys, translateXChange, translateYChange)

		self.xs = newXs
		self.ys = newYs
		print(xs, ys)
		print(self.xs, self.ys)


		points = pointsForDiagram()
		if len(xs) == 4:
			doc.append(NoEscape(r'Given the graph below, if the coordinates of a quadrilateral are $%s(%d,%d)$, $%s(%d,%d)$, $%s(%d,%d)$, and $%s(%d,%d)$, prove $%s%s%s%s$ is a %s.' % (points['A'], self.xs[0], self.ys[0], 
				points['B'], self.xs[1], self.ys[1], 
				points['C'], self.xs[2], self.ys[2], 
				points['D'], self.ys[3], self.ys[3],
				points['A'], points['B'], points['C'], points['D'],
				typeOfPolygonChosen)))
			self.annos = [points['A'], points['B'], points['C'], points['D']]
		else:
			doc.append(NoEscape(r'Given the graph below, if the coordinates of a triangle are $%s(%d,%d)$, $%s(%d,%d)$, and $%s(%d,%d)$, prove $%s%s%s$ is a %s.' % (points['A'], self.xs[0], self.ys[0], 
				points['B'], self.xs[1], self.xs[1], 
				points['C'], self.xs[2], self.ys[2], 
				points['A'], points['B'], points['C'],
				typeOfPolygonChosen)))
			self.annos = [points['A'], points['B'], points['C']]

		doc.append(NewLine())
		picLength = sizingXY(minn = -self.sizeOfGraph, maxx = self.sizeOfGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = -self.sizeOfGraph, xMax = self.sizeOfGraph, yMin = -self.sizeOfGraph, yMax = self.sizeOfGraph)
				#graphPolygon(doc = doc, x = self.xs, y = self.ys, annotations = self.annos, color = 'red')


	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape('tototo'))
class coordinateTestQuestion10():
	def __init__(self, typeOfPolygon = ['right triangle','isosceles triangle','isosceles right triangle','parallelogram','rhombus','square','rectangle'], sizeOfGraph = 10, secondPart = True):
		self.typeOfPolygon = typeOfPolygon
		self.sizeOfGraph = sizeOfGraph
		self.typeOfQuestionChosen = 'SA'
		self.secondPart = secondPart

	def addQuestion(self, doc = None):
		#for quadrilaterals, center is 0,0 use properties then translate the shape around the graph to add variety etc

		#isosceles triangle is 3 points of rhombus

		#isosceles right triangle is 3 points of square

		#right triangle is 3 points of rhombus including center

		centerX = 0
		centerY = 0

		#let's do a parallelogram first then mesh together after

		typeOfPolygonChosen = random.choice(self.typeOfPolygon)
	
		if typeOfPolygonChosen == 'parallelogram':
			#left then clockwise, 3rd quadrant
			x1 = random.randint(-self.sizeOfGraph, -3)
			y1 = random.choice([x for x in range(-self.sizeOfGraph, -2) if x not in [x1]])

			x3 = -x1
			y3 = -y1

			#stopping x2, y2 from being a rhombus and i think rectangle as well since x1's go with 3's etc
			x2 = random.choice([x for x in range(-self.sizeOfGraph, -2) if x not in [y1, -y1, x1]])
			y2 = random.choice([x for x in range(2, self.sizeOfGraph+1) if x not in [x1, -x1, y1]])

			x4 = -x2
			y4 = -y2
			xs = [x1, x2, x3, x4]
			ys = [y1, y2, y3, y4]
		elif typeOfPolygonChosen == 'rhombus' or typeOfPolygonChosen == 'isosceles triangle':
			#make a right triangle
			#start with x2, y2 as 0,0
			centerX = 0
			centerY = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			#top right point
			x3 = xChange
			y3 = yChange

			#bottom left
			x1 = -xChange
			y1 = -yChange

			#top left perpendicular to bottom left
			x2 = yChange*2
			y2 = -xChange*2

			x4 = -yChange*2
			y4 = xChange*2

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]

			if typeOfPolygonChosen == 'rhombus':
				xs = [x1, x2, x3, x4]
				ys = [y1, y2, y3, y4]
		elif typeOfPolygonChosen == 'isosceles right triangle' or typeOfPolygonChosen == 'square':
			#make a right triangle
			#start with x2, y2 as 0,0
			x2 = 0
			y2 = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			x3 = x2 + xChange
			y3 = y2 + yChange

			#now x1 is opposite reciprocal but dilated
			xChangeDilated = xChange
			#figure out scale factor
			scaleFactor = xChangeDilated/xChange
			yChangeDilated = yChange * scaleFactor

			#-xChange/yChange
			x1 = x2 + yChangeDilated
			y1 = y2 - xChangeDilated

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]
			if typeOfPolygonChosen == 'square':
				x4 = x1 + xChange
				y4 = y1 + yChange
				xs = [x1, x2, x3, x4]
				ys = [y1, y2, y3, y4]
				print(xs,ys)
		elif typeOfPolygonChosen == 'rectangle' or typeOfPolygonChosen == 'right triangle':
			#make a right triangle
			#start with x2, y2 as 0,0
			x2 = 0
			y2 = 0

			#assuming graph is square, make a slope that is possible to dilate because or else it will be isosceles
			xChange = random.choice([x for x in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if x not in [0]]) #this will allow to be dilated by 2
			yChange = random.choice([y for y in range(-self.sizeOfGraph//2, self.sizeOfGraph//2+1) if y not in [0, xChange, -xChange]])
			#could make an option for 0 slopes etc

			#rise over run --> yChange/xChange
			x3 = x2 + xChange
			y3 = y2 + yChange

			#now x1 is opposite reciprocal but dilated
			xChangeDilated = xChange*2
			#figure out scale factor
			scaleFactor = xChangeDilated/xChange
			yChangeDilated = yChange * scaleFactor

			#-xChange/yChange
			x1 = x2 + yChangeDilated
			y1 = y2 - xChangeDilated

			xs = [x1, x2, x3]
			ys = [y1, y2, y3]

			if typeOfPolygonChosen == 'rectangle':
				x4 = x1 + xChange
				y4 = y1 + yChange
				xs.append(x4)
				ys.append(y4)

		#translate around graph
		xs = [int(round(x)) for x in xs]
		ys = [int(round(x)) for x in ys]
		translateXChange = random.randint(-self.sizeOfGraph - min(xs), self.sizeOfGraph - max(xs))
		translateYChange = random.randint(-self.sizeOfGraph - min(ys), self.sizeOfGraph - max(ys))

		newXs, newYs = translateCoordinates(xs, ys, translateXChange, translateYChange)

		self.xs = newXs
		self.ys = newYs
		print(xs, ys)
		print(self.xs, self.ys)


		points = pointsForDiagram()
		if len(xs) == 4:
			doc.append(NoEscape(r'Given the graph below, if the coordinates of a quadrilateral are $%s(%d,%d)$, $%s(%d,%d)$, $%s(%d,%d)$, and $%s(%d,%d)$, prove $%s%s%s%s$ is a %s.' % (points['A'], self.xs[0], self.ys[0], 
				points['B'], self.xs[1], self.ys[1], 
				points['C'], self.xs[2], self.ys[2], 
				points['D'], self.ys[3], self.ys[3],
				points['A'], points['B'], points['C'], points['D'],
				typeOfPolygonChosen)))
			self.annos = [points['A'], points['B'], points['C'], points['D']]
		else:
			doc.append(NoEscape(r'Given the graph below, if the coordinates of a triangle are $%s(%d,%d)$, $%s(%d,%d)$, and $%s(%d,%d)$, prove $%s%s%s$ is a %s.' % (points['A'], self.xs[0], self.ys[0], 
				points['B'], self.xs[1], self.xs[1], 
				points['C'], self.xs[2], self.ys[2], 
				points['A'], points['B'], points['C'],
				typeOfPolygonChosen)))
			self.annos = [points['A'], points['B'], points['C']]

		doc.append(NewLine())
		picLength = sizingXY(minn = -self.sizeOfGraph, maxx = self.sizeOfGraph, size = 'medium')
		with doc.create(Center()):
			with doc.create(TikZ(options='x=%gcm, y=%gcm' % (picLength, picLength))):  
				grid(doc = doc, xMin = -self.sizeOfGraph, xMax = self.sizeOfGraph, yMin = -self.sizeOfGraph, yMax = self.sizeOfGraph)
				#graphPolygon(doc = doc, x = self.xs, y = self.ys, annotations = self.annos, color = 'red')

		if self.secondPart == True and (typeOfPolygonChosen == 'rhombus' or typeOfPolygonChosen == 'rectangle'):
			doc.append(VerticalSpace('3in'))
			doc.append(NoEscape(r'Explain why quadrilateral $%s$ is not a square. Prove it.' % (points['A']+points['B']+points['C']+points['D'])))

	def addAnswer(self, docAnswer = None):
		docAnswer.append(NoEscape('tototo'))
#parallel perpendicular

#(y-)=m(x-)
#y = form too

#are these two lines parallel (solve for y twice, compare slopes)

#find the equation of a line parallel that goes through a point

#mc if I have time


#doubleSidedPrinting = True means that a version or versionAnswerKey will be on an even number of pages #if i think it's
#going to be more than 1 page, do doubleSided 




#Descibe equations

def dictForDatabase(**kwargs):
	dict = {
	'performDilation':performDilation(**kwargs),
	'identifyDilation':identifyDilation(**kwargs)}
	return dict
def getQuestions(id, **kwargs):
	#all of the current question classes into a dictionary
	

	if id == 'performDilation':
		questionDictionary = {'performDilation':performDilation(**kwargs)}
	elif id == 'identifyDilation':
		questionDictionary = {'identifyDilation':identifyDilation(**kwargs)}
	#Returns the dictionary of classes
	return questionDictionary

nameOfDoc = '6.3 - Solving with GCF,DOTS,Trinomials - ET'
k = []

for choice in [1,2,3,4]:
	if choice == 1:
		k.append(solveTrinomials(bSign = ['positive','negative'], cSign = ['positive','negative'], aEqualsOne = True))
	elif choice == 2:
		k.append(solveTrinomials(bSign = ['positive','negative'], cSign = ['positive','negative'], aEqualsOne = False))
	elif choice == 3:
		k.append(solveGCF(numberMax = 3))
	elif choice == 4:
		k.append(solveDOTS(difficulty = [1,2], GCFNumber = random.choice([True,False]), GCFVariable = random.choice([True,False])))

createWorksheet(answersAttached = False, numberOfVersions = 60,
versionsCombined = True, collatedVersionsWithAnswers = False, nameOfDoc = nameOfDoc, questions = k, spaceBetween = '2.5in',
spaceBetweenMC = '.5in', spaceBetweenSA = '.1in', pageNumbers = True, columns = 3, doubleSidedPrinting = True,
docCreate = True, docAnswerCreate = True, referenceSheet = None, path = '/home/devin/devins_project/static/')



















