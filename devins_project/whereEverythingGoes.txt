Files:

createdQuestions.py----------------------
#As of now, created Questions go in here, maybe eventually broken down by subject?

dbconnect.py-----------
#File used to connect to the sql database

devins_project.py---------------
#__init__ file for Flask

functionsForInit.py----------------------
#files that are used for __init__ such as random html junk, accessing sql db, integer, strings, json junk

dingdong.py
#Contains a lot of useful functions, need to switch this/break it up into something more important

usedFunctions.py
#Similar to dingdong, contains a bunch of shit

questionFormatting.py-------------
#functions used to format questions, multiple choice etc

questionTemplate.py-------------------
#A Template of a given question for reference

test.py-----------------
#Used to test anything

updateToDatabase.py----------------------
#File used to update class questions from createdQuestions to the database (may eventually add a crontab to do this)

FOLDER: STATIC:
images, pdfs etc

FOLDER: TEMPLATES:

header.html------------------
#Contains all jquery (java) for the webapp to run

listOfQuestions.html---------------------
#Used to generate the list of questions on the main page, including snippets

main.html--------------------
#The main page that is loaded, listOfQuestions loads to this one???
***eventually will have more on this???

questionOptions.html--------------------
#Used to generate the different question options for each individual question

#STUFF FOR POLYGONS-----------------
labelPolygonCoordinates.py
polygon.py


DELETED:
info.py (seemed to contain nothing)
fuck.html (seemed like a header file?)


PATH - Goal: Create a worksheet

Go to cra.st/devin

devins_project.py
/devin 

runs header.html file that passes
title = any text
paragraph = any paragraph
cursor #used for the database
GETDISTINCTTOPICS #used to get topics from database
GETDISTINTSKILLS #used to get skills from database
{{This could be replaced with javascript}}
Header displays skills from topics (LEFT NAVBAR)
Header display DOCUMENT Name and options (RIGHT NAVBAR)
Header diplay #questionResults (Main content)

click VIEW(skill next to it)
header.html 
Javascript sends to /devin/displayQuestions in devins_project
	gets skill from clicked button
	connects to database using dbconnect.py
		gets questionIDS GETQUESTIONIDFORSKILL
		connects to db, selects, data, returns questionIDS
	renders questionOptions.html passing (questionIDS #questionIDs for each question
	, GETFILEPATHFORQUESTION #gets the file path for a question (defaults etc)
	, GETCLASSQUESTION #gets the class instance from createdQuestions
	, sorted #default func that will sort the keys of a dictionary
	, TYPEOFITEM #TYPEOFITEM that determine the type of item)
	returns template questionOptions.html to #questionResult in main.html
		Image SRC uses GETFILEPATHFORQUESTION (questionID, **kwargs)
			no kwargs available so, set to default
			file name = questionID + "default"
			remove special characters from filename
			creates file path in STATIC
			creates SNIPPET (nameOfDoc = 'default', questions = GETCLASSQUESTION(id))
				generates tex file snippeted
				{{may need to fix for large graphs???}}
			tex2pix changes tex to png
			remove tex file afterwards
			returns filepath back to questionOptions.html

		LOOPS through questionIDs
			creates an instance of the class GETCLASSQUESTION
			sorts default.kwargs (keys)
			LOOPS through keys
				if key is 'default', ignores
				check to see if value is bool (needs to check for bool first, so that it doesn't confuse bool with string)
				check to see if value is int
				check to see if first item in list is a string

				{{Would like to add notes about each kwarg, for instance, a header for the arg that displays, and a note for the kwarg if there is more information needed}}

Click SHOW OPTIONS
tab in questionOptions.html displays options as strings, bools, or ints

header.html
	if multiple checkboxes (list of strings) when item is clicked, it turns to bold via javascript

	anytime an option changes:
		dict = {}
		add id
		dict = {'id':currentID}
		LOOP through each option
			if it contains strings ie lists
			add only the bolded value to dict using the keyword {'name':'value'}

			if integer
			{'name':'value'}

			if boolean
			checked {'name':'True'}
			unchecked {'name':'False'}
		Flask to devins_project.py /generateSnippet passing in dict
		/generateSnippet
		gets kwargOptions from jquery
		sets givenID as dict['id'] and deletes it from the dict
		sets newKwargOptions by passing it through STRINGTOOTHERTYPES(DICT)
		STRINGTOOTHERTYPES keeps lists as list, changes 'true' and 'false' to boolean, changes ints and floats respectively
		gets a new filepath for class isntance GETFILEPATHFORQUESTION passing in (ID, **kwargs)
		return snippet to SRC for image under div in questionOption.html

	if true/false checkbox is clicked stores name as 'True' or 'False'

CLICK submit or hit enter with typed document name
header.html
STORING DOCUMENT NAME
in case a document name has not been set this java add the doc name to the beginning of a list with ['default']
devins_project /docNameProcess
sending doc name and return it do #docNameResult on header.html file

CLICK addQuestions
header.html ADDING CLASS NAME TO DOCUMENT LIST
start list of questions outside of function
start dict {}
dict['id'] for id where add button lies
LOOP through cards with this id
	if option is stringcheckboxes
	LOOP through children
		if checked, add {'name':['value1','value']} through second loop
		if integer, add {'name':'value'}
		if bool, add {'name':'value'} (if checked --> 'True')
Now we have dict = {all the kwargValues etc}
add the dict to listOfQuestionsDicts
ajax request to /devin/listOfDocQuestionsProcess
render listOfQuestions.html passing 
 (listOfQuestionDicts = listOfQuestionDicts
 (len and zip))
this creates the list of question on the right navbar

CLICK createDocument
if differnt doc options are checked, send their values of "true" or "false"
numberOfVersions stated etc
data = {dataKey:dataValue, listOfQuestionDicts as well}
ajax /devin/createDocumentApp
retreive data
set variables (docName, doc options, )
specify path of pdf
versionsDocs = [] **Used for the names of the documents, so I can loop through them later
versionsDocsAnswers = []
LOOP through number of versions
	q (questions) = []
	LOOP questionDict in listOfQuestionDicts:
		get ID
		change kwargs (json to python)
		add question to q (instance of class etc)
	docName =
	docNameAnswers =
	If more than 1 version, add version # to docName and docNameAnswers
	createWorksheet(questions, path, nameDoc, nameDocAnswers)
	append to versionsDocs and versionsDocAnswers
	now a whole bunch of shit for path name and shit (zipping files answer key attached, blah, blah)
set html download button (if zip file etc)
return it to result in header.html file

		

***********************************************
GOING THROUGH A QUESTION
***********************************************
see questionTemplate.py