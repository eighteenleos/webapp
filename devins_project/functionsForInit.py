from dbconnect import connection
import os
from createdQuestions import dictForDatabase, getQuestions
from documentCreation import worksheet, snippet, createDocument

def mergePDFs(output_path, input_paths):
	pdf_writer = PdfFileWriter()

	print(output_path)
	print(input_paths)
    
	for path in input_paths:
	    pdf_reader = PdfFileReader(path)
	    for page in range(pdf_reader.getNumPages()):
	        pdf_writer.addPage(pdf_reader.getPage(page))

	with open(output_path, 'wb') as fh:
	    pdf_writer.write(fh)

	#remove pdfs
	for original in input_paths:
		os.remove(original)

def zipFiles(outputPath, filesWithPath): 
	fantasy_zip = zipfile.ZipFile(outputPath + '.zip', 'w')
	 
	for file in filesWithPath:
		fantasy_zip.write(file, basename(file))

def representsInt(number):
    try: 
        int(number)
        return True
    except ValueError:
        return False

def representsFloat(number):
    try: 
        float(number)
        return True
    except ValueError:
        return False

def stringsToOtherTypes(dict):
	newDict = {}

	for key, value in dict.items():
		if key == 'id':
			kick = 'ass'
			newDict[key] = value
		elif type(value) == list:
			#Keep it this way
			newDict[key] = value
		
		elif value == 'True':
			newDict[key] = True
		
		elif value == 'False':
			newDict[key] = False

		elif representsInt(value) == True:
			newDict[key] = int(value)

		elif representsFloat(value) == True:
			newDict[key] = float(value)

	return newDict

#returns type of item as a string
def typeOfItem(item):
	print('typeOfItem')
	print('item is',item)
	if type(item) is str:
		print('string')
		return 'string'
	elif type(item) is int:
		print('int')
		return 'int'
	elif type(item) is bool:
		print('bool')
		return 'bool'

def stringToBool(string):
	if string == 'True':
		return True
	elif string == 'False':
		return False

def getDistinctSkills(cursor, topic):
	cursor, conn = connection()
	cursor.execute("""SELECT DISTINCT Skills FROM questionsSkills 
	INNER JOIN questionsTopics ON questionsSkills.ID = questionsTopics.ID WHERE questionsTopics.Topics = %s""", [topic])
	return [x[0] for x in cursor.fetchall()]

def getDistinctTopics(cursor):
	cursor, conn = connection()
	cursor.execute("""SELECT DISTINCT Topics FROM questionsTopics""")
	return [x[0] for x in cursor.fetchall()]

def getClassQuestion(id, **kwargs):
	print('getClassQuestion!!!!!!!!!!!!!!!!!!!')
	print(id)
	dictionaryStuff = getQuestions(id, **kwargs)
	return dictionaryStuff[id]

def getQuestionIDForSkill(cursor, skill):
	cursor, conn = connection()
	#connect to database

	#attempting to get id
	cursor.execute("""SELECT DISTINCT ID FROM questionsSkills WHERE questionsSkills.Skills = %s""",[skill])
	
	ids = [x[0] for x in cursor.fetchall()]
	
	return ids

def getOptionsForQuestion(questionID):
	cursor, conn = connection()
	cursor.execute("""SELECT DISTINCT Options FROM questionsOptions WHERE questionsOptions.ID = %s""", [questionID])
	options = [x[0] for x in cursor.fetchall()]
	print(options)
	for thing in options:
		print("THIS IS ONE OF THE OPTIONS::::::::::::::::::::", thing)
	return options

def getFilePathForQuestion(questionID, **kwargs):
	if not kwargs:
		fileName = questionID + '-' + 'Default'
		kwargs = {'default':True}
	else:
		junk = ""
		for key, item in zip(kwargs.keys(), kwargs.values()):
			junk += (str(key))
			junk += (str(item))
		fileName = questionID + '-' + junk
	#removing certain characters
	fileName = fileName.replace(' ', '')
	fileName = fileName.replace(',','')
	fileName = fileName.replace(']','')
	fileName = fileName.replace('[','')
	fileName = fileName.replace("'", '')

	#creates the file in the correct location
	filePath = '/home/devin/devins_project/static/images/' + fileName
	if os.path.isfile(filePath + '.png') == False:
		print('beforeSnippet')
		print(questionID)
		snippet(nameOfDoc = filePath, questions = [getClassQuestion(questionID, **kwargs)])

		f = open(filePath + '.tex')
		r = tex2pix.Renderer(f)
		r.mkpng(filePath + '.png')
		print('dick2')
		os.remove(filePath + '.tex')

	print('444444444444444444444444444444444444444444')
	filePath = filePath + '.png'

	#links the file to the symbioc link nick created etc
	filePath = 'http://cra.st/devin/static/images/' + fileName + '.png'

	#remove any commas or spaces from the filename
	print('555555555555555555555555555555555555555555')
	return filePath