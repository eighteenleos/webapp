from flask import Flask, render_template, url_for, flash, jsonify, request, send_file
from dbconnect import connection
import gc
import json
from documentCreation import createDocument, createWorksheet, snippet
import random
import createdQuestions
import os.path
import tex2pix 
from PyPDF2 import PdfFileWriter, PdfFileReader
import os
import zipfile
from os.path import basename
from createdQuestions import getQuestions, dictForDatabase
import time



app = Flask(__name__)

cursor, conn = connection()


def mergePDFs(output_path, input_paths):
	pdf_writer = PdfFileWriter()

	print(output_path)
	print(input_paths)
    
	for path in input_paths:
	    pdf_reader = PdfFileReader(path)
	    for page in range(pdf_reader.getNumPages()):
	        pdf_writer.addPage(pdf_reader.getPage(page))

	with open(output_path, 'wb') as fh:
	    pdf_writer.write(fh)

	#remove pdfs
	for original in input_paths:
		os.remove(original)

def zipFiles(outputPath, filesWithPath): 
	fantasy_zip = zipfile.ZipFile(outputPath + '.zip', 'w')
	 
	for file in filesWithPath:
		fantasy_zip.write(file, basename(file))

def representsInt(number):
    try: 
        int(number)
        return True
    except ValueError:
        return False

def representsFloat(number):
    try: 
        float(number)
        return True
    except ValueError:
        return False

def stringsToOtherTypes(dict):
	newDict = {}

	for key, value in dict.items():
		if key == 'id':
			kick = 'ass'
			newDict[key] = value
		elif type(value) == list:
			#Keep it this way
			newDict[key] = value
		
		elif value == 'True':
			newDict[key] = True
		
		elif value == 'False':
			newDict[key] = False

		elif representsInt(value) == True:
			newDict[key] = int(value)

		elif representsFloat(value) == True:
			newDict[key] = float(value)

	return newDict

#returns type of item as a string
def typeOfItem(item):
	print('typeOfItem')
	print('item is',item)
	if type(item) is str:
		print('string')
		return 'string'
	elif type(item) is int:
		print('int')
		return 'int'
	elif type(item) is bool:
		print('bool')
		return 'bool'

def stringToBool(string):
	if string == 'True':
		return True
	elif string == 'False':
		return False

@app.route('/devin/displaySubjects', methods = ['GET', 'POST'])
def displaySubjects():
	try:
		cursor, conn = connection()
		cursor.execute("""SELECT DISTINCT Subjects FROM questionsSubjects """)
		#connect to database
		#should be compatible for multiple questionIDs
		#questionIDs = getQuestionIDForSkill(cursor = cursor, skill = skill)
		subjects = [x[0] for x in cursor.fetchall()]
		htmlCode = render_template('subjects.html', subjects = subjects)
		print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

		return jsonify(result = htmlCode)
		
	except Exception as e:
		return str(e)

@app.route('/devin/displayTopics', methods = ['GET', 'POST'])
def displayTopics():
	try:
		subject = request.args['subject']
		print(subject)
		cursor, conn = connection()
		cursor.execute("""SELECT DISTINCT Topics FROM questionsTopics INNER JOIN questionsSubjects ON questionsTopics.ID = questionsSubjects.ID WHERE questionsSubjects.Subjects = %s""", [subject])
		#connect to database
		#should be compatible for multiple questionIDs
		#questionIDs = getQuestionIDForSkill(cursor = cursor, skill = skill)
		topics = [x[0] for x in cursor.fetchall()]
		htmlCode = render_template('topics.html', topics = topics)
		print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

		return jsonify(result = htmlCode)
		
	except Exception as e:
		return str(e)

@app.route('/devin/displaySkills', methods = ['GET', 'POST'])
def displaySkills():
	try:
		topic = request.args['topic']
		print(topic)
		cursor, conn = connection()
		cursor.execute("""SELECT DISTINCT Skills FROM questionsSkills INNER JOIN questionsTopics ON questionsSkills.ID = questionsTopics.ID WHERE questionsTopics.Topics = %s""", [topic])
		#connect to database
		#should be compatible for multiple questionIDs
		#questionIDs = getQuestionIDForSkill(cursor = cursor, skill = skill)
		skills = [x[0] for x in cursor.fetchall()]
		htmlCode = render_template('skills.html', skills = skills)
		print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

		return jsonify(result = htmlCode)
		
	except Exception as e:
		return str(e)

def getDistinctSkills(cursor, topic):
	cursor, conn = connection()
	cursor.execute("""SELECT DISTINCT Skills FROM questionsSkills 
	INNER JOIN questionsTopics ON questionsSkills.ID = questionsTopics.ID WHERE questionsTopics.Topics = %s""", [topic])
	return [x[0] for x in cursor.fetchall()]
def getDistinctTopics(cursor):
	cursor, conn = connection()
	cursor.execute("""SELECT DISTINCT Topics FROM questionsTopics""")
	return [x[0] for x in cursor.fetchall()]
def getClassQuestion(id, **kwargs):
	print('getClassQuestion!!!!!!!!!!!!!!!!!!!')
	print(id)
	dictionaryStuff = getQuestions(id, **kwargs)
	return dictionaryStuff[id]
def getQuestionIDForSkill(cursor, skill):
	cursor, conn = connection()
	#connect to database

	#attempting to get id
	cursor.execute("""SELECT DISTINCT ID FROM questionsSkills WHERE questionsSkills.Skills = %s""",[skill])
	
	ids = [x[0] for x in cursor.fetchall()]
	
	return ids
def getOptionsForQuestion(questionID):
	cursor, conn = connection()
	cursor.execute("""SELECT DISTINCT Options FROM questionsOptions WHERE questionsOptions.ID = %s""", [questionID])
	options = [x[0] for x in cursor.fetchall()]
	print(options)
	for thing in options:
		print("THIS IS ONE OF THE OPTIONS::::::::::::::::::::", thing)
	return options
def getFilePathForQuestion(questionID, **kwargs):
	if not kwargs:
		fileName = questionID + '-' + 'Default'
		kwargs = {'default':True}
	else:
		junk = ""
		for key, item in zip(kwargs.keys(), kwargs.values()):
			junk += (str(key))
			junk += (str(item))
		fileName = questionID + '-' + junk
	#removing certain characters
	fileName = fileName.replace(' ', '')
	fileName = fileName.replace(',','')
	fileName = fileName.replace(']','')
	fileName = fileName.replace('[','')
	fileName = fileName.replace("'", '')

	#creates the file in the correct location
	filePath = '/home/devin/devins_project/static/images/' + fileName
	if os.path.isfile(filePath + '.png') == False:
		print('beforeSnippet')
		print(questionID)
		snippet(nameOfDoc = filePath, questions = [getClassQuestion(questionID, **kwargs)])

		f = open(filePath + '.tex')
		r = tex2pix.Renderer(f)
		r.mkpng(filePath + '.png')
		print('dick2')
		os.remove(filePath + '.tex')

	print('444444444444444444444444444444444444444444')
	filePath = filePath + '.png'

	#links the file to the symbioc link nick created etc
	filePath = 'http://cra.st/devin/static/images/' + fileName + '.png'

	#remove any commas or spaces from the filename
	print('555555555555555555555555555555555555555555')
	return filePath






@app.route('/devin', methods=["GET","POST"])
def devin():
	title = 'This is the title of the page'
	paragraph = ['This is some paragraph']

	#test to see if i can produce a pic, change options, then it will change etc
	#first work on list of pictures
	#checkoff options?? - change picture???

	return render_template("header.html", cursor=cursor, getDistinctTopics=getDistinctTopics, getDistinctSkills=getDistinctSkills)

@app.route('/devin/createDocument')
def createDocument():
	try:		
		htmlCode = render_template('documentOptions.html')
		return jsonify(result = htmlCode)
		
	except Exception as e:
		return str(e)


@app.route('/devin/displayQuestions')
def displayQuestions():
	try:
		skill = request.args.get('skill', type = str)
		#gets skill from clicked on button, eventually may send topic or any other criteria

		cursor, conn = connection()
		#connect to database
		print('displayQuestions: made it past connection')
		#should be compatible for multiple questionIDs
		questionIDs = getQuestionIDForSkill(cursor = cursor, skill = skill)
		
		htmlCode = render_template('questionOptions.html', questionIDs = questionIDs, getFilePathForQuestion = getFilePathForQuestion, getClassQuestion = getClassQuestion, sorted = sorted, typeOfItem = typeOfItem)
		print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

		return jsonify(result = htmlCode)
		
	except Exception as e:
		return str(e)

@app.route('/devin/generateSnippet', methods = ["POST"])
def generateSnippet():
	try:
		kwargOptions = request.get_json()
		print('kwargOptions RAW', kwargOptions)
		givenID = kwargOptions['id']
		del kwargOptions['id']

		newKwargOptions = stringsToOtherTypes(kwargOptions)

		print('kwargoptions are these :::::::::::::::', newKwargOptions)

		newFilePath = getFilePathForQuestion(givenID, **newKwargOptions)


		return jsonify(result=newFilePath)

	except Exception as e:
		return str(e)

@app.route('/devin/docNameProcess', methods = ["POST"])
def docNameProcess():
	try:
		docNames = request.get_json()
		print(docNames)
		docName = docNames[0]
		return jsonify(result=docName)

	except Exception as e:
		return str(e)

@app.route('/devin/listOfDocQuestionsProcess', methods = ["POST"])
def listOfDocQuestionsProcess():
	try:
		listOfQuestionDicts = request.get_json()
		print(listOfQuestionDicts)

		htmlCode = render_template('listOfQuestions.html', listOfQuestionDicts = listOfQuestionDicts, len = len, zip = zip)

		return jsonify(result=htmlCode)

	except Exception as e:
		return str(e)

@app.route('/devin/createDocumentApp', methods = ["GET","POST"])
def createDocumentApp():
	try:
		startTime = time.time()
		shit = request.get_json()

		docName = shit['docName']
		answersAttached = stringToBool(shit['combinedAnswerKeyPDF'])
		listOfQuestionDicts = shit['listOfQuestionDicts']
		numberOfVersions = int(shit['numberOfVersions'])
		versionsCombined = stringToBool(shit['versionsCombined'])

		#only when answerAttached == True and versionsCombined == True and numberOfVersions > 1
		collatedVersions = stringToBool(shit['collatedVersionsAnswers'])

		downloadHtml = ""
		versionsDocs = []
		versionsDocsAnswers = []

		originalDocName = docName

		path = '/home/devin/devins_project/static/'

		#def createVersions
		for version in range(1, numberOfVersions+1):
			print(time.time() - startTime, version)
			q = []
			
			for questionDict in listOfQuestionDicts:
				idForQuestionDict = questionDict['id']
				
				temp = questionDict.copy()
				del temp['id']
				newKwargOptions = stringsToOtherTypes(temp) 

		   		#get the dictionary of questions and classes, selects the class with the given id
				q.append(createdQuestions.getQuestions(idForQuestionDict, **newKwargOptions)[idForQuestionDict])

			docName = originalDocName
			docNameAnswers = docName + "_Answers"

			if numberOfVersions > 1:
				docName += "_Version#%d" % version
				docNameAnswers += "_Version#%d" % version

			#check function for document options farther along
			#tempDoc = createDocument(nameOfDoc = docName)
			#tempDocAnswers = createDocument(nameOfDoc = docNameAnswers)

			#I believe this will make two files and options for doc would get passed through this
			createWorksheet(questions = q, path = path, nameOfDoc = docName, nameOfDocAnswers = docNameAnswers, doc = True, docAnswer = True)

			
			#put them all in a list, so I can iterate through the documents I've created
			versionsDocs.append(docName)
			versionsDocsAnswers.append(docNameAnswers)
		
		print(versionsDocs, versionsDocsAnswers)

		#createDocWithDocOptions
		if numberOfVersions == 1:
			if answersAttached == True:
				#merge answerSheet with doc and make new name (not version#1), create single file
				inputPaths = []
				inputPaths.append(path + originalDocName + ".pdf")
				inputPaths.append(path + originalDocName + "_Answers" + ".pdf")

				mergePDFs(path + originalDocName + "_AnswersAttached" + '.pdf', inputPaths)
				docDownload = path + docName + "_AnswersAttached" + '.pdf'

			elif answersAttached == False:
				#not merge, two buttons, zip files together
				zipFiles(path + originalDocName, [path + originalDocName + ".pdf", path + originalDocName + "_Answers" + ".pdf"])
				docDownload = path + docName + ".zip" 

		else:
			if versionsCombined == True:
				if answersAttached == True:
					if collatedVersions == True: #possibly make this checkbox not appear unless the other two are checked
						#merge doc with docAnswer collatedetly, zip loop
						inputPaths = []
						for d, a in zip(versionsDocs, versionsDocsAnswers):
							inputPaths.append(path + d + ".pdf")
							inputPaths.append(path + a + ".pdf")
						#now merge and should be only one file
						docDownload = path + originalDocName + "_AllVersions_AnswersAttached_Collated" + ".pdf"
						mergePDFs(docDownload, inputPaths)


					else:
						#merge doc with Answers, all docs, then all docAnswers, two separate loops
						inputPaths = []
						for d in versionsDocs:
							inputPaths.append(path + d + ".pdf")
						for a in versionsDocsAnswers:
							inputPaths.append(path + a + ".pdf")
						#now merge and should be only one file
						docDownload = path + originalDocName + "_AllVersions_AnswersAttached_Uncollated" + ".pdf"
						mergePDFs(docDownload, inputPaths)
						
				else:
					#merge all docs, merge all answers, 2 buttons to be zipped
					inputPathsForDocs = []
					inputPathsForAnswers = []
					for d in versionsDocs:
						inputPathsForDocs.append(path + d + ".pdf")
					for a in versionsDocsAnswers:
						inputPathsForAnswers.append(path + a + ".pdf")
					docDownload1 = path + originalDocName + "_AllVersions"
					docDownload2 = path + originalDocName + "_Answers_AllVersions"
					mergePDFs(docDownload1 + ".pdf", inputPathsForDocs)
					mergePDFs(docDownload2 + ".pdf", inputPathsForAnswers)
					zipFiles(path + originalDocName, [docDownload1 + ".pdf", docDownload2 + ".pdf"])
					docDownload = path + originalDocName + ".zip"


			else:
				if answersAttached == True:
					#so collated doesn't matter
					#zip loop, merge each doc with answer sheet
					#one button for each document, why would anyone use this??
					#merge each doc with answersheet, and make as many buttons
					mergedDocs = []
					for d, a in zip(versionsDocs, versionsDocsAnswers):
						mergePDFs(path + d + "_AnswersAttached" + ".pdf", [path + d + ".pdf", path + a + ".pdf"])
						mergedDocs.append(path + d + "_AnswersAttached" + ".pdf")
					zipFiles(path + originalDocName, mergedDocs)
					docDownload = path + originalDocName + ".zip"
				else:
					#zip all the files
					zipReadyDocs = []
					zipReadyAnswers = []
					for d in versionsDocs:
						zipReadyDocs.append(path + d + ".pdf")
					for a in versionsDocsAnswers:
						zipReadyAnswers.append(path + a + ".pdf")
					zipFiles(path + originalDocName, zipReadyDocs + zipReadyAnswers)
					docDownload = path + originalDocName + ".zip" 
		full_path = docDownload
		newPath = os.path.relpath(full_path, '/home/devin/devins_project/static/')
		
		downloadHtml = "<h5><a href = '/devin/returnDocument/%s' target = '_blank'>DOWNLOAD</a></h5>" % (newPath)

		return jsonify(result=downloadHtml)
		
	except Exception as e:
		return str("Exception is" + e)

@app.route('/devin/returnDocument/<docName>', methods=["GET","POST"])
def returnDocument(docName):
	try:
		return send_file('/home/devin/devins_project/static/' + docName, attachment_filename=docName)
	except Exception as e:
		return(e)

if __name__ == "__main__":
	app.run(host='0.0.0.0')

