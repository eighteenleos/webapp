from pylatex.base_classes import Environment, CommandBase, Arguments
from pylatex.package import Package
from pylatex import Document, Section, UnsafeCommand, TikZ, Command, Figure, VerticalSpace, NewPage, NewLine, SubFigure, HorizontalSpace, Center, Package
from pylatex import Document, PageStyle, Head, MiniPage, Foot, LargeText, MediumText, LineBreak, simple_page_number
import math
from pylatex.utils import NoEscape, escape_latex
import time
import random
from fractions import Fraction
from labelPolygonCoordinates import returnLabelCoordinates
from polygon import Shape, Point, MakeRightTriangle, CalcAndReturnLabelCoordsAsLists, ReturnPointCoordsAsLists

#Ideally all this shit would be in a different document, but let's not change too much for the time being
startTime = time.time()
def constantOperation(constant, notInBetween):
    if notInBetween == True:
        if constant > 0:
            string = '%d' % constant
        elif constant < 0:
            string = '-%d' % abs(constant)
        elif constant == 0:
            string = '0'
    else:
        if constant > 0:
            string = '+%d' % constant
        elif constant < 0:
            string = '-%d' % abs(constant)
        elif constant == 0:
            string = ''
    return string


#class partition(): #eventually kwargs with a= b=
    def __init__(self, kind, solution, directedKey):
        maxx = 10
        self.directedKey = directedKey
        self.solution = solution

        a = random.randint(1,10) #seems good
        b = random.randint(1,10)# don't care if a/b is 2/4 vs 1/2 etc fuck it

        multiples = list(range(a+b, maxx * 2 + 1, a + b))  # could make higher than 21 but this will fit on a 10 by 10 graph for now

        if solution == 'integer':
            distChoices = multiples
        elif solution == 'fraction':
            numbers = list(range(0, maxx * 2 + 1))
            for item in multiples:
                numbers.remove(item)
            distChoices = numbers

        x1 = -10
        y1 = -10

        xDist = random.choice(distChoices)
        yDist = random.choice(distChoices)

        x2 = x1 + xDist
        y2 = y1 + yDist
        if kind == 'horizontal':
            y2 = y1
        elif kind == 'vertical':
            x2 = x1

        #Move line araound 10 by 10 grid
        xChange = random.randint(0, maxx - x2)
        yChange = random.randint(0, maxx - y2)

        x1 = x1 + xChange
        x2 = x2 + xChange
        y1 = y1 + yChange
        y2 = y2 + yChange

        self.xP = (x1*b + x2*a)/(a+b)
        self.yP = (y1*b + y2*a)/(a+b)

        self.a = a
        self.b = b

        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2

        one, two, three = annotatePolygon('no',3)
        self.A = one[0]
        self.B = one[1]
        self.C = one[2]

    def addQuestion(self):
        if self.directedKey == 'directed':
            doc.append(NoEscape(r'Given the directed line segment $\overline{%s%s}$ ' % (self.A, self.C) +
                                r'with $%s(%d, %d)$ and $%s(%d, %d)$, ' % (self.A, self.x1, self.y1, self.C, self.x2, self.y2) +
                                r'determine point $%s$ that %s the segment in the ratio %s.' % (self.B, random.choice(['partitions','divides']),
                                random.choice(['%d to %d' % (self.a, self.b), '%d:%d' % (self.a,                      self.b)]))))
        elif self.directedKey == 'key':
            doc.append(NoEscape(r'Given the line segment $\overline{%s%s}$ ' % (self.A, self.C) +
                                r'with $%s(%d, %d)$ and $%s(%d, %d)$, ' % (
                                self.A, self.x1, self.y1, self.C, self.x2, self.y2) +
                                r'determine point $%s$ such that %s%s:%s%s is %s.' % (
                                self.B, self.A, self.B, self.B, self.C,
                                random.choice(['%d to %d' % (self.a, self.b), '%d:%d' % (self.a, self.b)]))))
        doc.append(NewLine())
        doc.append(VerticalSpace(r'1in'))
    def addAnswer(self):
        if self.solution == 'fraction':
            num = Fraction(self.xP).limit_denominator(100)
            xPnumer = num.numerator
            xPdenom = num.denominator

            num2 = Fraction(self.yP).limit_denominator(100)
            yPnumer = num2.numerator
            yPdenom = num2.denominator

            docAnswer.append(NoEscape(r'$%s(\frac{%d}{%d}, \frac{%d}{%d})$' % (self.B, xPnumer, xPdenom, yPnumer, yPdenom)))
            docAnswer.append(VerticalSpace(r'.25in'))
        else:
            docAnswer.append(NoEscape(r'$%s(%d, %d)$' % (self.B, self.xP, self.yP)))
            docAnswer.append(NewLine())
class givensProof():
    def __init__(self, type):
        self.type = type #sas, sss, aas, asa, hl
        self.x1, self.y1, self.x2, self.y2, blah, blah2 = translation(-10, 10, 'random', 'random', 'no')
        self.anno1, self.anno2, shit = annotatePolygon('no',3)

        self.A = self.anno1[0]
        self.B = self.anno1[1]
        self.C = self.anno1[2]

        self.AB = self.anno1[0] + self.anno1[1]
        self.BC = self.anno1[1] + self.anno1[2]
        self.CA = self.anno1[2] + self.anno1[0]

        self.D = self.anno2[0]
        self.E = self.anno2[1]
        self.F = self.anno2[2]

        self.DE = self.anno2[0] + self.anno2[1]
        self.EF = self.anno2[1] + self.anno2[2]
        self.FD = self.anno2[2] + self.anno2[0]

        if self.type == 'sas':
            self.info1 = r'$\angle{%s} \cong \ angle{%s}$' % (self.A + self.B + self.C, self.D + self.E + self.F)
            self.info2 = r'$\overline{%s} \cong \overline{%s}$' % (self.AB, self.DE)
            self.info3 = r'$\overline{%s} \cong \overline{%s}$' % (self.BC, self.EF)
            self.answer = 'SAS'
        elif self.type == 'sss':
            self.info1 = r'$\overline{%s} \cong \overline{%s}$' % (self.CA, self.FD)
            self.info2 = r'$\overline{%s} \cong \overline{%s}$' % (self.AB, self.DE)
            self.info3 = r'$\overline{%s} \cong \overline{%s}$' % (self.BC, self.EF)
            self.answer = 'SSS'
        elif self.type == 'asa':
            self.info1 = r'$\angle{%s} \cong \angle{%s}$' % (self.A + self.B + self.C, self.D + self.E + self.F)
            self.info2 = r'$\overline{%s} \cong \overline{%s}$' % (self.AB, self.DE)
            self.info3 = r'$\angle{%s} \cong \angle{%s}$' % (self.C + self.A + self.B, self.F + self.D + self.E)
            self.answer = 'ASA'
        elif self.type == 'aas':
            self.info1 = r'$\angle{%s} \cong \angle{%s}$' % (self.A + self.B + self.C, self.D + self.E + self.F)
            self.info2 = r'$\overline{%s} \cong \overline{%s}$' % (self.BC, self.EF)
            self.info3 = r'$\angle{%s} \cong \angle{%s}$' % (self.C + self.A + self.B, self.F + self.D + self.E)
            self.answer = 'AAS'
        #fuck HL for now

    def addQuestion(self):
        doc.append(NoEscape(r'Given the diagram below of $\bigtriangleup %s$ and $\bigtriangleup %s $, ' % (self.A + self.B + self.C, self.D + self.E + self.F) +
        self.info1 + ', ' + self.info2 + ',and ' + self.info3 + r'. Prove that $\bigtriangleup %s \cong \bigtriangleup %s$.' % (self.A + self.B + self.C, self.D + self.E + self.F)))

        length = sizingXY(-10, 10, 'large')

        with doc.create(Center()):
            with doc.create(TikZ(
                    options='auto, x=%gcm, y=%gcm' % (length, length))):  # can easily switch it for rectangles in the future

                graphPolygon(doc, self.x1, self.y1, [self.A, self.B, self.C], 'black')
                graphPolygon(doc, self.x2, self.y2, [self.D, self.E, self.F], 'black')

    def addAnswer(self):
        doc.append(NoEscape(r'%s' % self.answer))
        doc.append(NewLine())
class geometricMean():
    def __init__(self, roundPlace, type, secondStep):
        print(type)
        self.poly1, self.poly2, self.poly3 = annotatePolygon('no', 3)
        # see geometricMean in picsForCode
        # poly1 = ABC (left angle, 90 angle, right angle)
        # poly2 = ABD (A goes with A, 90 angle, leftover angle)
        # poly3 = BCD (going backwards::C goes with C, 90 degree angle, leftover)

        if roundPlace == 'random':
            self.round = random.randint(0, 3)
        else:
            self.round = roundPlace

        self.poly2[0] = self.poly1[0]  # A shared with poly1
        self.poly2[1] = self.poly2[1]  # D right angle
        self.poly2[2] = self.poly1[1]  # B other angle

        self.poly3[0] = self.poly1[1]  # B other angle
        self.poly3[1] = self.poly2[1]  # D right angle
        self.poly3[2] = self.poly1[2]  # C shared with poly1

        minn = 5
        maxx = 20

        self.baseAD = round(random.uniform(minn, maxx), self.round)
        self.baseCD = round(random.uniform(minn, maxx), self.round)
        self.heightBD = round((self.baseAD * self.baseCD) ** (1 / 2), self.round)
        self.hypBC = round((self.baseCD ** 2 + self.heightBD ** 2) ** (1 / 2), self.round)
        self.hypAB = round((self.baseAD ** 2 + self.heightBD ** 2) ** (1 / 2), self.round)
        self.hypAC = round(self.baseAD + self.baseCD, self.round)

        self.sides = {"AD": [self.poly2[0] + self.poly2[1], self.baseAD],
                      "CD": [self.poly3[1] + self.poly3[2], self.baseCD],
                      "BD": [self.poly2[1] + self.poly2[2], self.heightBD],
                      "BC": [self.poly3[0] + self.poly3[2], self.hypBC],
                      "AB": [self.poly2[0] + self.poly2[2], self.hypAB],
                      "AC": [self.poly1[0] + self.poly1[2], self.hypAC]}

        self.x1 = [0, self.baseAD, self.hypAC, self.baseAD, self.baseAD, self.baseAD]  # drawing outer triangle first ABC
        self.y1 = [0, self.heightBD, 0, 0, self.heightBD, 0]

        self.anno1 = [self.poly2[0], self.poly2[2], self.poly3[2], self.poly2[1], '', '']  # ABC

        self.info = r'In the diagram below, $\bigtriangleup %s$ is drawn with $\angle{%s}$ equal to $90^{\circ}$, and altitude $\overline{%s}$. ' % (self.poly1[0] + self.poly1[1] + self.poly1[2], self.poly1[1], self.sides['BD'][0]  )
        if type == 'legLegAltitude':
            self.question = r'Given that $%s = %g$ and $%s = %g$, find $%s$.' % (self.sides['AD'][0], self.sides['AD'][1], self.sides['CD'][0], self.sides['CD'][1], self.sides['BD'][0])  # given two legs, find altitude

            self.answer = r'$%s = %g$' % (self.sides['BD'][0], self.sides['BD'][1])

            if secondStep == 'yes':
                sideToFind = random.choice(['AB', 'BC'])
                self.question = r'Given that $%s = %g$ and $%s = %g$, find $%s$.' % (self.sides['AD'][0], self.sides['AD'][1], self.sides['CD'][0], self.sides['CD'][1], self.sides[sideToFind][0])
                self.answer = r'$%s = %d$' % (self.sides[sideToFind][0], self.sides[sideToFind][1])

            roundPlaces = ['whole number', 'tenth', 'hundreth', 'thousandth']

            self.question += r' Round your answer to the nearest %s.' % (roundPlaces[self.round])
        elif type == 'hypotenuseLegAltitude':
            legGiven = random.choice(['AD', 'CD'])
            # no need to have legFind because we're finding the altitude

            self.question = r'Given that $%s = %g$ and $%s = %g$, determine the length of $%s$. ' % (
            self.sides['AC'][0], self.sides['AC'][1], self.sides[legGiven][0], self.sides[legGiven][1],
            self.sides['BD'][0])

            self.answer = r'$%s = %g$' % (self.sides['BD'][0], self.sides['BD'][1])

            if secondStep == 'yes':
                sideToFind = random.choice(['AB', 'BC'])
                self.question += r'Given that $%s = %g$ and $%s = %g$, determine the length of $%s$. ' % (
                self.sides['AC'][0], self.sides['AC'][1], self.sides[legGiven][0], self.sides[legGiven][1],
                self.sides[sideToFind][0])

                self.answer = r'$%s = %d$' % (self.sides[sideToFind][0], self.sides[sideToFind][1])

            roundPlaces = ['whole number', 'tenth', 'hundreth', 'thousandth']

            self.question += r'Round your answer to the nearest %s.' % (roundPlaces[self.round])
        elif type == 'legAltitudeLeg':
            legs = ['AD', 'CD']
            legToFind = random.choice(legs)
            legs.remove(legToFind)
            legGiven = random.choice(legs)

            self.question = r'Given that $%s = %g$ and $%s = %g$, determine the length of $%s$. ' % (
            self.sides[legGiven][0], self.sides[legGiven][1], self.sides['BD'][0], self.sides['BD'][1],
            self.sides[legToFind][0])

            self.answer = r'$%s = %g$' % (self.sides[legToFind][0], self.sides[legToFind][1])

            if secondStep == 'yes':
                if legToFind == 'AD':
                    self.question = r'Given that $%s = %g$ and $%s = %g$, determine the length of $%s$. ' % (
                    self.sides[legGiven][0], self.sides[legGiven][1], self.sides['BD'][0], self.sides['BD'][1],
                    self.sides['AB'][0])

                    self.answer = r'%s = %d' % (self.sides['AB'][0], self.sides['AB'][1])
                if legToFind == 'CD':
                    self.question += r'Given that $%s = %g$ and $%s = %g$, determine the length of $%s$. ' % (

                    self.sides[legGiven][0], self.sides[legGiven][1], self.sides['BD'][0], self.sides['BD'][1],
                    self.sides['BC'][0])

                    self.answer = r'$%s = %d$' % (self.sides['BC'][0], self.sides['BC'][1])

            roundPlaces = ['whole number', 'tenth', 'hundreth', 'thousandth']

            self.question += r'Round your answer to the nearest %s.' % (roundPlaces[self.round])

    def addQuestion(self):
        doc.append(NoEscape(self.info + self.question))
        doc.append(NewLine())
        length = sizingXY(-20, 20, 'medium')

        with doc.create(Center()):
            with doc.create(TikZ(
                    options='auto, x=%gcm, y=%gcm' % (length, length))):  # can easily switch it for rectangles in the future

                graphPolygon(doc, self.x1, self.y1, self.anno1, 'black')

    def addAnswer(self):
        docAnswer.append(NoEscape(r'%s' % self.answer))
        docAnswer.append(NewLine())
class sideSpinner():
    def __init__(self, roundPlace):
        poly1, poly2, poly3 = annotatePolygon('no', 3)
        # poly1 = ABC big triangle
        # poly2 = AED little triangle (going ccw)

        poly2[0] = poly1[0]  # A shared with poly1

        if roundPlace == 'random':
            self.round = random.randint(0, 3)
        else:
            self.round = roundPlace

        minn = 1
        maxx = 10
        sf = round(random.uniform(2, 4), self.round)
        # for now using only whole numbers for sides
        baseDE = round(random.uniform(minn, maxx), 0)
        sideAD = round(random.uniform(minn, maxx), 0)
        sideAE = round(random.uniform(sideAD, sideAD * sf - 1), 0)

        baseBC = round(baseDE * sf, 0)
        sideAB = round(sideAD * sf, 0)
        sideAC = round(sideAE * sf, 0)

        sideBE = sideAB - sideAE  # should be rounded correectly
        sideCD = sideAC - sideAD

        sides = {"DE": [poly2[1] + poly2[2], baseDE],
                 "AD": [poly2[0] + poly2[1], sideAD],
                 "AE": [poly2[2] + poly2[0], sideAE],
                 "BC": [poly1[1] + poly1[2], baseBC],
                 "AB": [poly1[0] + poly1[1], sideAB],
                 "AC": [poly1[2] + poly1[0], sideAC],
                 "BE": [poly2[2] + poly1[1], sideBE],
                 "CD": [poly1[2] + poly2[1], sideCD]}

        self.x, self.y = polygon(-30, 30, 'right triangle', 'regular')

        #dilate the triangle through one of the vertices
        vertexChosen = random.randint(0,2)
        print('regular is', self.x, vertexChosen)
        self.dilX = [(item + -1 * self.x[vertexChosen]) * 1/sf + self.x[vertexChosen] for item in self.x]
        self.dilY = [(item + -1 * self.y[vertexChosen]) * 1/sf + self.y[vertexChosen] for item in self.y]

        # now i need to reflect over AB and then rotate up to go back onto the big triangle
        # text.centerX, test.centerY - find the center point, which is the shared angle and go one index lower, rule out exception where center is starting and ending #location for triangle
        # test.cornerIndex


        # shared angle is A, aX is the x value of point A in the triangle, etc

        if vertexChosen == 0:
            aX = self.dilX[0]
            aY = self.dilY[0]
            dX = self.dilX[1]
            dY = self.dilY[1]
            eX = self.dilX[2]
            eY = self.dilY[2]

        if vertexChosen == 1:
            aX = self.dilX[1]
            aY = self.dilY[1]
            dX = self.dilX[2]
            dY = self.dilY[2]
            eX = self.dilX[0]
            eY = self.dilY[0]

        if vertexChosen == 2:
            aX = self.dilX[2]
            aY = self.dilY[2]
            dX = self.dilX[0]
            dY = self.dilY[0]
            eX = self.dilX[1]
            eY = self.dilY[1]


        # find the slope of the line
        # find the y-intercept that would go through aX, aY
        # so y = mx +b, so y-mx = b
        if dX - aX != 0:
            slope = (dY - aY) / (dX - aX)
            yintercept = aY - slope * aX
            m = slope
            b = yintercept

            self.refX = [
                (1 - m ** 2) / (m ** 2 + 1) * itemX + (2 * m) / (m ** 2 + 1) * itemY - (2 * m * b) / (m ** 2 + 1) for
                itemX, itemY in zip(self.dilX, self.dilY)]
            self.refY = [(2 * m) / (m ** 2 + 1) * itemX + (m ** 2 - 1) / (m ** 2 + 1) * itemY + (2 * b) / (m ** 2 + 1)
                         for itemX, itemY in zip(self.dilX, self.dilY)]

        else:  # x = type line
            self.refX = [2 * (aX) - itemX for itemX, itemY in zip(self.dilX, self.dilY)]
            self.refY = [itemY for itemX, itemY in zip(self.dilX, self.dilY)]

        # reflected over the line

        # now we need to rotate this the small triangle back onto the big triangle,

        # now i need to determine the number of degrees of the shared angle.
        sharedSide1 = ((aX - dX) ** 2 + (aY - dY) ** 2) ** (1 / 2)
        sharedSide2 = ((aX - eX) ** 2 + (aY - eY) ** 2) ** (1 / 2)
        nonSharedSide = ((dX - eX) ** 2 + (dY - eY) ** 2) ** (1 / 2)

        cosAngle = ((sharedSide2) ** 2 + (sharedSide1) ** 2 - (nonSharedSide) ** 2) / (2 * sharedSide1 * sharedSide2)

        angle = math.acos(cosAngle)
        angle = math.degrees(angle)

        self.rotX, self.rotY = rotateCoordinates(self.refX, self.refY, self.x[vertexChosen], self.y[vertexChosen], 'cc', angle)
        # find point in which text.xy and test.dxy are the same to determine which annotation is the one they shared
        # then i can go in order and it wont  matter

        self.bigAnno = []
        self.smallAnno = []
        if vertexChosen == 0:
            self.bigAnno.append(poly1[0])
            self.bigAnno.append(poly1[1])
            self.bigAnno.append(poly1[2])
            self.smallAnno.append('')
            self.smallAnno.append(poly2[1])
            self.smallAnno.append(poly2[2])
        if vertexChosen == 1:
            self.bigAnno.append(poly1[2])
            self.bigAnno.append(poly1[0])
            self.bigAnno.append(poly1[1])
            self.smallAnno.append(poly2[2])
            self.smallAnno.append('')
            self.smallAnno.append(poly2[1])
        if vertexChosen == 2:
            self.bigAnno.append(poly1[1])
            self.bigAnno.append(poly1[2])
            self.bigAnno.append(poly1[0])
            self.smallAnno.append(poly2[1])
            self.smallAnno.append(poly2[2])
            self.smallAnno.append('')

        left = {'sW': 'AE', 'bW': 'AB', 'p': 'BE'}
        right = {'sW': 'AD', 'bW': 'AC', 'p': 'CD'}
        middle = {'sW': 'DE', 'bW': 'BC'}

        infos = [right['sW'], left['sW'], left['p']]
        random.shuffle(infos)
        info1 = infos[0]
        info2 = infos[1]
        info3 = infos[2]
        info4 = right['p']

        chosen = random.randint(1, 4)
        if chosen == 1:  # could be other angle pair? acb, ade
            angleEx = r'$\angle{%s%s%s} \cong \angle{%s}$' % (poly2[0], poly2[1], poly2[2], poly1[1])
        elif chosen == 2:
            angleEx = r'$\angle{%s%s%s} \cong \angle{%s%s%s}$' % (poly2[0], poly2[1], poly2[2], poly1[0], poly1[1], poly1[2])
        elif chosen == 3:
            angleEx = r'$\angle{%s%s%s} \cong \angle{%s}$' % (poly2[0], poly2[2], poly2[1], poly1[2])
        elif chosen == 4:
            angleEx = r'$\angle{%s%s%s} \cong \angle{%s%s%s}$' % (poly2[0], poly2[2], poly2[1], poly1[0], poly1[2], poly1[1])

        roundPlaces = ['whole number', 'tenth', 'hundreth', 'thousandth']

        self.question = r'In the diagram below,' + angleEx + r', and %s=%g, %s=%g, and %s=%g. Determine the length of %s. ' % (sides[info1][0], sides[info1][1], sides[info2][0], sides[info2][1], sides[info3][0], sides[info3][1], sides[info4][0]) + r'Round your answer to the nearest %s.' % (roundPlaces[self.round])

        self.answer = r'$%s = %g$' % (sides[info4][0], sides[info4][1])


    def addQuestion(self):
        doc.append(NoEscape(self.question))

        length = sizingXY(min(self.x + self.y + self.rotX + self.rotY), max(self.x + self.y + self.rotX + self.rotY), 'medium')

        with doc.create(Center()):
            with doc.create(TikZ(
                    options='auto, x=%gcm, y=%gcm' % (length, length))):  # can easily switch it for rectangles in the future
                graphPolygon(doc, self.x, self.y, self.bigAnno, 'yellow')
                #graphPolygon(doc, self.dilX, self.dilY, self.bigAnno, 'red')
                #graphPolygon(doc, , self.y, self.bigAnno, 'green')
                #graphPolygon(doc, self.refX, self.refY, self.bigAnno, 'green')


                graphPolygon(doc, self.rotX, self.rotY, self.smallAnno, 'black')

    def addAnswer(self):
        docAnswer.append(NoEscape(self.answer))



###############################################################################



import random
from fractions import gcd
def coprimes(max=10):
    max = int(max/2)
    #taken from generating coprime pairs on wikipedia

    #can be odd-even or odd-odd
    type = random.choice(['even-odd','odd-odd'])

    if type == 'even-odd':
        even = random.randrange(2,max,2)
        odd = random.randrange(1,even,2)
        m = even
        n = odd
    elif type == 'odd-odd':
        odd1 = random.randrange(3,max,2)
        odd2 = random.randrange(1, odd1, 2)
        m = odd1
        n = odd2

    branch = random.choice([1,2,3])

    if branch == 1:
        a = 2*m - n
        b = m
    elif branch == 2:
        a = 2*m + n
        b = m
    elif branch == 3:
        a = m + 2*n
        b = n

    return a, b

#does not count 1 as a prime number, as it's not?
def getPrimes(max = 10):
    n = max
    numbers = set(range(n, 1, -1))
    primes = []
    while numbers:
        p = numbers.pop()
        primes.append(p)
        numbers.difference_update(set(range(p*2, n+1, p)))
    return primes

def getComposites(max = 10):
    primes = getPrimes(max = 10)
    composites = []
    for item in range(1,max+1):
        if item not in primes:
            composites.append(item)
    return composites
    
def coprimeWith(number, max=10):
    listOfChoices = list(range(1,max+1))
    newList = []
    for item in listOfChoices:
        if gcd(number,item) == 1:
            newList.append(item)
            
    coPrimeNum = random.choice(newList)
    return coPrimeNum
    
def factors(value, positiveValues = True):
    f1List = []
    f2List = []
    
    if positiveValues == False:
        valueRange = list(range(-1*abs(value),abs(value)+1)) #abs for negative values
        valueRange.remove(0) #
    else:
        valueRange = list(range(1,abs(value)+1))
        
    for item in valueRange:
        if (value/item).is_integer() == True: #only integer factors
            f1List.append(item)
            f2List.append(int(value/item))
    
    f1New = f1List #eliminates wrap around, multiple values
    f2New = f2List
    #this will return two lists with corresponding index values equaling to the value given
    
    return f1New, f2New #factor 1 list and factor 2 list

#eventually probably want to declare a, like a=2, a=3, a=4 etc

#coA, coB, coC
#coA - [1, 'prime', 'not prime', anything] prime means coA will be a prime number, not prime is like 4 (2,2 or 1,4)
#coB - ['positive', 'negative', or '0'(DOTS)]
#coC - ['positive', 'negative']
#GCF number version, variable later will be easier
def trinomialFactor(coA = 1, maxSizeForA = 10, coB = 'positive', coC = 'positive', gcf = False, rootMax = 10):
    #a(bx+c)(dx+e)
    if coA == 'random':
        coA = random.choice(['prime','not prime',1])
    
    elif coA == 'prime': #meaning we want 2, 3, 5, 7 etc
        b = 1
        primes = getPrimes(maxSizeForA)
        d = random.choice(primes)
        
        #c and e need to be any numbers but not multiples of b and d resepcively 
        #make c and e positive, code below will take care of signs
        c = coprimeWith(b, max = rootMax)
        e = coprimeWith(d, max = rootMax)
        while b*e + c*d == 0:
            print('B is 0!!!')
            c = coprimeWith(b, max = rootMax) 
            e = coprimeWith(d, max = rootMax)
        
    elif coA == 'not prime': #meaning we could have 4, 6, etc
        #we are going to make A first, then find factors of it and assign them to b and d respectively
        bTimesD = random.choice(getComposites(max = maxSizeForA))
        while bTimesD == 1:
            print('1!!!!')
            bTimesD = random.choice(getComposites(max = maxSizeForA))
        bTimesDFactors1, bTimesDFactors2 = factors(bTimesD)
        b = random.choice(bTimesDFactors1)
        d = bTimesDFactors2[bTimesDFactors1.index(b)]
        
        #make c and e each respectively coprime with b and d such that there are no gcf's
        c = coprimeWith(b, max = rootMax) 
        e = coprimeWith(d, max = rootMax)
        while b*e + c*d == 0:
            print('B is 0!!!',b,c,d,e)
            c = coprimeWith(b, max = rootMax) 
            e = coprimeWith(d, max = rootMax)
    
    elif coA == 1:
        b = 1
        d = 1
        c = coprimeWith(b, max = rootMax)
        e = coprimeWith(d, max = rootMax)
        while b*e + c*d == 0:
            print('B is 0!!!',b,c,d,e)
            c = coprimeWith(b, max = rootMax) 
            e = coprimeWith(d, max = rootMax)
        
    coA = b * d

    if coC == 'positive': #c and e are the same sign
        if coB == 'positive': #c and e are positive
            c = c
            e = e
        elif coB == 'negative': #c and e are negative
            c = c * -1
            e = e * -1
    elif coC == 'negative':
        if coB == 'positive': #c and e are different signs, but the bigger number is positive
            if c > e:
                c = c
                e = e * -1
            else:
                e = e
                c = c * -1
        elif coB == 'negative': #c and e are different signs, but the bigger number is negative
            if c > e:
                e = e
                c = c * -1
            else:
                c = c
                e = e * -1

    coC = c * e
    coB = b*e + c*d

    return coA, coB, coC
    
print(trinomialFactor(coA = 'not prime', coB = 'negative', coC = 'negative'))

#need to ensure that b is not 0

#special kinds where the value for C's factors: one set of factors added is the same as another set of factors subtracted
#30 - (15 - 2 = 13) (10 + 3 = 13) where signs matter

#








##Notes##

#When I have the user pick details like this, add to a list even if only 1 option is selected

#maybe just ditch the whole regents number bull shit



class rotMinGivenPolygonFindMin():
    def __init__(self, variation = ["Given polygon, find minimum", "Higher vocabulary"], polygon = ['triangle', 'quadrilateral', 'pentagon', 'hexagon', 'heptagon', 'septagon', 'octagon', 'nonagon', 'decagon', 'n-sided polygon'], typeOfQuestion = ['Multiple Choice', 'Short Answer']):
        
        self.topics = ['Transformations']
        self.skills = ['Determine minimum degrees of rotation to map onto itself', 'Identify the number of sides for a regular polygon']

        #This is for database keeping track of regents questions etc
        #self.regents = ['August2016']
        #self.questionNumber = ['27']

        #When populating SQLdb, loop through all possible
        self.variation = variation

        #ID is topic + MAIN SKILL
        self.ID = self.topics + "GivenPolygonFindMinimum"

        self.polygon = polygon
        self.type = typeOfQuestion
    def addQuestion(self, doc = None):
        #below is for n-sided polygon
        factors360 = []
        numbers = list(range(11,361)) #starting at 11, since 10 downward has a name
        for item in numbers:
            if 360 % item == 0:
                factors360.append(item)
        if 0 in factors360:
            factors360.remove(0)
        if 360 in factors360:
            factors360.remove(360)

        x = random.choice(factors360)

        polygonDict = {'triangle': 3, 'quadrilateral': 4, 'pentagon': 5, 'hexagon': 6, 'heptagon':7, 'septagon': 7, 'octagon': 8, 'nonagon': 9, 'decagon': 10, '%d-sided polygon' % x: x}

        #Want to replace n-sided with actual side of polygon    
        if 'n-sided polygon' in self.polygon:
            givenPolygon['n-sided polygon'] = '%d-sided polygon' % x
        
        givenPolygon = random.choice(self.polygon)
        variation = random.choice(self.variation)
        typeOfQuestion = random.choice(self.typeOfQuestion)
        
        if variation == "Given polygon, find minimum": #minimum number of degrees basic
            #Same question for both kinds of questions
            self.questionString = r'What is the minimum number degrees of rotation to carry a %s onto itself?' % givenPolygon
       
            if typeOfQuestion == 'Short Answer':
                self.questionString = r'If a regular %s is rotated $%d^{\circ}$, will it rotate onto itself? Explain' % (givenPolygon, degrees)
            elif typeOfQuestion == 'Multiple Choice':
                wrongChoices = ['triangle','quadrilateral','pentagon','hexagon','heptagon','octagon','nonagon','decagon']

                if givenPolygon in wrongChoices:
                    wrongChoices.remove(wrongChoices)

                c2 = random.choice(wrongChoices)
                self.choice2 = r'$%d^{\circ}$' % (360/polygonDict[c2])
                wrongChoices.remove(c2)

                c3 = random.choice(wrongChoices)
                self.choice3 = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
                wrongChoices.remove(c3)

                c4 = random.choice(wrongChoices)
                self.choice4 = r'$%d^{\circ}$' % (360/polygonDict[c4])
                wrongChoices.remove(c4)
        
        elif variation == "Higher vocabulary":
            if typeOfQuestion == 'Short Answer':
                self.questionString = r'A regular %s is rotated in a %s direction about its center. Determine and state the minimum number of degrees in the rotation such that the %s will coincide with itself.' % (givenPolygon, random.choice(['clockwise','counterclockwise']), givenPolygon)   
            elif typeOfQuestion == 'Multiple Choice':
                self.questionString = r'A regular %s is rotated in a %s direction about its center. What is the minimum number of degrees in the rotation such that the %s will coincide with itself?' % (givenPolygon, random.choice(['clockwise','counterclockwise']), givenPolygon)   
                wrongChoices = ['triangle','quadrilateral','pentagon','hexagon','heptagon','octagon','nonagon','decagon']

                if givenPolygon in wrongChoices:
                    wrongChoices.remove(wrongChoices)

                c2 = random.choice(wrongChoices)
                self.choice2 = r'$%d^{\circ}$' % (360/polygonDict[c2])
                wrongChoices.remove(c2)

                c3 = random.choice(wrongChoices)
                self.choice3 = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
                wrongChoices.remove(c3)
                
                c4 = random.choice(wrongChoices)
                self.choice4 = r'$%d^{\circ}$' % (360/polygonDict[c4])
                wrongChoices.remove(c4) 

        self.answerString = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
        
        doc.append(NoEscape(self.questionString))
        doc.append(NewLine())
        if typeOfQuestion == 'Multiple Choice':
            doc, choices = multipleChoice(doc = doc, choices = [self.answerString, self.choice2, self.choice3, self.choice4])
            answerChoice = choices.index(r'$%d^{\circ}$' % (360/polygonDict[givenPolygon]))
            self.answerString = r'(%d) $%d^{\circ}$' % (answerChoice+1,360/polygonDict[givenPolygon])
            
        doc.append(VerticalSpace(r'1in'))

    def addAnswer(self, docAnswer = None):
        docAnswer.append(NoEscape(self.answerString))
        docAnswer.append(NewLine())

"""
#############Lining up variation with different regents questions?????
class questionTemplate():
    def __init__(self, variation = ["Variation1", "Variation2"], typeOfQuestion = ['Multiple Choice', 'Short Answer']):
        self.topics = ['Transformations']
        self.skills = ['Determine minimum degrees of rotation to map onto itself', 'Identify the number of sides for a regular polygon']

        #This is for database keeping track of regents questions etc
        self.regents = ['August2016']
        self.questionNumber = ['27']

        #When populating SQLdb, loop through all possible
        self.variation = variation

        #ID is topic + MAIN SKILL
        self.ID = self.topics + "GivenPolygonFindMinimum"

        self.polygon = polygon
        self.type = typeOfQuestion
    def addQuestion(self, doc = None):
        #below is for n-sided polygon
        factors360 = []
        numbers = list(range(11,361)) #starting at 11, since 10 downward has a name
        for item in numbers:
            if 360 % item == 0:
                factors360.append(item)
        if 0 in factors360:
            factors360.remove(0)
        if 360 in factors360:
            factors360.remove(360)

        x = random.choice(factors360)

        polygonDict = {'triangle': 3, 'quadrilateral': 4, 'pentagon': 5, 'hexagon': 6, 'heptagon':7, 'septagon': 7, 'octagon': 8, 'nonagon': 9, 'decagon': 10, '%d-sided polygon' % x: x}

        #Want to replace n-sided with actual side of polygon    
        if 'n-sided polygon' in self.polygon:
            givenPolygon['n-sided polygon'] = '%d-sided polygon' % x
        
        givenPolygon = random.choice(self.polygon)
        variation = random.choice(self.variation)
        typeOfQuestion = random.choice(self.typeOfQuestion)
        
        if variation == "Given polygon, find minimum": #minimum number of degrees basic
            #Same question for both kinds of questions
            self.questionString = r'What is the minimum number degrees of rotation to carry a %s onto itself?' % givenPolygon
            
            if typeOfQuestion == 'Short Answer':
                self.questionString = r'If a regular %s is rotated $%d^{\circ}$, will it rotate onto itself? Explain' % (givenPolygon, degrees)
            else typeOfQuestion == 'Multiple Choice':
                wrongChoices = ['triangle','quadrilateral','pentagon','hexagon','heptagon','octagon','nonagon','decagon']

                if givenPolygon in wrongChoices:
                    wrongChoices.remove(wrongChoices)

                c2 = random.choice(wrongChoices)
                self.choice2 = r'$%d^{\circ}$' % (360/polygonDict[c2])
                wrongChoices.remove(c2)

                c3 = random.choice(wrongChoices)
                self.choice3 = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
                wrongChoices.remove(c3)
                
                c4 = random.choice(wrongChoices)
                self.choice4 = r'$%d^{\circ}$' % (360/polygonDict[c4])
                wrongChoices.remove(c4)
        
        elif variation == "Higher vocabulary":
            if typeOfQuestion == 'Short Answer':
                self.questionString = r'A regular %s is rotated in a %s direction about its center. Determine and state the minimum number of degrees in the rotation such that the %s will coincide with itself.' % (givenPolygon, random.choice(['clockwise','counterclockwise']), givenPolygon)   
            else typeOfQuestion == 'Multiple Choice':
                self.questionString = r'A regular %s is rotated in a %s direction about its center. What is the minimum number of degrees in the rotation such that the %s will coincide with itself?' % (givenPolygon, random.choice(['clockwise','counterclockwise']), givenPolygon)   
                wrongChoices = ['triangle','quadrilateral','pentagon','hexagon','heptagon','octagon','nonagon','decagon']

                if givenPolygon in wrongChoices:
                    wrongChoices.remove(wrongChoices)

                c2 = random.choice(wrongChoices)
                self.choice2 = r'$%d^{\circ}$' % (360/polygonDict[c2])
                wrongChoices.remove(c2)

                c3 = random.choice(wrongChoices)
                self.choice3 = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
                wrongChoices.remove(c3)
                
                c4 = random.choice(wrongChoices)
                self.choice4 = r'$%d^{\circ}$' % (360/polygonDict[c4])
                wrongChoices.remove(c4) 

        self.answerString = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
        
        doc.append(NoEscape(self.questionString))
        doc.append(NewLine())
        if typeOfQuestion == 'Multiple Choice':
            doc, choices = multipleChoice(doc = doc, choices = [self.answerString, self.choice2, self.choice3, self.choice4])
            answerChoice = choices.index(r'$%d^{\circ}$' % (360/polygonDict[givenPolygon]))
            self.answerString = r'(%d) $%d^{\circ}$' % (answerChoice+1,360/polygonDict[givenPolygon])
            
        doc.append(VerticalSpace(r'1in'))

    def addAnswer(self, docAnswer = None):
        docAnswer.append(NoEscape(self.answerString))
        docAnswer.append(NewLine())
"""

#EXAMPLES TO TEST SHIT FOR THE WEB APP
class rotationMinimumNumberOfDegrees():
    def __init__(self, type = "Given polygon, find minimum"):
        self.topics = ['Transformations']
        self.skills = ['Determine minimum degrees of rotation to map onto itself']
        self.regents = 'June2015'
        self.questionNumber = '4'
        self.variations = ["Given a polygon, find the mininum number of degrees", 
        "Given a minimum number of degrees, find the polygon", 
        "Given a polygon, which number of degrees (multiples)", 
        "Given a number of degrees (multiples), which polygon"]

        #according to variation etc
        self.regents = ['June2015',
        'August2015',
        'January2018',
        'January2017']

        self.ID = self.regents + "." + self.questionNumber
        
        
        self.type = type
    def addQuestion(self, correct = True, doc = None):
        factors360 = []
        numbers = list(range(3,361))
        for item in numbers:
            if 360 % item == 0:
                factors360.append(item)
        if 0 in factors360:
            factors360.remove(0)
        if 360 in factors360:
            factors360.remove(360)

        x = random.choice(factors360)
        polygonDict = {'triangle': 3, 'quadrilateral': 4, 'pentagon': 5, 'hexagon': 6, 'octagon': 8, 'decagon': 10,
                       '%d-sided polygon' % x: x}

        givenPolygon = random.choice(
            ['triangle', 'quadrilateral', 'pentagon', 'hexagon', 'octagon', 'decagon', '%d-sided polygon' % x])

        if self.type == "Given polygon, find minimum": #minimum number of degrees basic
            self.questionString = r'What is the minimum number of degrees for a regular %s to rotate onto itself?' % givenPolygon
            print(360/polygonDict[givenPolygon])
            self.answerString = r'$%d^{\circ}$' % (360/polygonDict[givenPolygon])
        elif self.type == "Given minimum, find polygon":
            self.questionString = r'What is the name of regular polygon whose minimum number of degrees to rotate onto itself is $%d^{\circ}$?' % (360/polygonDict[givenPolygon])
            self.answerString = r'%s' % givenPolygon
        elif self.type == "multiples": #multiples
            multiplesGiven = []
            minGiven = 360/polygonDict[givenPolygon]
            multiplesNot = []
            minNot = 360/(polygonDict[givenPolygon]+1)

            stuff = list(range(1, polygonDict[givenPolygon]+1))
            for item in stuff:
                multiplesGiven.append(item*minGiven)
                multiplesNot.append(item*minNot)

            if correct == True: #given polygon degrees will be a multiple
                degrees = random.choice(multiplesGiven)
                self.answerString = r'Yes. $%d^{\circ}$ is %d rotations. $%d^{\circ}$ is the minimum number of degrees. Every $%d^{\circ}$ rotates the polygon onto itself' % (degrees, multiplesGiven.index(degrees)+1, minGiven, minGiven)
            elif correct == False:
                degrees = random.choice(multiplesNot)
                self.answerString = r'No. $%d^{\circ}$ is the minimum number of degrees. Every $%d^{\circ}$ rotates the polygon onto itself, and $%d^{\circ}$ is not a multiple of $%d^{\circ}$. $\frac{%d^{\circ}}{%d^{\circ}} = %g$' % (minGiven, minGiven, degrees, minGiven, degrees, minGiven, degrees/minGiven)

            self.questionString = r'If a regular %s is rotated $%d^{\circ}$, will it rotate onto itself? Explain' % (givenPolygon, degrees)    

        doc.append(NoEscape(self.questionString))
        doc.append(NewLine())
        doc.append(VerticalSpace(r'1in'))

    def addAnswer(self, docAnswer = None):
        docAnswer.append(NoEscape(self.answerString))
        docAnswer.append(NewLine())
class partition(): #eventually kwargs with a= b=
    def __init__(self):
        maxx = 10
        self.topics = [['Similarity']]
        self.skills = [['Paritition a line segment']]
        self.regents = 'MakingThisShitUp'
        self.questionNumber = '6' #what to do for multiple regents???
        self.ID = self.regents + "." + self.questionNumber
        self.options = [["Directed"],["Undirected"],['Integer Solution']]

    def addQuestion(self, directedKey = 'directed', solution = 'integer', kind = 'diagonal', doc = None):
        self.directedKey = directedKey
        self.solution = solution

        maxx = 10

        a = random.randint(1,10) #seems good
        b = random.randint(1,10)# don't care if a/b is 2/4 vs 1/2 etc fuck it

        multiples = list(range(a+b, maxx * 2 + 1, a + b))  # could make higher than 21 but this will fit on a 10 by 10 graph for now

        if solution == 'integer':
            distChoices = multiples
        elif solution == 'fraction':
            numbers = list(range(0, maxx * 2 + 1))
            for item in multiples:
                numbers.remove(item)
            distChoices = numbers

        x1 = -10
        y1 = -10

        xDist = random.choice(distChoices)
        yDist = random.choice(distChoices)

        x2 = x1 + xDist
        y2 = y1 + yDist
        if kind == 'horizontal':
            y2 = y1
        elif kind == 'vertical':
            x2 = x1

        #Move line araound 10 by 10 grid
        xChange = random.randint(0, maxx - x2)
        yChange = random.randint(0, maxx - y2)

        x1 = x1 + xChange
        x2 = x2 + xChange
        y1 = y1 + yChange
        y2 = y2 + yChange

        self.xP = (x1*b + x2*a)/(a+b)
        self.yP = (y1*b + y2*a)/(a+b)

        self.a = a
        self.b = b

        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2

        one, two, three = annotatePolygon('no',3)
        self.A = one[0]
        self.B = one[1]
        self.C = one[2]

        if self.directedKey == 'directed':
            doc.append(NoEscape(r'Given the directed line segment $\overline{%s%s}$ ' % (self.A, self.C) +
                                r'with $%s(%d, %d)$ and $%s(%d, %d)$, ' % (self.A, self.x1, self.y1, self.C, self.x2, self.y2) +
                                r'determine point $%s$ that %s the segment in the ratio %s.' % (self.B, random.choice(['partitions','divides']),
                                random.choice(['%d to %d' % (self.a, self.b), '%d:%d' % (self.a,                      self.b)]))))
        elif self.directedKey == 'key':
            doc.append(NoEscape(r'Given the line segment $\overline{%s%s}$ ' % (self.A, self.C) +
                                r'with $%s(%d, %d)$ and $%s(%d, %d)$, ' % (
                                self.A, self.x1, self.y1, self.C, self.x2, self.y2) +
                                r'determine point $%s$ such that %s%s:%s%s is %s.' % (
                                self.B, self.A, self.B, self.B, self.C,
                                random.choice(['%d to %d' % (self.a, self.b), '%d:%d' % (self.a, self.b)]))))
        doc.append(NewLine())
        doc.append(VerticalSpace(r'1in'))

    def addAnswer(self, docAnswer = None):
        if self.solution == 'fraction':
            num = Fraction(self.xP).limit_denominator(100)
            xPnumer = num.numerator
            xPdenom = num.denominator

            num2 = Fraction(self.yP).limit_denominator(100)
            yPnumer = num2.numerator
            yPdenom = num2.denominator

            docAnswer.append(NoEscape(r'$%s(\frac{%d}{%d}, \frac{%d}{%d})$' % (self.B, xPnumer, xPdenom, yPnumer, yPdenom)))
            docAnswer.append(VerticalSpace(r'.25in'))
        else:
            docAnswer.append(NoEscape(r'$%s(%d, %d)$' % (self.B, self.xP, self.yP)))
class testExample2():
    def __init__(self):
        #may want to have a dictioanry of toipcs with skills etc, some way to differntiate between one skill going with one topic or another
        self.topics = ['Transformations','Circles']
        self.skills = ['To identify a sequence of transformations','To determine the radius given diameter']
        self.regents = 'June2015'
        self.questionNumber = '7'
        self.ID = self.regents + "." + self.questionNumber
        self.options = ["Fuck fuck", "Eat a dick", "No No nO"]
    def addQuestion(self):
        print('hi')

    def addAnswer(self):
        print('hi')




#example of a question
#class name is name of question (question number will be okay for now)
class bryanExample():
    def __init__(self, maxOfGraph = 10, typeOfQuestion = 'multiple choice'): #any options would go in here
        
        '''You can skip these for now: I'll eventually tell you which topic, skill, regents, etc
        self.topics = [['Similarity']]
        self.skills = [['Paritition a line segment']]
        self.regents = 'MakingThisShitUp'
        self.questionNumber = '6' #what to do for multiple regents???
        self.ID = self.regents + "." + self.questionNumber
        '''
        
        #Math stuff would go here.
        
        #if a string contains latex code, it is preceded with the letter 'r'
        self.question = r'This is the question!!!! An example of latex crap would be $\overline{%s%s}$. This will make line segment AB have a line over the and the AB. Most of the weird crap I google when I do not know it. Latex is surrounded by $$' % ('A','B')
        
        self.answer = 'This is the answer!!!'
        
        
        
    def addQuestion(self, doc = None): #doc is the document that pylatex adds to when we make our document
        doc.append(NoEscape(self.question)) #NoEscape is used within pylatex to take in latex strings [strings with 'r' in front]
        doc.append(NewLine())
        doc.append(VerticalSpace(r'1in'))

    def addAnswer(self, docAnswer = None): #docAnswer is the answerkey document
        docAnswer.append(NoEscape(self.answer))


