import random

def circle(center, radius, label, options):
    options = str(options)
    center = str(center)
    radius = str(radius)
    label = str(label)
    doc.append(Command('draw[%s] %s node {%s} circle (%s);' % (options, center, label, radius)))
def grid(min, max):  # always do square
    doc.append(Command('draw[thin,gray,step=1] (%d,%d) grid (%d,%d);' % (min, min, max, max)))
    doc.append(Command('draw[line width=.8, black, <->] (%g,0) -- (%g,0);' % (min - .5, max + .5)))
    doc.append(Command('draw (%d,0) node {x};' % (max + 1)))
    doc.append(Command('draw[line width=.8, black, <->] (0,%g) -- (0,%g);' % (min - .5, max + .5)))
    doc.append(Command('draw (0,%d) node {y};' % (max + 1)))
def com(stuff):
    doc.append(Command(stuff + ';'))
def sizingXY(minn, maxx, size):  # small, medium, large
    lengthOfGrid = maxx - minn
    # 8.5 x 11 = 21.59cm x 2.94cm subtract margins = 7.5 x 9.5 = 19.05cm x 24.13
    # small = 1/5 of whole width/length
    # medium = 2/5 of whole width/length
    # large = 3/5 of whole width/length
    if size == 'small':
        squareLength = 1 / 5 * 19.05  # for now we're going to do square pictures, we can do rectangles in the future
    elif size == 'medium':
        squareLength = 2 / 5 * 19.05
    elif size == 'large':
        squareLength = 3 / 5 * 19.05

    xyLength = squareLength / lengthOfGrid
    return xyLength
class transformation():
    def __init__(self, x, y, minn, maxx):  # minn/maxx b/c of min/max problems
        import math
        self.minn = minn
        self.maxx = maxx

    def translation(self, x, y):  # worry about overlap later, just get nonoverlapping for now
        # going to not make it overlap
        xLength = max(x) - min(x)  # gives minimum distance
        yLength = max(y) - min(y)
        # this makes it so it won't overlap at all.
        xChoices = list(range(self.minn - min(x), -xLength - 1)) + list(range(xLength + 1, self.maxx - max(x) + 1))
        yChoices = list(range(self.minn - min(y), -yLength - 1)) + list(range(yLength + 1, self.maxx - max(y) + 1))
        xChange = random.choice(xChoices)
        yChange = random.choice(yChoices)

        self.tX = [item + xChange for item in x]
        self.tY = [item + yChange for item in y]

    def reflection(self, x, y, lineOfReflectionList):

        line = random.choice(lineOfReflectionList)
        tempX = x.copy()
        tempY = y.copy()

        if line == "xaxis":
            tempY[:] = [-1 * itemY for itemY in y]  # y changes value but x stays the same
            lineStr = "x-axis"

        elif line == "yaxis":
            tempX[:] = [(-1 * itemX) for itemX in x]  # x changes value but y stays the same
            lineStr = "y-axis"

        elif line == "y=":
            a = random.randint(math.floor((minn + max(tempY)) / 2), math.floor((maxx + min(tempY)) / 2))
            # midpoint between minn and max value because max value will be the farthest away

            tempY[:] = [2 * a - itemY for itemY in y]
            lineStr = "y = %d" % a

        elif line == "x=":
            a = random.randint(math.floor((minn + max(tempX)) / 2), math.floor((maxx + min(tempX)) / 2))

            tempX[:] = [2 * a - itemX for itemX in x]

            lineStr = "x = %d" % a

        elif line == "y=xc":
            c = random.randint(min(tempX) - tempY[tempX.index(min(tempX))], max(tempX) - tempY[
                tempX.index(max(tempX))])  # need to figure out how to do the confines of this
            tempX[:] = [itemY + -1 * c for itemY in y]
            tempY[:] = [itemX + c for itemX in x]

        elif line == "y=-xc":
            c = random.randint(minn, maxx)  # need to figure out how to do the confines of this
            tempX[:] = -1 * (itemY + -1 * c)
            tempY[:] = -1 * itemX + c

        elif line == 'y=mx+b':
            m = random.randint(-5, 5)
            b = random.randint(-10, 10)
            # got formula below from file online
            # as of now they could go off the graph...
            tempX[:] = [(1 - m ** 2) / (m ** 2 + 1) * itemX + 2 * m / (m ** 2 + 1) * itemY - 2 * m * b / (m ** 2 + 1)
                        for itemX, itemY in zip(x, y)]
            tempY[:] = [2 * m / (m ** 2 + 1) * itemX + (m ** 2 - 1) / (m ** 2 + 1) * itemY - 2 * b / (m ** 2 + 1) for
                        itemX, itemY in zip(x, y)]

        if line == "xaxis" or "yaxis":
            answer = "Reflection over the " + lineStr
        else:
            answer = "Reflection over " + lineStr  # modify, fine for now

        self.refX = tempX
        self.refY = tempY
        self.answer = answer
        self.line = line

    def rotation(self, x, y, centerX, centerY, degrees, direction):  # minn maxx of graph???
        if degrees == 'random' and direction == 'random':  # if wanting random degrees such as [90,180,270] and not caring about direction
            deg = [90, 180, 270]
            dir = ['ccw', 'cc']
            degrees = random.choice(deg)
            direction = random.choice(dir)

        # convert angle to radians since math.cos and math.sin take in angle in radians
        if direction == 'ccw':
            angleRot = math.radians(degrees)
        if direction == 'cc':
            angleRot = math.radians(360 - degrees)
        # this angle is always counterclockwise
        tX = [itemX + -centerX for itemX in x]  # translate to origin
        tY = [itemY + -centerY for itemY in y]

        rotX = [itemX * math.cos(angleRot) - itemY * math.sin(angleRot) for itemX, itemY in zip(tX, tY)]  # rotate
        rotY = [itemX * math.sin(angleRot) + itemY * math.cos(angleRot) for itemX, itemY in zip(tX, tY)]

        # translate Back
        newX = [itemX + centerX for itemX in rotX]  # translate to origin
        newY = [itemY + centerY for itemY in rotY]
        if direction == "cc":
            directionStr = "clockwise"
            directionValue = -1
        if direction == "ccw":
            directionStr = "counterclockwise"
            directionValue = 1

        answer = "Rotation " + str(degrees) + " " + directionStr + "\n around the point (" + str(centerX) + "," + str(
            centerY) + ")"
        # even though we changed the degrees in the calculation, it will be fixed for the answer here.

        self.rotX = newX
        self.rotY = newY
        self.answer = answer
        self.directionValue = directionValue
        self.degrees = degrees

    def dilation(self, x, y, centerOption,
                 scaleFactorOption):  # bring the center to the origin, then multiply coordinates and move back
        self.scaleFactorOption = scaleFactorOption
        if centerOption == 'inside':  # more or less
            self.centerX = math.floor((max(x) + min(x)) / 2)
            self.centerY = math.floor((max(y) + min(y)) / 2)
        elif centerOption == 'random':
            self.centerX = random.choice(range(-5, 6))  # ensures that graph won't be too big
            self.centerY = random.choice(range(-5, 6))  # could put this lower if you want smaller graph
        elif centerOption == 'corner':
            self.centerX = random.choice(x)
            self.centerY = y[x.index(self.centerX)]
            self.cornerIndex = x.index(self.centerX)
        else:
            self.centerX = centerOption[0]
            self.centerY = centerOption[1]

        if scaleFactorOption == 'random':
            self.scaleFactorOption = random.choice(['integer', 'fraction'])
            scaleFactorOption = self.scaleFactorOption
        if scaleFactorOption == 'integer':
            self.sF = random.choice([2, 3])  # keeping it low
            self.dX = [self.sF * (item + -1 * self.centerX) + self.centerX for item in x]
            self.dY = [self.sF * (item + -1 * self.centerY) + self.centerY for item in y]
        if scaleFactorOption == 'fraction':
            self.sF = random.choice([2, 3])  # going to change the triangles
            # this makes a problem when dilating a triangle of your own. must need coordinate divible by both 2 and 3
            self.dX = x
            self.dY = y

            self.tempX = [self.sF * (item + -1 * self.centerX) + self.centerX for item in x]
            self.tempY = [self.sF * (item + -1 * self.centerY) + self.centerY for item in y]

            self.x = self.tempX
            self.y = self.tempY
def annotatePolygon(prime, vertices):
    import random

    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "V", "W",
               "X", "Y", "Z"]

    polygonName1 = []

    one = random.choice(letters)
    letters.remove(one)
    two = random.choice(letters)
    letters.remove(two)
    three = random.choice(letters)
    letters.remove(three)
    four = random.choice(letters)
    letters.remove(four)

    polygonName2 = []

    one2 = random.choice(letters)
    letters.remove(one2)
    two2 = random.choice(letters)
    letters.remove(two2)
    three2 = random.choice(letters)
    letters.remove(three2)
    four2 = random.choice(letters)
    letters.remove(four2)

    polygonName3 = []

    one3 = random.choice(letters)
    letters.remove(one3)
    two3 = random.choice(letters)
    letters.remove(two3)
    three3 = random.choice(letters)
    letters.remove(three3)
    four3 = random.choice(letters)
    letters.remove(four3)
    if vertices == 1:
        polygonName = [one]
    if vertices == 2:
        polygonName = [one, two]
    if vertices == 3:  # suitable for now
        polygonName = [one, two, three]
        if prime == ('yes' or 'y'):
            polygonNamePrime = [one + r'^{\prime}', two + r'^{\prime}', three + r'^{\prime}']
            polygonNameDoublePrime = [one + r"$\prime\prime$", two + r"$\prime\prime$", three + r"$\prime\prime$"]
        if prime == ('no' or 'n'):
            polygonName1 = [one, two, three]
            polygonName2 = [one2, two2, three2]
            polygonName3 = [one3, two3, three3]
    if vertices == 4:
        polygonName = one + two + three + four
        if prime == ('yes' or 'y'):
            polygonNamePrime = [one + "$\prime$", two + "$\prime$", three + "$\prime$", four + "$\prime$"]
            polygonNameDoublePrime = [one + "$\prime$", two + "$\prime$", three + "$\prime$", four + "c"]
    if prime == 'yes':
        return polygonName, polygonNamePrime, polygonNameDoublePrime
    if prime == 'no':
        return polygonName, polygonName2, polygonName3
def polygon(minn, maxx, type, size):
    if size == 'smaller':
        maxAmount = (maxx - 1) // 3
    elif size == 'regular':
        maxAmount = (maxx - 1) // 2

    if type == 'scalene triangle':
        optionsX = list(range(-maxAmount, maxAmount + 1))
        optionsY = list(range(-maxAmount, maxAmount + 1))
        x1 = 0
        y1 = 0

        # eliminate 1 length triangles, two is the smallest
        optionsX.remove(-1)
        optionsX.remove(1)
        optionsY.remove(-1)
        optionsY.remove(1)

        x2 = random.choice(optionsX)
        if x2 == 0:  # so that x2 and y2 are not the same as x1 and y1
            optionsY.remove(0)
        y2 = random.choice(optionsY)

        if x2 in optionsX:
            optionsX.remove(x2)
        if y2 in optionsY:
            optionsY.remove(y2)

        # now we're going to remove items from optiosn that are within less than half the length of x or y
        stuffX = optionsX
        stuffY = optionsY

        for ox in optionsX:
            xDist = abs(x2 - x1)
            if x2 - xDist // 2 - 1 <= ox <= x2 + xDist // 2 + 1:
                stuffX.remove(ox)

        for oy in optionsY:
            yDist = abs(y2 - y1)
            if y2 - yDist // 2 - 1 <= oy <= y2 + yDist // 2 + 1:
                stuffY.remove(oy)
        x3 = random.choice(stuffX)
        y3 = random.choice(stuffY)

        xs = [x1, x2, x3]
        ys = [y1, y2, y3]
        return xs, ys
    elif type == 'right triangle':
        optionsXY = []
        minnMaxx = list(range(-maxAmount, maxAmount + 1))
        for x in minnMaxx:
            for y in minnMaxx:
                optionsXY.append([x, y])

        x1 = 0
        y1 = 0

        # eliminate 1 length triangles, two is the smallest
        optionsXY.remove([0, 1])
        optionsXY.remove([-1, 1])
        optionsXY.remove([-1, 0])
        optionsXY.remove([-1, -1])
        optionsXY.remove([0, -1])
        optionsXY.remove([1, -1])
        optionsXY.remove([1, 0])
        optionsXY.remove([1, 1])

        optionsXY.remove([x1, y1])

        choice2 = random.choice(optionsXY)
        x2 = choice2[0]
        y2 = choice2[1]

        optionsXY.remove(choice2)

        # rotate this line 90 cc or 90 ccw around either x2y2, or x1y1
        rotateOption = random.randint(1, 2)
        if rotateOption == 1:  # totate around 00 cc
            x3 = -y2
            y3 = x2
        elif rotateOption == 2:
            x3 = y2
            y3 = -x2
        elif rotateOption == 3:  # something's wrong...
            x3 = -(y1 - y2) + y2
            y3 = (x1 - x2) + x2
        elif rotateOption == 4:  # and here too
            x3 = (y1 - y2) + y2
            y3 = -(x1 - x2) + x2
        xs = [x1, x2, x3]
        ys = [y1, y2, y3]
        return xs, ys
def translation(minn, maxx, x, y, xChangeAmount, yChangeAmount, overlap):
    # Change Amount = '+', '-', '0'

    # Overlap mean you probably don't care about xChangeAmount etc, so it's not going to abide by that

    # Take xs and ys, dependent on translationOptions. Ex + +, move to bottom left corner then move it around randomly so that it will still be able to translate + +
    xLength = max(x) - min(x)
    yLength = max(y) - min(y)
    if xChangeAmount == 'random':
        xChangeAmount = random.choice(['+', '-', '0'])
    if yChangeAmount == 'random':
        yChangeAmount = random.choice(['+', '-', '0'])
    if overlap == 'random':
        overlap = random.choice(['yes', 'no'])

    if xChangeAmount == '+':  # going to move triangle somewhere on grid so that it can afterwards go some amount to the right
        xChange = random.randint(minn - min(x), (maxx - (xLength + 1)) - max(
            x))  # can add 0 up til maxx - xLength then - 1 so that future translation doesn't overlap
    elif xChangeAmount == '-':
        xChange = random.randint(minn - (min(x) - (xLength + 1)), maxx - max(x))
    elif xChangeAmount == '0':
        xChange = 0
    if yChangeAmount == '+':  # going to move triangle somewhere on grid so that it can afterwards go some amount to the right
        yChange = random.randint(minn - min(y), (maxx - (yLength + 1)) - max(
            y))  # can add 0 up til maxx - xLength then - 1 so that future translation doesn't overlap
    elif yChangeAmount == '-':
        yChange = random.randint(minn - (min(y) - (yLength + 1)), maxx - max(y))
    elif yChangeAmount == '0':
        yChange = 0
    xs = []
    ys = []

    for itemX, itemY in zip(x, y):
        xs.append(itemX + xChange)
        ys.append(itemY + yChange)

    x = xs
    y = ys
    if overlap == 'no':
        if minn + xLength < min(x):
            leftChange = minn - min(x)  # plus one so that the translation does not overlap at all
            leftChoices = list(range(leftChange, -xLength - 1 + 1))  # the most we could add negatively is xLength+1
        else:
            leftChoices = []

        if maxx - xLength > max(x):
            rightChange = maxx - max(x)
            rightChoices = list(range(xLength + 1, rightChange + 1))  # smallest we can do is xLength +1
        else:
            rightChoices = []

        xChoices = leftChoices + rightChoices

        if minn + yLength < min(y):
            downChange = minn - min(y)  # plus one so that the translation does not overlap at all
            downChoices = list(range(downChange, -yLength - 1 + 1))
        else:
            downChoices = []
        if maxx - yLength > max(y):
            upChange = maxx - max(y)
            upChoices = list(range(yLength + 1, upChange + 1))
        else:
            upChoices = []

        yChoices = downChoices + upChoices

        if yChangeAmount == 'random':
            yTrans = random.choice(yChoices)
        elif yChangeAmount == "+":
            yTrans = random.choice(upChoices)
        elif yChangeAmount == '-':
            yTrans = random.choice(downChoices)
        elif yChangeAmount == '0':
            yTrans = 0
        else:
            yTrans = random.choice(yChoices)

        if xChangeAmount == 'random':
            xTrans = random.choice(xChoices)
        elif xChangeAmount == "+":
            xTrans = random.choice(rightChoices)
        elif xChangeAmount == '-':
            xTrans = random.choice(leftChoices)
        elif xChangeAmount == '0':
            xTrans = 0
        else:
            xTrans = random.choice(xChoices)

    elif overlap == 'yes':  #
        if min(x) - xLength >= minn:
            leftChoices = list(range(-xLength, 1))  # the most we could add negatively is xLength+1
        else:
            leftChange = minn - min(x)
            leftChoices = list(range(leftChange, 1))

        if max(x) + xLength <= maxx:
            rightChoices = list(range(0, xLength + 1))  # smallest we can do is xLength +1
        else:
            rightChange = maxx - max(x)
            rightChoices = list(range(0, rightChange))

        xChoices = leftChoices + rightChoices

        if min(y) - yLength >= minn:
            downChoices = list(range(-yLength, 1))  # the most we could add negatively is xLength+1
        else:
            downChange = minn - min(y)
            downChoices = list(range(downChange, 1))

        if max(y) + yLength <= maxx:
            upChoices = list(range(0, yLength + 1))  # smallest we can do is xLength +1
        else:
            upChange = maxx - max(y)
            upChoices = list(range(0, upChange))

        yChoices = downChoices + upChoices

        if overlap == 'yes' and overlap == 'yes':
            overlapType = random.randint(1, 2)
            if overlapType == 1:
                xChoices = [0]
            elif overlapType == 2:
                yChoices = [0]

        xTrans = random.choice(xChoices)  # could add 0 in there if i wanted to
        yTrans = random.choice(yChoices)

    if yTrans == 0 and xTrans == 0:  # so a translation will actually happen
        if len(yChoices) > 1:
            yChoices.remove(0)
            yTrans = random.choice(yChoices)
        elif len(xChoices) > 1:
            xChoices.remove(0)
            xTrans = random.choice(xChoices)
        print('Identity Translation')

    transXs = []
    transYs = []
    for itemX in x:
        transXs.append(itemX + xTrans)
    for itemY in y:
        transYs.append(itemY + yTrans)
    return x, y, transXs, transYs, xTrans, yTrans
def reflection(minn, maxx, x, y, nameOfReflectionList, overlapping):  # assuming triangle to start
    import random
    # nameOfReflection: x=, y=, x= not 0, y= not 0, x=0, y=0, xaxis, yaxis
    # overlap = 'no' or 'yes'
    nameOfReflection = random.choice(nameOfReflectionList)
    # just like translation, need to move current triangle to somewhere so it can do then be reflected
    xLength = max(x) - min(x)
    yLength = max(y) - min(y)
    gridSize = maxx - minn

    if nameOfReflection == 'x=' or nameOfReflection == 'x=not0':
        # need (xLength+1)*2 room to do the reflection, so farthest i can always move it left (to -10) is minn - min(x) to  minn + (gridSize - (xLength+1)*2)
        xChange = random.randint(minn - min(x), minn + (gridSize - (xLength + 1) * 2) - min(x))
        yChange = random.randint(minn - min(y), maxx - max(y))  # y can go anywhere

        xs = []
        ys = []

        for itemX, itemY in zip(x, y):
            xs.append(itemX + xChange)
            ys.append(itemY + yChange)

        x = xs
        y = ys

        if overlapping == 'no':
            if (max(x) + 1 + 1 + xLength) <= maxx:  # there are lines on the right that work
                # the farthest right line would be maxx - min(x) // 2
                rightChoices = list(range(min(x) + xLength + 1, (maxx - min(x)) // 2 + min(
                    x) + 1))  # first part is the closest line that has to work, then second part is
            else:
                rightChoices = []
            if (min(x) - 1 - 1 - xLength) >= minn:  # there are line on the left that work
                leftChoices = list(range(max(x) - (max(x) - minn) // 2, max(x) - xLength - 1 + 1))
            else:
                leftChoices = []
            refValChoices = rightChoices + leftChoices
        elif overlapping == 'yes':
            choices = list(range(min(x), max(x) + 1))
            refValChoices = []
            for item in choices:
                if item - xLength >= minn and item + xLength <= maxx:
                    refValChoices.append(item)

        refVal = random.choice(refValChoices)
        if nameOfReflection == 'x=not0':
            if 0 in refValChoices:
                refValChoices.remove(0)
                refVal = random.choice(refValChoices)

        refX = []
        refY = []
        for itemX, itemY in zip(x, y):
            refX.append(2 * refVal - itemX)
            refY.append(itemY)

    elif nameOfReflection == 'y=' or nameOfReflection == 'y=not0':
        # need (xLength+1)*2 room to do the reflection, so farthest i can always move it left (to -10) is minn - min(x) to  (minn + gridSize - (xLength+1)*2) - min(x)
        yChange = random.randint(minn - min(y), minn + (gridSize - (yLength + 1) * 2) - min(y))
        xChange = random.randint(minn - min(x), maxx - max(x))  # y can go anywhere

        xs = []
        ys = []

        for itemX, itemY in zip(x, y):
            xs.append(itemX + xChange)
            ys.append(itemY + yChange)

        x = xs
        y = ys

        if overlapping == 'no':
            if (max(y) + 1 + 1 + yLength) <= maxx:  # there are lines on the up that work
                # the farthest up line would be maxx - min(y) // 2
                upChoices = list(range(min(y) + yLength + 1, (maxx - min(y)) // 2 + min(
                    y) + 1))  # first part is the closest line that has to work, then second part is
            else:
                upChoices = []
            if (min(y) - 1 - 1 - yLength) >= minn:  # there are line on the left that work
                downChoices = list(range(max(y) - (max(y) - minn) // 2, max(y) - yLength - 1 + 1))
            else:
                downChoices = []
            refValChoices = upChoices + downChoices
        elif overlapping == 'yes':
            choices = list(range(min(y), max(y) + 1))
            refValChoices = []
            for item in choices:
                if item - yLength >= minn and item + yLength <= maxx:
                    refValChoices.append(item)

        refVal = random.choice(refValChoices)
        if nameOfReflection == 'y=not0':
            if 0 in refValChoices:
                refValChoices.remove(0)
                refVal = random.choice(refValChoices)

        refX = []
        refY = []
        for itemX, itemY in zip(x, y):
            refY.append(2 * refVal - itemY)
            refX.append(itemX)

    elif nameOfReflection == 'yaxis' or nameOfReflection == 'x=0':
        if overlapping == 'no':
            # can't touch yaxis, so the point it can go to leftward is -1 - xLength to 1 + xlength
            # it can go all the way to the minn value, so minn - min(x) but it has to be less than 0 for the max(x), so min(x) + xLength + 1
            # leftXChange = list(range((0 - xLength - 1) - min(x), (0 + xLength + 1) - min(x) + 1))
            leftXChange = list(range(minn - min(x), (0 - xLength - 1) - min(x) + 1))
            rightXChange = list(range(0 + xLength + 1, maxx - max(x) + 1))
            xChange = random.choice(leftXChange + rightXChange)
            yChange = random.randint(minn - min(y), maxx - max(y))
        else:
            xChange = random.choice(list(range((0 - xLength) - min(x), (0 + xLength) - max(x) + 1)))
            yChange = random.randint(minn - min(y), maxx - max(y))

        xs = []
        ys = []

        for itemX, itemY in zip(x, y):
            xs.append(itemX + xChange)
            ys.append(itemY + yChange)

        x = xs
        y = ys

        refVal = 0

        refX = []
        refY = []
        for itemX, itemY in zip(x, y):
            refX.append(2 * refVal - itemX)
            refY.append(itemY)

    elif nameOfReflection == 'xaxis' or nameOfReflection == 'y=0':
        if overlapping == 'no':
            # can't touch Xaxis, so the point it can go to downward is \
            # it can go all the way to the minn value, so minn - min(x) but it has to be less than 0 for the max(x), so min(x) + xLength + 1
            downYChange = list(range(minn - min(y), (0 - yLength - 1) - min(y) + 1))
            upYChange = list(range(0 + yLength + 1, maxx - max(y) + 1))
            yChange = random.choice(downYChange + upYChange)
            xChange = random.randint(minn - min(x), maxx - max(x))
        else:
            yChange = random.choice(list(range((0 - yLength) - min(y), (0 + yLength) - max(y) + 1)))
            xChange = random.randint(minn - min(x), maxx - max(x))

        xs = []
        ys = []

        for itemX, itemY in zip(x, y):
            xs.append(itemX + xChange)
            ys.append(itemY + yChange)

        x = xs
        y = ys

        refVal = 0

        refX = []
        refY = []
        for itemX, itemY in zip(x, y):
            refY.append(2 * refVal - itemY)
            refX.append(itemX)

    return x, y, refX, refY, refVal  # need to add part for x= or y= or whatever
def rotation(minn, maxx, x, y, center, degrees, direction, overlapping):
    # change for random center, random degrees, random direction, etc
    if degrees == 'random':  # if wanting random degrees such as [90,180,270] and not caring about direction
        deg = [90, 180, 270]
        degrees = random.choice(deg)
    if direction == 'random':
        dir = ['ccw', 'cc']
        direction = random.choice(dir)
    if center == 'random':
        center = random.choice(['origin', 'other'])
    if overlapping == 'random':
        overlpaping = random.choice(['yes', 'no'])

    if center == 'origin':
        centerX = 0
        centerY = 0
        if overlapping == 'no':
            xChangeLeft = list(range(minn - min(x), centerX - 1 - max(x) + 1))  # b/c length is always less than maxx
            xChangeRight = list(range(centerX + 1 - min(x), maxx - max(x) + 1))

            xChange = random.choice(xChangeLeft + xChangeRight)

            yChangeDown = list(range(minn - min(x), centerY - 1 - max(y) + 1))
            yChangeUp = list(range(centerY + 1 - min(y), maxx - max(y) + 1))

            yChange = random.choice(yChangeDown + yChangeUp)

            xs = []  # moving the shape so that any 90, 180, 270 rotation can happen around the center without overlapping
            ys = []

            for itemX, itemY in zip(x, y):
                xs.append(itemX + xChange)
                ys.append(itemY + yChange)

            x = xs
            y = ys
        else:  # overlapping == 'yes'
            spot = random.choice(['vertex', 'middle'])
            if spot == 'vertex':
                chosenVertex = random.randint(0, len(x) - 1)
                xChange = centerX - x[chosenVertex]
                yChange = centerY - y[
                    chosenVertex]  # want to move it so shape is touching 0,0::easiest way right now is to move vertex to 0,0 or make middle of shape ish 0,0
            elif spot == 'middle':
                xLength = max(x) - min(x)
                yLength = max(y) - min(y)
                xMid = xLength / 2
                yMid = yLength / 2

                xCoordinate = int(xMid + min(x))
                yCoordinate = int(yMid + min(y))

                xChange = centerX - xCoordinate
                yChange = centerY - yCoordinate
    elif center == 'other':
        xLength = max(x) - min(x)
        yLength = max(y) - min(y)
        # Essentially going to make a square around the origin, acting as the center of rotation, then will translate that square around the actual grid to get other centers
        maxDist = max([xLength, yLength])
        centers = []

        for cX in range(minn + maxDist + 1, maxx - (maxDist + 1) + 1):
            for cY in range(minn + maxDist + 1, maxx - (maxDist + 1) + 1):
                centers.append([cX, cY])

        centers.remove([0, 0])
        center = random.choice(centers)
        centerX = center[0]
        centerY = center[1]

        if overlapping == 'no':
            xChangeLeft = list(range(centerX - 1 - maxDist - min(x), centerX - 1 - max(
                x) + 1))  # biggest leftward it could go is one less than centerX, smallest is centerX - 1 - maxDist, which shouldn't be off the graph
            xChangeRight = list(range(centerX + 1 - min(x), centerX + 1 + maxDist - max(x) + 1))

            xChange = random.choice(xChangeLeft + xChangeRight)

            yChangeDown = list(range(centerY - 1 - maxDist - min(y), centerY - 1 - max(y) + 1))
            yChangeUp = list(range(centerY + 1 - min(y), centerY + 1 + maxDist - max(y) + 1))

            yChange = random.choice(yChangeDown + yChangeUp)
        else:  # overlapping == 'yes'
            spot = random.choice(['vertex', 'middle'])
            if spot == 'vertex':
                chosenVertex = random.randint(0, len(x) - 1)
                xChange = centerX - x[chosenVertex]
                yChange = centerY - y[
                    chosenVertex]  # want to move it so shape is touching 0,0::easiest way right now is to move vertex to 0,0 or make middle of shape ish 0,0
            elif spot == 'middle':
                xLength = max(x) - min(x)
                yLength = max(y) - min(y)
                xMid = xLength / 2
                yMid = yLength / 2

                xCoordinate = int(xMid + min(x))
                yCoordinate = int(yMid + min(y))

                xChange = centerX - xCoordinate
                yChange = centerY - yCoordinate

    xs = []  # moving the shape so that any 90, 180, 270 rotation can happen around the center without overlapping
    ys = []

    for itemX, itemY in zip(x, y):
        xs.append(itemX + xChange)
        ys.append(itemY + yChange)

    x = xs
    y = ys

    if direction == 'ccw':
        angleRot = math.radians(degrees)
    if direction == 'cc':
        angleRot = math.radians(360 - degrees)
    # this angle is always counterclockwise
    tX = [itemX + -centerX for itemX in x]  # translate to origin
    tY = [itemY + -centerY for itemY in y]

    rotX = [itemX * int(math.cos(angleRot)) - itemY * int(math.sin(angleRot)) for itemX, itemY in
            zip(tX, tY)]  # rotate, add ints so that coordinates wouldn't be floats
    rotY = [itemX * int(math.sin(angleRot)) + itemY * int(math.cos(angleRot)) for itemX, itemY in zip(tX, tY)]

    # translate Back
    rotX = [int(itemX) + centerX for itemX in rotX]  # translate to origin
    rotY = [int(itemY) + centerY for itemY in rotY]

    return x, y, rotX, rotY
def pointTriangle(center, P, A, B, C):
    import numpy

    v0 = numpy.subtract(C, A)
    v1 = numpy.subtract(B, A)
    v2 = numpy.subtract(P, A)

    dot00 = numpy.dot(v0, v0)
    dot01 = numpy.dot(v0, v1)
    dot02 = numpy.dot(v0, v2)
    dot11 = numpy.dot(v1, v1)
    dot12 = numpy.dot(v1, v2)

    invDenom = 1 / (dot00 * dot11 - dot01 * dot01)
    u = (dot11 * dot02 - dot01 * dot12) * invDenom
    v = (dot00 * dot12 - dot01 * dot02) * invDenom

    if ((u > 0) and (v > 0) and (u + v < 1)) == True:  # > < inside,
        if center == 'inside':
            return True
        else:
            return False
    elif ((u >= 0) and (v >= 0) and (u + v < 1)) == False:
        if center == 'outside':
            return True
        else:
            return False
    else:
        if center == 'on':
            return True
        else:
            return False
def graphPolygon(x, y, nodes):  # could make it idealy outside the figure but fuck it for now
    import random
    minX = min(x)
    minY = min(y)
    maxX = max(x)
    maxY = max(y)

    string = 'draw[line width = 1.5]'
    for itemX, itemY in zip(x, y):
        string += ' (%d,%d) --' % (itemX, itemY)
    string += ' cycle'

    for itemX, itemY, itemN in zip(x, y, nodes):
        realXNodes = []
        realYNodes = []

        xTries = [1, 0, -1, -1, -1, 0, 1, 1]  # maybe eliminate some of these when axes are in play..??????
        yTries = [-1, -1, -1, 0, 1, 1, 1,
                  0]  # once this works, we can roll through corner pieces first as they will probably be more likely
        dist = .5

        for xT, yT in zip(xTries, yTries):
            xNode = itemX + dist * xT
            yNode = itemY + dist * yT

            p = [xNode, yNode]
            a = [x[0], y[0]]
            b = [x[1], y[1]]
            c = [x[2], y[2]]

            if pointTriangle('outside', p, a, b, c) == True:
                realXNodes.append(xNode)
                realYNodes.append(yNode)
                # could have it end here but would rather have multiple points
        xNode = random.choice(realXNodes)
        yNode = random.choice(realYNodes)
        com('node at (%g,%g) {%s}' % (xNode, yNode, itemN))
    com(string)
def graphAngle(x, y, nodes):
    import random
    minX = min(x)
    minY = min(y)
    maxX = max(x)
    maxY = max(y)

    string = 'draw[line width = 1.5]'
    counter = 0
    for itemX, itemY in zip(x, y):
        counter += 1
        print(counter)
        if counter > 2:
            string += ' (%d,%d);' % (itemX, itemY)
        else:
            string += ' (%d,%d) --' % (itemX, itemY)

    for itemX, itemY, itemN in zip(x, y, nodes):
        realXNodes = []
        realYNodes = []

        xTries = [1, 0, -1, -1, -1, 0, 1, 1]  # maybe eliminate some of these when axes are in play..??????
        yTries = [-1, -1, -1, 0, 1, 1, 1,
                  0]  # once this works, we can roll through corner pieces first as they will probably be more likely
        dist = .5

        for xT, yT in zip(xTries, yTries):
            xNode = itemX + dist * xT
            yNode = itemY + dist * yT

            p = [xNode, yNode]
            a = [x[0], y[0]]
            b = [x[1], y[1]]
            c = [x[2], y[2]]

            if pointTriangle('outside', p, a, b, c) == True:
                realXNodes.append(xNode)
                realYNodes.append(yNode)
                # could have it end here but would rather have multiple points
        xNode = random.choice(realXNodes)
        yNode = random.choice(realYNodes)
        com('node at (%g,%g) {%s}' % (xNode, yNode, itemN))
    com(string)
def graphLine(x, y, nodes):
    import random

    string = 'draw[line width = 1.5]'
    string += ' (%d,%d) -- (%d,%d)' % (x[0], y[0], x[1], y[1])

    # make a triangle with an angle of 180
    x.append(x[0])
    y.append(y[0])
    nodes.append('')
    counter = 0
    for itemX, itemY, itemN in zip(x, y, nodes):
        counter += 1
        realXNodes = []
        realYNodes = []

        xTries = [1, 0, -1, -1, -1, 0, 1, 1]  # maybe eliminate some of these when axes are in play..??????
        yTries = [-1, -1, -1, 0, 1, 1, 1,
                  0]  # once this works, we can roll through corner pieces first as they will probably be more likely
        dist = .5

        for xT, yT in zip(xTries, yTries):
            xNode = itemX + dist * xT
            yNode = itemY + dist * yT

            p = [xNode, yNode]
            a = [x[0], y[0]]
            b = [x[1], y[1]]
            c = [x[2], y[2]]

            if pointTriangle('outside', p, a, b, c) == True:
                realXNodes.append(xNode)
                realYNodes.append(yNode)
                # could have it end here but would rather have multiple points
        xNode = random.choice(realXNodes)
        yNode = random.choice(realYNodes)

        if counter != 3:
            com('node at (%g,%g) {%s}' % (xNode, yNode, itemN))
    com(string)
def dilation(minn, maxx, x, y, center, sf, overlapping):
    # for scalefactor the most i really need is 2,3 or 1/2 1/3, maybe 3/2, 5/2, 2/3, 4/3
    # in any case if the original triangle is small enough to always get 3 times bigger somewhere on the grid, we should be able to do all of the above
    # center options: no overlap == inside, outside
    # overlap = on, vertex

    xLength = max(x) - min(x)
    yLength = max(y) - min(y)
    gridSize = maxx - minn

    maxInt = math.floor(gridSize / (max([xLength, yLength])))

    # regardless if sf is interger or fraction, it will start as integer and then we'll just switch afterwards
    sfVal = random.randint(2, maxInt)
    # http: // blackpawn.com / texts / pointinpoly / default.html FOR EXAMPLANATION
    import numpy

    centerPoints = []

    for xP in range(min(x), max(x) + 1):
        for yP in range(min(y), max(y) + 1):
            p = [xP, yP]
            a = [x[0], y[0]]
            b = [x[1], y[1]]
            c = [x[2], y[2]]
            if pointTriangle(center, p, a, b, c) == True:
                centerPoints.append(p)

    if center == 'vertex':
        centerPoints = [[x[0], y[0]], [x[1], y[1]], [x[2], y[2]]]

    centerChosen = random.choice(centerPoints)

    centerX = centerChosen[0]
    centerY = centerChosen[1]

    # if i dilate the triangle in the spot it currently is, then i can move it so that the confines are on the graph, thus determining a new center
    tempDX = [sfVal * (item + -1 * centerX) + centerX for item in x]
    tempDY = [sfVal * (item + -1 * centerY) + centerY for item in y]

    # check to make sure it can be moved onto the grid
    if max(tempDY) - min(tempDY) > gridSize:
        print('oh no y')
    if max(tempDX) - min(tempDX) > gridSize:
        print('oh no x')

    # so now i have the triangle dilated, so i need to make it so max(x) < maxx and min(x) > minn, which i hope is possible
    xChange = random.randint(minn - min(tempDX), maxx - max(tempDX))
    yChange = random.randint(minn - min(tempDY), maxx - max(tempDY))

    # translate the center as well
    centerX = centerX + xChange
    centerY = centerY + yChange

    centerCoord = [centerX, centerY]

    xs = []
    ys = []

    for itemX, itemY in zip(x, y):
        xs.append(itemX + xChange)
        ys.append(itemY + yChange)

    x = xs
    y = ys

    dX = []
    dY = []

    for ddx, ddy in zip(tempDX, tempDY):
        dX.append(ddx + xChange)
        dY.append(ddy + yChange)

    # for now sf is 2,3, 1/2, 3/2 or 1/3 or 2/3 because orignal image is 2 times or 3 times and fration are still smaller than original
    # could eventually do sf of 2, 4, 6, so that I could then reverse it and do 5/2 (2.5), 4/3 etc

    if sf == 'integer':
        sfVal = sfVal
        return centerCoord, sfVal, x, y, dX, dY
    elif sf == 'fraction':
        if sfVal == 2:
            oo = random.randint(1, 2)
            if oo == 1:
                sfVal = 1 / sfVal
            elif oo == 2:
                sfVal = 3 / sfVal
        elif sfVal == 3:
            oo = random.randint(1, 2)
            if oo == 1:
                sfVal = 1 / sfVal
            elif oo == 2:
                sfVal = 2 / sfVal
        return centerCoord, sfVal, x, y, dX, dY
def constantOperation(constant):
class transformation():
    def __init__(self, x, y, minn, maxx):  # minn/maxx b/c of min/max problems
        import math
        self.minn = minn
        self.maxx = maxx

    def translation(self, x, y):  # worry about overlap later, just get nonoverlapping for now
        # going to not make it overlap
        xLength = max(x) - min(x)  # gives minimum distance
        yLength = max(y) - min(y)
        # this makes it so it won't overlap at all.
        xChoices = list(range(self.minn - min(x), -xLength - 1)) + list(range(xLength + 1, self.maxx - max(x) + 1))
        yChoices = list(range(self.minn - min(y), -yLength - 1)) + list(range(yLength + 1, self.maxx - max(y) + 1))
        xChange = random.choice(xChoices)
        yChange = random.choice(yChoices)

        self.tX = [item + xChange for item in x]
        self.tY = [item + yChange for item in y]

    def reflection(self, x, y, lineOfReflectionList):

        line = random.choice(lineOfReflectionList)
        tempX = x.copy()
        tempY = y.copy()

        if line == "xaxis":
            tempY[:] = [-1 * itemY for itemY in y]  # y changes value but x stays the same
            lineStr = "x-axis"

        elif line == "yaxis":
            tempX[:] = [(-1 * itemX) for itemX in x]  # x changes value but y stays the same
            lineStr = "y-axis"

        elif line == "y=":
            a = random.randint(math.floor((minn + max(tempY)) / 2), math.floor((maxx + min(tempY)) / 2))
            # midpoint between minn and max value because max value will be the farthest away

            tempY[:] = [2 * a - itemY for itemY in y]
            lineStr = "y = %d" % a

        elif line == "x=":
            a = random.randint(math.floor((minn + max(tempX)) / 2), math.floor((maxx + min(tempX)) / 2))

            tempX[:] = [2 * a - itemX for itemX in x]

            lineStr = "x = %d" % a

        elif line == "y=xc":
            c = random.randint(min(tempX) - tempY[tempX.index(min(tempX))], max(tempX) - tempY[
                tempX.index(max(tempX))])  # need to figure out how to do the confines of this
            tempX[:] = [itemY + -1 * c for itemY in y]
            tempY[:] = [itemX + c for itemX in x]

        elif line == "y=-xc":
            c = random.randint(minn, maxx)  # need to figure out how to do the confines of this
            tempX[:] = -1 * (itemY + -1 * c)
            tempY[:] = -1 * itemX + c

        elif line == 'y=mx+b':
            m = random.randint(-5, 5)
            b = random.randint(-10, 10)
            # got formula below from file online
            # as of now they could go off the graph...
            tempX[:] = [(1 - m ** 2) / (m ** 2 + 1) * itemX + 2 * m / (m ** 2 + 1) * itemY - 2 * m * b / (m ** 2 + 1)
                        for itemX, itemY in zip(x, y)]
            tempY[:] = [2 * m / (m ** 2 + 1) * itemX + (m ** 2 - 1) / (m ** 2 + 1) * itemY - 2 * b / (m ** 2 + 1) for
                        itemX, itemY in zip(x, y)]

        if line == "xaxis" or "yaxis":
            answer = "Reflection over the " + lineStr
        else:
            answer = "Reflection over " + lineStr  # modify, fine for now

        self.refX = tempX
        self.refY = tempY
        self.answer = answer
        self.line = line

    def rotation(self, x, y, centerX, centerY, degrees, direction):  # minn maxx of graph???
        if degrees == 'random' and direction == 'random':  # if wanting random degrees such as [90,180,270] and not caring about direction
            deg = [90, 180, 270]
            dir = ['ccw', 'cc']
            degrees = random.choice(deg)
            direction = random.choice(dir)

        # convert angle to radians since math.cos and math.sin take in angle in radians
        if direction == 'ccw':
            angleRot = math.radians(degrees)
        if direction == 'cc':
            angleRot = math.radians(360 - degrees)
        # this angle is always counterclockwise
        tX = [itemX + -centerX for itemX in x]  # translate to origin
        tY = [itemY + -centerY for itemY in y]

        rotX = [itemX * math.cos(angleRot) - itemY * math.sin(angleRot) for itemX, itemY in zip(tX, tY)]  # rotate
        rotY = [itemX * math.sin(angleRot) + itemY * math.cos(angleRot) for itemX, itemY in zip(tX, tY)]

        # translate Back
        newX = [itemX + centerX for itemX in rotX]  # translate to origin
        newY = [itemY + centerY for itemY in rotY]
        if direction == "cc":
            directionStr = "clockwise"
            directionValue = -1
        if direction == "ccw":
            directionStr = "counterclockwise"
            directionValue = 1

        answer = "Rotation " + str(degrees) + " " + directionStr + "\n around the point (" + str(centerX) + "," + str(
            centerY) + ")"
        # even though we changed the degrees in the calculation, it will be fixed for the answer here.

        self.rotX = newX
        self.rotY = newY
        self.answer = answer
        self.directionValue = directionValue
        self.degrees = degrees

    def dilation(self, x, y, centerOption,
                 scaleFactorOption):  # bring the center to the origin, then multiply coordinates and move back
        self.scaleFactorOption = scaleFactorOption
        if centerOption == 'inside':  # more or less
            self.centerX = math.floor((max(x) + min(x)) / 2)
            self.centerY = math.floor((max(y) + min(y)) / 2)
        elif centerOption == 'random':
            self.centerX = random.choice(range(-5, 6))  # ensures that graph won't be too big
            self.centerY = random.choice(range(-5, 6))  # could put this lower if you want smaller graph
        elif centerOption == 'corner':
            self.centerX = random.choice(x)
            self.centerY = y[x.index(self.centerX)]
            self.cornerIndex = x.index(self.centerX)
        else:
            self.centerX = centerOption[0]
            self.centerY = centerOption[1]

        if scaleFactorOption == 'random':
            self.scaleFactorOption = random.choice(['integer', 'fraction'])
            scaleFactorOption = self.scaleFactorOption
        if scaleFactorOption == 'integer':
            self.sF = random.choice([2, 3])  # keeping it low
            self.dX = [self.sF * (item + -1 * self.centerX) + self.centerX for item in x]
            self.dY = [self.sF * (item + -1 * self.centerY) + self.centerY for item in y]
        if scaleFactorOption == 'fraction':
            self.sF = random.choice([2, 3])  # going to change the triangles
            # this makes a problem when dilating a triangle of your own. must need coordinate divible by both 2 and 3
            self.dX = x
            self.dY = y

            self.tempX = [self.sF * (item + -1 * self.centerX) + self.centerX for item in x]
            self.tempY = [self.sF * (item + -1 * self.centerY) + self.centerY for item in y]

            self.x = self.tempX
            self.y = self.tempY

