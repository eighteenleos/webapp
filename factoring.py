import random
from fractions import gcd
def coprimes(max=10):
    max = int(max/2)
    #taken from generating coprime pairs on wikipedia

    #can be odd-even or odd-odd
    type = random.choice(['even-odd','odd-odd'])

    if type == 'even-odd':
        even = random.randrange(2,max,2)
        odd = random.randrange(1,even,2)
        m = even
        n = odd
    elif type == 'odd-odd':
        odd1 = random.randrange(3,max,2)
        odd2 = random.randrange(1, odd1, 2)
        m = odd1
        n = odd2

    branch = random.choice([1,2,3])

    if branch == 1:
        a = 2*m - n
        b = m
    elif branch == 2:
        a = 2*m + n
        b = m
    elif branch == 3:
        a = m + 2*n
        b = n

    return a, b

#does not count 1 as a prime number, as it's not?
def getPrimes(max = 10):
	n = max
	numbers = set(range(n, 1, -1))
	primes = []
	while numbers:
		p = numbers.pop()
		primes.append(p)
		numbers.difference_update(set(range(p*2, n+1, p)))
	return primes

def getComposites(max = 10):
	primes = getPrimes(max = 10)
	composites = []
	for item in range(1,max+1):
		if item not in primes:
			composites.append(item)
	return composites
	
def coprimeWith(number, max=10):
	listOfChoices = list(range(1,max+1))
	newList = []
	for item in listOfChoices:
		if gcd(number,item) == 1:
			newList.append(item)
			
	coPrimeNum = random.choice(newList)
	return coPrimeNum
	
def factors(value, positiveValues = True):
	f1List = []
	f2List = []
	
	if positiveValues == False:
		valueRange = list(range(-1*abs(value),abs(value)+1)) #abs for negative values
		valueRange.remove(0) #
	else:
		valueRange = list(range(1,abs(value)+1))
		
	for item in valueRange:
		if (value/item).is_integer() == True: #only integer factors
			f1List.append(item)
			f2List.append(int(value/item))
	
	f1New = f1List #eliminates wrap around, multiple values
	f2New = f2List
	#this will return two lists with corresponding index values equaling to the value given
	
	return f1New, f2New #factor 1 list and factor 2 list

#eventually probably want to declare a, like a=2, a=3, a=4 etc

#coA, coB, coC
#coA - [1, 'prime', 'not prime', anything] prime means coA will be a prime number, not prime is like 4 (2,2 or 1,4)
#coB - ['positive', 'negative', or '0'(DOTS)]
#coC - ['positive', 'negative']
#GCF number version, variable later will be easier
def trinomialFactor(coA = 1, maxSizeForA = 10, coB = 'positive', coC = 'positive', gcf = False, rootMax = 10):
    #a(bx+c)(dx+e)
	if coA == 'random':
		coA = random.choice(['prime','not prime',1])
	
	elif coA == 'prime': #meaning we want 2, 3, 5, 7 etc
		b = 1
		primes = getPrimes(maxSizeForA)
		d = random.choice(primes)
		
		#c and e need to be any numbers but not multiples of b and d resepcively 
		#make c and e positive, code below will take care of signs
		c = coprimeWith(b, max = rootMax)
		e = coprimeWith(d, max = rootMax)
		while b*e + c*d == 0:
			print('B is 0!!!')
			c = coprimeWith(b, max = rootMax) 
			e = coprimeWith(d, max = rootMax)
		
	elif coA == 'not prime': #meaning we could have 4, 6, etc
		#we are going to make A first, then find factors of it and assign them to b and d respectively
		bTimesD = random.choice(getComposites(max = maxSizeForA))
		while bTimesD == 1:
			print('1!!!!')
			bTimesD = random.choice(getComposites(max = maxSizeForA))
		bTimesDFactors1, bTimesDFactors2 = factors(bTimesD)
		b = random.choice(bTimesDFactors1)
		d = bTimesDFactors2[bTimesDFactors1.index(b)]
		
		#make c and e each respectively coprime with b and d such that there are no gcf's
		c = coprimeWith(b, max = rootMax) 
		e = coprimeWith(d, max = rootMax)
		while b*e + c*d == 0:
			print('B is 0!!!',b,c,d,e)
			c = coprimeWith(b, max = rootMax) 
			e = coprimeWith(d, max = rootMax)
	
	elif coA == 1:
		b = 1
		d = 1
		c = coprimeWith(b, max = rootMax)
		e = coprimeWith(d, max = rootMax)
		while b*e + c*d == 0:
			print('B is 0!!!',b,c,d,e)
			c = coprimeWith(b, max = rootMax) 
			e = coprimeWith(d, max = rootMax)
		
	coA = b * d

	if coC == 'positive': #c and e are the same sign
		if coB == 'positive': #c and e are positive
			c = c
			e = e
		elif coB == 'negative': #c and e are negative
			c = c * -1
			e = e * -1
	elif coC == 'negative':
		if coB == 'positive': #c and e are different signs, but the bigger number is positive
			if c > e:
				c = c
				e = e * -1
			else:
				e = e
				c = c * -1
		elif coB == 'negative': #c and e are different signs, but the bigger number is negative
			if c > e:
				e = e
				c = c * -1
			else:
				c = c
				e = e * -1

	coC = c * e
	coB = b*e + c*d

	return coA, coB, coC
	
print(trinomialFactor(coA = 'not prime', coB = 'negative', coC = 'negative'))

#need to ensure that b is not 0

#special kinds where the value for C's factors: one set of factors added is the same as another set of factors subtracted
#30 - (15 - 2 = 13) (10 + 3 = 13) where signs matter

#
