Want:
Return a list of both x and y polygon coordinates for a variety of polygons and specifications
Ex:
def triangle(type = 'right'):
	x = [1,2,3]
	y = [0,1,2]
	return x, y
	

For: 

1) Coordinate proofs - prove a shape is that shape given the set of points
2) Find the area of a triangle on the coordinate plane. Usually these have integer sides for the right triangle and the right triangle is not have any flat/vertical sides etc
3) Any question that has someone finding angles using Triangle Sum Theorem (Interior Anlges of a Triangle add to 180) etc
4) Trigonometry Problems etc


Info:

Specifications:
-integer points
-based on minX, maxX, minY, maxY (so i could have a right triangle on any kind of graph etc)
-other specifications
-size (according to minX, maxX, minY, maxY) [So that I can choose a small, medium, or large right triangle etc]

**You'll have a much better idea if we should group all of these as one function (def polygonCoordinates) or split them up like (def triangleCoordinates or def rightTriangleCoordinates), it's really up to you since you're a lot better at this than me**

Right Triangles:
-integer legs and opposite
-no flats/vertical

IsoscelesRightTriangle:
-no flats/vertical 

Trapezoid:
-no flats/vertical

Isosceles Trapezoid:
-no flats/vertical

Paralellogram:
-no flats/vertical

Rectangle:
-no flats/vertical

Square:
-no flats/vertical

Rhombus:
-no flats/vertical

Scalene Triangle:
-no flats/vertical

Obtuse Triangle:
-no flats/vertical

Acute Triangle:
-no flats/vertical

Kite:
-no flats/vertical

Regular Polgyons (pentagon, hexagon, septagon, octagon, nonagaon, decagon, etc):
if you can make any with integer points, cool, i don't know if it's even possible

Let me know if you have any questions, it's quite extensive, but it will help in a huge amount of ways. After this is Transformations (Translations, Rotations, Reflection, and Dilations).