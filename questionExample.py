#example of a question
#class name is name of question (question number will be okay for now)
class bryanExample():
    def __init__(self, maxOfGraph = 10, typeOfQuestion = 'multiple choice'): #any options would go in here
        
        '''You can skip these for now: I'll eventually tell you which topic, skill, regents, etc
        self.topics = [['Similarity']]
        self.skills = [['Paritition a line segment']]
        self.regents = 'MakingThisShitUp'
        self.questionNumber = '6' #what to do for multiple regents???
        self.ID = self.regents + "." + self.questionNumber
        '''
        
        #Math stuff would go here.
        
        #if a string contains latex code, it is preceded with the letter 'r'
        self.question = r'This is the question!!!! An example of latex crap would be $\overline{%s%s}$. This will make line segment AB have a line over the and the AB. Most of the weird crap I google when I do not know it. Latex is surrounded by $$' % ('A','B')
        
        self.answer = 'This is the answer!!!'
        
        
        
    def addQuestion(self, doc = None): #doc is the document that pylatex adds to when we make our document
        doc.append(NoEscape(self.question)) #NoEscape is used within pylatex to take in latex strings [strings with 'r' in front]
        doc.append(NewLine())
        doc.append(VerticalSpace(r'1in'))

    def addAnswer(self, docAnswer = None): #docAnswer is the answerkey document
        docAnswer.append(NoEscape(self.answer))