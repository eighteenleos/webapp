Algebra #199
Time: 1hr

Sample output:
A corporation is building a rectangular football stadium that has an area of 4000 square feet. The football stadium must be 30 feet longer than its width. Determine algebraically the dimensions of the football stadium, in feet.

The dimensions are 80 by 50 feet.

from random import randint
from random import randrange

class Algebra199(object):
	def __init__(self):
		self.questionString = ""
		self.answerString = ""
		self.width = randrange(40, 80, 10)
		self.length = self.width + randrange(10, 50, 10)
		self.area = self.width * self.length
		self.MakeQuestionAndAnswerStrings()
	
	def MakeQuestionAndAnswerStrings(self):
		institutionChoices = ["school", "corporation", "company", "construction crew"]
		rectangularChoices = ["soccer field", "ice-skating rink", "football stadium", "building"]
		unitChoices = ["yards", "meters", "feet"]

		institutionExample = institutionChoices[randint(0, len(institutionChoices) - 1)] 
		rectangularExample = rectangularChoices[randint(0, len(rectangularChoices) - 1)] 
		unitExample = unitChoices[randint(0, len(unitChoices) - 1)] 

		self.questionString = "A " + institutionExample + " is building a rectangular " + rectangularExample + \
		" that has an area of " + str(self.area) + " square " + unitExample + ". The " + rectangularExample + \
		" must be " + str(self.length - self.width) + " " + unitExample + " longer than its width. Determine algebraically the" + \
		" dimensions of the " + rectangularExample + ", in " + unitExample + "."

		self.answerString = "The dimensions are " + str(self.length) + " by " + str(self.width) + " " + unitExample + "."

algebraAnswer = Algebra199()
#print(algebraAnswer.questionString)
#print()
#print(algebraAnswer.answerString)

